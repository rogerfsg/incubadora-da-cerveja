
function selecaoAutomaticaDeAreasDoMenu(componente){

	var estado;
	
	if(componente.checked){
		
		estado = true;
		
	}
	else{
	
		estado = false;
		
	}
	
	var valor = componente.value;
	
	var arrayElementos = document.getElementsByTagName("input");
	
	var i;
	for(i=0; i < arrayElementos.length; i++){
	
		if(arrayElementos[i].type == "checkbox" && arrayElementos[i].id.indexOf("_" + valor + "_") > -1){
		
			arrayElementos[i].checked = estado;
						
		}
		
	}

	if(estado == true){
		
		var pedacos = componente.id.split("_");
				
		var numeroPais = pedacos.length - 2;
		
		var idComponenteAtual = componente.id;
		var componenteAtual = componente;
		
		for(var i=0; i < numeroPais; i++){
			
			idComponenteAtual = idComponenteAtual.replace("_" + componenteAtual.value, "");
			
			if(document.getElementById(idComponenteAtual)){
				
				componenteAtual = document.getElementById(idComponenteAtual);
				componenteAtual.checked = estado;

			}
						
		}
		
	}
	
	return;
	
}
