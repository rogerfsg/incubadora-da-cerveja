
if(!Array.indexOf){
	
    Array.prototype.indexOf = function(obj){
	   
        for(var i=0; i<this.length; i++){
    	  
            if(this[i] === obj){
        	 
                return i;
            
            }
         
        }
      
        return -1;
      
    };
   
}

function adicionarLoading($el, label){
    if(typeof $el !== 'undefined'){
        if($el.siblings('img.carregando').length == 0){
            //<img src="assets/images/ajax-loaders/17.gif">
            label = typeof label == 'undefined' ? '' : label;
            $el.after('<img  class="carregando" src="css/flatty/assets/images/ajax-loaders/17.gif">' + label + '');
        }
    }
}

function removerLoading($el){
    if(typeof $el !== 'undefined'){
        $el.siblings('img.carregando').remove();
    }
}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}
function mostrarOverlay() {
    
    if($('.ui-widget-overlay').length == 0){
    
        var $overlay = $('<div class="ui-widget-overlay"></div>').hide().appendTo('body');

    }

    $('.ui-widget-overlay').fadeIn();    
    $('.ui-widget-overlay').width($(document).width());
    $('.ui-widget-overlay').height($(document).height());
    
}

function ocultarOverlay(){
    
    $('.ui-widget-overlay').hide();
    
}


function setCursor(el,st,end) {
    if(el.setSelectionRange) {
        el.focus();
        el.setSelectionRange(st,end);
    }
    else {
        if(el.createTextRange) {
            
            range=el.createTextRange();
            range.collapse(true);
            range.moveEnd('character',end);
            range.moveStart('character',st);
            range.select();
        }
    }
}


function irComEfeitoParaMarcador(seletorJQuery){
	
    var elemento = $(seletorJQuery);
	
    var folga = 50;
	
    var y = elemento.offset().top
    var x = elemento.offset().left - (screen.width/2);
	
    $('html,body').animate({
        scrollTop: y - folga, 
        scrollLeft: x - folga
    }, 1500);
	
};

function abrirCaixaDeDialogo(url, titulo, largura){
	
    if(typeof largura == 'undefined'){
		
        largura = 800;
		
    }
	
    window.setTimeout(function(){

        parent.carregarConteudoDialog(url);

    }, 500);

    window.setTimeout(function(){

        parent.$("#div_dialog").dialog("option", "width", largura);
        parent.$("#div_dialog").dialog("option", "title", titulo);
        parent.$("#div_dialog").dialog("open");

    }, 1000);

}

function imprimirMensagemSucesso(textoMensagem){

    document.getElementById("textoMensagemSucesso").innerHTML = textoMensagem;
    document.getElementById("caixaDeMensagemSucesso").style.display = "block";

    if(dialogo != undefined){

        if(dialogo != true){

            var posicao = $(document).scrollTop() + ($(window).height() /2) - 53;

            $('#textoMensagemSucesso').css('top', posicao + 'px');
            document.getElementById("botao_sucesso_ok").focus();

        }

    }
        
}

function imprimirMensagemErro(textoMensagem){

    setTimeout("mostrarMensagemDeErro('" + textoMensagem + "');", 200);
    	
}

function mostrarMensagemDeErro(textoMensagem){
    Alerta(textoMensagem);
     
//     if(false){
//        
//        alert(textoMensagem);
//        
//    }
//    else{
//
//        document.getElementById("textoMensagemErro").innerHTML = textoMensagem;	
//        document.getElementById("caixaDeMensagemErro").style.display = "block";
//
//        if(dialogo != undefined){
//
//            if(dialogo != true){
//
//                var posicao = $(document).scrollTop() + ($(window).height() /2) - 53;
//
//                $('#caixaDeMensagemErro').css('top', posicao + 'px');
//                document.getElementById("botao_erro_ok").focus();
//
//            }
//
//        }
//
//    }
}

function marcarTodosOsCheckBoxesCujoIdContenha(isChecked, conteudoId){
	
    var inputs = document.getElementsByTagName("input");
	
    for(var i=0; i < inputs.length; i++){
		
        if(inputs[i].type == "checkbox" && inputs[i].id.indexOf(conteudoId) != -1){
			
            inputs[i].checked = isChecked;
			
        }
		
    }
	
}

function divAcompanhandoRolagemNoCantoInferiorEsquerdo(idDiv){
	
    var el = $("#" + idDiv);
    var padding = 20 ;
	
    el.css('left', padding);
	
    var comando = function(){
		
        el.css('top', $(window).scrollTop() + ($(window).height() - el.height()) - padding);
        el.css('left', $(window).scrollLeft() + padding);
		
    };
	
    $(document).ready(comando);
	
    $(window).resize(comando);
    $(window).scroll(comando);
	
}

function alerta(strMensagem, tipoAlerta){

    document.getElementById("textoMensagemAlerta").innerHTML = strMensagem;
		
    if(tipoAlerta == undefined){
		
        document.getElementById("box_alerta").style.display = "none";
        document.getElementById("botao_mensagem_alerta").focus();
		
    }
	
}

function removerAcentos(palavra) {
	
    var com_acento = '����������������������������������������������';
    var sem_acento = 'aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC';
    var nova='';
	
    for(i=0;i<palavra.length;i++) {
	
        if (com_acento.indexOf(palavra.substr(i,1))>=0) {
	
            nova+=sem_acento.substr(com_acento.indexOf(palavra.substr(i,1)),1);
	
        }else{
			
            nova+=palavra.substr(i,1);
		
        }
	
    }
	
    return nova;
	
}

function filtrarColuna(coluna, valor){
	
    var tr = coluna.parentNode;
	
    var colunas = tr.childNodes;
	
    var i;
	
    //achando o indice da coluna
    for(i=0; i < colunas.length; i++){
				
        if(colunas[i].isEqualNode(coluna)){

            break;
			
        }
		
    }
	
    var secoes = tr.parentNode.parentNode.childNodes;
	
    var k;
    for(k=0; k < secoes.length; k++){
		
        if(secoes[k].nodeName.toLowerCase() == "tbody"){
			
            break;
			
        }
		
    }
	
    var listaDeTr = secoes[k].childNodes;

    var j;	
	
    for(j=0; j < listaDeTr.length; j++){
		
        if(listaDeTr[j].nodeName.toLowerCase() == "tr"){

            if(removerAcentos(listaDeTr[j].childNodes[i].innerHTML.toLowerCase()).indexOf(removerAcentos(valor.toLowerCase())) == -1){
				
                listaDeTr[j].style.display = "none";
				
            }
            else{
				
                listaDeTr[j].style.display = "table-row";
				
            }
		
        }
			
    }
	
}

function sleep(milliseconds) {
	
    var start = new Date().getTime();
  
    for (var i = 0; i < 1e7; i++) {
   
        if ((new Date().getTime() - start) > milliseconds){
		  
            break;
		  
        }
	  
    }
  
}

function barraDeAcoesAcompanhandoRolagem(elementoRolagem){

    var elRolagem = window;
	
    if(elementoRolagem != undefined){
		
        elRolagem = elementoRolagem;
						
    }
	
    var comando = function(){
		
        var el = $('#div_barra_de_acoes');
        var padding = 0 ;
				
        el.css('position', 'absolute');
        el.css('left', 0);
        el.css('width', '100%');
		
        el.css('top', $(elRolagem).scrollTop() + ($(elRolagem).height() - el.height()) - padding);
		
    };
	
    $(elRolagem).scroll(comando);
    $(elRolagem).resize(comando);
	
    $(document).ready(comando);
	
}

function acompanharRolagemHorizontal(elementoJQuery, espacoAntesEmPixels){

    if(typeof espacoAntesEmPixels == 'undefined'){
	
        espacoAntesEmPixels = 10;
		
    }
	
    var el = $(elementoJQuery);
	
    var comando = function(){
		
        if($.browser.msie && parseInt($.browser.version) <= 6){
		
            el.css('marginLeft', ($(window).scrollLeft() + espacoAntesEmPixels)/2);
					
        }
        else{
		
            el.css('marginLeft', ($(window).scrollLeft() + espacoAntesEmPixels));
				
        }
		
    };
	
    $(window).scroll(comando);
	
}

function carregarConteudoDialog(url){
	
    document.getElementById('conteudo_dialog').src = url + '&navegacao=false&dialog=true';
	
}

String.prototype.trim = function(){

    return this.replace(/^\s+|\s+$/g,"");
        
};

function adicionarConteudoAoRepositorioInvisivel(conteudo){
	
    document.getElementById("repositorio_invisivel").innerHTML += conteudo;

}

function verificarSelecaoRadioButton(nome){
	
    var elementos = document.getElementsByName(nome);
	
    var i;
    for(i=0; i < elementos.length; i++){
	
        if(elementos[i].checked == true){
		
            return i;
			
        }
		
    }
	
    return false;
	
}

function getVariavelGET(nomeVariavel){
	
    var url = document.location.href;
	
    var partes = url.split("&");
	
    var i;
    for(i=0; i < partes.length; i++){
	
        var parte = partes[i].split("=");
		
        if(parte[0] == nomeVariavel){
		
            return parte[1];
			
        }

    }
	
}

function getEvent(e)
{

    if (e.pageX == null && e.clientX != null ) {
        var b = document.body;
        e.pageX = e.clientX + (e && e.scrollLeft || b.scrollLeft || 0);
        e.pageY = e.clientY + (e && e.scrollTop || b.scrollTop || 0);
    }
    
    return e;

}

function getPosicaoCentralDoElemento(e, manterTopo, manterEsquerda) {
	
    var complementoX = Math.floor(e.offsetWidth /2);
    var complementoY = Math.floor(e.offsetHeight /2);

    var _x = 0;
    var _y = 0;
    	
    if(document.all && false){
    	
        while( e != null && !isNaN( e.offsetLeft ) && !isNaN( e.offsetTop ) ) {
        	
            _x += e.offsetLeft - e.scrollLeft;
            _y += e.offsetTop - e.scrollTop;
            e = e.parentNode;
            
        }
    	    	
    }
    else{
    
        while(e != null)
        {

            _x = parseInt(_x) + parseInt(e.offsetLeft);
            _y = parseInt(_y) + parseInt(e.offsetTop);
            e = e.offsetParent;
		
        }
		
    }
    
    if(manterTopo != true)
        _y = _y + complementoY;
	
    if(manterEsquerda != true)
        _x = _x + complementoX;

    return {
        top: _y, 
        left: _x
    };
    
}

function autoRedimensionarDialog(){

    parent.$("#div_dialog").dialog("option", "height", $(document).height() + 45);
	
}

function adicionarEvento(componente, evento, funcao){
	
    if(componente.addEventListener){  
	   
        componente.addEventListener(evento, funcao, false);  
	   
    }
    else if(componente.attachEvent){  
	   
        componente.attachEvent('on' + evento, funcao);  
     
    }  
		
}

function acharElementoPai(elementoFilho, tipoElementoPai){
	
    var pai = elementoFilho.parentNode;
	
    while(pai != null){
	
        if(pai.tagName == tipoElementoPai){
			
            return pai;
			
        }
        else{
			
            pai = pai.parentNode;
			
        }
	
    }
	
}

function mudarActionForm(form, booleano, actionTrue, actionFalse){
	
    var action = "";
	
    if(booleano == true){
	
        action = actionTrue;
		
    }
    else{
		
        action = actionFalse;
		
    }
	
    document.getElementById(form).action = action;
		
}

function alterarVisibilidadeDiv(idDiv, classVisivel, classInvisivel, idImagem, srcExpandir, srcEsconder, idFieldset, funcaoExpansao){

    if(typeof(idDiv.length) != "undefined"){

        var i;
        for(i=0; i < idDiv.length; i++){

            if(document.getElementById(idDiv[i]).className == classVisivel){

                document.getElementById(idDiv[i]).className = classInvisivel;

                if(i==0)
                    document.getElementById(idImagem).src = srcExpandir;

                if(idFieldset != undefined){

                    document.getElementById(idFieldset).style.borderWidth = "0px";

                }

            }
            else{

                document.getElementById(idDiv[i]).className = classVisivel;

                if(i==0)
                    document.getElementById(idImagem).src = srcEsconder;

                if(idFieldset != undefined){

                    document.getElementById(idFieldset).style.borderWidth = "1px";

                }

                if(funcaoExpansao != "" && funcaoExpansao != undefined)
                    eval(funcaoExpansao);

            }

        }

    }
	
}

Date.fromUKFormat = function(sUK) 
{ 
	
    var A = sUK.split(/[\\\/]/); 
    A = [A[1],A[0],A[2]]; 
    return new Date(Date.parse(A.join('/'))); 
  
};

function divComPosicionamentoAbsoluto(p_div)
{
	
    if(p_div.split){
	  
        p_div = document.getElementById(p_div);
	  
    }
	
    var is_ie6 =
    document.all && 
    (navigator.userAgent.toLowerCase().indexOf("msie 6.") != -1);
   
    if (is_ie6){
	   
        var html =
        "<iframe style=\"position: absolute; display: block; " +
        "z-index: -1; width: 100%; height: 100%; top: 0; left: 0;" +
        "filter: mask(); background-color: #ffffff; \"></iframe>";
      
        if (p_div) p_div.innerHTML += html;

        var olddisplay = p_div.style.display;
        p_div.style.display = 'none';
        p_div.style.display = olddisplay;
      
    };
   
}


function diferencaEntreDatasEmMeses(idDataMenor, idDataMaior, idDisplay){
	
    var dataMenor = document.getElementById(idDataMenor).value;
    var dataMaior = document.getElementById(idDataMaior).value;
	
    if(dataMenor.length == 7)
        dataMenor = "01/" + dataMenor;
	
    if(dataMaior.length == 7)
        dataMaior = "01/" + dataMaior;
	
    if(dataMaior.length == 4)
        dataMaior = "01/01/" + dataMaior;
	
    var datDate1= Date.fromUKFormat(dataMaior);
    var datDate2= Date.fromUKFormat(dataMenor);
    var datediff = ((datDate1-datDate2)/(24*60*60*1000)); 
	
    if(isNaN(datediff)){
		
        document.getElementById(idDisplay).innerHTML = "";
		
    }
    else if(datediff < 0){
		
        alerta("Data de T�rmino menor que a Data de In�cio!");
        document.getElementById(idDataMaior).value = "";
		
    }
    else{
		
        var diferencaImpressao = Math.floor(datediff/30);
		
        if(diferencaImpressao == 1){
			
            diferencaImpressao = diferencaImpressao + " mes";
			
        }
        else{
			
            diferencaImpressao = diferencaImpressao + " meses";
			
        }
	
        document.getElementById(idDisplay).innerHTML = "Diferen�a entre datas: <b>" 
        + diferencaImpressao + "</b>";
	
    }
	
}

function selecionaValorSelect(id_do_select, valor_a_selecionar, funcaoAvaliacao){
	
    var i;

    if(funcaoAvaliacao.length > 0);
    if(!eval(funcaoAvaliacao + "(valor_a_selecionar)"))
        return false;
	
    var componente = document.getElementById(id_do_select);
	
    if(componente == undefined){
	
        componente = id_do_select;
		
    }
	
    var achouValor = false;

    for(i=0; i < componente.options.length; i++){
	
        if(componente.options[i].value == valor_a_selecionar){

            componente.options[i].selected = "selected";
            achouValor = true;
            break;
			
        }
		
    }

    if(!achouValor){
		
        componente.options[0].selected = "selected";
		
    }
		
}

function strNiveisRaiz(numeroNiveis){

    var strRetorno = "";

    for(var i=0; i < numeroNiveis; i++){
	
        strRetorno += "../";
	
    }
	
    return strRetorno;

}

function popupModoDeImpressaoDePDF(classe, metodo, arquivoSaidaSemExtensao, strGET){
	
    day = new Date();
    id = day.getTime();
	
    eval("document.location.href='pdf.php?classe=" + classe + "&metodo=" + metodo + "&arquivoSaidaSemExtensao=" + arquivoSaidaSemExtensao + "&" + strGET + "';");

}

function popupModoDeImpressaoDeScriptEmPDF(scriptComParametros, arquivoSaidaSemExtensao){
	
    day = new Date();
    id = day.getTime();
	
    eval("document.location.href='pdf.php?script=" + scriptComParametros + "&arquivoSaidaSemExtensao=" + arquivoSaidaSemExtensao + "';");

}

function popupModoDeImpressaoDeGraficos(urlImagem, largura, altura){

    largura += 20;
    altura += 20;
	
    var x = (screen.width - largura) / 2; 
    var y = (screen.height - altura) / 2; 
		
    day = new Date();
    id = day.getTime();
		
    eval("page" + id + " = window.open(urlImagem, '" + id + "', 'toolbar=0,scrollbars=yes,location=0,statusbar=0,menubar=1,resizable=0,width=" + largura + ",height=" + altura + ",left=" + x + ",top=" + y + "');");
	
}

function popupModoDeImpressaoDeRelatorios(URL, largura, altura) {

    var x = (screen.width - largura) / 2; 
    var y = (screen.height - altura) / 2; 
	
    day = new Date();
    id = day.getTime();
	
    eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=1,scrollbars=yes,location=0,statusbar=0,menubar=1,resizable=0,width=" + largura + ",height=" + altura + ",left=" + x + ",top=" + y + "');");

}

function popupCentralizado(URL, largura, altura) {

    var x = (screen.width - largura) / 2; 
    var y = (screen.height - altura) / 2; 
	
    day = new Date();
    id = day.getTime();
	
    eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=yes,location=0,statusbar=0,menubar=0,resizable=0,width=" + largura + ",height=" + altura + ",left=" + x + ",top=" + y + "');");

}

function ocultarUltimaDivSelecionada(){
	
    if(document.getElementById("ultimo_popup_selecionado").value != ""){
		
        var idUltimoSelecionado = document.getElementById("ultimo_popup_selecionado").value;
		
        if(idUltimoSelecionado != null && document.getElementById(idUltimoSelecionado))
		
            document.getElementById(idUltimoSelecionado).style.display = "none";
		
    }
	
}

function travarTelaComOverlay(funcaoParaChamarConteudo){
	
    var altura = document.offsetHeight;
    var largura = document.offsetWidth;
	
    var divOverlay = document.createElement("DIV");
    divOverlay.className = "div_overlay";
    divOverlay.style.width = largura;
    divOverlay.style.height = altura;
    divOverlay.id = "div_overlay";
	
}

function mostrarDivNaPosicaoCursor(event, id_div, mostrarOsDemaisPopups){

    event = getEvent(event);
		
    var objDiv = document.getElementById(id_div);
	
    if(document.getElementById('seletor_cor')){
	
        var corDaFonte = document.getElementById('seletor_cor').style.backgroundColor;
        objDiv.style.color = corDaFonte;
		
    }
	
    if(mostrarOsDemaisPopups == undefined || mostrarOsDemaisPopups == false){
		
        if(document.getElementById("ultimo_popup_selecionado").value != ""){
			
            var idUltimoSelecionado = document.getElementById("ultimo_popup_selecionado").value;
			
            document.getElementById(idUltimoSelecionado).style.display = "none";
			
        }
	
    }


    var deslocamentoX = 0, deslocamentoY = 0;
	
    if(document.all){
	
        deslocamentoX = document.documentElement.scrollLeft;
        deslocamentoY = document.documentElement.scrollTop;
		
    }
	
    objDiv.style.top = event.pageY + deslocamentoY;
    objDiv.style.left = event.pageX + deslocamentoX;
    objDiv.style.display = "block";
	
    document.getElementById("ultimo_popup_selecionado").value = id_div;
	
}

function removerMascaraDeValorMonetario(valorComMascara){
    
    var valor = valorComMascara;
    
    valor = valor.replace('.', '');
    valor = valor.replace(',', '.');
    valor = parseFloat(valor);
    
    return valor;
    
}

function adicionarMascaraAValorMonetario(valorSemMascara){

    var num = valorSemMascara;

    x = 0;

    if(num<0) {
        num = Math.abs(num);
        x = 1;
    }

    if(isNaN(num)) num = "0";
    cents = Math.floor((num*100+0.5)%100);

    num = Math.floor((num*100+0.5)/100).toString();

    if(cents < 10) cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
        num = num.substring(0,num.length-(4*i+3))+'.'
        +num.substring(num.length-(4*i+3));

    ret = num + ',' + cents;

    if (x == 1) ret = ' - ' + ret;
    return ret;

}