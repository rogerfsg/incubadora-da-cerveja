
function confirmarExclusao(url, mensagem){
    
    if(confirm(mensagem)){
    
        location.href = url;
        
    }
    else{
    
        return false;   
        
    }
    
}

function confirmarReset(mensagem){
    
    if(confirm(mensagem)){
    
        return true;
        
    }
    else{
    
        return false;   
        
    }
    
}


// limpa todos caracteres que n�o s�o n�meros
function filtraNumeros(campo) {
    var s = "";
    var cp = "";
    vr = campo;
    tam = vr.length;
    for (i = 0; i < tam; i++) {
        if (vr.substring(i, i + 1) == "0" ||
            vr.substring(i, i + 1) == "1" ||
            vr.substring(i, i + 1) == "2" ||
            vr.substring(i, i + 1) == "3" ||
            vr.substring(i, i + 1) == "4" ||
            vr.substring(i, i + 1) == "5" ||
            vr.substring(i, i + 1) == "6" ||
            vr.substring(i, i + 1) == "7" ||
            vr.substring(i, i + 1) == "8" ||
            vr.substring(i, i + 1) == "9") {
            s = s + vr.substring(i, i + 1);
        }
    }
    return s;
    //return campo.value.replace("/", "").replace("-", "").replace(".", "").replace(",", "")
}
// Mascara Telefone DD e 9 digitos
function mascaraTelefone(v) {
    v = v.replace(/\D/g, "");             //Remove tudo o que n�o � d�gito
    v = v.replace(/^(\d{2})(\d)/g, "($1) $2"); //Coloca par�nteses em volta dos dois primeiros d�gitos
    v = v.replace(/(\d)(\d{4})$/, "$1-$2");    //Coloca h�fen entre o quarto e o quinto d�gitos
    return v;
}


function formataTelefone(campo, evt) {
    //(00) 0000-0000
    var xPos = PosicaoCursor(campo);
    evt = getEvent(evt);
    var tecla = getKeyCode(evt);
    if (!teclaValida(tecla))
        return;

    vr = campo.value = filtraNumeros(filtraCampo(campo));
    tam = vr.length;
	campo.value = mascaraTelefone(vr);
   

    //(
    //    if(xPos == 1 || xPos == 3 || xPos == 5 || xPos == 9)
    //        xPos = xPos +1
    MovimentaCursor(campo, xPos);
}


function formataCPF(campo, evt) {
    //999.999.999-99
    var xPos = PosicaoCursor(campo);
    evt = getEvent(evt);
    var tecla = getKeyCode(evt);
    if (!teclaValida(tecla))
        return;

    vr = campo.value = filtraNumeros(filtraCampo(campo));
    tam = vr.length;
    if (tam >= 3 && tam < 6)
        campo.value = vr.substr(0, 3) + '.' + vr.substr(3);
    else if (tam >= 6 && tam < 9)
        campo.value = vr.substr(0, 3) + '.' + vr.substr(3, 3) + '.' + vr.substr(6);
    else if (tam >= 9)
        campo.value = vr.substr(0, 3) + '.' + vr.substr(3, 3) + '.' + vr.substr(6, 3) + '-' + vr.substr(9);
    MovimentaCursor(campo, xPos);
}

// limpa todos os caracteres especiais do campo solicitado
function filtraCampo(campo) {
    var s = "";
    var cp = "";
    vr = campo.value;
    tam = vr.length;
    for (i = 0; i < tam; i++) {
        if (vr.substring(i, i + 1) != "/"
            && vr.substring(i, i + 1) != "-"
            && vr.substring(i, i + 1) != "."
            && vr.substring(i, i + 1) != "("
            && vr.substring(i, i + 1) != ")"
            && vr.substring(i, i + 1) != ":"
            && vr.substring(i, i + 1) != ",") {
            s = s + vr.substring(i, i + 1);
        }
    }
    return s;
    //return campo.value.replace("/", "").replace("-", "").replace(".", "").replace(",", "")
}
function formataCNPJ(campo, evt) {
    //99.999.999/9999-99
    var xPos = PosicaoCursor(campo);
    evt = getEvent(evt);
    var tecla = getKeyCode(evt);
    if (!teclaValida(tecla))
        return;

    vr = campo.value = filtraNumeros(filtraCampo(campo));
    tam = vr.length;

    if (tam >= 2 && tam < 5)
        campo.value = vr.substr(0, 2) + '.' + vr.substr(2);
    else if (tam >= 5 && tam < 8)
        campo.value = vr.substr(0, 2) + '.' + vr.substr(2, 3) + '.' + vr.substr(5);
    else if (tam >= 8 && tam < 12)
        campo.value = vr.substr(0, 2) + '.' + vr.substr(2, 3) + '.' + vr.substr(5, 3) + '/' + vr.substr(8);
    else if (tam >= 12)
        campo.value = vr.substr(0, 2) + '.' + vr.substr(2, 3) + '.' + vr.substr(5, 3) + '/' + vr.substr(8, 4) + '-' + vr.substr(12);
    MovimentaCursor(campo, xPos);
}

//evita criar mascara quando as teclas s�o pressionadas
function teclaValida(tecla) {
    if (tecla == 8 //backspace
    //Esta evitando o post, quando s�o pressionadas estas teclas.
    //Foi comentado pois, se for utilizado o evento texchange, � necessario o post.
        || tecla == 9 //TAB
        || tecla == 27 //ESC
        || tecla == 16 //Shif TAB 
        || tecla == 45 //insert
        || tecla == 46 //delete
        || tecla == 35 //home
        || tecla == 36 //end
        || tecla == 37 //esquerda
        || tecla == 38 //cima
        || tecla == 39 //direita
        || tecla == 40)//baixo
        return false;
    else
        return true;
}


//descobre qual a posi��o do cursor no campo
function PosicaoCursor(textarea) {
    var pos = 0;
    if (typeof (document.selection) != 'undefined') {
        //IE
        var range = document.selection.createRange();
        var i = 0;
        for (i = textarea.value.length; i > 0; i--) {
            if (range.moveStart('character', 1) == 0)
                break;
        }
        pos = i;
    }
    if (typeof (textarea.selectionStart) != 'undefined') {
        //FireFox
        pos = textarea.selectionStart;
    }

    if (pos == textarea.value.length)
        return 0; //retorna 0 quando n�o precisa posicionar o elemento
    else
        return pos; //posi��o do cursor
}

//Recupera o c�digo da tecla que foi pressionado
function getKeyCode(evt) {
    var code;
    if (typeof (evt.keyCode) == 'number')
        code = evt.keyCode;
    else if (typeof (evt.which) == 'number')
        code = evt.which;
    else if (typeof (evt.charCode) == 'number')
        code = evt.charCode;
    else
        return 0;

    return code;
}


// move o cursor para a posi��o pos
function MovimentaCursor(textarea, pos) {
    if (pos <= 0)
        return; //se a posi��o for 0 n�o reposiciona

    if (typeof (document.selection) != 'undefined') {
        //IE
        var oRange = textarea.createTextRange();
        var LENGTH = 1;
        var STARTINDEX = pos;

        oRange.moveStart("character", -textarea.value.length);
        oRange.moveEnd("character", -textarea.value.length);
        oRange.moveStart("character", pos);
        //oRange.moveEnd("character", pos);
        oRange.select();
        textarea.focus();
    }
    if (typeof (textarea.selectionStart) != 'undefined') {
        //FireFox
        textarea.selectionStart = pos;
        textarea.selectionEnd = pos;
    }
}



function validarEmail(componente, label) {

	var db = true;
	var comp = document.getElementById(componente);
	var addr = componente.value;
	
	if (addr == '') return true;
	
	var invalidChars = '\/\'\\ ";:?!()[]\{\}^|';
	for (i=0; i<invalidChars.length; i++) {
	   if (addr.indexOf(invalidChars.charAt(i),0) > -1) {
	      if (db) alerta('Endere�o de Email cont�m caracteres inv�lidos');
	      comp.focus();
	      return false;
	   }
	}
	for (i=0; i<addr.length; i++) {
	   if (addr.charCodeAt(i)>127) {
	      if (db) alerta("Endere�o de email deve conter somente caracteres ASCII.");
	      componente.focus();
	      return false;
	   }
	}
	
	var atPos = addr.indexOf('@',0);
	if (atPos == -1) {
	   if (db) alerta('Endere�o de Email deve conter um @');
	   return false;
	}
	if (atPos == 0) {
	   if (db) alerta('Endere�o de Email n�o pode come�ar um @');
	   return false;
	}
	if (addr.indexOf('@', atPos + 1) > - 1) {
	   if (db) alerta('Endere�o de Email deve conter somente um @');
	   return false;
	}
	if (addr.indexOf('.', atPos) == -1) {
	   if (db) alerta('Endere�o de Email deve conter um . na parte do dom�nio');
	   return false;
	}
	if (addr.indexOf('@.',0) != -1) {
	   if (db) alerta('O ponto n�o deve ser imediatamente depois do @');
	   return false;
	}
	if (addr.indexOf('.@',0) != -1){
	   if (db) alerta('O ponto n�o deve preceder o @');
	   return false;
	}
	if (addr.indexOf('..',0) != -1) {
	   if (db) alerta('N�o podem haver 2 pontos juntos');
	   return false;
	}
	
	var suffix = addr.substring(addr.lastIndexOf('.')+1);
	if (suffix.length != 2 && suffix != 'com' && suffix != 'net' && suffix != 'org' && suffix != 'edu' && suffix != 'int' && suffix != 'mil' && suffix != 'gov' & suffix != 'arpa' && suffix != 'biz' && suffix != 'aero' && suffix != 'name' && suffix != 'coop' && suffix != 'info' && suffix != 'pro' && suffix != 'museum') {
	   if (db) alerta('Dom�nio prim�rio inv�lido');
	   return false;
	}
	
	return true;
	
}


function validarCPF(componente){

     var cpf = document.getElementById(componente).value;

     erro = new String;
     
     if (cpf.length < 11) erro += "S�o necess�rios 11 digitos para verifica��o do CPF! \n\n";
     var nonNumbers = /\D/;
     if (nonNumbers.test(cpf)) erro += "A verificacao de CPF suporta apenas numeros! \n\n";
     if (cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" || cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999"){
             erro += "N�mero de CPF invalido!"
     }
      var a = [];
      var b = new Number;
      var c = 11;
      for (i=0; i<11; i++){
              a[i] = cpf.charAt(i);
              if (i < 9) b += (a[i] * --c);
      }
      if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }
      b = 0;
      c = 11;
      for (y=0; y<10; y++) b += (a[y] * c--);
      if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }
      if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10])){
              erro +="Digito verificador com problema!";
      }
      if (erro.length > 0){
              alerta(erro);
              return false;
      }
      return true;
      
}

function validarCampos(formulario){

	var retorno = Spry.Widget.Form.validate(formulario);
	
	if(retorno == false){
		
            imprimirMensagemErro("Existem erros no preenchimento dos dados, verifique as mensagens respectivas a cada campo.");
        	
	}
	//validarTelefones(formulario);
	return retorno;
	
}


function validarTelefones(formulario){
    
    
    $(formulario).children($(".telefonemasc")).each(function() {
        
        var input = this.children('input');
        var spanFormatoInvalido = this.children('span[id^=textfieldInvalidFormatMsg]');
        var spanObrigatorio = this.children('span[id^=textfieldRequiredMsg]');
        if(input.value.length == 0){
            spanObrigatorio.css({ "visibility": 'true'});
            return false;
        }
        else if(input.value.length < 14 || input.value.length > 15){
            spanFormatoInvalido.css({ "visibility": 'true'});
            return false;
        }
    });
    
    
}
