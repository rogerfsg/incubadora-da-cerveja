
omegaApp.run(
    ["$http", "$rootScope", "dialogs", "facebookService", function($http, $rootScope, dialogs, facebookService) {

        $rootScope.AppConstants = AppConstants;

        $rootScope.GeneralUtil = GeneralUtil;
        $rootScope.StringUtil = StringUtil;
        $rootScope.NumberUtil = NumberUtil;
        $rootScope.ArrayUtil = ArrayUtil;
        $rootScope.DateUtil = DateUtil;

        facebookService.runCache();

}]);
