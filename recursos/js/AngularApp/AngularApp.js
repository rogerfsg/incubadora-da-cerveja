var omegaApp = angular.module('omegaApp',
    ['ngCookies', 'ngRoute', 'ui.bootstrap', 'omega-translations', 'checklist-model', 'ngSanitize',
        'ngAnimate', 'ui.validate', 'ui.select', 'maskMoney', 'dialogs.main', 'ui.mask', 'ui.utils.masks',
        'frapontillo.bootstrap-switch']);

omegaApp.config(
    [ "$routeProvider", "$locationProvider", "$translateProvider", "uiSelectConfig",
        function ($routeProvider, $locationProvider, $translateProvider, uiSelectConfig) {

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });

        $translateProvider.registerAvailableLanguageKeys(["en-US","pt-BR"], {
            "en_*": "en-US",
            "pt_*": "pt-BR"
        });

        $translateProvider.determinePreferredLanguage();
        $translateProvider.fallbackLanguage("pt-BR");

    }]
);
