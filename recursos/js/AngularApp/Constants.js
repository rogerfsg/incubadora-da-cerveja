//inserir constantes do projeto
var AppConstants =
{
    TIPO_CADASTRO:
    {
        BREWPUB: 'brewpub',
        CERVEJARIA: 'cervejaria',
        CERVEJEIRO_CIGANO: 'cigano',
        DEGUSTADOR: 'degustador',
        DISTRIBUIDOR: 'distribuidor'
    },

    TIPOS_CADASTRO_EMPRESARIAL: ['brewpub', 'cigano', 'cervejaria'],

    BANCO:
    {
        CAIXA_ECONOMICA_FEDERAL: 104
    },

    SECAO_CADASTRO_CERVEJEIRO:
    {
        DADOS_BASICOS: 'dados_basicos',
        REDES_SOCIAIS: 'redes_sociais',
        LOCALIZACAO: 'localizacao',
        CADASTRAR_CERVEJA: 'cadastrar_cerveja',
        DADOS_BANCARIOS: 'dados_bancarios'
    }

};

