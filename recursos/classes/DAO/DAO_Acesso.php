<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Acesso
    * DATA DE GERA��O: 13.05.2017
    * ARQUIVO:         DAO_Acesso.php
    * TABELA MYSQL:    acesso
    * BANCO DE DADOS:  cobranca
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Acesso extends Generic_DAO
    {

        // *************************
        // DECLARA��O DE ATRIBUTOS
        // *************************

        public $id;
        public $usuario_id_INT;
        public $objUsuario;
        public $data_login_DATETIME;
        public $data_logout_DATETIME;
        public $excluido_BOOLEAN;
        public $excluido_DATETIME;

        public $nomeEntidade;

        public $data_login_DATETIME_UNIX;
        public $data_logout_DATETIME_UNIX;
        public $excluido_DATETIME_UNIX;

        public $label_id;
        public $label_usuario_id_INT;
        public $label_data_login_DATETIME;
        public $label_data_logout_DATETIME;
        public $label_excluido_BOOLEAN;
        public $label_excluido_DATETIME;

        // **********************
        // M�TODO CONSTRUTOR
        // **********************
        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->nomeEntidade = "Acesso";
            $this->nomeTabela = "acesso";
            $this->campoId = "id";
            $this->campoLabel = "id";
        }

        public function valorCampoLabel()
        {
            return $this->getId();
        }

        public function getFkObjUsuario()
        {
            if ($this->objUsuario == null)
            {
                $this->objUsuario = new EXTDAO_Usuario($this->getConfiguracaoDAO());
            }
            $idFK = $this->getUsuario_id_INT();
            if (!strlen($idFK))
            {
                $this->objUsuario->clear();
            }
            else
            {
                if ($this->objUsuario->getId() != $idFK)
                {
                    $this->objUsuario->select($idFK);
                }
            }

            return $this->objUsuario;
        }

        public function getComboBoxAllUsuario($objArgumentos)
        {
            $objArgumentos->nome = "usuario_id_INT";
            $objArgumentos->id = "usuario_id_INT";
            $objArgumentos->valueReplaceId = false;

            $this->objUsuario = $this->getFkObjUsuario();

            return $this->objUsuario->getComboBox($objArgumentos);
        }

        public function __actionAdd()
        {
            $mensagemSucesso = I18N::getExpression("O acesso foi cadastrada com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $msg = $this->insert();
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionAddAjax()
        {
            $mensagemSucesso = I18N::getExpression("O acesso foi cadastrado com sucesso.");
            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $msg = $this->insert();
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionEdit()
        {
            $mensagemSucesso = I18N::getExpression("O acesso foi modificada com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $msg = $this->update($this->getId(), $_POST, $i);
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }

                $this->select($this->getId());
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionRemove()
        {
            $mensagemSucesso = I18N::getExpression("O acesso foi exclu�da com sucesso.");

            $urlSuccess = Helper::getUrlAction("list_acesso", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_acesso", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $msg = $this->delete("$registroRemover");
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);
        }

        // **********************
        // M�TODOS GETTER's
        // **********************
        public function getId()
        {
            return $this->id;
        }

        public function getUsuario_id_INT()
        {
            return $this->usuario_id_INT;
        }

        function getData_login_DATETIME_UNIX()
        {
            return $this->data_login_DATETIME_UNIX;
        }

        public function getData_login_DATETIME()
        {
            return $this->data_login_DATETIME;
        }

        function getData_logout_DATETIME_UNIX()
        {
            return $this->data_logout_DATETIME_UNIX;
        }

        public function getData_logout_DATETIME()
        {
            return $this->data_logout_DATETIME;
        }

        public function getExcluido_BOOLEAN()
        {
            return $this->excluido_BOOLEAN;
        }

        function getExcluido_DATETIME_UNIX()
        {
            return $this->excluido_DATETIME_UNIX;
        }

        public function getExcluido_DATETIME()
        {
            return $this->excluido_DATETIME;
        }

        // **********************
        // M�TODOS SETTER's
        // **********************
        function setId($val)
        {
            $this->id = $val;
        }

        function setUsuario_id_INT($val)
        {
            $this->usuario_id_INT = $val;
        }

        function setData_login_DATETIME($val)
        {
            $this->data_login_DATETIME = $val;
        }

        function setData_logout_DATETIME($val)
        {
            $this->data_logout_DATETIME = $val;
        }

        function setExcluido_BOOLEAN($val)
        {
            $this->excluido_BOOLEAN = $val;
        }

        function setExcluido_DATETIME($val)
        {
            $this->excluido_DATETIME = $val;
        }


        // **********************
        // SELECT
        // **********************
        function select($id)
        {
            $sql = "SELECT * , UNIX_TIMESTAMP(data_login_DATETIME) AS data_login_DATETIME_UNIX, UNIX_TIMESTAMP(data_logout_DATETIME) AS data_logout_DATETIME_UNIX, UNIX_TIMESTAMP(excluido_DATETIME) AS excluido_DATETIME_UNIX FROM acesso WHERE id = $id;";
            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && ($msg->erro() || $msg->resultadoVazio()))
            {
                $this->database->closeResult();

                return $msg;
            }
            $result = $this->database->result;
            $row = $this->database->fetchObject($result);
            $this->database->closeResult();

            $this->id = $row->id;

            $this->usuario_id_INT = $row->usuario_id_INT;

            $this->data_login_DATETIME = $row->data_login_DATETIME;
            $this->data_login_DATETIME_UNIX = $row->data_login_DATETIME_UNIX;

            $this->data_logout_DATETIME = $row->data_logout_DATETIME;
            $this->data_logout_DATETIME_UNIX = $row->data_logout_DATETIME_UNIX;

            $this->excluido_BOOLEAN = $row->excluido_BOOLEAN;

            $this->excluido_DATETIME = $row->excluido_DATETIME;
            $this->excluido_DATETIME_UNIX = $row->excluido_DATETIME_UNIX;
        }

        // **********************
        // EMPTY
        // **********************
        public function clear()
        {
            $this->id = null;
            $this->usuario_id_INT = null;
            $this->data_login_DATETIME = null;
            $this->data_logout_DATETIME = null;
            $this->excluido_BOOLEAN = null;
            $this->excluido_DATETIME = null;
        }


        // **********************
        // DELETE
        // **********************

        public function delete($id)
        {
            $sql = "UPDATE acesso SET excluido_BOOLEAN=1, excluido_DATETIME=NOW() WHERE id = $id;";
            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
        }

        // **********************
        // INSERT
        // **********************

        public function insert()
        {
            $this->id = ""; //limpar chave com autoincremento
            $this->excluido_BOOLEAN = "0";

            $sql = "INSERT INTO acesso ( usuario_id_INT , data_login_DATETIME , data_logout_DATETIME , excluido_BOOLEAN , excluido_DATETIME ) VALUES ( {$this->usuario_id_INT} , {$this->data_login_DATETIME} , {$this->data_logout_DATETIME} , {$this->excluido_BOOLEAN} , {$this->excluido_DATETIME} )";
            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
        }


        //*************************************************
        //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
        //*************************************************

        public function nomeCampoId()
        {
            return "id";
        }

        public function nomeCampoUsuario_id_INT()
        {
            return "usuario_id_INT";
        }

        public function nomeCampoData_login_DATETIME()
        {
            return "data_login_DATETIME";
        }

        public function nomeCampoData_logout_DATETIME()
        {
            return "data_logout_DATETIME";
        }

        public function nomeCampoExcluido_BOOLEAN()
        {
            return "excluido_BOOLEAN";
        }

        public function nomeCampoExcluido_DATETIME()
        {
            return "excluido_DATETIME";
        }




        //************************************************************************
        //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
        //************************************************************************

        public function imprimirCampoUsuario_id_INT($objArguments)
        {
            $objArguments->nome = "usuario_id_INT";
            $objArguments->id = "usuario_id_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoData_login_DATETIME($objArguments)
        {
            $objArguments->nome = "data_login_DATETIME";
            $objArguments->id = "data_login_DATETIME";

            return $this->campoDataTime($objArguments);
        }

        public function imprimirCampoData_logout_DATETIME($objArguments)
        {
            $objArguments->nome = "data_logout_DATETIME";
            $objArguments->id = "data_logout_DATETIME";

            return $this->campoDataTime($objArguments);
        }

        public function imprimirCampoExcluido_BOOLEAN($objArguments)
        {
            $objArguments->nome = "excluido_BOOLEAN";
            $objArguments->id = "excluido_BOOLEAN";

            return $this->campoBoolean($objArguments);
        }

        public function imprimirCampoExcluido_DATETIME($objArguments)
        {
            $objArguments->nome = "excluido_DATETIME";
            $objArguments->id = "excluido_DATETIME";

            return $this->campoDataTime($objArguments);
        }




        //**********************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
        //**********************************************************************************

        public function formatarParaSQL()
        {
            if ($this->usuario_id_INT == null)
            {
                $this->usuario_id_INT = "null";
            }

            if ($this->excluido_BOOLEAN == null)
            {
                $this->excluido_BOOLEAN = "null";
            }

            $this->data_login_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_login_DATETIME);
            $this->data_logout_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_logout_DATETIME);
            $this->excluido_DATETIME = $this->formatarDataTimeParaComandoSQL($this->excluido_DATETIME);
        }


        //****************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
        //****************************************************************************

        public function formatarParaExibicao()
        {
            $this->data_login_DATETIME = $this->formatarDataTimeParaExibicao($this->data_login_DATETIME);
            $this->data_logout_DATETIME = $this->formatarDataTimeParaExibicao($this->data_logout_DATETIME);
            $this->excluido_DATETIME = $this->formatarDataTimeParaExibicao($this->excluido_DATETIME);
        }


        // ****************************
        // CRIAR VARIAVEIS DE SESSAO
        // ****************************

        public function createSession()
        {
            Helper::setSession("id", $this->id);
            Helper::setSession("usuario_id_INT", $this->usuario_id_INT);
            Helper::setSession("data_login_DATETIME", $this->data_login_DATETIME);
            Helper::setSession("data_logout_DATETIME", $this->data_logout_DATETIME);
            Helper::setSession("excluido_BOOLEAN", $this->excluido_BOOLEAN);
            Helper::setSession("excluido_DATETIME", $this->excluido_DATETIME);
        }

        // ***************************
        // LIMPAR SESSAO
        // ***************************

        public function limparSession()
        {
            Helper::clearSession("id");
            Helper::clearSession("usuario_id_INT");
            Helper::clearSession("data_login_DATETIME");
            Helper::clearSession("data_logout_DATETIME");
            Helper::clearSession("excluido_BOOLEAN");
            Helper::clearSession("excluido_DATETIME");
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL SESSION
        // ****************************

        public function setBySession($numReg)
        {
            $this->id = Helper::SESSION("id{$numReg}");
            $this->usuario_id_INT = Helper::SESSION("usuario_id_INT{$numReg}");
            $this->data_login_DATETIME = Helper::SESSION("data_login_DATETIME{$numReg}");
            $this->data_logout_DATETIME = Helper::SESSION("data_logout_DATETIME{$numReg}");
            $this->excluido_BOOLEAN = Helper::SESSION("excluido_BOOLEAN{$numReg}");
            $this->excluido_DATETIME = Helper::SESSION("excluido_DATETIME{$numReg}");
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL POST
        // ****************************

        public function setByPost($numReg)
        {
            $this->id = Helper::POST("id{$numReg}");
            $this->usuario_id_INT = Helper::POST("usuario_id_INT{$numReg}");
            $this->data_login_DATETIME = Helper::POST("data_login_DATETIME{$numReg}");
            $this->data_logout_DATETIME = Helper::POST("data_logout_DATETIME{$numReg}");
            $this->excluido_BOOLEAN = Helper::POST("excluido_BOOLEAN{$numReg}");
            $this->excluido_DATETIME = Helper::POST("excluido_DATETIME{$numReg}");
        }

        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL GET
        // ****************************

        public function setByGet($numReg)
        {
            $this->id = Helper::GET("id{$numReg}");
            $this->usuario_id_INT = Helper::GET("usuario_id_INT{$numReg}");
            $this->data_login_DATETIME = Helper::GET("data_login_DATETIME{$numReg}");
            $this->data_logout_DATETIME = Helper::GET("data_logout_DATETIME{$numReg}");
            $this->excluido_BOOLEAN = Helper::GET("excluido_BOOLEAN{$numReg}");
            $this->excluido_DATETIME = Helper::GET("excluido_DATETIME{$numReg}");
        }

        // **********************
        // UPDATE
        // **********************

        public function update($id, $tipo = null, $numReg = 1)
        {
            $upd = "";
            if (isset($tipo["usuario_id_INT{$numReg}"]) || $tipo == null)
            {
                $upd .= "usuario_id_INT = $this->usuario_id_INT, ";
            }

            if (isset($tipo["data_login_DATETIME{$numReg}"]) || $tipo == null)
            {
                $upd .= "data_login_DATETIME = $this->data_login_DATETIME, ";
            }

            if (isset($tipo["data_logout_DATETIME{$numReg}"]) || $tipo == null)
            {
                $upd .= "data_logout_DATETIME = $this->data_logout_DATETIME, ";
            }

            if (isset($tipo["excluido_BOOLEAN{$numReg}"]) || $tipo == null)
            {
                $upd .= "excluido_BOOLEAN = $this->excluido_BOOLEAN, ";
            }

            if (isset($tipo["excluido_DATETIME{$numReg}"]) || $tipo == null)
            {
                $upd .= "excluido_DATETIME = $this->excluido_DATETIME, ";
            }

            $upd = substr($upd, 0, -2);

            $sql = " UPDATE acesso SET $upd WHERE id = $id ";

            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
        }

    } // classe: fim
