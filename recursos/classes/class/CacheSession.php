<?php

/**
 * AES Encryption Class. This class supports three lenghts of key (128, 192, 256)
 * @author Marcin F. Wi�niowski <marcin.wisniowski@mfw.pl>
 * @version 1.0.2
 * @licence GPLv3
 * Based on Federal Information Processing Standards Publication 197 - 26th November 2001
 */
class CacheSession
{
    
    
    public static function setEmailJaCadastradoNaCentral($email, $simNao){
        Helper::setSession("setEmailJaCadastradoNaCentral", $email);
        Helper::setSession("setEmailJaCadastradoNaCentralValor", $simNao);
    }
    public static function getEmailJaCadastradoNaCentral($email){
        if(Helper::SESSION("setEmailJaCadastradoNaCentral") == $email)
            return Helper::SESSION("setEmailJaCadastradoNaCentralValor");
        return null;
    }
    
    public function clear(){
        
        Helper::clearSession("setEmailJaCadastradoNaCentral");
        Helper::clearSession("setEmailJaCadastradoNaCentralValor");
    }
    
    
}

?>