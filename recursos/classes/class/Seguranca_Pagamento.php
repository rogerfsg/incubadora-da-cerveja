<?

class Seguranca_Pagamento extends InterfaceSeguranca
{
    //lista de paginas que nao precisam de autenticacao
    public static $paginasComAutenticacao = array("formas_pagamento", "finalizacao_pedido",
        "meus_horarios", "minhas_cobrancas", "visualizar_cobranca", "alterar_senha");

    //actions que precisam de autenticacao
    public static $actionsComAutenticacao = array(
        "alterarSenha",
        "processarPedido");

    public static $paginasSemTopo = array("venda_corrigir_cupom", "omega_empresa_web",
        "possuo_grupo", "nao_lembro_meu_grupo", "meus_grupos", "ainda_nao_possuo_grupo",
        "cadastrar_grupo", "pacotes_pelo_sistema");

    public static $paginasSemAutenticacao = array(array("paginas", "logar_pelo_sistema"));

    public function getAcoesLiberadasParaUsoAutenticado()
    {
        return null;
    }

    public static function verificaPermissao($tipo, $page)
    {
        if (Seguranca_Pagamento::isAutenticado())
        {
            return true;
        }
        else
        {
            for ($i = 0; $i < count(ConstanteSegurancaPagamento::$paginasSemAutenticacao); $i++)
            {
                if (ConstanteSegurancaPagamento::$paginasSemAutenticacao[$i][0] == $tipo
                    && ConstanteSegurancaPagamento::$paginasSemAutenticacao[$i][1] == $page
                )
                {
                    return true;
                }
            }
        }
        return false;
    }

    public static function getSenhaDoClienteLogado()
    {

        $id = Seguranca_Pagamento::getEmail();

        if (!strlen($id))
        {
            return null;
        }
        return EXTDAO_Cliente::getSenhaDecryptPeloEmail($id);
    }

    public function getLabel()
    {
        $token = Seguranca_Pagamento::getEmail();
        return $token;
    }

    public function verificaSenhaECorporacao(
        $email,
        $senha,
        $corporacao,
        $db = null,
        $criptografia = null)
    {
        if ($db == null)
        {
            $db = new Database ();
        }
        HelperLog::verbose("verificaSenhaECorporacao::verificaSenha");
        $msg = $this->login(
            array("email" => $email), $senha, $db, true);

        if ($msg->mCodRetorno == PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO)
        {
            $idUsuario = $this->getId();
            if (empty($idUsuario))
            {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::AUTENTICACAO_INVALIDA,
                    "O usuario $idUsuario n?o foi encontrado na base SICOB.");
            }
            $idCorporacao = EXTDAO_Corporacao::getIdCorporacao(
                $corporacao,
                $db);

            if (empty($idCorporacao))
            {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                    "O grupo {$corporacao} n?o existe no nosso cadastro.");
            }
            $idClienteAssinatura = EXTDAO_Corporacao::getIdClienteAssinaturaDoClienteDaCorporacao(
                $idCorporacao,
                $idUsuario,
                $db);

            $session = SessionRedis::getSingleton();
            if ($session != null)
            {
                $session->logout();
            }
            if (!empty($idClienteAssinatura))
            {
                return new Mensagem_token(
                    PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                    I18N::getExpression("O usu�rio faz parte do grupo $corporacao."),
                    $idClienteAssinatura);
            }
            else
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                                    I18N::getExpression("O usu�rio n�o faz parte do grupo $corporacao"));
            }
        }
        else
        {
            return $msg;
        }
    }

    public function verificaSenha(
        $dados,
        $senha,
        $db = null,
        $registrarAcesso = false)
    {
        try
        {
            if ($db == null)
            {
                $db = new Database();
            }
            $email = $dados["email"];
            $criptografia = Registry::get('Crypt');
            $senhaC = $criptografia->crypt($senha);
            $q = "SELECT DISTINCT c.id AS usuario_id,
                                    IF(c.nome IS NOT NULL AND c.nome <> '', c.nome, c.razao_social) AS usuario_nome,
                                    c.email_cobranca1 AS usuario_email
                                    FROM cliente AS c
                                    WHERE c.email_cobranca1='{$email}' 
                                    AND c.senha='{$senhaC}'
                                    AND c.excluido_DATETIME IS NULL
                                    AND c.senha IS NOT NULL
                                    AND c.senha <> ''";

            $msg = $db->queryMensagem($q);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            if ($db->rows() > 0)
            {
                if ($registrarAcesso)
                {
                    $objsCliente = Helper::getResultSetToMatriz($db->result, 1, 0);

                    $objCliente = $objsCliente[0];
                    return new Mensagem_protocolo(
                        $objCliente,
                        PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                        "Login realizado com suceso.");
                }
                else
                {
                    return new Mensagem(
                        PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                        "Login realizado com suceso.");
                }
            }
            else
            {
                $idCliente = EXTDAO_Cliente::getClientePorEmail($email, $db);
                if (!strlen($idCliente))
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::EMAIL_DO_USUARIO_INEXISTENTE,
                                        "N�o existe usu�rio cadastrado com o email $email");
                }
                else
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                                        "A senha digitada n�o corresponde a senha cadastrada no email {$email}. Utilize a funcionalidade 'Relembrar Senha' na tela de login para recuperar seu acesso.");
                }
            }
        }
        catch (Exception $exc)
        {
            return new Mensagem(null, null, $exc);
        }
    }

    public function existeUsuarioDoEmail($email, $db = null)
    {
        $idCliente = EXTDAO_Cliente::getClientePorEmail($email, $db);
        return strlen($idCliente) ? true : false;
    }

    public function __actionLogin($dados, $senha, $db = null)
    {
        try
        {
            HelperLog::verbose("validarLogin::verificaSenha");
            $nextAction = Helper::POST("next_action");

            $mensagem = $this->login($dados, $senha, $db, true);

            if ($mensagem->mCodRetorno == PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO)
            {
                $idCliente = Seguranca_Pagamento::getId();

                $assinaturas = EXTDAO_Cliente_assinatura::getClienteAssinaturasNaoCanceladasDoCliente($idCliente, $db);
                if (count($assinaturas))
                {
                    $ass = $assinaturas[0];

                    return BO_SIHOP::logaNoSistemaDaAssinatura(null, Seguranca_Pagamento::getEmail(), $ass[1], $ass[0]);
                }

                if (is_numeric(Helper::POST("cobranca_referencia")))
                {
                    $cobranca = Helper::POST("cobranca_referencia");
                    $msgSucesso = urlencode("Login realizado com sucesso!");
                    return array("index.php?tipo=paginas&page=visualizar_cobranca&cobranca_id_INT1={$cobranca}&msgSucesso=$msgSucesso");
                }
                elseif ($nextAction == "pagina_inicial")
                {
                    $msgSucesso = urlencode("Login realizado com sucesso!");
                    return array("index.php?tipo=paginas&page=assinaturas_do_cliente&msgSucesso=$msgSucesso");
                }
                elseif ($nextAction == "recarregar_pagina")
                {
                    $paginaAnterior = Helper::getUrlDeReferenciaSemVariaveisDeMensagem();

                    if (substr_count($paginaAnterior, "&") == 0)
                    {
                        $paginaAnterior .= "?";
                    }
                    $msgSucesso = urlencode("Login realizado com sucesso!");
                    return array("{$paginaAnterior}&msgSucesso=$msgSucesso");
                }
            }
            else
            {
                if ($nextAction == "recarregar_pagina")
                {
                    $paginaAnterior = Helper::getUrlDeReferenciaSemVariaveisDeMensagem();

                    if (substr_count($paginaAnterior, "&") == 0)
                    {
                        $paginaAnterior .= "?";
                    }

                    return array("{$paginaAnterior}&msgErro=N?o foi poss?vel efetuar o login com os dados de acesso informados.");
                }
                else
                {
                    return array("login.php?msgErro=N?o foi poss?vel efetuar o login com os dados de acesso informados.");
                }
            }
        }
        catch (Exception $exc)
        {
            return array("login.php?msgErro=" . urlencode($exc->getMessage()));
        }
    }

    public function __actionLogout($mensagemUsuario = null)
    {
        if ($mensagemUsuario == null)
        {
            $mensagemUsuario = MENSAGEM_LOGOUT_CLIENTE;
        }
        $this->logout();

        return array("login.php?msgSucesso={$mensagemUsuario}");
    }

    public static function isAutenticado()
    {
        $obj = Registry::get('Seguranca_Pagamento');

        return $obj->__isAutenticado();
    }

    public function factory()
    {
        return new Seguranca_Pagamento();
    }

    protected function getSenhaDecrypt($dados, $db)
    {
        $senha = EXTDAO_Cliente::getSenhaDecryptPeloEmail($dados['email'], $db);
        return $senha;
    }

    public function onRegistraAcesso($dados)
    {
    }

    public function gravarLogin($id, $db)
    {
        $objAcesso = new EXTDAO_Acesso_cliente($db);
        $objAcesso->gravarLogin($id);
    }

    public function isPaginaRestrita($pagina)
    {
        return in_array($pagina, Seguranca_Pagamento::$paginasComAutenticacao);
    }

    public function isAcaoRestrita($classe, $acao)
    {
        return in_array($acao, Seguranca_Pagamento::$actionsComAutenticacao);
    }

    public function verificarPermissaoEmAcoesDoUsuarioCorrente($classe, $metodo)
    {
        if (!$this->__isAutenticado())
        {
            return false;
        }
        return true;
    }

    public function verificarPermissaoEmPaginasDoUsuarioCorrente()
    {
        $paginaAtual = Helper::GET("page");
        $diretorioPagina = Helper::GET("tipo");

        return true;
    }
}
