<?

class Seguranca extends InterfaceSegurancaUsuario
{   
    //lista de paginas que nao precisam de autenticacao
    public static $paginasExcecao = array("lembrar_senha", "actions", "possuo_grupo", "webservice");
    //actions que nao precisam de autenticacao
    public static $actionsExcecao = array(

        "loginSistemaOmega",
        "getDominioDaCorporacaoId",
        "getConfiguracoesDeBancoDaCorporacao",
        "getEstadoDaCorporacao",
        "lembrarSenha",
        "login",
        "efetuarLogout",
        "irParaGrupo",
        "isServidorOnline",
        "getDominioDaCorporacao",
        "verificaEstadoDaAssinatura",
        "acharGrupo",
        "logaNoSistemaDaAssinatura",
        "getCaracteristicasDoPacoteDaCorporacao",
        "editaCliente",
        "getNomeDaCorporacao",
        "getCorporacoesParaSincronizacaoDoHosting",
        "getIdCorporacao",
        "updateIdSihopAssinatura",
        "reservaCorporacaoEClienteAssinatura",
        "getNomeCorporacao",
        "relembrarSenhaPorEmail",
        "isAdministradorDaCorporacao",
        "removerTodasAsPermissoesDaCategoriaDePermissao",

        "trocarEmailDoClientePeloSistema",
        "getCorporacoesDoCliente",
        "actionlogaNoSistemaDaCorporacaoCript",
        "teste",
        "procedimentoVerificaExistenciaDoCliente",
        "existeMigracaoParaOClienteAssinatura",
        "actionPacotes",
        "alterarSenha",
        "verificaExistenciaDoCliente",
        "cadastrarClienteNaAssinatura"
        );
    

    public function __construct()
    {
        $this->paginaExcecao = Seguranca::$paginasExcecao;
    }


    public function getAcoesLiberadasParaUsoAutenticado(){
        return null;
    }
    public function __actionLogin($dados, $senha, $db = null)
    {
        try {
        
            if($db == null)$db = new Database();

            HelperLog::verbose("validarLogin::verificaSenha");
            $nextAction = Helper::POST("next_action");

            $mensagem = $this->login($dados, $senha, $db, true);

            if ($mensagem->ok()) {
                if (Helper::POST("next_action") == "fecharEAtualizar") {

                    Helper::imprimirCabecalhoParaFormatarAction();
                    Helper::imprimirMensagem("Login realizado com sucesso, aguarde redirecionamento...", MENSAGEM_OK);
                    Helper::imprimirComandoJavascript("window.opener.location.reload(true);");
                    Helper::imprimirComandoJavascriptComTimer("window.close();", 3);
                    exit();
                } else {
                    return array("index.php?msgSucesso=$mensagemPadrao");
                }
            } else {
                if (Helper::POST("next_action") == "fecharEAtualizar") {
                    $complementoGET = "next_action=fecharEAtualizar&";
                }
                return array("login.php?{$complementoGET}msgErro=N�o foi poss�vel efetuar o login com os dados de acesso informados.");
            }
        } catch (Exception $exc) {
            return array("login.php?msgErro=".  urlencode($exc->getMessage()));
        }
    }
    
    
    public function __actionLogout($mensagemUsuario = null)
    {
        $this->logout();
        header("location: login.php?msgSucesso={$mensagemUsuario}");
    }
    
    public function factory()
    {
        return new Seguranca();
    }
    
    public static function isAutenticado(){
        $obj = Registry::get('Seguranca');
        
        return $obj->__isAutenticado();
    }
    
    public function isPaginaRestrita($pagina){
        return !in_array($pagina, Seguranca::$paginasExcecao);
    }
    public function isAcaoRestrita($classe, $acao)
    {
        if(!in_array($acao, Seguranca::$actionsExcecao)) 
            return true;
        
        return false;
    }
}

