<?php

require_once acharRaizWorkspace(). DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'/classes/Funcoes.php';

$singletonFuncoes = Funcoes::getSingleton();

$classe = Helper::POSTGET("class");

$singletonFuncoes->setDiretorios(array(
    array($classe, Helper::acharRaiz()."recursos/classes/", array("class/", "EXTDAO/", "DAO/")),
    array($classe, Helper::acharRaiz()."web_service/", array("BO/", "protocolo/")),
    array($classe, Helper::acharRaiz()."web_service/BO/", array("entidade/", "util/")),
    array($classe, Helper::acharRaiz()."web_service/protocolo/", array("in/", "out/")),
    array($classe, Helper::acharRaiz()."web_service/protocolo/out/", array("comum/", "entidade/")),
    array($classe, Helper::acharRaizWorkspace().DIRETORIO_BIBLIOTECAS_COMPARTILHADAS, array("classes/", "php/", "adm_flatty", "adm_padrao")),
    array($classe, Helper::acharRaizWorkspace().DIRETORIO_BIBLIOTECAS_COMPARTILHADAS."UI/", array("I18N/")),
    array($classe, Helper::acharRaizWorkspace().DIRETORIO_BIBLIOTECAS_COMPARTILHADAS."classes/", array("protocolo/")),
    array($classe, Helper::acharRaizWorkspace().DIRETORIO_BIBLIOTECAS_COMPARTILHADAS."adm_flatty/", array("imports/")),
    array($classe, Helper::acharRaizWorkspace().DIRETORIO_BIBLIOTECAS_COMPARTILHADAS."adm_padrao/", array("imports/")),
    array($classe, Helper::acharRaizWorkspace().DIRETORIO_BIBLIOTECAS_COMPARTILHADAS."adm_incubadora_cerveja/", array("imports/")),
));


require_once acharRaizWorkspace().DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'/imports/instancias.php';
require_once '../'.PATH_RELATIVO_PROJETO.'imports/instancias.php';