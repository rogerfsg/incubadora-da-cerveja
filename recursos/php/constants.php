<?


//utilize essa flag se o banco possuir EXCLUIDO_BOOLEAN nos registros
define('MODO_BANCO_COM_CORPORACAO', false);
define('BANCO_COM_TRATAMENTO_EXCLUSAO', true);

define('IDENTIFICADOR_SISTEMA', 'CBR');
define('PERIODO_EM_DIAS_CRIACAO_COBRANCAS', 10);
define('PERIODO_EM_DIAS_ENVIO_MENSAGENS_ANTES_VENCIMENTO', 10);
define('PERIODO_EM_DIAS_ENVIO_MENSAGENS_APOS_VENCIMENTO', 5);
define('HORA_PADRAO_ENVIO_MENSAGENS', "09:00:00");

define('PAGINA_INICIAL_PADRAO',"paginas/assinaturas_do_cliente.php");

define('TITULO_PAGINAS_CLIENTE', "IncubadoraDaCerveja.com.br");

define('EMAIL_PAGSEGURO', "financeiro@workoffline.com.br");
define('TOKEN_PAGSEGURO', "4180F8B83FAA4C378B7082476852FA72");

define('NOME_SISTEMA_PONTO_ELETRONICO', "Ponto Eletr&ocirc;nico");

define('FACEBOOK_APP_ID', '173030516679427');
define('FACEBOOK_APP_SECRET', 'c7a8dc4de1610a676ad43584aa313295');

define('MERCADO_PAGO_CLIENT_ID', "6141267104021415");
define('MERCADO_PAGO_CLIENT_SECRET', "ffkR6tmDMdVidi1POXQgthq7Cl34AZeC");

define('URL_SITE', "https://www.incubadoradacerveja.com.br");

class SingletonRaizWorkspace{
    public static $raizWorkspace = null;
}

function acharRaizWorkspace(){
    if(SingletonRaizWorkspace::$raizWorkspace != null)
        return SingletonRaizWorkspace::$raizWorkspace ;
    $pathDir = '';
    $niveis = 0;

    if(php_sapi_name() != 'cli') {

        $pathDir = $_SERVER["SCRIPT_FILENAME"];
        $niveis = substr_count($pathDir,"/");
    }
    else {

        $pathDir = getcwd();
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'){
            $niveis = substr_count($pathDir,"\\");
        } else {
            $niveis = substr_count($pathDir,"/");
        }

    }


    $root = "";

    for($i = 0; $i < $niveis; $i++)
    {
        if(is_dir("{$root}__workspaceRoot")) {

            SingletonRaizWorkspace::$raizWorkspace  = $root;
            return SingletonRaizWorkspace::$raizWorkspace ;

        }
        else
            $root .= "../";
    }
    SingletonRaizWorkspace::$raizWorkspace = "";
    return SingletonRaizWorkspace::$raizWorkspace ;
}