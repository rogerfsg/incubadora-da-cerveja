<?php

header("Content-type: text/html; charset=iso-8859-1", true);

include_once '../languages/pt-br.php';
include_once 'constants.php';
include_once 'database_config.php';
include_once 'funcoes.php';

include_once '../classes/EXTDAO/EXTDAO_Cobranca_mensagem.php';
include_once '../classes/DAO/DAO_Cobranca_mensagem.php';
include_once '../classes/class/Database.php';
include_once '../classes/class/Helper.php';
include_once '../classes/class/Email_Message.php';

EXTDAO_Cobranca::criarCobrancasEMensagensDeCobrancasMensaisDoMesSeguinte();
EXTDAO_Cobranca_mensagem::enviarMensagensDoDia();

?>
