<?
define('PATH_RELATIVO_PROJETO', 'adm/');


if (substr_count($_SERVER["HTTP_HOST"], "workoffline.com.br") >= 1
|| substr_count($_SERVER["HTTP_HOST"], "incubadoradacerveja.com.br") >= 1) {

    define('REDIS_SCHEME', "tcp");
    define('REDIS_HOST', "database1.workoffline.com.br");
    define('REDIS_PORT', 7333);

    define('IDENTIFICADOR_SESSAO', "IC_");
    define('TITULO_PAGINAS', "IncubadoraDaCerveja.com.br");

    define('NOME_BANCO_DE_DADOS_PRINCIPAL', "incubadora_da_cerveja");
    define('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "");

    define('BANCO_DE_DADOS_HOST', "database1.workoffline.com.br");
    define('BANCO_DE_DADOS_PORTA', "3306");
    define('BANCO_DE_DADOS_USUARIO', "root");
    define('BANCO_DE_DADOS_SENHA', "cobige@4");

    define('PATH_MYSQL', '');

    define('ENDERECO_DE_ACESSO_PAGAMENTO', "http://my.workoffline.com.br/");
    define('DOMINIO_DE_ACESSO', "https://incubadoradacerveja.com.br/");
    define('DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://hosting.workoffline.com.br/adm/webservice.php?class=Servicos_web&action=");

    define('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'biblioteca-compartilhada/public_html/');

} else {
    define('REDIS_SCHEME', "tcp");
    define('REDIS_HOST', "127.0.0.1");
    define('REDIS_PORT', 6379);
    if (substr_count($_SERVER["HTTP_HOST"], "empresa.omegasoftware.com.br") >= 1) {

        define('IDENTIFICADOR_SESSAO', "OMG_CC_ADM2_");
        define('TITULO_PAGINAS', "IncubadoraDaCerveja");

        define('NOME_BANCO_DE_DADOS_PRINCIPAL', "incubadora_da_cerveja");
        define('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "incubadora_da_cerveja_parametros");

        define('BANCO_DE_DADOS_HOST', "vps.omegasoftware.com.br");
        define('BANCO_DE_DADOS_PORTA', "3307");
        define('BANCO_DE_DADOS_USUARIO', "root");
        define('BANCO_DE_DADOS_SENHA', "EeXKjzeC4Rum");

        define('PATH_MYSQL', '');

        define('ENDERECO_DE_ACESSO_PAGAMENTO', "http://empresa.omegasoftware.com.br/site/");
        define('DOMINIO_DE_ACESSO', "http://empresa.omegasoftware.com.br/site/");
        define('DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://hospedagem.empresa.omegasoftware.com.br/adm/webservice.php?class=Servicos_web&action=");

        define('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecasCompartilhadas/');

    } elseif (strtolower($_SERVER["DOCUMENT_ROOT"]) == strtolower("D:/wamp64/www")
        && $_SERVER["SERVER_ADMIN"] == "eduardo@omegasoftware.com.br") {

        define('IDENTIFICADOR_SESSAO', "INCUBADORA_DA_CERVEJA");
        define('TITULO_PAGINAS', "Incubadora da Cerveja");

        define('NOME_BANCO_DE_DADOS_PRINCIPAL', "incubadora_da_cerveja");
        define('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "incubadora_da_cerveja_parametros");

        define('BANCO_DE_DADOS_HOST', "localhost");
        define('BANCO_DE_DADOS_PORTA', "3306");
        define('BANCO_DE_DADOS_USUARIO', "root");
        define('BANCO_DE_DADOS_SENHA', "");

        define('PATH_MYSQL', 'D:\wamp64\bin\mysql\mysql5.7.19\bin');

        define('ENDERECO_DE_ACESSO_PAGAMENTO', "http://127.0.0.1/OmegaEmpresa/Cobranca/Trunk/site/");
        define('DOMINIO_DE_ACESSO', "127.0.0.1/OmegaEmpresa/IncubadoraDaCerveja/Trunk");
        define('DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://127.0.0.1/OmegaEmpresa/Hospedagem/Trunk/adm/webservice.php?class=Servicos_web&action=");

        define('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecaCompartilhada/Trunk/');

    } else {

        define('IDENTIFICADOR_SESSAO', "OMG_CC_ADM1");
        define('TITULO_PAGINAS', "IncubadoraDaCerveja");

        define('NOME_BANCO_DE_DADOS_PRINCIPAL', "cobranca");
        define('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "omegasoftware_interno_parametros");

        define('BANCO_DE_DADOS_HOST', "localhost");
        define('BANCO_DE_DADOS_PORTA', "3306");
        define('BANCO_DE_DADOS_USUARIO', "root");
        define('BANCO_DE_DADOS_SENHA', "");

        define('PATH_MYSQL', 'C:\wamp64\bin\mysql\mysql5.7.14\bin');

        define('ENDERECO_DE_ACESSO_PAGAMENTO', "http://empresa.omegasoftware.com.br/site/");
        define('DOMINIO_DE_ACESSO', "http://127.0.0.1/cobranca/CO10003Corporacao/");
        define('DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://127.0.0.1/hospedagem/HO10002/adm/webservice.php?class=Servicos_web&action=");

        define('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecasCompartilhadas/BC10002Corporacao/');
    }

}



$workspaceRaiz = acharRaizWorkspace();
include_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'/classes/constants.php';
include_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'/classes/Helper.php';
include_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'/classes/PROTOCOLO_SISTEMA.php';
include_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'/classes/protocolo/Interface_mensagem.php';
include_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'/classes/protocolo/Mensagem.php';
include_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'/classes/protocolo/Mensagem_protocolo.php';
include_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'/classes/protocolo/Mensagem_token.php';
include_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'/classes/protocolo/Mensagem_vetor_protocolo.php';
include_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'/classes/protocolo/Mensagem_vetor_token.php';


