<?

define('PATH_RELATIVO_PROJETO', 'client_area/');


define('MENSAGEM_LOGOUT_CLIENTE', "Logout realizado com sucesso");
 

if (substr_count($_SERVER["HTTP_HOST"], "incubadoradacerveja.com.br") >= 1) {

    define('REDIS_SCHEME', "tcp");
    define('REDIS_HOST', "database1.workoffline.com.br");
    define('REDIS_PORT', 7333);

    define('IDENTIFICADOR_SESSAO', "IC_");
    define('TITULO_PAGINAS', "IncubadoraDaCerveja.com.br");

    define('NOME_BANCO_DE_DADOS_PRINCIPAL', "incubadora_da_cerveja");
    define('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "");

    define('BANCO_DE_DADOS_HOST', "45.33.121.90");
    define('BANCO_DE_DADOS_PORTA', "3306");
    define('BANCO_DE_DADOS_USUARIO', "root");
    define('BANCO_DE_DADOS_SENHA', "cobige@4");
    
    define('PATH_MYSQL', '');

    
    define('DOMINIO_DE_ACESSO', "http://incubadoradacerveja.com.br");
    
    define('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'biblioteca-compartilhada/public_html/');

}else{

    define('REDIS_SCHEME', "tcp");
    define('REDIS_HOST', "127.0.0.1");
    define('REDIS_PORT', 6379);
    if (strtolower($_SERVER["DOCUMENT_ROOT"]) == strtolower("D:/wamp64/www")
        && $_SERVER["SERVER_ADMIN"] == "eduardo@omegasoftware.com.br") {

        define('IDENTIFICADOR_SESSAO', "OMG_CO_CLI2");
        define('TITULO_PAGINAS', "My WorkOffline");

        define('NOME_BANCO_DE_DADOS_PRINCIPAL', "incubadora_da_cerveja");
        define('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "incubadora_da_cerveja_parametros");

        define('BANCO_DE_DADOS_HOST', "localhost");
        define('BANCO_DE_DADOS_PORTA', "3306");
        define('BANCO_DE_DADOS_USUARIO', "root");
        define('BANCO_DE_DADOS_SENHA', "");

        define('PATH_MYSQL', 'D:\wamp64\bin\mysql\mysql5.7.19\bin');

        define('ENDERECO_DE_ACESSO_PAGAMENTO', "http://127.0.0.1/OmegaEmpresa/Cobranca/Trunk/site/");
        define('DOMINIO_DE_ACESSO', "127.0.0.1/OmegaEmpresa/Cobranca/Trunk");
        define('DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://127.0.0.1/OmegaEmpresa/Hospedagem/Trunk/adm/webservice.php?class=Servicos_web&action=");

        define('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecaCompartilhada/Trunk/');
    } else {

        define('IDENTIFICADOR_SESSAO', "OMG_CO_CLI3");
        define('TITULO_PAGINAS', "My WorkOffline");

        define('NOME_BANCO_DE_DADOS_PRINCIPAL', "incubadora_da_cerveja");
        define('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "incubadora_da_cerveja_parametros");

        define('BANCO_DE_DADOS_HOST', "localhost");
        define('BANCO_DE_DADOS_PORTA', "3306");
        define('BANCO_DE_DADOS_USUARIO', "root");
        define('BANCO_DE_DADOS_SENHA', "");

        define('PATH_MYSQL', 'C:\wamp64\bin\mysql\mysql5.7.14\bin');

        define('ENDERECO_DE_ACESSO_PAGAMENTO', "http://empresa.omegasoftware.com.br/site/");
        define('DOMINIO_DE_ACESSO', "localhost/incubadoradacerveja/IC10000");
        define('DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://127.0.0.1/hospedagem/HO10002/adm/webservice.php?class=Servicos_web&action=");

        define('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecasCompartilhadas/BC10002Corporacao/');
    }
}


define('ENDERECO_DE_ACESSO', "http://" + DOMINIO_DE_ACESSO + "/");
define('ENDERECO_DE_ACESSO_SSL', "https://" + DOMINIO_DE_ACESSO + "/");

//echo "afdg:sdafg: ".DOMINIO_DE_ACESSO;



$workspaceRaiz = acharRaizWorkspace();
include_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'/classes/constants.php';
include_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'/classes/Helper.php';
include_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'/classes/PROTOCOLO_SISTEMA.php';
include_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'/classes/protocolo/Interface_mensagem.php';
include_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'/classes/protocolo/Mensagem.php';
include_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'/classes/protocolo/Mensagem_protocolo.php';
include_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'/classes/protocolo/Mensagem_token.php';
include_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'/classes/protocolo/Mensagem_vetor_protocolo.php';
include_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'/classes/protocolo/Mensagem_vetor_token.php';


