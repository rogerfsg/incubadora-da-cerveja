<?php

?>

<div class="dark padding-40-0">
    <div class="container">
        <div class="row">

            <div class="col-sm-3">


                <div class="barril-animado stout" id="barril-a">
                    <div class="container-barril">

                        <img src="imgs/especifico/barril-frame.png" class="frame-barril" />

                        <div class="liquido">
                            <div class="efeito-topo-liquido"></div>
                            <div class="bubble bubble1"></div>
                            <div class="bubble bubble2"></div>
                            <div class="bubble bubble3"></div>
                            <div class="bubble bubble4"></div>
                            <div class="bubble bubble5"></div>
                        </div>

                    </div>

                </div>

            </div>

            <div class="col-sm-3">


                <div class="barril-animado dark-ale" id="barril-b">
                    <div class="container-barril">

                        <img src="imgs/especifico/barril-frame.png" class="frame-barril" />

                        <div class="liquido">
                            <div class="efeito-topo-liquido"></div>
                            <div class="bubble bubble1"></div>
                            <div class="bubble bubble2"></div>
                            <div class="bubble bubble3"></div>
                            <div class="bubble bubble4"></div>
                            <div class="bubble bubble5"></div>
                        </div>

                    </div>

                </div>

            </div>

            <div class="col-sm-3">



                <div class="barril-animado golden-ale" id="barril-c">
                    <div class="container-barril">

                        <img src="imgs/especifico/barril-frame.png" class="frame-barril" />

                        <div class="liquido">
                            <div class="efeito-topo-liquido"></div>
                            <div class="bubble bubble1"></div>
                            <div class="bubble bubble2"></div>
                            <div class="bubble bubble3"></div>
                            <div class="bubble bubble4"></div>
                            <div class="bubble bubble5"></div>
                        </div>

                    </div>

                </div>

            </div>

            <div class="col-sm-3">

                <div class="barril-animado ipa" id="barril-d">
                    <div class="container-barril">

                        <img src="imgs/especifico/barril-frame.png" class="frame-barril" />

                        <div class="liquido">
                            <div class="efeito-topo-liquido"></div>
                            <div class="bubble bubble1"></div>
                            <div class="bubble bubble2"></div>
                            <div class="bubble bubble3"></div>
                            <div class="bubble bubble4"></div>
                            <div class="bubble bubble5"></div>
                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>
</div>


<script type="text/javascript">

    $(document).ready(function() {

        AppUtils.criarBarril('barril-a', 30, 100);
        AppUtils.criarBarril('barril-b', 50, 100);
        AppUtils.criarBarril('barril-c', 70, 100);
        AppUtils.criarBarril('barril-d', 100, 100);

        AppUtils.criarBarril('barril-5', 30, 50);
        AppUtils.criarBarril('barril-6', 50, 50);
        AppUtils.criarBarril('barril-7', 70, 50);
        AppUtils.criarBarril('barril-8', 100, 50);

    });

</script>

