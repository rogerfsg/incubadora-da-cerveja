
<div  class="dark section  padding-40-0" ng-controller="barrisController as vm">
    <div class="container">
        <div class="row">

            <div class="col-xs-12 matchHeight scrollme padding-bottom-20" >
                <div class="">
                    <header>
                        <h1>Barris / Festas</h1>
                    </header>
                    <p style="display: none;">Barris das minhas festas, das festas dos meus amigos Degustadores que fui convidado, das Brew Pub's, dos Ciganos e das Cervejarias!</p>
                </div>
            </div>
            <div class="col-sm-12 matchHeight " ng-cloak >

                <div class="col-sm-2 product " >
                    <a href="#" ng-click="vm.abrirFormularioCadastroBarril()" >
                        <span>Festa!</span>
                        <div ng-init="vm.criarBarrilCadastro()"></div>
                        <div class="barril-animado pilsen" id="barril-cadastro" >
                            <div class="container-barril">

                                <img src="imgs/especifico/barril-frame.png" class="frame-barril" />

                                <div class="liquido">
                                    <div class="efeito-topo-liquido"></div>
                                    <div class="bubble bubble1"></div>
                                    <div class="bubble bubble2"></div>
                                    <div class="bubble bubble3"></div>
                                    <div class="bubble bubble4"></div>
                                    <div class="bubble bubble5"></div>
                                </div>

                            </div>

                        </div>

                        <div ng-init="vm.criarBarrilCadastro()" ></div>

                    </a>
                    <h3>Novo</h3>
                    <p>Criar o barril da minha festa</p>
                </div>
                <!-- Products -->
                <div class="col-sm-2 product " ng-cloak ng-repeat="b in vm.barris.barrisMeus track by $index">

                    <a href="#" ng-click="vm.abrirBarril(b)">
                        <span>Gerenciar</span>
                        <div ng-init="vm.criarBarril('barril-' + $index, b)"></div>
                        <div class="barril-animado {{b.tipoCerveja}}" id="barril-{{$index}}" >
                            <div class="container-barril">

                                <img src="imgs/especifico/barril-frame.png" class="frame-barril" />

                                <div class="liquido">
                                    <div class="efeito-topo-liquido"></div>
                                    <div class="bubble bubble1"></div>
                                    <div class="bubble bubble2"></div>
                                    <div class="bubble bubble3"></div>
                                    <div class="bubble bubble4"></div>
                                    <div class="bubble bubble5"></div>
                                </div>

                            </div>

                        </div>

                        <div ng-init="vm.criarBarril('barril-' + $index, b)"></div>

                    </a>
                    <h3>Meu barril</h3>
                    <h4>{{b.precoMedio}}</h4>
                    <p>{{b.totalMlVendido}} / {{b.totalMl}}</p>
                    <p></p>
                </div>

                <div class="col-sm-2 product " ng-repeat="b in vm.barris.barrisPrivadosDegustadores track by $index">

                    <a href="#" ng-click="vm.abrirBarril(b)">
                        <span>Participar</span>
                        <div ng-init="vm.criarBarril('barril-' + $index, b)"></div>
                        <div class="barril-animado" id="barril-{{$index}}" >
                            <div class="container-barril">

                                <img src="imgs/especifico/barril-frame.png" class="frame-barril" />

                                <div class="liquido">
                                    <div class="efeito-topo-liquido"></div>
                                    <div class="bubble bubble1"></div>
                                    <div class="bubble bubble2"></div>
                                    <div class="bubble bubble3"></div>
                                    <div class="bubble bubble4"></div>
                                    <div class="bubble bubble5"></div>
                                </div>

                            </div>

                        </div>

                        <div ng-init="vm.criarBarril('barril-' + $index, b)"></div>

                    </a>

                    <h3>{{b.cervejeiro}}</h3>
                    <h4>{{b.precoMedio}}</h4>
                    <p>{{b.totalMlVendido}} / {{b.totalMl}}</p>
                    <p></p>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="separador-main container">&nbsp;</div>
