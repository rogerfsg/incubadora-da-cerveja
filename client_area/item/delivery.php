<!-- Section -->
<div class="dark padding-40-0" ng-controller="deliveryController as vm">
    <div class="container">
        <div class="row">

            <div class="col-xs-12 col-md-6 matchHeight scrollme" >
                <div class="texto-paragrafos-menores">
                    <header>
                        <h1>Em breve: Delivery de cerveja artesanal em BH</h1>
                        <h2>Compre cerveja em bitcoin (com frete incluso)!</h2>
                    </header>

                    <p>Delivery em Belo Horizonte de cerveja artesanal, venda s� em Bitcoin!</p>
                    <p>Marcas que novas que ainda n�o chegaram em BH!</p>
                    <p>O frete j� est� incluso, regi�o de atendimento: Centro-Sul, Prado e Gutierrez.</p>
                    <p>Na compra de duas ou mais garrafas, 5% de desconto!</p>

                    <div class="fb-share-button"
                         data-href="https://incubadoradacerveja.com.br/index.php"
                         data-layout="button_count" data-size="small" data-mobile-iframe="true"><a target="_blank"
                                                                                                   href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fincubadoradacerveja.com.br%2Findex.php;src=sdkpreparse" class="fb-xfbml-parse-ignore">
                            Compartilhar</a>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-md-6 matchHeight">
                <div class="row">

                    <div class="col-xs-12 col-md-6 icon-grid">
                        <div class="product">
                            <a href="#">
                                <span>Em breve...</span>
                                <img src="imgs/template/FancyBottle.svg" class="svg">
                            </a>
                            <h3>Pale ale</h3>
                            <h3>0,0012 BTC</h3>
                            <p>Garrafa de 500 ml</p>

                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6 icon-grid ">
                        <div class="product">
                            <a href="#">
                                <span>Em breve...</span>
                                <img src="imgs/template/FancyBottle.svg" class="svg">
                            </a>
                            <h3>Pilsen</h3>
                            <h3>0,0012 BTC</h3>
                            <p>Garrafa de 500 ml</p>

                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6 icon-grid no-border-bottom">
                        <div class="product">
                            <a href="#">
                                <span>Em breve...</span>
                                <img src="imgs/template/FancyBottle.svg" class="svg">
                            </a>
                            <h3>Stout</h3>
                            <h3>0,0012 BTC</h3>
                            <p>Garrafa de 500 ml</p>

                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6 icon-grid no-border-bottom">
                        <div class="product">
                            <a href="#">
                                <span>Em breve...</span>
                                <img src="imgs/template/FancyBottle.svg" class="svg">
                            </a>
                            <h3>Lager</h3>
                            <h3>0,0012 BTC</h3>
                            <p>Garrafa de 500 ml</p>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>

<div class="separador-main container">&nbsp;</div>

<!-- Section -->
<div class="dark padding-40-0" ng-controller="deliveryController as vm">
    <div class="container">
        <div class="row">

            <div class="col-xs-12 col-md-12 matchHeight scrollme" >
                <div class="">
                    <header>
                        <h1>Newsletter</h1>
                        <h2>Fique por dentro de todas as novidades que v�m por a�!</h2>
                    </header>

                    <div class="">
                        <div class="col-sm-12">

                            Informe seu e-mail:

                            <input class='form-control'

                                   placeholder=''
                                   ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/"
                                   name="fieldEmail"
                                   ng-model="vm.email"
                                   type='text'>

                            <span class='help-block'
                                  ng-show="showValidationMessage('fieldEmail', mainForm, { 'email': 'O e-mail � inv�lido', required: 'Campo Obrigat�rio'})">
                                                </span>
                        </div>
                        <div class="col-sm-12">
                            <button class='btn btn-default' type='submit' ng-click="vm.cadastrar()">
                                <i class='icon-save'></i>
                                Cadastrar
                            </button>
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

<div class="separador-main container">&nbsp;</div>

