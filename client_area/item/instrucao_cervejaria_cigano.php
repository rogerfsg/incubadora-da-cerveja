<!-- Section -->
<div class="dark padding-40-0">
    <div class="container">
        <div class="row">

            <div class="col-xs-12 col-md-6 matchHeight scrollme " >
                <div class="">
                    <header>
                        <h1>Cigano</h1>
                        <h2>Sua casa � aqui!</h2>
                    </header>
                    <p>Relacione com os Degustadores, venda sem estoque!</p>
                    <p>Relacione com as Cervejarias!</p>

                    <a href="#" class="btn btn-default"><span>Show me more</span></a>
                </div>
            </div>

            <div class="col-xs-12 col-md-6 matchHeight">
                <div class="row">
                    <div class="col-xs-12 col-md-6 icon-grid">
                        <img src="imgs/template/Hops.svg" class="svg"  />
                        <h4>Cadastre</h4>
                        <p>Cadastre sua cervejaria</p>
                    </div>
                    <div class="col-xs-12 col-md-6 icon-grid">
                        <img src="imgs/template/Sun.svg" class="svg"  />
                        <h4>Aguarde Ciganos</h4>
                        <p>Aguarde os pedidos dos ciganos para produ��o</p>
                    </div>
                    <div class="col-xs-12 col-md-6 icon-grid no-border-bottom">
                        <img src="imgs/template/Barrel.svg" class="svg"  />
                        <h4>Produza</h4>
                        <p>Produza o barril do cigano</p>
                    </div>
                    <div class="col-xs-12 col-md-6 icon-grid no-border-bottom">
                        <img src="imgs/template/BottleCrate.svg" class="svg" alt="Unrivalled taste" />
                        <h4>Entregue</h4>
                        <p>Realize as entregas aos degustadores ou aguarde a busca na cervejaria</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="separador-main container">&nbsp;</div>
