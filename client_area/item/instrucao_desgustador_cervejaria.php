<?php

?>

<div class="col-xs-12 col-md-6 matchHeight">
    <div class="row">
        <div class="col-xs-12 col-md-6 icon-grid">
            <img src="imgs/template/Hops.svg" class="svg"  />
            <h4>Barril</h4>
            <p>Fa�a seu pedido de um Barril do Cigano ou da Cervejaria</p>
        </div>
        <div class="col-xs-12 col-md-6 icon-grid">
            <img src="imgs/template/Sun.svg" class="svg"  />
            <h4>Aguarde</h4>
            <p>Aguarde a arrecada��o do Barril do local. Assim que o Barril se completa, o n�mero de dias do prazo de produ��o come�a a ser contado</p>
        </div>
        <div class="col-xs-12 col-md-6 icon-grid no-border-bottom">
            <img src="imgs/template/Barrel.svg" class="svg"  />
            <h4>Divirta-se</h4>
            <p>Receba a cerveja em casa ou busque na cervejaria</p>
        </div>
    </div>
</div>