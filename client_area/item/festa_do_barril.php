<!-- Section -->
<div class="dark padding-40-0" ng-controller="deliveryController as vm">
    <div class="container">
        <div class="row">

            <div class="col-xs-12 col-md-12 matchHeight scrollme painel1 take" >
                <div class="texto-paragrafos-menores">
                    <header >
                        <h1>Festa do Barril</h1>
                        <h2>Em qualquer Brew Pub do Brasil!</h2>
                    </header>

                    <p class="painel1A take">Festa do Barril � uma festa que vende a cerveja antecipadamente, evitando filas no Brew Pub!<p>
                    <p class="painel1B take">E ainda pode te render umas cervejas gr�tis! Se voc� convidar a galera!</p>
                    <p class="painel1C take">Quem quer rir tem que fazer rir, n�o � mesmo? hehehe</p>
                </div>
            </div>

            <div class="col-xs-4 col-md-4 matchHeight painel2 take">
                <div class="row">
                    <div class="col-xs-12 col-md-12 icon-grid">
                        <img src="imgs/template/BottleCrate.svg" class="svg"  />
                        <h4>1 - Pedir cervejas!</h4>
                        <p>Voc� acessa para comprar as cervejas que vai beber na festa</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-4 col-md-4 matchHeight painel3 take">
                <div class="row">
                    <div class="col-xs-12 col-md-12 icon-grid">
                        <img src="imgs/template/Sign.svg" class="svg"  />
                        <h4>2 - Compra conclu�da</h4>
                        <p>� emitido um ticket QR code na confirma��o da compra, esse controla o seu saldo de cerveja</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-4 col-md-4 matchHeight painel4 take">
                <div class="row">
                    <div class="col-xs-12 col-md-12 icon-grid no-border-bottom">
                        <img src="imgs/template/Tank.svg" class="svg"  />
                        <h4>3 - Convide os amigos!</h4>
                        <p>Voc� tamb�m ganha um link para convidar seus amigos pelo WhatsApp, Facebook, Email, etc...</p>
                        <p>Seu amigo clica no link e acessa a incubadoradacerveja.com.br para realizar a compra de cerveja</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-4 col-md-4 matchHeight painel5 take">
                <div class="row">
                    <div class="col-xs-12 col-md-12 icon-grid no-border-bottom">
                        <img src="imgs/template/Sign.svg" class="svg"  />
                        <h4>4 - Seu amigo realiza a compra</h4>
                        <p>Se ele concluir a compra, voc� acumula em cerveja 5% do total que ele pediu. Ou seja, se ele comprar 1 litro, voc� ganha 50ml</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-4 col-md-4 matchHeight painel6 take">
                <div class="row">
                    <div class="col-xs-12 col-md-12 icon-grid no-border-bottom">
                        <img src="imgs/template/Tankard.svg" class="svg"  />
                        <h4>5 - Festa</h4>
                        <p>A cada 500 ml voc� ganha uma caneca de 500ml na festa!</p>
                    </div>

                </div>
            </div>
            <div class="col-xs-12 col-md-12 matchHeight painel7 take">
                <div class="row">
                    <p>
                        <a href="index.php?tipo=paginas&page=cadastro.cervejeiro&tipoDoUsuario=brewpub" class="btn btn-default only-public-area"><span>Tenho um Brew Pub e quero fazer a festa do Barril!</span></a>
                    </p>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="separador-main container">&nbsp;</div>

