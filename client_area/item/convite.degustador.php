<?php
/**
 * Created by PhpStorm.
 * User: W10
 * Date: 10/03/2018
 * Time: 12:11
 */
?>


<a class="btn btn-info btn-lg" data-toggle="modal" href="#modal-convite-degustador" id="bt-modal-convite-degustador"
   role="button" style="display: none;">Abrir convite degustador</a>

<div class='modal fade ' id='modal-convite-degustador' tabindex='-1' ng-controller="barrilController as vm">
    <div class='modal-dialog ' ng-cloak>
        <div class='modal-content'>
            <div class='modal-header'>
                <button aria-hidden='true' class='close' data-dismiss='modal' type='button'>�</button>
                <h4 class='modal-title'>Convite da Cervejada</h4>
            </div>
            <div class='modal-body'>
                <div class='box'>
                    <div class='box-content'>
                        <form name="vm.formularioConviteDegustador" novalidate>


                            <div class='box-content'>
                                <ul>
                                    <h3>{{vm.getEstadoBarril()}}</h3>

                                </ul>
                            </div>

                            <div class="box-content col-xs-12 container-imagem-convite padding-bottom-20"
                                 ng-show="vm.gerarQrCode() != null">

                                <div class="col-sm-4"></div>
                                <div class="  col-sm-3  conteudo-imagem-convite" id="capture-convite">
                                    <h4>Meu Convite</h4>
                                    <div class="" id="qrcode-convite"></div>
                                    <p>
                                        <small>N�o compartilhe essa imagem</small>
                                    </p>
                                </div>
                                <div class="col-sm-4"></div>

                            </div>
                            <div class="box-content col-xs-12 container-imagem-convite"
                                 ng-show="vm.gerarQrCode() != null">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4 conteudo-imagem-convite" id="capture-convite">

                                    <button class="btn-sm btn-success"
                                            type="button"
                                            ng-click="vm.downloadQrCode()"
                                            ng-hide="vm.getChaveBarril() == null"
                                    >
                                        <i class="glyphicon glyphicon-download"></i>
                                        Download convite
                                    </button>
                                </div>
                                <div class="col-sm-3"></div>
                            </div>

                            <div class='padding-bottom-20'>
                                <div class="row" >
                                    <h4 class='col-form-label'>Novo pedido</h4>
                                    <h3 ng-show="vm.barrilEstaCheio()"><?php echo I18N::getExpression(" O barril encheu!"); ?></h3>
                                    <ul ng-hide="vm.barrilEstaCheio()">

                                        <li ng-hide="vm.checaLimiteVolume()" >
                                            <span style="color: red;" ng-bind="vm.mensagemLimite()"></span>
                                        </li>
                                        <li ng-show="!vm.barrilEstaCheio()">

                                            <p >Quantos litros voc� vai beber?</p>

                                            <input class='form-control '
                                                   ng-model="vm.qtdLitroNovoPedido"
                                                   type='text'
                                                   min="0"
                                                   max="{{vm.getTotalRestante()}}"
                                                   ui-number-mask="0"
                                                   maxlength="2"

                                            >

                                            <p  >
                                                <small>Pre�o do litro
                                                    R$ <span ng-bind="vm.precoLitro()"></span></small>
                                            </p>

                                            <p >
                                                R$<span ng-bind="vm.calculaCustoDaBebida()"></span>
                                            </p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="row" >
                                    <div class="col-sm-3">
                                        <button class="btn-sm btn-success"
                                                type="button"
                                                ng-click="vm.beberClick(true)"
                                                ng-hide="
                                                vm.barrilEstaCheio()
                                                ||
                                                vm.getIdEstadoBarril() != 1
                                                ||
                                                (
                                                vm.getIdEstadoConvite() != 4
                                                && vm.getIdEstadoConvite() != 2)"
                                        >
                                            <i class="glyphicon glyphicon-ok"></i>
                                            Vou beber e j� paguei
                                        </button>
                                        <button class="btn-sm btn-success"
                                                type="button"
                                                ng-click="vm.beberClick(true)"

                                                ng-show="
                                        !vm.barrilEstaCheio()
                                         && vm.getIdEstadoBarril() == 1
                                         && vm.getIdEstadoConvite() == 3

                                        ">
                                            <i class="glyphicon glyphicon-ok"></i>
                                            Vou beber mais e j� paguei
                                        </button>
                                    </div>
                                    <div class="col-sm-2">
                                        <button class="btn-sm btn-warning"
                                                type="button"
                                                ng-click="vm.beberClick(false)"
                                                ng-hide="vm.getIdEstadoConvite() != 4 || vm.getIdEstadoBarril() != 1">
                                            <i class="glyphicon glyphicon-ok"></i>


                                            Vou mas sem beber
                                        </button>
                                    </div>

                                    <div class="col-sm-2">
                                        <button class="btn-sm btn-info"
                                                type="button"
                                                ng-click="vm. comentarClick()">
                                            <i class="glyphicon glyphicon-send"></i>
                                            Enviar mensagem
                                        </button>
                                    </div>

                                </div>
                            </div>


                            <div class='box-content'>
                                <h4 class='col-form-label'>Dados Festa</h4>
                                <ul>
                                    <li>
                                        <label class='col-form-label' >Cervejeiro</label>
                                        <h3>{{vm.getNomeCervejeiro()}}</h3>
                                    </li>
                                    <li>
                                        <label class='col-form-label' >Endere�o</label>
                                        <h3>{{vm.getEndereco()}}</h3>
                                    </li>
                                    <li>

                                        <label class='col-form-label' >Data da festa</label>
                                        <h3>{{vm.getDataFesta()}}</h3>
                                    </li>
                                    <li>
                                        <label class='col-form-label' >Data da confirma��o</label>
                                        <h3>{{vm.getDataConfirmacao()}}</h3>
                                    </li>
                                </ul>
                            </div>

                            <div class='padding-bottom-20'>
                                <h4 class='col-form-label'>dados banc�rios para dep�sitos dos convidados</h4>
                                <ul>
                                    <li>

                                        <label class='col-form-label' >Agencia: </label>
                                        <h3>{{vm.getDadosBancarios().agencia}}</h3>
                                    </li>
                                    <li>
                                        <label class='col-form-label' >Conta: </label>
                                        <h3>{{vm.getDadosBancarios().conta}}</h3>
                                    </li>
                                    <li>
                                        <label class='col-form-label' >Operacao: </label>
                                        <h3>{{vm.getDadosBancarios().operacao}}</h3>
                                    </li>
                                    <li>
                                        <label class='col-form-label' >Nome Titular: </label>
                                        <h3>{{vm.getDadosBancarios().titular}}</h3>
                                    </li>
                                </ul>
                                <small>Aten��o: esse convite � do seu amigo para voc�, � de sua responsabilidade essa transfer�ncia banc�ria. Esse convite n�o � uma venda! Se for, denuncie!</small>
                            </div>
                            <div class='padding-bottom-20'>
                                <h4 class='col-form-label'>Meus pedidos</h4>
                                <ul>
                                    <li>

                                        <table class="table table-striped" >

                                            <thead>
                                            <tr>
                                                <th>Data</th>
                                                <th>Quantidade (litro)</th>
                                                <th>Valor (R$)</th>
                                                <th>Estado</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr ng-repeat="pedido in vm.getPedidos()">
                                                <td>
                                                    <span ng-bind="pedido.data"></span>
                                                </td>
                                                <td>
                                                    <span ng-bind="pedido.qtdLitros"></span>
                                                </td>
                                                <td>
                                                    <span ng-bind="pedido.valor"></span>
                                                </td>
                                                <td>
                                                    <span ng-bind="pedido.estadoPedido"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="5" ></td>
                                            </tr>
                                            <tr>

                                                <td>
                                                    Confirmado

                                                </td>
                                                <td>
                                                    <span ng-bind="vm.saldoConfirmado()"></span>

                                                </td>
                                                <td colspan="2">

                                                </td>
                                            </tr>
                                            <tr>

                                                <td>
                                                    N�o confirmado

                                                </td>
                                                <td>
                                                    <span ng-bind="vm.saldoNaoConfirmado()"></span>

                                                </td>
                                                <td colspan="2">

                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </li>
                                </ul>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>
