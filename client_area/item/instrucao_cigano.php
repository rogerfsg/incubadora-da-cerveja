<!-- Section -->
<div class="dark padding-40-0">
    <div class="container">
        <div class="row">

            <div class="col-xs-12 col-md-6 matchHeight scrollme " >
                <div class="">
                    <header>
                        <h1>Cigano</h1>
                        <h2>Sua casa � aqui</h2>
                    </header>
                    <p>J� tem uma cervejaria Cigana?</p>
                    <p>Realize o planejamento do inicio ao fim com as Cervejarias e os Degustadores</p>
                    <p>Conhe�a o gosto do seu p�blico com nossa ferramenta anal�tica</p>
                    <p>Integre o seu sistema com nossa API</p>
                    <a
                            href="index.php?tipo=paginas&page=cadastro.cervejeiro&tipoDoUsuario=cigano"
                            class="btn btn-default only-public-area"
                            style="white-space: normal;  word-wrap: break-word;">
                        <span>Sou cigano e quero morar aqui!</span>
                    </a>

                    <div class="fb-share-button"
                         data-href="https://incubadoradacerveja.com.br/index.php?tipo=paginas&amp;pagina=cadastro.cervejeiro&amp;tipoCervejeiro=cigano"
                         data-layout="button_count" data-size="small" data-mobile-iframe="true"><a target="_blank"
                                                                                                   href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fincubadoradacerveja.com.br%2Findex.php%3Fpagina%3Dcadastro.cervejeiro&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">
                            Compartilhar</a>
                    </div>

                </div>
            </div>

            <div class="col-xs-12 col-md-6 matchHeight">
                <div class="row">
                    <div class="col-xs-12 col-md-6 icon-grid">
                        <img src="imgs/template/Keg.svg" class="svg"  />
                        <h4>Barril</h4>
                        <p>Cadastre o barril, escolha a brassagem m�nima, o tipo de cerveja do seu r�tulo no MAPA e indique a cervejaria onde ir� produzir</p>
                    </div>
                    <div class="col-xs-12 col-md-6 icon-grid">
                        <img src="imgs/template/Sign.svg" class="svg" alt="Aguarde os pedidos dos degustadores" />
                        <h4>Pedidos</h4>
                        <p>Aguarde os pedidos dos Degustadores</p>
                    </div>
                    <div class="col-xs-12 col-md-6 icon-grid no-border-bottom">
                        <img src="imgs/template/Barrel.svg" class="svg" alt="Produza na cervejaria" />
                        <h4>Produza</h4>
                        <p>Produza o barril na cervejaria indicada</p>
                    </div>
                    <div class="col-xs-12 col-md-6 icon-grid no-border-bottom">
                        <img src="imgs/template/BottleCrate.svg" class="svg" alt="Unrivalled taste" />
                        <h4>Entregue</h4>
                        <p>Realize as entregas aos degustadores ou aguarde a busca na cervejaria</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="separador-main container">&nbsp;</div>