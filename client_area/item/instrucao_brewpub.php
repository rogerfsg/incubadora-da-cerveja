<!-- Section -->
<div class="dark padding-40-0">
    <div class="container">
        <div class="row">

            <div class="col-xs-12 col-md-6 matchHeight scrollme " >
                <div class="">
                    <header>
                        <h1>Brew Pub</h1>
                        <h2>Festa do Barril no seu Brew Pub</h2>
                    </header>
                    <p>Organize aqui a festa do barril no seu Brew Pub!</p>
                    <p>Atraia novos clientes e ofere�a essa comodidade do convite por QR Code.</p>
                    <p>Cada convidado possui um convite contendo o QR Code identificador, o saldo � controlado por esse mecanismo.</p>
                    <p>Toda hora que for servir a cerveja, aponte a camera para o QR Code e valide o saldo do cliente. Pronto, simples assim!</p>

                    <a style="white-space: normal;" href="index.php?tipo=paginas&page=cadastro.cervejeiro&tipoDoUsuario=brewpub" class="btn btn-default only-public-area"><span>Tenho um Brew Pub e quero fazer parte!</span></a>

                    <div class="fb-share-button"
                         data-href="https://incubadoradacerveja.com.br/index.php?tipo=paginas&amp;pagina=cadastro.cervejeiro&amp;tipoCervejeiro=brewpub"
                         data-layout="button_count" data-size="small" data-mobile-iframe="true"><a target="_blank"
                                                                                                   href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fincubadoradacerveja.com.br%2Findex.php%3Fpagina%3Dcadastro.cervejeiro&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">
                            Compartilhar</a>
                    </div>

                </div>
            </div>

            <div class="col-xs-12 col-md-6 matchHeight">
                <div class="row">
                    <div class="col-xs-12 col-md-6 icon-grid">
                        <img src="imgs/template/Keg.svg" class="svg"  />
                        <h4>Barril</h4>
                        <p>Cadastre sua festa e compartilhe o link na sua p�gina do facebook ou envie como mensagem pelo facebook aos convidados. Para comprar a cerveja da festa voc� tem que indicar o custo m�dio por litro.</p>
                    </div>
                    <div class="col-xs-12 col-md-6 icon-grid">
                        <img src="imgs/template/Sun.svg" class="svg"  />
                        <h4>Aguarde</h4>
                        <p>Aguarde a confirma��o de seus convidados e a indica��o deles de quanta bebida ir�o consumir</p>
                    </div>
                    <div class="col-xs-12 col-md-6 icon-grid no-border-bottom">
                        <img src="imgs/template/3DSixPack.svg" class="svg"  />
                        <h4>Organize</h4>
                        <p>Organize a festa do Barril no seu Brew Pub</p>
                    </div>
                    <div class="col-xs-12 col-md-6 icon-grid no-border-bottom">
                        <img src="imgs/template/Tankard.svg" class="svg"  />
                        <h4>Festa</h4>
                        <p>No dia da festa os seus gar�ons ir�o controlar a venda de bebidas apenas com a camera do App, a cada novo pedido, aponte para o convite e pronto! Sirva!</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="separador-main container">&nbsp;</div>