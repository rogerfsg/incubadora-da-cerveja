<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCnkq1W7ZKi9q4SDSlpBNVfdMZIf-iTpPc&libraries=places">

</script>

<!-- Section -->
<div class="dark padding-40-0" ng-controller="fazerPedidoSimplesController as vm">
    <div class="container">
        <div class="row">

            <div class="container-fluid dark section no-padding margin-bottom-20">
                <div class="row">
                    <div class="col-sm-12 icon-grid">
                        <img src="imgs/especifico/saint-stello/logo-white.png" />
                        <h2 class="destaque">Cervejaria Saint Stello</h2>
                        <p>Conhe�as as Cervejas Artesanais Saint Stelo. S�o composi��es variadas, para garantir a diversifica��o de sabores e, assim, agradar a todos os paladares.</p>
                        <p>Aproveite as ofertas e veja como ganhar desconto de at� 50%!</p>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-md-12 matchHeight scrollme border-bottom padding-bottom-20" >
                <div class="texto-paragrafos-menores">
                    <header>
                        <h3>Vejas as regras para ter valor do desconto reembolsado!</h3>
                    </header>

                    <h4>V�LIDO SOMENTES PARA PESSOAS F�SICAS</h4>

                    <p>1 - Fa�a a compra em nosso site;</p>
                    <p>2 - Receba a suas cervejas;</p>
                    <p>3 - Poste no seu Instagram fotos de cada cerveja com os seus amigos;</p>
                    <p>4 - Nos envie o link do POST;</p>
                    <p>5 - Receba o estorno parcial em seu cart�o de cr�dito.</p>

                    <br />

                    <button class="btn btn-default botao-adicionar-ao-pedido" onclick="$('#trigger-modal-regulamento').click()">Ver mais detalhes</button>

                    <br /><br />

                    <div class="fb-share-button"
                         data-href="https://incubadoradacerveja.com.br/index.php"
                         data-layout="button_count" data-size="small" data-mobile-iframe="true"><a target="_blank"
                                                                                                   href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fincubadoradacerveja.com.br%2Findex.php;src=sdkpreparse" class="fb-xfbml-parse-ignore">
                            Compartilhar</a>
                    </div>

                    <!--
                    <br /><br />
                    <h4>PESSOA JUR�DICA</h4>

                    <p>1 - Fa�a a compra em nosso site</p>
                    <p>2 - Receba a suas cervejas</p>
                    <p>3 - Poste no seu Instagram fotos de cada cerveja com os seus amigos</p>
                    <p>4 - Nos envie o link do seu Post no Instagram</p>
                    <p>5 - Receba o estorno parcial em seu cart�o de cr�dito</p>

                    <div class="fb-share-button"
                         data-href="https://incubadoradacerveja.com.br/index.php"
                         data-layout="button_count" data-size="small" data-mobile-iframe="true">
                            <a target="_blank"
                               href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fincubadoradacerveja.com.br%2Findex.php;src=sdkpreparse"
                               class="fb-xfbml-parse-ignore">
                                Compartilhar
                            </a>
                    </div>
                    -->

                </div>
            </div>

            <!-- INICIO - Janelas Modais -->

            <!-- Regulamento -->
            <div class="modal fade" id="modal-regulamento" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                <div class="modal-dialog light" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Regulamento da Promo��o - Saint Stello</h4>
                        </div>
                        <div class="modal-body">

                            <p><b>Vejas as regras para ter os 50% do valor das cervejas adquiridas reembolsado:</b></p>
                            <p class="margin-bottom-10">&bull;&nbsp;Fa�a o pedido em nosso site</p>
                            <p class="margin-bottom-10">&bull;&nbsp;Receba as cervejas adquiridas em sua casa (v�lido para a cidade de Belo Horizonte, nas regi�es atendidas pelo servi�o de entrega Rappi)</p>
                            <p class="margin-bottom-10">&bull;&nbsp;Poste no seu Instagram fotos de cada uma das cervejas com os seus amigos, marcando a Incubadora da Cerveja, a Cervejaria Saint Stello e mais 2 amigos</p>
                            <p class="margin-bottom-10">&bull;&nbsp;Nos envie o link do Post no Instagram</p>
                            <p class="margin-bottom-10">&bull;&nbsp;Faremos a an�lise em at� 2 dias �teis e transmitiremos a ordem de reembolso � operadora de cart�o de cr�dito</p>
                            <p class="margin-bottom-10">&bull;&nbsp;O valor ser� creditado em sua fatura do cart�o de cr�dito utilizado para realizar o pedido</p>

                        </div>
                    </div>
                </div>
            </div>

            <!-- American Lager-->
            <div class="modal fade" id="modal-american-lager" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                <div class="modal-dialog light" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">American Lager</h4>
                        </div>
                        <div class="modal-body">

                            <img src="imgs/especifico/saint-stello/americagr.png" alt="Cerveja" class="imagem-garrafa">

                            <p>Leve, de teor alco�lico moderado e colora��o dourado claro, � uma cerveja refrescante com sabor do puro malte Pilsen e l�pulo americano Cascade.
                                Excelente para dias quentes e harmoniza com churrasco, burgers e petiscos.</p><br>

                            <img src="imgs/especifico/saint-stello/harmonizacao/american.png" alt="Cerveja" class="imagem-harmonizacao">

                            <button class="center btn btn-default botao-adicionar-ao-pedido" ng-click="vm.adicionarProdutoAoPedido('americanLager', 'modal-american-lager')">Adicionar ao Pedido</button>

                        </div>
                    </div>
                </div>
            </div>

            <!-- Session IPA-->
            <div class="modal fade" id="modal-session-ipa" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                <div class="modal-dialog light" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Session IPA</h4>
                        </div>
                        <div class="modal-body">

                            <img src="imgs/especifico/saint-stello/ipagr.png" alt="Cerveja" class="imagem-garrafa">

                            <p>Arom�tica, refrescante e com amargor marcante, caracter�sticas do estilo IPA. � elaborada com quatro tipos diferentes de l�pulos.
                                Sendo uma session, seu teor alco�lico � moderado. Harmoniza com burgers, frutos do mar e churrasco.</p><br>

                            <img src="imgs/especifico/saint-stello/harmonizacao/ipa.png" alt="Cerveja" class="imagem-harmonizacao">

                            <button class="center btn btn-default botao-adicionar-ao-pedido" ng-click="vm.adicionarProdutoAoPedido('sessionIpa', 'modal-session-ipa')">Adicionar ao Pedido</button>

                        </div>
                    </div>
                </div>
            </div>

            <!-- Witbier-->
            <div class="modal fade" id="modal-witbier" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                <div class="modal-dialog light" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Witbier</h4>
                        </div>
                        <div class="modal-body">

                            <img src="imgs/especifico/saint-stello/witgr.png" alt="Cerveja" class="imagem-garrafa">

                            <p>Estilo Belga, � uma cerveja de trigo condimentada com sementes de coentro e cascas de laranja.
                                C�trica e arom�tica, � leve e de teor alco�lico baixo, com colora��o amarelo- palha.
                                Vai muito bem com petiscos como frutos do mar, saladas e peixes.</p><br>

                            <img src="imgs/especifico/saint-stello/harmonizacao/witbier.png" alt="Cerveja" class="imagem-harmonizacao">

                            <button class="center btn btn-default botao-adicionar-ao-pedido" ng-click="vm.adicionarProdutoAoPedido('witbier', 'modal-witbier')">Adicionar ao Pedido</button>

                        </div>
                    </div>
                </div>
            </div>

            <!-- Bohemian Pilsner-->
            <div class="modal fade" id="modal-bohemian-pilsner" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                <div class="modal-dialog light" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Bohemian Pilsner</h4>
                        </div>
                        <div class="modal-body">

                            <img src="imgs/especifico/saint-stello/bohemiagr.png" alt="Cerveja" class="imagem-garrafa">

                            <p>Elaborada com l�pulo Saaz, proveniente da Rep�blica Tcheca, tem sabor com fortes caracter�sticas maltadas e amargor pronunciado.
                                Com colora��o dourado profundo, corpo e carbonata��o m�dios. Combina com grelhados, assados e ensopados.</p><br>

                            <img src="imgs/especifico/saint-stello/harmonizacao/bohemian.png" alt="Cerveja" class="imagem-harmonizacao">

                            <button class="center btn btn-default botao-adicionar-ao-pedido" ng-click="vm.adicionarProdutoAoPedido('bohemianPilsner', 'modal-bohemian-pilsner')">Adicionar ao Pedido</button>

                        </div>
                    </div>
                </div>
            </div>

            <!-- Weiss-->
            <div class="modal fade" id="modal-weiss" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                <div class="modal-dialog light" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Weiss</h4>
                        </div>
                        <div class="modal-body">

                            <img src="imgs/especifico/saint-stello/logo.png" alt="Cerveja" class="imagem-garrafa">

                            <p>As famosas cervejas de trigo, chamadas de Weizenbier, ou somente Weiss, s�o cervejas claras e opacas, t�picas do sul da Alemanha.
                                Geralmente no sabor se sobressai o trigo com o qual foram produzidas.</p><br>

                            <img src="imgs/especifico/saint-stello/harmonizacao/bohemian.png" alt="Cerveja" class="imagem-harmonizacao">

                            <button class="center btn btn-default botao-adicionar-ao-pedido" ng-click="vm.adicionarProdutoAoPedido('weiss', 'modal-weiss')">Adicionar ao Pedido</button>

                        </div>
                    </div>
                </div>
            </div>

            <!-- Stout-->
            <div class="modal fade" id="modal-stout" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                <div class="modal-dialog light" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Stout</h4>
                        </div>
                        <div class="modal-body">

                            <img src="imgs/especifico/saint-stello/logo.png" alt="Cerveja" class="imagem-garrafa">

                            <p>As American Stouts apresentam amargor pronunciado e aromas de variedades americanas de l�pulos, geralmente c�tricos,
                                complementando os aromas de torrefa��o caracter�sticos do estilo Stout. </p><br>

                            <img src="imgs/especifico/saint-stello/harmonizacao/bohemian.png" alt="Cerveja" class="imagem-harmonizacao">

                            <button class="center btn btn-default botao-adicionar-ao-pedido" ng-click="vm.adicionarProdutoAoPedido('stout', 'modal-stout')">Adicionar ao Pedido</button>

                        </div>
                    </div>
                </div>
            </div>


            <!-- FIM - Janelas Modais -->

            <button type="button"
                    id="trigger-modal-regulamento"
                    class="display-none btn btn-primary"
                    data-toggle="modal"
                    data-target="#modal-regulamento"></button>

            <button type="button"
                    id="trigger-modal-american-lager"
                    class="display-none btn btn-primary"
                    data-toggle="modal"
                    data-target="#modal-american-lager"></button>

            <button type="button"
                    id="trigger-modal-session-ipa"
                    class="display-none btn btn-primary"
                    data-toggle="modal"
                    data-target="#modal-session-ipa"></button>

            <button type="button"
                    id="trigger-modal-witbier"
                    class="display-none btn btn-primary"
                    data-toggle="modal"
                    data-target="#modal-witbier"></button>

            <button type="button"
                    id="trigger-modal-bohemian-pilsner"
                    class="display-none btn btn-primary"
                    data-toggle="modal"
                    data-target="#modal-bohemian-pilsner"></button>

            <button type="button"
                    id="trigger-modal-weiss"
                    class="display-none btn btn-primary"
                    data-toggle="modal"
                    data-target="#modal-weiss"></button>

            <button type="button"
                    id="trigger-modal-stout"
                    class="display-none btn btn-primary"
                    data-toggle="modal"
                    data-target="#modal-stout"></button>


            <div id="lista-produtos-disponiveis-compra" class="col-xs-12 col-md-12 matchHeight padding-bottom-40">
                <div class="row">

                    <div class="container-fluid dark section no-padding margin-top-40 margin-bottom-20">
                        <div class="row">
                            <div class="col-sm-12">
                                <h2 class="destaque" style="text-align: center;">Fa�a seu pedido!</h2>
                                <p class="center-text">Acesse os r�tulos abaixo para saber mais sobre cada cerveja e adicion�-las ao seu pedido.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-4 icon-grid">
                        <div class="product">
                            <a href="javascript:void(0);" onclick="$('#trigger-modal-american-lager').click()">
                                <span>Ver mais</span>
                                <img class="garrafa-grid-pedido-simples" src="imgs/especifico/saint-stello/americagr.png">
                            </a>
                            <h3 class="destaque">American Lager</h3>
                            <h3>R$ 18,00</h3>
                            <p>Garrafa de 500 ml</p>

                        </div>
                    </div>

                    <!--<div class="col-xs-12 col-md-4 icon-grid">
                        <div class="product">
                            <a href="javascript:void(0);" onclick="$('#trigger-modal-session-ipa').click()">
                                <span>Ver mais</span>
                                <img class="garrafa-grid-pedido-simples" src="imgs/especifico/saint-stello/ipagr.png">
                            </a>
                            <h3 class="destaque">Session IPA</h3>
                            <h3>R$ 18,00</h3>
                            <p>Garrafa de 500 ml</p>

                        </div>
                    </div>-->

                    <div class="col-xs-12 col-md-4 icon-grid">
                        <div class="product">
                            <a href="javascript:void(0);" onclick="$('#trigger-modal-witbier').click()">
                                <span>Ver mais</span>
                                <img class="garrafa-grid-pedido-simples" src="imgs/especifico/saint-stello/witgr.png">
                            </a>
                            <h3 class="destaque">Witbier</h3>
                            <h3>R$ 18,00</h3>
                            <p>Garrafa de 500 ml</p>

                        </div>
                    </div>

                    <div class="col-xs-12 col-md-4 icon-grid no-border-right">
                        <div class="product">
                            <a href="javascript:void(0);" onclick="$('#trigger-modal-bohemian-pilsner').click()">
                                <span>Ver mais</span>
                                <img class="garrafa-grid-pedido-simples" src="imgs/especifico/saint-stello/bohemiagr.png">
                            </a>
                            <h3 class="destaque">Bohemian Pilsner</h3>
                            <h3>R$ 18,00</h3>
                            <p>Garrafa de 500 ml</p>

                        </div>
                    </div>

                    <div class="col-md-push-2 col-xs-12 col-md-4 icon-grid no-border-bottom">
                        <div class="product">
                            <a href="javascript:void(0);" onclick="$('#trigger-modal-weiss').click()">
                                <span>Ver mais</span>
                                <img class="garrafa-grid-pedido-simples" src="imgs/especifico/saint-stello/witgr.png">
                            </a>
                            <h3 class="destaque">Weiss</h3>
                            <h3>R$ 18,00</h3>
                            <p>Garrafa de 500 ml</p>

                        </div>
                    </div>

                    <div class="col-md-push-2 col-xs-12 col-md-4 icon-grid no-border-bottom no-border-right">
                        <div class="product">
                            <a href="javascript:void(0);" onclick="$('#trigger-modal-stout').click()">
                                <span>Ver mais</span>
                                <img class="garrafa-grid-pedido-simples" src="imgs/especifico/saint-stello/witgr.png">
                            </a>
                            <h3 class="destaque">Stout</h3>
                            <h3>R$ 18,00</h3>
                            <p>Garrafa de 500 ml</p>

                        </div>
                    </div>

                </div>
            </div>

            <div class="col-xs-12 col-md-12 matchHeight border-top padding-top-20"
                 ng-if="!GeneralUtil.isNullOrEmptyObject(vm.itensPedido)">

                <div class="row">

                    <div class="container-fluid dark section no-padding margin-top-20 margin-bottom-20">

                        <div class="row">
                            <div class="col-sm-12">
                                <h2 class="destaque" style="text-align: center;">Revise a sua compra</h2>
                                <p class="center-text">Revise o seu pedido para prosseguir com os dados de entrega.</p>
                            </div>
                        </div>

                        <div class="row" id="pedido-simples-lista-produtos">
                            <div class="col-sm-12 pedido-simples-lista-produtos">

                                <div class="scrollable-area dataTables_wrapper padding-top-20">
                                    <table class="table table-bordered table-striped omega-list-table">
                                        <thead>
                                        <tr>
                                            <th class="omega-list-record-th">
                                                Produto
                                            </th>

                                            <th class="omega-list-record-th">
                                                Tamanho
                                            </th>

                                            <th class="omega-list-record-th">
                                                Pre�o Unit�rio
                                            </th>

                                            <th class="omega-list-record-th">
                                                Quantidade
                                            </th>

                                            <th class="omega-list-record-th">
                                                Pre�o Total
                                            </th>

                                            <th class="omega-list-record-th">
                                                &nbsp;
                                            </th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="(chave, valor) in vm.itensPedido">
                                                <td class="omega-list-record-content-td">
                                                    <a href="javascript:void(0);" ng-click="vm.verDetalhesProdutoCarrinho(vm.itensPedido[chave])">{{vm.itensPedido[chave].descricao}}</a>
                                                </td>
                                                <td class="omega-list-record-content-td">
                                                    {{vm.itensPedido[chave].embalagem}}
                                                </td>
                                                <td class="omega-list-record-content-td">
                                                    {{NumberUtil.getCurrencyFormattedString(vm.itensPedido[chave].preco)}}
                                                </td>
                                                <td class="omega-list-record-content-td">
                                                    {{vm.itensPedido[chave].quantidade}}
                                                </td>
                                                <td class="omega-list-record-content-td">
                                                    {{NumberUtil.getCurrencyFormattedString(vm.itensPedido[chave].preco*vm.itensPedido[chave].quantidade)}}
                                                </td>
                                                <td class="omega-list-record-content-td center-text">
                                                    <img class="remove-icon" src="imgs/padrao/remove-icon.png" ng-click="vm.removerProdutoDoPedido(chave)" />
                                                </td>
                                            </tr>
                                            <tr class="tr-frete-pedido">
                                                <td class="omega-list-record-content-td" colspan="4">
                                                    Frete (Entrega a pre�o fixo em Belo Horizonte)
                                                </td>
                                                <td class="omega-list-record-content-td">
                                                    {{NumberUtil.getCurrencyFormattedString(vm.getValorFretePedido())}}
                                                </td>
                                                <td class="omega-list-record-content-td center-text">
                                                    &nbsp;
                                                </td>
                                            </tr>

                                            <tr class="tr-valor-total-pedido">
                                                <td class="omega-list-record-content-td" colspan="4">
                                                    Valor Total do Pedido
                                                </td>
                                                <td class="omega-list-record-content-td">
                                                    {{NumberUtil.getCurrencyFormattedString(vm.getValorTotalPedido())}}
                                                </td>
                                                <td class="omega-list-record-content-td center-text">
                                                    &nbsp;
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>

                        <div class='row'>

                            <div class="col-sm-12 center-text">

                                <button class="center btn btn-default botao-adicionar-ao-pedido" ng-click="vm.irParaListaDeProdutos()">Adicionar mais Produtos ao Pedido</button>

                                <button ng-if="!vm.secaoEnderecoVisivel" class="center btn btn-default botao-adicionar-ao-pedido" ng-click="vm.exibirSecaoEndereco()">Informar Dados para Entrega</button>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div ng-if="vm.secaoEnderecoVisivel && !GeneralUtil.isNullOrEmptyObject(vm.itensPedido)"
                 id="pedido-simples-secao-endereco"
                 class="col-xs-12 col-md-12 matchHeight border-top padding-top-20">

                <div class="row">

                    <div class="container-fluid dark section no-padding margin-bottom-20">

                        <!-- INICIO: DADOS PESSOAIS DO COMPRADOR E ENDERE�O DE ENTREGA -->
                        <div class='row padding-top-20' >

                            <div class="col-sm-12">
                                <h2 class="destaque" style="text-align: center;">Informe seus dados e o endere�o de entrega</h2>
                                <p class="center-text margin-bottom-5">As entregas ser�o feitas pelo nosso servi�o parceiro de delivery - Rappi <a href="https://www.rappi.com.br/" target="_blank">(saiba mais)</a></p>
                                <p class="center-text margin-bottom-20">Informe abaixo o endere�o de entrega para verificar se est� dispon�vel na �rea de cobertura do servi�o</p>
                            </div>

                            <form name="mainForm"
                                  class="col-sm-12 form form-horizontal"
                                  style="margin-bottom: 0;">

                                <div class='col-sm-12'>

                                    <div class='box'>
                                        <div class='box-content'>

                                            <!--Dados Pessoais -->
                                            <div class='form-group'
                                                 ng-class="getFormControlCssClass(mainForm.fieldNomeCompleto)">

                                                <label class='col-md-3 control-label' for='fieldNome'>Nome Completo</label>

                                                <div class='col-md-6'>

                                                    <input type="text"
                                                           ng-model="vm.formDataDadosCliente.nomeCompleto"
                                                           class="form-control"
                                                           ng-disabled="vm.disabledFields.fieldNomeCompleto"
                                                           ng-required="vm.requiredFields.fieldNomeCompleto"
                                                           name="fieldNomeCompleto"
                                                           id="fieldNomeCompleto"
                                                           placeholder=""
                                                           omega-control-type="TextInput"
                                                           maxlength="255"/>

                                                    <span class="help-block"
                                                          ng-show="showValidationMessage('fieldNomeCompleto', mainForm , {maxlength: 'O tamanho m�ximo do nome fantasia deve ser de 100 caracteres.', required: 'Campo obrigat�rio.'})">
                                                        <p ng-repeat="messages in getValidationMessages('fieldNomeCompleto', mainForm)">
                                                            {{messages}}
                                                        </p>
                                                    </span>

                                                </div>
                                            </div>

                                            <div class='form-group'
                                                 ng-class="getFormControlCssClass(mainForm.fieldCpf)">

                                                <label class='col-md-3 control-label' for='fieldCpf'>CPF</label>

                                                <div class='col-md-6'>

                                                    <input class='form-control'
                                                           ng-disabled="vm.disabledFields.fieldCpf"
                                                           ng-required="vm.requiredFields.fieldCpf"
                                                           placeholder=''
                                                           ui-br-cpf-mask
                                                           name="fieldCpf"
                                                           ng-model="vm.formDataDadosCliente.cpf"
                                                           type='text'>

                                                    <span class="help-block"
                                                          ng-show="showValidationMessage('fieldCpf', mainForm , {cpf: 'CPF inv�lido', required: 'Campo obrigat�rio.'})">
                                                        <p ng-repeat="messages in getValidationMessages('fieldCpf', mainForm)">
                                                            {{messages}}
                                                        </p>
                                                    </span>

                                                </div>
                                            </div>

                                            <div class='form-group'
                                                 ng-class="getFormControlCssClass(mainForm.fieldEmail)">

                                                <label class='col-md-3 control-label' for='fieldEmail'>E-mail de Contato</label>

                                                <div class='col-md-6'>

                                                    <input class='form-control'
                                                           ng-disabled="vm.disabledFields.fieldEmail"
                                                           ng-required="vm.requiredFields.fieldEmail"
                                                           placeholder=''
                                                           ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/"
                                                           name="fieldEmail"
                                                           ng-model="vm.formDataDadosCliente.email"
                                                           type='text'>

                                                    <span class='help-block'
                                                          ng-show="showValidationMessage('fieldEmail', mainForm, { 'pattern': 'O e-mail � inv�lido', required: 'Campo obrigat�rio'})">
                                                        <p ng-repeat="messages in getValidationMessages('fieldEmail', mainForm)">
                                                            {{messages}}
                                                        </p>
                                                    </span>

                                                </div>
                                            </div>

                                            <div class='form-group'
                                                 ng-class="getFormControlCssClass(mainForm.fieldTelefone)">

                                                <label class='col-md-3 control-label' for='fieldTelefone'>Telefone de Contato</label>

                                                <div class='col-md-6'>

                                                    <input class='form-control'
                                                           ng-disabled="vm.disabledFields.fieldTelefone"
                                                           ng-required="vm.requiredFields.fieldTelefone"
                                                           placeholder=''
                                                           ui-br-phone-number
                                                           name="fieldTelefone"
                                                           ng-model="vm.formDataDadosCliente.telefone"
                                                           type='text'>

                                                    <span class='help-block'
                                                          ng-show="showValidationMessage('fieldTelefone', mainForm, { 'br-phone-number': 'O telefone � inv�lido', required: 'Campo obrigat�rio'})">
                                                        <p ng-repeat="messages in getValidationMessages('fieldTelefone', mainForm)">
                                                            {{messages}}
                                                        </p>
                                                    </span>

                                                </div>

                                            </div>

                                            <div class='form-group'
                                                 ng-class="getFormControlCssClass(mainForm.fieldInstagram)">

                                                <label class='col-md-3 control-label' for='fieldInstagram'>Instagram</label>

                                                <div class='col-md-6'>

                                                    <input type="text"
                                                           ng-model="vm.formDataDadosCliente.instagram"
                                                           class="form-control"
                                                           ng-disabled="vm.disabledFields.fieldInstagram"
                                                           ng-required="vm.requiredFields.fieldInstagram"
                                                           name="fieldInstagram"
                                                           id="fieldInstagram"
                                                           placeholder=""
                                                           omega-control-type="TextInput"
                                                           maxlength="100"/>

                                                    <span class="help-block"
                                                          ng-show="showValidationMessage('fieldInstagram', mainForm , {maxlength: 'O tamanho m�ximo do Instragram deve ser de 100 caracteres.', required: 'O Instagram � de preenchimento obrigat�rio.'})">
                                                        <p ng-repeat="messages in getValidationMessages('fieldInstagram', mainForm)">
                                                            {{messages}}
                                                        </p>
                                                    </span>

                                                </div>
                                            </div>

                                            <!--Endere�o de Entrega -->
                                            <div class='form-group'
                                                 ng-class="vm.showMensagemEnderecoInvalido ? 'has-error' : ''">

                                                <label class='col-md-3 control-label' for='fieldEnderecoAutocomplete'>Endere�o de Entrega</label>

                                                <div class='col-md-6'>

                                                    <input class='form-control'
                                                           ng-disabled="vm.disabledFields.fieldEnderecoAutocomplete"

                                                           placeholder=''
                                                           name="fieldEnderecoAutocomplete"
                                                           id="fieldEnderecoAutocomplete"
                                                           type='text'>

                                                    <span class='help-block' ng-if="vm.showMensagemEnderecoInvalido">
                                                        <p>Endere�o inv�lido ou n�o dispon�vel para entrega</p>
                                                    </span>

                                                </div>
                                            </div>

                                            <div class='form-group'
                                                 ng-if="vm.isEnderecoGooglePlacesSelecionado"
                                                 ng-class="getFormControlCssClass(mainForm.fieldEnderecoLogradouro)">

                                                <label class='col-md-3 control-label' for='fieldEnderecoLogradouro'>Logradouro</label>

                                                <div class='col-md-6'>

                                                    <input class='form-control'
                                                           ng-disabled="vm.disabledFields.fieldEnderecoLogradouro"
                                                           placeholder=""
                                                           name="fieldEnderecoLogradouro"
                                                           ng-model="vm.formDataEndereco.logradouro"
                                                           maxlength="255"
                                                           type='text'>

                                                    <span class='help-block'
                                                          ng-show="showValidationMessage('fieldLogradouro', mainForm, { maxlength: 'O tamanho m�ximo do logradouro deve ser de 255 caracteres.', required: 'Campo Obrigat�rio'})">
                                                        <p ng-repeat="messages in getValidationMessages('fieldLogradouro', mainForm)">
                                                            {{messages}}
                                                        </p>
                                                    </span>

                                                </div>

                                            </div>

                                            <div class='form-group'
                                                 ng-if="vm.isEnderecoGooglePlacesSelecionado"
                                                 ng-class="getFormControlCssClass(mainForm.fieldEnderecoNumero)">

                                                <label class='col-md-3 control-label' for='fieldEnderecoLogradouro'>N�mero</label>

                                                <div class='col-md-6'>

                                                    <input type="text"
                                                           ng-model="vm.formDataEndereco.numero"
                                                           class="form-control"
                                                           ng-disabled="vm.disabledFields.fieldEnderecoNumero"

                                                           name="fieldEnderecoNumero"
                                                           id="fieldEnderecoNumero"
                                                           placeholder=""
                                                           omega-control-type="TextInput"
                                                           maxlength="20"/>

                                                    <span class="help-block"
                                                          ng-show="showValidationMessage('fieldEnderecoNumero', mainForm , {maxlength: 'O tamanho m�ximo do n�mero deve ser de 20 caracteres.', required: 'O N�mero � de preenchimento obrigat�rio.'})">
                                                        <p ng-repeat="messages in getValidationMessages('fieldEnderecoNumero', mainForm)">
                                                            {{messages}}
                                                        </p>
                                                    </span>

                                                </div>

                                            </div>

                                            <div class='form-group'
                                                 ng-if="vm.isEnderecoGooglePlacesSelecionado"
                                                 ng-class="getFormControlCssClass(mainForm.fieldEnderecoComplemento)">

                                                <label class='col-md-3 control-label' for='fieldEnderecoComplemento'>Complemento</label>

                                                <div class='col-md-6'>

                                                    <input type="text"
                                                           ng-model="vm.formDataEndereco.complemento"
                                                           class="form-control"
                                                           ng-disabled="vm.disabledFields.fieldEnderecoComplemento"

                                                           name="fieldEnderecoComplemento"
                                                           id="fieldEnderecoComplemento"
                                                           placeholder=""
                                                           omega-control-type="TextInput"
                                                           maxlength="20"/>

                                                    <span class="help-block"
                                                          ng-show="showValidationMessage('fieldEnderecoComplemento', mainForm , {maxlength: 'O tamanho m�ximo do complemento deve ser de 20 caracteres.', required: 'O N�mero � de preenchimento obrigat�rio.'})">
                                                        <p ng-repeat="messages in getValidationMessages('fieldEnderecoComplemento', mainForm)">
                                                            {{messages}}
                                                        </p>
                                                    </span>

                                                </div>

                                            </div>

                                            <div class='form-group'
                                                 ng-if="vm.isEnderecoGooglePlacesSelecionado"
                                                 ng-class="getFormControlCssClass(mainForm.fieldEnderecoBairro)">

                                                <label class='col-md-3 control-label' for='fieldEnderecoBairro'>Bairro</label>

                                                <div class='col-md-6'>

                                                    <input type="text"
                                                           ng-model="vm.formDataEndereco.bairro"
                                                           class="form-control"
                                                           ng-disabled="vm.disabledFields.fieldEnderecoBairro"

                                                           name="fieldEnderecoBairro"
                                                           id="fieldEnderecoBairro"
                                                           placeholder=""
                                                           omega-control-type="TextInput"
                                                           maxlength="100"/>

                                                    <span class="help-block"
                                                          ng-show="showValidationMessage('fieldEnderecoBairro', mainForm , {maxlength: 'O tamanho m�ximo do bairro deve ser de 100 caracteres.', required: 'O N�mero � de preenchimento obrigat�rio.'})">
                                                        <p ng-repeat="messages in getValidationMessages('fieldEnderecoBairro', mainForm)">
                                                            {{messages}}
                                                        </p>
                                                    </span>

                                                </div>

                                            </div>

                                            <div class='form-group'
                                                 ng-if="vm.isEnderecoGooglePlacesSelecionado"
                                                 ng-class="getFormControlCssClass(mainForm.fieldEnderecoMunicipio)">

                                                <label class='col-md-3 control-label' for='fieldEnderecoMunicipio'>Munic�pio</label>

                                                <div class='col-md-6'>

                                                    <input type="text"
                                                           ng-model="vm.formDataEndereco.municipio"
                                                           class="form-control"
                                                           ng-disabled="vm.disabledFields.fieldEnderecoMunicipio"

                                                           name="fieldEnderecoMunicipio"
                                                           id="fieldEnderecoMunicipio"
                                                           placeholder=""
                                                           omega-control-type="TextInput"
                                                           maxlength="100"/>

                                                    <span class="help-block"
                                                          ng-show="showValidationMessage('fieldEnderecoMunicipio', mainForm , {maxlength: 'O tamanho m�ximo do munic�pio deve ser de 100 caracteres.', required: 'O N�mero � de preenchimento obrigat�rio.'})">
                                                        <p ng-repeat="messages in getValidationMessages('fieldEnderecoMunicipio', mainForm)">
                                                            {{messages}}
                                                        </p>
                                                    </span>

                                                </div>

                                            </div>

                                            <div class='form-group'
                                                 ng-if="vm.isEnderecoGooglePlacesSelecionado"
                                                 ng-class="getFormControlCssClass(mainForm.fieldEnderecoEstado)">

                                                <label class='col-md-3 control-label' for='fieldEnderecoEstado'>Estado</label>

                                                <div class='col-md-6'>

                                                    <input type="text"
                                                           ng-model="vm.formDataEndereco.estado"
                                                           class="form-control"
                                                           ng-disabled="vm.disabledFields.fieldEnderecoEstado"
                                                           name="fieldEnderecoEstado"
                                                           id="fieldEnderecoEstado"
                                                           placeholder=""
                                                           omega-control-type="TextInput"
                                                           maxlength="50"/>

                                                    <span class="help-block"
                                                          ng-show="showValidationMessage('fieldEnderecoEstado', mainForm , {maxlength: 'O tamanho m�ximo do estado deve ser de 50 caracteres.', required: 'O N�mero � de preenchimento obrigat�rio.'})">

                                                        <p ng-repeat="messages in getValidationMessages('fieldEnderecoEstado', mainForm)">
                                                            {{messages}}
                                                        </p>

                                                    </span>

                                                </div>

                                            </div>

                                            <div class='form-group'
                                                 ng-if="vm.isEnderecoGooglePlacesSelecionado"
                                                 ng-class="getFormControlCssClass(mainForm.fieldEnderecoCep)">

                                                <label class='col-md-3 control-label' for='fieldEnderecoCep'>CEP</label>

                                                <div class='col-md-6'>

                                                    <input type="text"
                                                           ng-model="vm.formDataEndereco.cep"
                                                           class="form-control"
                                                           ng-disabled="vm.disabledFields.fieldEnderecoCep"

                                                           name="fieldEnderecoCep"
                                                           id="fieldEnderecoCep"
                                                           placeholder=""
                                                           omega-control-type="TextInput"
                                                           maxlength="100"/>

                                                    <span class="help-block"
                                                          ng-show="showValidationMessage('fieldEnderecoCep', mainForm , {required: 'O CEP � de preenchimento obrigat�rio.'})">
                                                        <p ng-repeat="messages in getValidationMessages('fieldEnderecoCep', mainForm)">
                                                            {{messages}}
                                                        </p>
                                                    </span>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                                <div ng-if="vm.secaoEnderecoVisivel" class="col-xs-12 col-md-12 matchHeight border-top padding-top-20"
                                     ng-if="!GeneralUtil.isNullOrEmptyObject(vm.itensPedido)">

                                    <div class="row">

                                        <div class="container-fluid dark section no-padding margin-bottom-20">

                                            <!-- INICIO: ENDERE�O DE ENTREGA -->
                                            <div class='row padding-top-20' >

                                                <div class="col-sm-12">
                                                    <h2 class="destaque" style="text-align: center;">Confirme o seu pedido</h2>
                                                    <p class="center-text margin-bottom-20">Clique no bot�o abaixo para avan�ar a tela para confirmar os dados do pedido e realizar o pagamento.</p>
                                                </div>

                                                <div class="col-sm-12 center-text">

                                                    <button class="center btn btn-default botao-adicionar-ao-pedido" ng-click="vm.validarDadosAndFinalizarPedido(mainForm)">Finalizar Pedido</button>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </form>

                        </div>
                        <!-- FIM: DADOS PESSOAIS DO COMPRADOR E ENDERE�O DE ENTREGA  -->

                    </div>

                </div>
            </div>



        </div>

        <!--Defini��o do template do dialog exibido caso tenha problema no endereco -->
        <script type="text/ng-template" id="dialog-problema-endereco-informado.html">
            <div class="modal-header">
                <h4 class="modal-title">
                    <span class="glyphicon"></span>
                    <?= I18N::getExpression("Endere�o inv�lido"); ?>
                </h4>
            </div>
            <div class="modal-body">
                <span class="help-block"><?= I18N::getExpression("O endere�o informado n�o � v�lido ou est� fora da �rea de cobertura de entrega."); ?></span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" ng-click="closeMessageDialog()">
                    <?= I18N::getExpression("Fechar"); ?>
                </button>
            </div>
        </script>

    </div>
</div>

<div class="separador-main container">&nbsp;</div>

