<!-- Section -->
<div class="dark padding-40-0">
    <div class="container">
        <div class="row">

            <div class="col-xs-12 col-md-6 matchHeight scrollme " >
                <div class="">
                    <header>
                        <h1>Degustador</h1>
                        <h2>Todo apreciador de Cerveja Artesanal</h2>
                    </header>
                    <p>Organize aqui a cervejada do futebol com os amigos, da festan�a com a fam�lia e de todas as comemora��es regadas a cerveja boa, a qualquer hora. F�cil e pr�tico!</p>
                    <p>Adquira sua cerveja artesanal antes mesmo da fabrica��o. Qual a vantagem? � mais barato e permite a aprecia��o dos mais diversos tipos de cervejas artesanais.</p>
                    <p>Conhe�a as cervejas Ciganas. Fabricadas por autores muito apaixonados e dispostos a arriscar tudo ou nada para proporcionar lhe proporcionar uma cerveja artesanal de qualidade.</p>
                    <p>Compre aqui tamb�m das Cervejarias Tradicionais velhas de guerra. Elas n�o poderiam ficar de fora!</p>

                    <a style="white-space: normal; word-wrap: break-word;" href="index.php?tipo=paginas&page=cadastro.degustador" class="btn btn-default only-public-area"><span>Tenho mais de 18 anos e quero fazer parte!</span></a>

                    <div class="fb-share-button"
                         data-href="https://incubadoradacerveja.com.br/index.php?tipo=paginas&amp;pagina=cadastro.degustador"
                         data-layout="button_count" data-size="small" data-mobile-iframe="true"><a target="_blank"
                                                                                                   href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fincubadoradacerveja.com.br%2Findex.php%3Fpagina%3Dcadastro.cervejeiro&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">
                            Compartilhar</a>
                    </div>

                </div>
            </div>

            <div class="col-xs-12 col-md-6 matchHeight">
                <div class="row">
                    <div class="col-xs-12 col-md-6 icon-grid">
                        <img src="imgs/template/Keg.svg" class="svg"  />
                        <h4>Barril</h4>
                        <p>Cadastre sua festa e envie o link do convite para seus amigos. Para comprar a cerveja da festa voc� tem que indicar o custo m�dio por litro.</p>
                    </div>
                    <div class="col-xs-12 col-md-6 icon-grid">
                        <img src="imgs/template/Sun.svg" class="svg"  />
                        <h4>Aguarde</h4>
                        <p>Aguarde a confirma��o de seus convidados e a indica��o deles de quanta bebida ir�o consumir</p>
                    </div>
                    <div class="col-xs-12 col-md-6 icon-grid no-border-bottom">
                        <img src="imgs/template/3DSixPack.svg" class="svg"  />
                        <h4>Compras</h4>
                        <p>Com a lista de confirma��o, fa�a as compras</p>
                    </div>
                    <div class="col-xs-12 col-md-6 icon-grid no-border-bottom">
                        <img src="imgs/template/Tankard.svg" class="svg"  />
                        <h4>Divirta-se</h4>
                        <p style="word-wrap: break-word;">Compartilhe seu gosto por cerveja com seus amigos e a comunidade cervejeira. Publique as fotos da sua festa no Instagram, marcando #IncubadoraDaCerveja</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="separador-main container">&nbsp;</div>

