<div ng-app="omegaApp">
    <div ng-controller="acompanhamentoFestaDegustadorController as vm">

        <div class='row' id='content-wrapper'>
            <div class='col-sm-12 no-padding'>
                <form name="mainForm" class="col-sm-8 col-sm-push-2 form form-horizontal" style="margin-bottom: 0;">

                    <div class='row padding-20-0' ng-cloak>
                        <div class='col-sm-12'>

                            <div class="container-fluid dark section no-padding margin-bottom-20">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 icon-grid titulo-pagina">
                                        <img src="imgs/template/Chalice.svg" class="svg"/>
                                        <h4>Sua Festa</h4>
                                        <p>Gerenciamento do seu evento</p>
                                    </div>
                                </div>
                            </div>

                            <div class='box'>
                                <div class='box-content'>

                                    <div class="dark padding-20-0">
                                        <div class="omega-container">
                                            <div class="row">

                                                <header>
                                                    <h1>Status atual da festa:</h1>
                                                    <h2>{{vm.dadosBarril.barril.estadoBarril}}</h2>
                                                </header>

                                                <div class='col-sm-12 padding-bottom-20'
                                                     ng-hide="vm.dadosBarril.barril.idEstadoBarril == 5 ||
                                                            vm.dadosBarril.barril.idEstadoBarril == 6 ||
                                                            vm.dadosBarril.barril.idEstadoBarril == 7">

                                                    Link do convite:

                                                    <input class='form-control'
                                                           value="{{vm.getLinkConvite()}}"
                                                           type='text'
                                                           id="LinkConvite"
                                                           readonly
                                                    >

                                                </div>

                                                <div class='col-xs-12 col-sm-12 barra-botoes'>

                                                    <button class="col-xs-12 col-sm-3 btn-info matchHeight"
                                                            type="button"
                                                            ng-click="vm.copyLinkConvite()">

                                                        <i class="glyphicon glyphicon-send"></i> &nbsp;
                                                        Copiar Link
                                                    </button>

                                                    <button class="col-xs-12 col-sm-3 btn-success matchHeight"
                                                            type="button"
                                                            ng-click="vm.enviarConviteWhatsApp()"
                                                            ng-hide="vm.dadosBarril.barril.idEstadoBarril == 5 ||
                                                            vm.dadosBarril.barril.idEstadoBarril == 6 ||
                                                            vm.dadosBarril.barril.idEstadoBarril == 7">

                                                        <i class="glyphicon glyphicon-send"></i>
                                                        &nbsp;Enviar convite pelo WhatsApp

                                                    </button>

                                                    <button class="col-xs-12 col-sm-3 btn-warning"
                                                            type="button"
                                                            style="display: none;"
                                                            ng-click="vm.editarBarril()"
                                                            ng-hide="vm.dadosBarril.barril.idEstadoBarril == 5 ||
                                                            vm.dadosBarril.barril.idEstadoBarril == 6 ||
                                                            vm.dadosBarril.barril.idEstadoBarril == 7">

                                                        <i class="glyphicon glyphicon-edit"></i>
                                                        &nbsp;Editar barril

                                                    </button>

                                                    <button class="col-xs-12 col-sm-3 btn-info matchHeight"
                                                            type="button"
                                                            ng-click="vm.enviarConvite()"
                                                            ng-hide="vm.dadosBarril.barril.idEstadoBarril == 5 ||
                                                            vm.dadosBarril.barril.idEstadoBarril == 6 ||
                                                            vm.dadosBarril.barril.idEstadoBarril == 7">

                                                        <i class="glyphicon glyphicon-send"></i>
                                                        &nbsp;Enviar convite pelo Facebook

                                                    </button>

                                                    <button class="col-xs-12 col-sm-3 btn-grey matchHeight"
                                                            type="button"
                                                            ng-click="vm.cameraQrCode()">

                                                        <i class="glyphicon glyphicon-qrcode"></i>
                                                        &nbsp;Leitor de QR Code de Convites

                                                    </button>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="separador omega-container">&nbsp;</div>

                                    <div class="dark padding-20-0">
                                        <div class="omega-container">
                                            <div class="row">

                                                <header>
                                                    <h1>Dados da Festa</h1>
                                                    <h2>Informa��es gerais sobre a festa</h2>
                                                </header>

                                                <ul class="lista-dados-festa">
                                                    <li>
                                                        <label class='col-form-label'>Endere�o:</label>

                                                        <p>{{vm.dadosBarril.objJson.endereco.logradouro}},
                                                            {{vm.dadosBarril.objJson.endereco.numero}}
                                                            {{vm.dadosBarril.objJson.endereco.complemento}}
                                                        <p>Bairro {{vm.dadosBarril.objJson.endereco.bairro}}</p>
                                                        <p>
                                                            {{vm.dadosBarril.objJson.endereco.municipio}}/{{vm.dadosBarril.objJson.endereco.estado}}</p>

                                                    </li>
                                                    <li ng-if="!isNullOrEmpty(vm.dadosBarril.objJson.dataDaFesta)">
                                                        <label class='col-form-label'>Data da festa:</label>
                                                        <p>{{vm.dadosBarril.objJson.dataDaFesta}}</p>
                                                    </li>
                                                    <li ng-if="!isNullOrEmpty(vm.dadosBarril.objJson.dataConfirmacao)">
                                                        <label class='col-form-label'>Data da confirma��o:</label>
                                                        <p>{{vm.dadosBarril.objJson.dataConfirmacao}}</p>
                                                    </li>
                                                    <li>
                                                        <label class='col-form-label'>Convidados:</label>
                                                        <p>{{vm.dadosBarril.totalConvidados}} /
                                                            {{vm.dadosBarril.maxConvidados}}</p>
                                                    </li>
                                                </ul>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="separador omega-container">&nbsp;</div>

                                    <div class="dark padding-20-0">
                                        <div class="omega-container">
                                            <div class="row">

                                                <header>
                                                    <h1>Estoque de bebidas</h1>
                                                    <h2>Em litros</h2>
                                                </header>

                                                <ul class="lista-dados-festa">
                                                    <li ng-if="!isNullOrEmpty(vm.dadosBarril.totalLitros)">
                                                        <label class='col-form-label'>Total de cerveja arrecada at� o
                                                            momento:</label>
                                                        <p>{{vm.dadosBarril.totalLitros}}</p>
                                                    </li>
                                                    <li ng-if="!isNullOrEmpty(vm.dadosBarril.totalLitrosNaoVerificados)">
                                                        <label class='col-form-label'>Total de cerveja n�o
                                                            verificada:</label>
                                                        <p>{{vm.dadosBarril.totalLitrosNaoVerificados}}</p>
                                                    </li>
                                                    <li>
                                                        <label class='col-form-label'>Volume total de cerveja da
                                                            festa:</label>
                                                        <p>{{vm.dadosBarril.barril.volumeTotal}} litros</p>
                                                    </li>
                                                </ul>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="separador omega-container">&nbsp;</div>

                                    <div class="dark padding-20-0">
                                        <div class="omega-container">
                                            <div class="row">

                                                <header>
                                                    <h1>Dados Banc�rios</h1>
                                                    <h2>para dep�sitos dos convidados</h2>
                                                </header>

                                                <ul class="lista-dados-festa">
                                                    <li>
                                                        <label class='col-form-label'>Titular: </label>
                                                        <p>{{vm.dadosBarril.objJson.dadosBancarios.titular}}</p>
                                                    </li>
                                                    <li>

                                                        <label class='col-form-label'>CPF: </label>
                                                        <p>{{vm.dadosBarril.objJson.dadosBancarios.cpf}}</p>
                                                    </li>
                                                    <li>

                                                        <label class='col-form-label'>Agencia: </label>
                                                        <p>{{vm.dadosBarril.objJson.dadosBancarios.agencia}}</p>
                                                    </li>
                                                    <li>
                                                        <label class='col-form-label'>Tipo conta: </label>
                                                        <p>{{vm.dadosBarril.objJson.dadosBancarios.tipoConta}}</p>
                                                    </li>
                                                    <li>
                                                        <label class='col-form-label'>Conta: </label>
                                                        <p>
                                                            {{vm.dadosBarril.objJson.dadosBancarios.conta}}-{{vm.dadosBarril.objJson.dadosBancarios.digitoVerificadorConta}}</p>
                                                    </li>
                                                    <li ng-if="!isNullOrEmpty(vm.dadosBarril.objJson.dadosBancarios.operacao)">
                                                        <label class='col-form-label'>Operacao: </label>
                                                        <p>{{vm.dadosBarril.objJson.dadosBancarios.operacao}}</p>
                                                    </li>

                                                </ul>
                                                <small><b>Aten��o:</b> esse convite � do seu amigo para voc�, � de sua
                                                    responsabilidade essa transfer�ncia banc�ria. Esse convite n�o � uma
                                                    venda!
                                                    Se for, denuncie!
                                                </small>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="separador omega-container">&nbsp;</div>

                                    <div class="dark padding-20-0">
                                        <div class="omega-container">
                                            <div class="row">

                                                <header>
                                                    <h1>Convidados que ir�o beber</h1>
                                                    <h2>Amigos do facebook que aceitaram o convite e informaram que ir�o
                                                        consumir cerveja</h2>
                                                </header>

                                                <table class="table table-bordered table-striped omega-list-table">
                                                    <thead>
                                                    <tr>
                                                        <th>Nome</th>
                                                        <th>Total Pago (em litros)</th>
                                                        <th>Total Consumido (em litros)</th>
                                                        <th>Saldo (em litros)</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr ng-repeat="s in vm.dadosBarril.saldo.convidados">
                                                        <td>
                                                            {{s.nomeCervejeiro}}
                                                        </td>
                                                        <td>
                                                            {{s.pago}}
                                                        </td>
                                                        <td>
                                                            {{s.bebido}}
                                                        </td>
                                                        <td>
                                                            {{s.saldo}}
                                                        </td>
                                                    </tr>

                                                    <tr ng-if="isNullOrEmpty(vm.dadosBarril.saldo.convidados) || vm.dadosBarril.saldo.convidados.length == 0">
                                                        <td colspan="4">

                                                            <div class="alert alert-warning">
                                                                <h4>
                                                                    <i class="icon-warning-sign"></i>
                                                                    <?= I18N::getExpression("Aviso"); ?>
                                                                </h4>
                                                                <?= I18N::getExpression("Nenhum convidado aceitou o convite at� o momento."); ?>
                                                            </div>

                                                        </td>
                                                    </tr>

                                                    <tr class="totalizador-tabela">
                                                        <td>&nbsp;</td>
                                                        <td>Come�ou com:</td>
                                                        <td colspan="2">{{vm.dadosBarril.saldo.totalPago}} litro(s)</td>
                                                    </tr>
                                                    <tr class="totalizador-tabela">
                                                        <td>&nbsp;</td>
                                                        <td>J� foi bebido:</td>
                                                        <td colspan="2">{{vm.dadosBarril.saldo.totalBebido}} litro(s)
                                                        </td>
                                                    </tr>
                                                    <tr class="totalizador-tabela">
                                                        <td>&nbsp;</td>
                                                        <td>Ainda resta:</td>
                                                        <td colspan="2">{{vm.dadosBarril.saldo.totalSaldo}} litro(s)
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="separador omega-container">&nbsp;</div>

                                    <div class="dark padding-20-0" ng-hide="vm.dadosBarril.convidadosSemBeber.length == 0">
                                        <div class="omega-container">
                                            <div class="row">

                                                <header>
                                                    <h1>Convidados que n�o ir�o beber</h1>
                                                    <h2>Amigos do facebook que aceitaram o convite e informaram que n�o
                                                        ir�o consumir cerveja</h2>
                                                </header>

                                                <table class="table table-bordered table-striped omega-list-table">
                                                    <thead>
                                                    <tr>
                                                        <th>Data</th>
                                                        <th>Nome</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr ng-repeat="convidado in vm.dadosBarril.convidadosSemBeber">

                                                        <td>
                                                            {{convidado.data}}
                                                        </td>
                                                        <td>
                                                            {{convidado.nomeConvidado}}
                                                        </td>

                                                    </tr>

                                                    </tbody>
                                                </table>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="separador omega-container">&nbsp;</div>

                                    <div class="dark padding-20-0" ng-hide="vm.dadosBarril.convidadosNaoConfirmados.length == 0">
                                        <div class="omega-container">
                                            <div class="row">

                                                <header>
                                                    <h1>Convidados n�o confirmados</h1>
                                                    <h2>Amigos do facebook que voc� enviou o convite mas que ainda n�o confirmaram a presen�a</h2>
                                                </header>

                                                <table class="table table-bordered table-striped omega-list-table">
                                                    <thead>
                                                    <tr>
                                                        <th>Data que aceitou o convite</th>
                                                        <th>Nome</th>
                                                        <th>A��es</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr ng-repeat="convidado in vm.dadosBarril.convidadosNaoConfirmados">
                                                        <td>
                                                            {{convidado.data}}
                                                        </td>
                                                        <td>
                                                            {{convidado.nomeConvidado}}
                                                        </td>
                                                        <td>
                                                            <button class="btn-sm btn-danger"
                                                                    type="button"
                                                                    ng-hide="pedido.idEstadoPedido != 1"
                                                                    ng-click="vm.confirmaPedidoClick(false, pedido.idPedido)">

                                                                <i class="glyphicon glyphicon-remove-circle"></i> &nbsp;
                                                                N�o pagou

                                                            </button>

                                                            <button class="btn-sm btn-success"
                                                                    type="button"
                                                                    ng-hide="pedido.idEstadoPedido != 1"
                                                                    ng-click="vm.confirmaPedidoClick(true, pedido.idPedido)">

                                                                <i class="glyphicon glyphicon-remove-circle"></i>                                                                &nbsp;
                                                                Pagou

                                                            </button>

                                                            <button class="btn-sm btn-info"
                                                                    type="button"
                                                                    ng-click="vm.comentarPedidoClick( pedido.idPedido)">

                                                                <i class="glyphicon glyphicon-send"></i>
                                                                Enviar mensagem

                                                            </button>

                                                        </td>
                                                    </tr>

                                                    </tbody>
                                                </table>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="separador omega-container">&nbsp;</div>

                                    <div class="dark padding-20-0">
                                        <div class="omega-container">
                                            <div class="row">

                                                <header>
                                                    <h1>Vaquinha</h1>
                                                    <h2>Controle do rateio da despesa</h2>
                                                </header>

                                                <table class="table table-bordered table-striped omega-list-table"
                                                       style="word-break: break-all;">
                                                    <thead>
                                                    <tr>
                                                        <th>Data</th>
                                                        <th>Nome</th>
                                                        <th>Quantidade (litros)</th>
                                                        <th>Valor (R$)</th>
                                                        <th>J� pagou?</th>
                                                        <th>A��es</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr ng-repeat="pedido in vm.pedidos">
                                                        <td>
                                                            {{pedido.data}}
                                                        </td>
                                                        <td>
                                                            {{pedido.nomeConvidado}}
                                                        </td>

                                                        <td>
                                                            {{pedido.qtdLitros}}
                                                        </td>
                                                        <td>
                                                            {{pedido.valor}}
                                                        </td>
                                                        <td>
                                                            {{pedido.estadoPedido}}
                                                        </td>

                                                        <td class="box-content">
                                                            <button class="btn-sm btn-danger"
                                                                    type="button"
                                                                    ng-hide="pedido.idEstadoPedido != 1"
                                                                    ng-click="vm.confirmaPedidoClick(false, pedido)"
                                                                    style="margin-right: 5px;"
                                                            >N�o pagou
                                                            </button>

                                                            <button class="btn-sm btn-success"
                                                                    type="button"
                                                                    ng-hide="pedido.idEstadoPedido != 1"
                                                                    ng-click="vm.confirmaPedidoClick(true, pedido)"
                                                            >Pagou
                                                            </button>
                                                            <button class="btn-sm btn-info"
                                                                    type="button"
                                                                    ng-click="vm.comentarPedidoClick( pedido.idPedido)">

                                                                <i class="glyphicon glyphicon-send"></i>
                                                                Enviar mensagem

                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="6">

                                                            <div class="alert alert-warning">
                                                                <h4>
                                                                    <i class="icon-warning-sign"></i>
                                                                    <?= I18N::getExpression("Aviso"); ?>
                                                                </h4>
                                                                <?= I18N::getExpression("Nenhum convite aceito at� o momento."); ?>
                                                            </div>

                                                        </td>
                                                    </tr>
                                                    <tr class="totalizador-tabela">
                                                        <td>&nbsp;</td>
                                                        <td>Confirmado</td>
                                                        <td>{{vm.dadosBarril.totalLitrosConfirmados}} litros</td>
                                                        <td colspan="3">R$ {{vm.dadosBarril.valorConfirmado}}</td>
                                                    </tr>
                                                    <tr class="totalizador-tabela">
                                                        <td>&nbsp;</td>
                                                        <td>N�o Confirmado</td>
                                                        <td>{{vm.dadosBarril.totalLitrosNaoConfirmados}} litros</td>
                                                        <td colspan="3">R$ {{vm.dadosBarril.valorNaoConfirmado}}</td>
                                                    </tr>
                                                    </tbody>
                                                </table>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="separador omega-container">&nbsp;</div>

                                    <div class="dark padding-20-0">
                                        <div class="omega-container">
                                            <div class="row">

                                                <header>
                                                    <h1>O que j� foi bebido?</h1>
                                                    <h2>Controle o que j� foi bebido durante a festa</h2>
                                                </header>

                                                <table class="table table-bordered table-striped omega-list-table">
                                                    <thead>
                                                    <tr>
                                                        <th>Id</th>
                                                        <th>Data</th>
                                                        <th>Quantidade (litros)</th>
                                                        <th>Convidado</th>
                                                        <th>Status</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr ng-repeat="pd in vm.pedidosDebito">
                                                        <td>
                                                            {{pd.idPedidoDebito}}
                                                        </td>
                                                        <td>
                                                            {{pd.data}}
                                                        </td>
                                                        <td>
                                                            {{pd.qtdLitros}}
                                                        </td>
                                                        <td>
                                                            {{pd.nomeCervejeiro}}
                                                        </td>
                                                        <td>
                                                            {{pd.status}}
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td colspan="5">

                                                            <div class="alert alert-warning">
                                                                <h4>
                                                                    <i class="icon-warning-sign"></i>
                                                                    <?= I18N::getExpression("Aviso"); ?>
                                                                </h4>
                                                                <?= I18N::getExpression("N�o houve consumo at� o momento."); ?>
                                                            </div>

                                                        </td>
                                                    </tr>

                                                    <tr class="totalizador-tabela">
                                                        <td>&nbsp;</td>
                                                        <td>Total</td>
                                                        <td colspan="3">{{vm.dadosBarril.totalLitrosJaBebidos}} litro(s)</td>
                                                    </tr>

                                                    </tbody>
                                                </table>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="separador omega-container">&nbsp;</div>

                                    <div class="dark padding-20-0">
                                        <div class="omega-container">
                                            <div class="row">

                                            <button class="btn btn-sm btn-danger"
                                                    type="button"
                                                    ng-show="vm.dadosBarril.barril.idEstadoBarril < 4
                                                    || vm.dadosBarril.barril.idEstadoBarril == 9
                                                    || vm.dadosBarril.barril.idEstadoBarril == 14"
                                                    ng-click="vm.cancelarFestaClick()">
                                                <i class="glyphicon glyphicon-remove-circle"></i>
                                                &nbsp;Cancelar Festa
                                            </button>

                                            </div>

                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
