<!--Defini��o da View Angular JS -->
<div class='row' id='content-wrapper' ng-app="omegaApp">
    <div class='col-sm-12 no-padding' ng-controller="cadastroCervejeiroController as vm">

        <div class="hidden" ng-init="vm.formDataCervejeiro.tipoPessoa = '<?= $tipoCervejeiro ?>'"></div>

        <div class="container-fluid dark section no-padding margin-bottom-20">
            <div class="row">
                <div class="col-sm-12 icon-grid">
                    <img src="imgs/template/Hops.svg" class="svg"/>
                    <h4>Degustador</h4>
                    <p>Sou amante da cerveja e desejo organizar ou participar de uma festa privada para convidados!</p>
                </div>
            </div>
        </div>

        <form name="mainForm"
              class="col-sm-8 col-sm-push-2 form form-horizontal"
              style="margin-bottom: 0;">


            <div class='row'>
                <div class='col-sm-12'>

                    <header class="centred">
                        <h2>Endere�o</h2>
                        <h4>Endere�o da festa</h4>
                    </header>

                    <div class='box'>
                        <div class='box-content'>


                            <?php include_once("paginas/google.maps.autocomplete.address.php") ?>

                        </div>
                    </div>
                </div>
            </div>

            <div class='row'>
                <div class='col-sm-12'>

                    <header class="centred">
                        <h2>Dados Banc�rios</h2>
                        <h4>Para dep�sito dos convidados</h4>
                    </header>

                    <div class='box'>
                        <div class='box-content'>


                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldCpf)">

                                <label class='col-md-3 control-label' for='fieldCpf'>CPF</label>

                                <div class='col-md-6'>

                                    <input class='form-control'
                                           ng-disabled="vm.disabledFields.fieldCpf"
                                           ng-required="vm.requiredFields.fieldCpf"
                                           placeholder=''
                                           ui-br-cpf-mask
                                           name="fieldCpf"
                                           ng-model="vm.formDataCervejeiro.cpf"
                                           type='text'>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldCpf', mainForm , {cpf: 'CPF inv�lido', required: 'O CPF � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldCpf', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>
                            </div>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldDadosBancariosBanco)">

                                <label class='col-md-3 control-label' for='fieldDadosBancariosBanco'>Banco</label>

                                <div class='col-md-6'>

                                    <select class='form-control'
                                            ng-disabled="vm.disabledFields.fieldDadosBancariosBanco"
                                            ng-required="vm.requiredFields.fieldDadosBancariosBanco"
                                            placeholder=''
                                            name="fieldDadosBancariosBanco"
                                            ng-model="vm.formDataCervejeiro.dadosBancarios.banco"
                                            type='text'>

                                        <option ng-repeat="recordBancoFebraban in vm.bancosFebraban track by $index"
                                                value="{{recordBancoFebraban.Codigo}}">
                                            {{recordBancoFebraban.Nome}} (C�digo {{recordBancoFebraban.Codigo}})
                                        </option>

                                    </select>

                                    <span class='help-block'
                                          ng-show="showValidationMessage('fieldDadosBancariosBanco', mainForm, { required: 'Campo Obrigat�rio'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldDadosBancariosBanco', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>

                            </div>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldDadosBancariosTitular)">

                                <label class='col-md-3 control-label' for='fieldDadosBancariosTitular'>Titular da
                                    Conta</label>

                                <div class='col-md-6'>

                                    <input class='form-control'
                                           ng-disabled="disabledFields.fieldDadosBancariosTitular"
                                           ng-required="requiredFields.fieldDadosBancariosTitular"
                                           placeholder=''
                                           name="fieldDadosBancariosTitular"
                                           ng-model="vm.formDataCervejeiro.dadosBancarios.titular"
                                           maxlength="255"
                                           type='text'>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldDadosBancariosTitular', mainForm , { maxlength: 'O nome do titular deve ter no m�ximo 255 caracteres.', required: 'O titular da conta � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldDadosBancariosTitular', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>
                            </div>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldDadosBancariosAgencia)">

                                <label class='col-md-3 control-label' for='fieldDadosBancariosAgencia'>Ag�ncia</label>

                                <div class='col-md-6'>

                                    <input type="text"
                                           ng-model="vm.formDataCervejeiro.dadosBancarios.agencia"
                                           class="form-control"
                                           ng-disabled="vm.disabledFields.fieldDadosBancariosAgencia"
                                           ng-required="vm.requiredFields.fieldDadosBancariosAgencia"
                                           name="fieldDadosBancariosAgencia"
                                           id="fieldDadosBancariosAgencia"
                                           placeholder=""
                                           omega-control-type="TextInput"
                                           maxlength="20"/>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldDadosBancariosAgencia', mainForm , { required: 'A ag�ncia � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldDadosBancariosAgencia', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>

                            </div>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldDadosBancariosTipoConta)">

                                <label class='col-md-3 control-label' for='fieldDadosBancariosTipoConta'>Tipo de
                                    Conta</label>

                                <div class='col-md-6'>

                                    <select class='form-control'
                                            ng-disabled="vm.disabledFields.fieldDadosBancariosTipoConta"
                                            ng-required="vm.requiredFields.fieldDadosBancariosTipoConta"
                                            placeholder=''
                                            name="fieldDadosBancariosTipoConta"
                                            ng-model="vm.formDataCervejeiro.dadosBancarios.tipoConta"
                                            type='text'>

                                        <option ng-repeat="recordTipoContaBancaria in vm.tiposContasBancarias track by $index"
                                                value="{{recordTipoContaBancaria.chave}}">
                                            {{recordTipoContaBancaria.descricao}}
                                        </option>

                                    </select>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldDadosBancariosTipoConta', mainForm , { required: 'O tipo de conta � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldDadosBancariosTipoConta', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>
                            </div>


                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldDadosBancariosConta)">

                                <label class='col-md-3 control-label' for='fieldDadosBancariosConta'>N�mero da
                                    conta</label>

                                <div class='col-md-6'>

                                    <input type="text"
                                           ng-model="vm.formDataCervejeiro.dadosBancarios.conta"
                                           class="form-control"
                                           ng-disabled="vm.disabledFields.fieldDadosBancariosConta"
                                           ng-required="vm.requiredFields.fieldDadosBancariosConta"
                                           name="fieldDadosBancariosConta"
                                           id="fieldDadosBancariosConta"
                                           placeholder=""
                                           omega-control-type="TextInput"
                                           maxlength="20"/>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldDadosBancariosConta', mainForm , { required: 'O n�mero da conta � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldDadosBancariosConta', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class='form-actions form-actions-padding-sm btn-wrap'>
                <div class='row'>
                    <div class='col-md-10 col-md-offset-2'>
                        <button class='btn btn-default' type='submit' ng-click="vm.submeterCadastro(mainForm)">
                            <i class='icon-save'></i>
                            Prosseguir
                        </button>
                    </div>
                </div>
            </div>

        </form>

    </div>
</div>

<!--Defini��o do Controller Angular JS -->
<script type="text/javascript" src="angular/app/cadastro.barril.degustador.controller.js"></script>
