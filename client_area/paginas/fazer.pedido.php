<!--Defini��o da View Angular JS -->
<div class='row'
     id='content-wrapper'
     ng-app="omegaApp">

    <div class='col-sm-12 no-padding' ng-controller="cadastroCervejeiroController as vm">

        <div class="hidden" ng-init="vm.formDataCervejeiro.tipoPessoa = '<?= $tipoCervejeiro ?>'"></div>

        <div class="container-fluid dark section no-padding margin-bottom-20">
            <div class="row">
                <div class="col-sm-12 icon-grid">
                    <img src="imgs/template/Hops.svg" class="svg"/>
                    <h4>Tomar uma</h4>
                    <p>Vai beber quanto?</p>
                </div>
            </div>
        </div>

        <form name="mainForm"
              class="col-sm-8 col-sm-push-2 form form-horizontal"
              ng-cloak>

            <div class='row'>
                <div class='col-sm-12'>

                    <header class="centred">
                        <h2>Cerveja</h2>
                        <h4>Quantos ml?</h4>
                    </header>

                    <div class='box'>
                        <div class='box-content'>

                            <div class='form-group'>

                                <label class='col-md-3 control-label' for='fieldEmail'>Saldo atual</label>

                                <div class='col-md-6'>

                                    <span ng-bind="vm.qtdMlSaldoAtual"></span>
                                </div>
                            </div>
                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldQtdMlCerveja)">

                                <label class='col-md-3 control-label' for='fieldEmail'>Quantidade de cerveja</label>

                                <div class='col-md-6'>

                                    <input class='form-control'
                                           placeholder=''
                                           ng-model="vm.qtdMlEscolhida"
                                           type='number'>
                                </div>
                            </div>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldPrecoTotal)">

                                <label class='col-md-3 control-label' for='fieldPrecoTotal'>Pre�o total</label>

                                <div class='col-md-6'>

                                    <input class='form-control'
                                           placeholder=''
                                           ng-model="vm.precoTotal"
                                           type='number'>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class='form-actions form-actions-padding-sm btn-wrap'>
                <div class='row'>
                    <div class='col-md-10 col-md-offset-2'>
                        <button class='btn btn-default' type='submit' ng-click="vm.cadastrar()">
                            <i class='icon-save'></i>
                            Gerar QR Code
                        </button>
                    </div>
                </div>
            </div>

        </form>

    </div>
</div>

<!--Defini��o do Controller Angular JS -->
<script type="text/javascript" src="angular/app/fazer.pedido.controller.js"></script>

<div id="qrcode"></div>
<script type="text/javascript">
    var qrcode = null;
    function geraQrCode(texto)
    {
        if (qrcode == null)
        {
            qrcode = new QRCode(document.getElementById("qrcode"), {
                text: texto,
                width: 128,
                height: 128,
                colorDark: "#000000",
                colorLight: "#ffffff",
                correctLevel: QRCode.CorrectLevel.H
            });
        }
        else
        {
            qrcode.clear(); // clear the code.
            qrcode.makeCode(texto); // make another code.
        }
    }

</script>