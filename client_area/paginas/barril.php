<?php

    $convite = Helper::POSTGET("convite");
    $idDoBarril = null;
    if (strlen($convite))
    {
        $c = new Crypt();
        $get = $c->decryptGet($convite);
        if (isset($get) && isset($get["hash"]) && isset($get["hash"]["idDoBarril"]))
        {
            $idDoBarril = $get["hash"]["idDoBarril"];
        }
        Helper::imprimirComandoJavascript("var idDoBarril = $idDoBarril;", false);
    }

?>

<!--Defini��o da View Angular JS -->
<div class='row' id='content-wrapper' ng-app="omegaApp">
    <div ng-controller="barrilController as vm">

        <div class='row' id='content-wrapper'>
            <div class='col-sm-12 no-padding'>
                <form name="vm.formularioConviteDegustador" class="col-sm-8 col-sm-push-2 form form-horizontal" style="margin-bottom: 0;" novalidate>

                    <div class='row padding-20-0' ng-cloak>
                        <div class='col-sm-12'>

                            <div class="container-fluid dark section no-padding margin-bottom-20">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 icon-grid titulo-pagina">
                                        <img src="imgs/template/Chalice.svg" class="svg"/>
                                        <h4>Convite</h4>
                                        <p>Voc� foi convidado para a cervejada!</p>
                                    </div>
                                </div>
                            </div>

                            <div class='box'>
                                <div class='box-content'>

                                    <div class="dark padding-20-0">
                                        <div class="omega-container">
                                            <div class="row">

                                                <header>
                                                    <h1>Status atual da festa:</h1>
                                                    <h2>{{vm.getEstadoBarril()}}</h2>
                                                </header>

                                                <div class='col-sm-12 padding-bottom-20 container-imagem-convite'
                                                     ng-show="vm.gerarQrCode() != null">

                                                    <div class="col-sm-pull-4 col-sm-4 col-sm-push-4 conteudo-imagem-convite" id="capture-convite">
                                                        <h4>Meu Convite</h4>
                                                        <div id="qrcode-convite"></div>
                                                        <p>
                                                            <small>N�o compartilhe essa imagem</small>
                                                        </p>
                                                    </div>

                                                </div>

                                                <div class='col-sm-12 padding-bottom-20'
                                                     ng-show="vm.gerarQrCode() != null">

                                                    <div class="col-sm-pull-4 col-sm-4 col-sm-push-4 conteudo-imagem-convite">

                                                        <button class="col-xs-12 col-sm-12 btn-sm btn-success"
                                                                type="button"
                                                                ng-click="vm.downloadQrCode()"
                                                                ng-hide="vm.getChaveBarril() == null"
                                                        >
                                                            <i class="glyphicon glyphicon-download"></i>
                                                            Download do Convite
                                                        </button>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="separador omega-container">&nbsp;</div>

                                    <div class="dark padding-20-0">
                                        <div class="omega-container">
                                            <div class="row">

                                                <header>
                                                    <h1>Novo pedido</h1>
                                                    <h2 ng-hide="vm.barrilEstaCheio()">Adiquira mais cerveja da festa</h2>
                                                    <h2 ng-show="vm.barrilEstaCheio()"><?php echo I18N::getExpression("O barril encheu!"); ?></h2>
                                                </header>

                                                <div class='col-sm-12 padding-bottom-20' ng-hide="vm.barrilEstaCheio()">

                                                    <div ng-hide="vm.checaLimiteVolume()">
                                                        <span style="color: red;" ng-bind="vm.mensagemLimite()"></span>
                                                    </div>

                                                    Quantos litros voc� vai beber?

                                                    <input class='form-control'
                                                           ng-model="vm.qtdLitroNovoPedido"
                                                           type='text'
                                                           min="0"
                                                           max="{{vm.getTotalRestante()}}"
                                                           ui-number-mask="0"
                                                           maxlength="2"

                                                    >

                                                    <ul class="lista-dados-festa">
                                                        <li>
                                                            <label class='col-form-label'>Pre�o do litro:</label>
                                                            <p>R$ {{vm.precoLitro()}}</p>
                                                        </li>
                                                        <li>
                                                            <label class='col-form-label'>Valor total:</label>
                                                            <p>R$ {{vm.calculaCustoDaBebida()}}</p>
                                                        </li>
                                                    </ul>

                                                </div>

                                                <div class='col-xs-12 col-sm-12 barra-botoes' ng-hide="vm.barrilEstaCheio()">

                                                    <button class="btn-sm btn-success col-xs-12 col-sm-3 matchHeight"
                                                            type="button"
                                                            ng-click="vm.beberClick(true)"
                                                            ng-hide="vm.barrilEstaCheio() || vm.getIdEstadoBarril() != 1 ||
                                                                    (vm.getIdEstadoConvite() != 4 && vm.getIdEstadoConvite() != 2)">

                                                        <i class="glyphicon glyphicon-ok"></i>
                                                        Vou beber e j� paguei

                                                    </button>

                                                    <button class="btn-sm btn-success col-xs-12 col-sm-3 matchHeight"
                                                            type="button"
                                                            ng-click="vm.beberClick(true)"
                                                            ng-show="!vm.barrilEstaCheio()
                                                                     && vm.getIdEstadoBarril() == 1
                                                                     && vm.getIdEstadoConvite() == 3">

                                                        <i class="glyphicon glyphicon-ok"></i>
                                                        Vou beber mais e j� paguei

                                                    </button>

                                                    <button class="btn-sm btn-warning col-xs-12 col-sm-3 matchHeight"
                                                            type="button"
                                                            ng-click="vm.beberClick(false)"
                                                            ng-hide="vm.getIdEstadoConvite() != 4 || vm.getIdEstadoBarril() != 1">

                                                        <i class="glyphicon glyphicon-ok"></i>
                                                        Vou � festa, mas sem beber

                                                    </button>

                                                    <button class="btn-sm btn-info col-xs-12 col-sm-3 matchHeight"
                                                            type="button"
                                                            ng-click="vm. comentarClick()">

                                                        <i class="glyphicon glyphicon-send"></i>
                                                        Enviar mensagem

                                                    </button>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="separador omega-container">&nbsp;</div>

                                    <div class="dark padding-20-0">
                                        <div class="omega-container">
                                            <div class="row">

                                                <header>
                                                    <h1>Dados da Festa</h1>
                                                    <h2>Informa��es gerais sobre a festa</h2>
                                                </header>

                                                <ul class="lista-dados-festa">
                                                    <li>
                                                        <label class='col-form-label'>Organizador:</label>
                                                        <p>{{vm.getNomeCervejeiro()}}</p>
                                                    </li>
                                                    <li>
                                                        <label class='col-form-label'>Endere�o:</label>
                                                        <p>{{vm.getEndereco()}}</p>
                                                    </li>
                                                    <li>
                                                        <label class='col-form-label'>Data da festa:</label>
                                                        <p>{{vm.getDataFesta()}}</p>
                                                    </li>
                                                    <li>
                                                        <label class='col-form-label'>Data da confirma��o:</label>
                                                        <p>{{vm.getDataConfirmacao()}}</p>
                                                    </li>
                                                </ul>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="separador omega-container">&nbsp;</div>

                                    <div class="dark padding-20-0">
                                        <div class="omega-container">
                                            <div class="row">

                                                <header>
                                                    <h1>Dados banc�rios</h1>
                                                    <h2>para dep�sito</h2>
                                                </header>

                                                <ul class="lista-dados-festa">
                                                    <li>

                                                        <label class='col-form-label'>Agencia: </label>
                                                        <p>{{vm.getDadosBancarios().agencia}}</p>
                                                    </li>
                                                    <li>
                                                        <label class='col-form-label'>Conta: </label>
                                                        <p>{{vm.getDadosBancarios().conta}}</p>
                                                    </li>
                                                    <li>
                                                        <label class='col-form-label'>Operacao: </label>
                                                        <p>{{vm.getDadosBancarios().operacao}}</p>
                                                    </li>
                                                    <li>
                                                        <label class='col-form-label'>Nome Titular: </label>
                                                        <p>{{vm.getDadosBancarios().titular}}</p>
                                                    </li>
                                                </ul>

                                                <small><b>Aten��o:</b> esse convite � do seu amigo para voc�, � de sua responsabilidade essa
                                                    transfer�ncia banc�ria. Esse convite n�o � uma venda! Se for, denuncie!
                                                </small>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="separador omega-container">&nbsp;</div>

                                    <div class="dark padding-20-0">
                                        <div class="omega-container">
                                            <div class="row">

                                                <header>
                                                    <h1>Meus pedidos</h1>
                                                    <h2>realizados para a festa</h2>
                                                </header>

                                                <table class="table table-bordered table-striped omega-list-table">

                                                    <thead>
                                                        <tr>
                                                            <th>Data</th>
                                                            <th>Quantidade (em litros)</th>
                                                            <th>Valor (R$)</th>
                                                            <th>Estado Atual</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr ng-repeat="pedido in vm.getPedidos()">
                                                            <td>
                                                                <span ng-bind="pedido.data"></span>
                                                            </td>
                                                            <td>
                                                                <span ng-bind="pedido.qtdLitros"></span>
                                                            </td>
                                                            <td>
                                                                <span ng-bind="pedido.valor"></span>
                                                            </td>
                                                            <td>
                                                                <span ng-bind="pedido.estadoPedido"></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">

                                                                <div class="alert alert-warning">
                                                                    <h4>
                                                                        <i class="icon-warning-sign"></i>
                                                                        <?= I18N::getExpression("Aviso"); ?>
                                                                    </h4>
                                                                    <?= I18N::getExpression("Voc� n�o realizou nenhum pedido at� o momento."); ?>
                                                                </div>

                                                            </td>
                                                        </tr>
                                                        <tr class="totalizador-tabela">
                                                            <td>
                                                                Confirmado
                                                            </td>
                                                            <td>
                                                                {{vm.saldoConfirmado()}}
                                                            </td>
                                                            <td colspan="2">&nbsp;</td>
                                                        </tr>
                                                        <tr class="totalizador-tabela">
                                                            <td>
                                                                N�o confirmado
                                                            </td>
                                                            <td>
                                                                {{vm.saldoNaoConfirmado()}}
                                                            </td>
                                                            <td colspan="2">&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>
                </form>

            </div>

        </div>

        <div class='col-md-12' ng-show="vm.isOnlineFacebook()">
            <h2>J� � cadastrado? Fa�a seu login! </h2>
            <div class="fb-login-button" data-max-rows="1" data-size="small" data-button-type="login_with"
                 data-show-faces="true" data-auto-logout-link="true" data-use-continue-as="true"
                 style="color: white !important;"
                 onlogin="broadcastLoginAngularJs()"
                <?php echo HelperFacebook::PERMISSIONS; ?>
            ></div>
        </div>

    </div>

</div>
