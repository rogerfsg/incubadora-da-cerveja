<!--Defini��o da View Angular JS -->
<div class='row' id='content-wrapper' ng-app="omegaApp">
    <div class='col-sm-12 no-padding' ng-controller="cadastroDegustadorController as vm">

        <div class="container-fluid dark section no-padding margin-bottom-20">
            <div class="row">
                <div class="col-sm-12 icon-grid">
                    <img src="imgs/template/Hops.svg" class="svg"/>
                    <h4>Degustador</h4>
                    <p>Gosto muito de cerveja e desejo organizar ou participar
                        de uma festa organizada na Incubadora da Cerveja!</p>
                </div>
            </div>
        </div>

        <form name="mainForm"
              class="col-sm-8 col-sm-push-2 form form-horizontal"
              style="margin-bottom: 0;">

            <div class='row'>
                <div class='col-sm-12'>

                    <header class="centred">
                        <h2>Informe</h2>
                        <h4>Seus dados</h4>
                    </header>

                    <div class='box'>
                        <div class='box-content'>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldEmail)">

                                <label class='col-md-3 control-label' for='fieldEmail'>E-mail de Contato</label>

                                <div class='col-md-6'>

                                    <input class='form-control'
                                           ng-disabled="disabledFields.fieldEmail"
                                           ng-required="requiredFields.fieldEmail"
                                           placeholder=''
                                           ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/"
                                           name="fieldEmail"
                                           ng-model="vm.email"
                                           type='text'>

                                    <span class='help-block'
                                          ng-show="showValidationMessage('fieldEmail', mainForm, { 'email': 'O e-mail � inv�lido', required: 'Campo Obrigat�rio'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldEmail', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>
                            </div>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldTelefone)">

                                <label class='col-md-3 control-label' for='fieldTelefone'>Telefone de Contato</label>

                                <div class='col-md-6'>

                                    <input class='form-control'
                                           ng-disabled="disabledFields.fieldTelefone"
                                           ng-required="requiredFields.fieldTelefone"
                                           placeholder=''
                                           ui-br-phone-number
                                           name="fieldTelefone"
                                           ng-model="vm.telefone"
                                           type='text'>

                                    <span class='help-block'
                                          ng-show="showValidationMessage('fieldTelefone', mainForm, { 'br-phone-number': 'O telefone � inv�lido', required: 'Campo Obrigat�rio'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldTelefone', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>


            <div class='row'>
                <div class='col-sm-12'>

                    <header class="centred">
                        <h2>Seu gosto</h2>
                        <h4>Que tipo de cerveja voc� gosta?</h4>
                    </header>

                    <div class='box'>
                        <div class='box-content'>

                            <div class='form-group'>

                                <label class='col-md-3 control-label' for='fieldCervejaGosta'>Cervejas que gosta</label>

                                <div class='col-sm-2'>

                                    <label ng-repeat="tipoCerveja in vm.tiposCerveja" class="opcao-cerveja-degustador">
                                        <input
                                                type="checkbox"
                                                ng-model="vm.cervejasQueGosta[tipoCerveja.id]"
                                                ng-click="vm.cervejaQueGostaClick(tipoCerveja)"
                                        > {{tipoCerveja.nome}}
                                    </label>
                                </div>
                            </div>

                            <div class='form-group'>

                                <label class='col-md-3 control-label' for='fieldEmail'>Cervejas que N�O gosta</label>

                                <div class='col-sm-2'>

                                    <label ng-repeat="tipoCerveja in vm.tiposCerveja" class="opcao-cerveja-degustador">
                                        <input
                                                type="checkbox"
                                                ng-model="vm.cervejasQueNaoGosta[tipoCerveja.id]"
                                                ng-click="vm.cervejaQueNaoGostaClick(tipoCerveja)"
                                        > {{tipoCerveja.nome}}
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class='form-actions form-actions-padding-sm btn-wrap'>
                <div class='row'>
                    <div class='col-md-10 col-md-offset-2'>
                        <button class='btn btn-default' type='submit' ng-click="vm.salvar()">
                            <i class='icon-save'></i>
                            Prosseguir
                        </button>
                    </div>
                </div>
            </div>

        </form>

    </div>
</div>

<!--Defini��o do Controller Angular JS -->
<script type="text/javascript" src="angular/app/cadastro.degustador.controller.js"></script>
