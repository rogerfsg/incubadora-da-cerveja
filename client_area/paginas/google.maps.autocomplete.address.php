<div class='form-group' id='content-wrapper'>
    <label class='col-md-3 control-label' for='fieldTelefone'>Endere�o</label>

    <div class='col-md-6'>

        <div id="locationField">
            <input id="autocomplete" placeholder="Digite o endere�o" type="text" class="form-control"
                   autocomplete="nope">
        </div>
    </div>
</div>
<div id="controls" style="display: none;">
    <select id="country">
        <option value="br">Brasil</option>
    </select>
</div>
<div class='form-group'>
    <div class='col-md-12'>
        <div id="map" style=" width: 100%; height: 300px;"></div>


        <script>

            var map, places, infoWindow;
            var markers = [];
            var autocomplete;
            var countryRestrict = {'country': 'br'};
            var MARKER_PATH = 'https://developers.google.com/maps/documentation/javascript/images/marker_green';
            var hostnameRegexp = new RegExp('^https?://.+?/');
            var countries = {
                'au': {
                    center: {lat: -25.3, lng: 133.8},
                    zoom: 4
                },
                'br': {
                    center: {lat: -14.2, lng: -51.9},
                    zoom: 3
                },
                'ca': {
                    center: {lat: 62, lng: -110.0},
                    zoom: 3
                },
                'fr': {
                    center: {lat: 46.2, lng: 2.2},
                    zoom: 5
                },
                'de': {
                    center: {lat: 51.2, lng: 10.4},
                    zoom: 5
                },
                'mx': {
                    center: {lat: 23.6, lng: -102.5},
                    zoom: 4
                },
                'nz': {
                    center: {lat: -40.9, lng: 174.9},
                    zoom: 5
                },
                'it': {
                    center: {lat: 41.9, lng: 12.6},
                    zoom: 5
                },
                'za': {
                    center: {lat: -30.6, lng: 22.9},
                    zoom: 5
                },
                'es': {
                    center: {lat: 40.5, lng: -3.7},
                    zoom: 5
                },
                'pt': {
                    center: {lat: 39.4, lng: -8.2},
                    zoom: 6
                },
                'us': {
                    center: {lat: 37.1, lng: -95.7},
                    zoom: 3
                },
                'uk': {
                    center: {lat: 54.8, lng: -4.6},
                    zoom: 5
                }
            };
            function initMap()
            {
                map = new google.maps.Map(document.getElementById('map'), {
                    zoom: countries['us'].zoom,
                    center: countries['us'].center,
                    mapTypeControl: false,
                    panControl: false,
                    zoomControl: false,
                    streetViewControl: false
                });
                infoWindow = new google.maps.InfoWindow({
                    content: document.getElementById('info-content')
                });

                // Create the autocomplete object and associate it with the UI input control.
                // Restrict the search to the default country, and to place type "cities".
                autocomplete = new google.maps.places.Autocomplete(
                    /** @type {!HTMLInputElement} */ (
                        document.getElementById('autocomplete')), {
                        types: ['(cities)'],
                        componentRestrictions: countryRestrict
                    });

                places = new google.maps.places.PlacesService(map);
                autocomplete.addListener('place_changed', onPlaceChanged);

                // Add a DOM event listener to react when the user selects a country.
                document.getElementById('country').addEventListener(
                    'change', setAutocompleteCountry);

            }

            // When the user selects a city, get the place details for the city and
            // zoom the map in on the city.
            function onPlaceChanged()
            {
                var place = autocomplete.getPlace();
                if (place.geometry)
                {
                    map.panTo(place.geometry.location);
                    map.setZoom(15);
                }
                else
                {
                    document.getElementById('autocomplete').placeholder = 'Enter a city';
                }
            }
            function clearMarkers()
            {
                for (var i = 0; i < markers.length; i++)
                {
                    if (markers[i])
                    {
                        markers[i].setMap(null);
                    }
                }
                markers = [];
            }

            // Set the country restriction based on user input.
            // Also center and zoom the map on the given country.
            function setAutocompleteCountry()
            {
                var country = document.getElementById('country').value;
                if (country == 'all')
                {
                    autocomplete.setComponentRestrictions([]);
                    map.setCenter({lat: 15, lng: 0});
                    map.setZoom(2);
                }
                else
                {
                    autocomplete.setComponentRestrictions({'country': country});
                    map.setCenter(countries[country].center);
                    map.setZoom(countries[country].zoom);
                }
                clearResults();
                clearMarkers();
            }

            function dropMarker(i)
            {
                return function ()
                {
                    markers[i].setMap(map);
                };
            }

        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDGw-2yveQravpjzLaRRk5fOVrIUFs-ob4&libraries=places&callback=initMap"
                async defer></script>
    </div>
</div>