<?php

?>

<link href="../client_area/css/barril.animacao.css" rel="stylesheet">

<style>

    header h1 {
        font-size: 92px;
        margin-bottom: 20px;
    }

    header h2 {
        color: #ffffff !important;
    }

    .barril-animado {
        margin-top: 60px;
    }

</style>

<header class="centred">
    <h1>Incubadora da Cerveja</h1>
    <h2>www.incubadoradacerveja.com.br</h2>
</header>

<div class="barril-animado pilsen" id="teste-logo-barril">
    <div class="container-barril">

        <img src="imgs/especifico/barril-frame.png" class="frame-barril"/>

        <div class="liquido">
            <div class="efeito-topo-liquido"></div>
            <div class="bubble bubble1"></div>
            <div class="bubble bubble2"></div>
            <div class="bubble bubble3"></div>
            <div class="bubble bubble4"></div>
            <div class="bubble bubble5"></div>
        </div>

    </div>

</div>

<script type="text/javascript">

    AppUtils.criarBarril('teste-logo-barril', 70, 800);

</script>


