<!-- Section -->
<div class="dark padding-40-0" ng-controller="deliveryController as vm" style="overflow: hidden;">
    <div class="container" style="overflow: hidden;">
        <div class="row " style="overflow: hidden;">

            <div class="col-xs-12 col-md-12   painel1 take" >
                <div class="texto-paragrafos-menores">
                    <header >
                        <h1>Festa do Barril</h1>
                        <h2>Em qualquer Brew Pub do Brasil!</h2>
                    </header>


                </div>
            </div>
            <div class="col-xs-12 col-md-12   painel1 take" >
                <div class="texto-paragrafos-menores">
                    <p class="painel1A take">Festa do Barril � uma festa que vende a cerveja antecipadamente, evitando filas no Brew Pub!<p>
                    <p class="painel1B take">E ainda pode te render umas cervejas gr�tis! </p>
                    <p class="painel1C take">Se voc� convidar a galera! </p>
                </div>
            </div>
            <div class="col-xs-12 col-md-12  painel2 take">
                <div class="row">
                    <div class="col-xs-12 col-md-4 icon-grid">
                        <img src="imgs/template/BottleCrate.svg" class="svg"  />
                        <h4>1 - Pedir cervejas!</h4>
                        <p>Voc� acessa para comprar as cervejas que vai beber na festa</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-12  painel3 take">
                <div class="row">
                    <div class="col-xs-12 col-md-4 icon-grid">
                        <img src="imgs/template/Sign.svg" class="svg"  />
                        <h4>2 - Convidar a galera</h4>
                        <p>Voc� ganha um link para convidar seus amigos pelo WhatsApp, Facebook, etc...</p>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-md-12  painel4 take">
                <div class="row">
                    <div class="col-xs-12 col-md-4 icon-grid no-border-bottom">
                        <img src="imgs/template/Sign.svg" class="svg"  />
                        <h4>3 - Compra do amigo</h4>
                        <p>Seu amigo faz a compra e voc� acumula em cerveja 5% do total que ele pediu.</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-12  painel5 take">
                <div class="row">
                    <div class="col-xs-12 col-md-4 icon-grid no-border-bottom">
                        <img src="imgs/template/Tankard.svg" class="svg"  />
                        <h4>4 - Festa</h4>
                        <p>A cada 500 ml voc� ganha uma caneca de 500ml na festa!</p>
                    </div>

                </div>
            </div>
            <div class="col-xs-12 col-md-12  painel6 take">
                <div class="row">
                    <header >
                        <h2>Tenho um Brew Pub e quero fazer a festa do Barril!</h2>
                        <h1>Barril, barril, barril!</h1>
                    </header>
                    <div class="col-xs-12 col-md-4 no-border-bottom">

                        <p>
                            <a href="index.php?tipo=paginas&page=cadastro.cervejeiro&tipoDoUsuario=brewpub" class="btn btn-default only-public-area"><span>Cadastrar</span></a>
                        </p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="separador-main container">&nbsp;</div>


<script type="text/javascript">
    function sleep(ms) {
        var start = new Date().getTime(), expire = start + ms;
        while (new Date().getTime() < expire) { }
        return;
    }
    var tempo1 = 1000;
    var tempo = 2500;
    var visibilidade = 'block'
    $('document').ready(function(){
        $('.take').css('display', 'none');

        function recursivo(i){
            if(i == 7) return ;

            var anterior = i - 1;
            setTimeout(function() {
                $('.painel' + anterior).css('display', 'none');
                $('.painel' + i).css('display', visibilidade);
                sleep(tempo);
                recursivo(i + 1)
            },tempo);
        }
        setTimeout(function() {
            $('.painel1').css('display', visibilidade);
            sleep(tempo1);

            setTimeout(function() {
                $('.painel1A').css('display', visibilidade);
            setTimeout(function() {
                $('.painel1B').css('display', visibilidade);
                sleep(tempo1);
                setTimeout(function() {
                    $('.painel1C').css('display', visibilidade);
                    sleep(tempo1);
                    recursivo(2);

                },tempo);
            },tempo);
            },tempo);
        },tempo);



//        for(var i = 2; i<=7; i++){
//            var anterior = i - 1;
//            $('.painel' + anterior).css('display', 'none');
//            $('.painel' + i).css('display', visibilidade);
//            sleep(tempo);
//        }
    });

</script>