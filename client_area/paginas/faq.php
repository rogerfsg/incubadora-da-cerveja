

<!-- Section -->
<div class="container-fluid dark section" id='content-wrapper'>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="history">
                    <span class="date">Faq</span>
                    <article class="animateme matchHeight" data-when="enter" data-from="0.75" data-to="0" data-opacity="0">
                        <header>
                            <span>Degustador Organizador</span>
                            <h2>Como fa�o uma festa?</h2>
                        </header>
                        <p>Clique para cadastrar um barril, preencha os campos. Envie o link do convite para os seus
                            amigos. Todo convidado deve ser seu amigo do facebook.</p>
                    </article>
                    <article class="animateme matchHeight" data-when="enter" data-from="0.75" data-to="0" data-opacity="0">
                        <header>
                            <span>Degustador Organizador</span>
                            <h2>Posso vender convite?</h2>
                        </header>
                        <p>N�o pode vender convite por meio dessa ferramenta, somente o perfil Brew Pub</p>
                    </article>
                    <article class="animateme matchHeight" data-when="enter" data-from="0.75" data-to="0" data-opacity="0">
                        <header>
                            <span>Degustador Organizador</span>
                            <h2>O barril encheu e ai?</h2>
                        </header>
                        <p>Nenhum convidado � capaz de pedir mais bebida para a vaquinha nesse ponto. Ta na hora de
                            come�ar os preparativos!</p>
                    </article>
                    <article class="animateme matchHeight" data-when="enter" data-from="0.75" data-to="0" data-opacity="0">
                        <header>
                            <span>Degustador Organizador</span>
                            <h2>Cancelar festa</h2>
                        </header>
                        <p>O cancelamento de festa � realizado alterando o status do convite e disponibilizando um link
                            para voc� compartilhar com os convidados. Lembre de devolver o dinheiro de quem j� pagou a
                            vaquinha</p>
                    </article>
                    <article class="animateme matchHeight" data-when="enter" data-from="0.75" data-to="0" data-opacity="0">
                        <header>
                            <span>Degustador</span>
                            <h2>Pagamento</h2>
                        </header>
                        <p>O pagamento da vaquinha � uma transa��o entre o degustador convidado e o dono da vaquinha,
                            como voc�s s�o amigos, isso n�o ser� um problema. N�o realize nenhuma transa��o com quem
                            voc� n�o conhece e n�o � amigo. A vaquinha � apenas para amigos e n�o tem fim comercial!</p>
                    </article>
                    <article class="animateme matchHeight" data-when="enter" data-from="0.75" data-to="0" data-opacity="0">
                        <header>
                            <span>Degustador</span>
                            <h2>Convite Qr Code?</h2>
                        </header>
                        <p>Todo degustador convidado da festa tem um QR Code. Quando for beber na festa, basta
                            apresenta-lo ao gar�om e pedir! O seu saldo ser� abatido!</p>
                    </article>
                    <article class="animateme matchHeight" data-when="enter" data-from="0.75" data-to="0" data-opacity="0">
                        <header>
                            <span>Degustador Organizador</span>
                            <h2>Leitor Qr Code?</h2>
                        </header>
                        <p>Para maior controle durante a festa, utilize o leitor de qr code, para abater o saldo de quem
                            pedir bebida. Aponte o leitor para o qr code do convite da pessoa, digite a quantidade de ml
                            desejada e aguarde a confirma��o da transa��o. Caso o convidado n�o tenha mais saldo de
                            cerveja, ser� emitido uma mensagem.</p>
                    </article>

                </div>
            </div>
        </div>
    </div>
</div>
