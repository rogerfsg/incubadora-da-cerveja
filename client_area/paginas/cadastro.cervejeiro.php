<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCnkq1W7ZKi9q4SDSlpBNVfdMZIf-iTpPc&libraries=places">

</script>

<?php

    //    //utilizado no cadastro de usu�rio (para diferenciar o tipo que est� sendo cadastrado)
    //    $tipoDoUsuario = Helper::GET("tipoDoUsuario") ? "'" . Helper::GET("tipoDoUsuario") . "'" : "null";
    //
    //    //valida se pertence ao usu�rio a partir da sess�o do facebook
    //    $idBarril = Helper::GET("idBarril") ? Helper::GET("idBarril") : "null";
    //
    //    //cadastro do barril
    //    $isCadastroDoBarril = Helper::GET("isCadastroDoBarril") == "true" ? Helper::GET("isCadastroDoBarril") : "false";

?>

<!--Defini��o da View Angular JS -->
<div class='row' id='content-wrapper' ng-app="omegaApp">
    <div class='col-sm-12 no-padding' ng-controller="cadastroCervejeiroController as vm" ng-cloak>

        <!-- Quando acessa a tela pelo botao Novo barril da tela principal, o tipo do usuario nao foi definido -->
        <div ng-if="vm.isTipoUsuarioIndefinido()">

            <div class='titulo-sessao-interna container-fluid no-padding border-bottom-grey-1'>

                <div class="col-md-12">

                    <header>
                        <span class="date">Realize seu cadastro</span>
                        <h2>Que tipo de perfil � o seu? Escolha abaixo!</h2>
                    </header>

                </div>

            </div>

            <div class="container-fluid dark section no-padding">

                <div class="row">

                    <div class="col-xs-12 col-md-3 icon-grid clickable"
                         ng-click="vm.cadastrarDegustador()">

                        <img src="imgs/template/Chalice.svg" class="svg"/>
                        <h4>Degustador</h4>
                        <p>Gosto muito de cerveja e desejo organizar ou participar
                            de uma festa organizada na Incubadora da Cerveja!</p>

                    </div>

                    <div class="col-xs-12 col-md-3 icon-grid clickable"
                         ng-click="vm.tipoDoUsuario = AppConstants.TIPO_CADASTRO.CERVEJARIA">

                        <img src="imgs/template/3DSixPack.svg" class="svg"/>
                        <h4>Cervejaria</h4>
                        <p>Sou uma cervejaria artesanal licenciada e desejo
                            comercializar a minha produ��o de cerveja artesanal</p>

                    </div>

                    <div class="col-xs-12 col-md-3 icon-grid clickable"
                         ng-click="vm.tipoDoUsuario = AppConstants.TIPO_CADASTRO.CERVEJEIRO_CIGANO">

                        <img src="imgs/template/Tank.svg" class="svg"/>
                        <h4>Cervejeiro Cigano</h4>
                        <p>Sou cigano e desejo compartilhar os custos de produ��o da minha cerveja com os meus amigos do
                            Facebook</p>

                    </div>

                    <div class="col-xs-12 col-md-3 icon-grid clickable"
                         ng-click="vm.tipoDoUsuario = AppConstants.TIPO_CADASTRO.BREWPUB">

                        <img src="imgs/template/Keg.svg" class="svg"/>
                        <h4>Brewpub</h4>
                        <p>Sou um brewpub e desejo comercializar a minha produ��o
                            de cerveja artesanal em meu pr�prio estebelecimento</p>

                    </div>

                </div>

            </div>

            <div class="separador margin-bottom-20">&nbsp;</div>

            <div class='titulo-sessao-interna container-fluid no-padding no-border-bottom'>

                <div class="col-md-12">

                    <header>
                        <span class="date">J� � cadastrado?</span>
                        <h2>Fa�a o seu login atrav�s do Facebook</h2>
                    </header>

                </div>

                <div class='col-md-12 margin-bottom-20 only-public-facebook-area'>

                    <div class="fb-login-button"
                         data-max-rows="1"
                         data-size="medium"
                         data-button-type="continue_with"
                         data-show-faces="false"
                         data-auto-logout-link="true"
                         data-use-continue-as="true"
                         style="color: white !important;"
                         onlogin="broadcastLoginAngularJs()"
                        <?php echo HelperFacebook::PERMISSIONS; ?>
                    ></div>

                </div>

            </div>

        </div>

        <!-- Topo da p�gina - Cadastro de usu�rios -->
        <div ng-if="vm.isCadastroDoUsuario()">

            <div class="container-fluid dark section no-padding margin-bottom-20"
                 ng-if="vm.isTipoUsuario(AppConstants.TIPO_CADASTRO.CERVEJEIRO_CIGANO)">

                <div class="row">
                    <div class="col-sm-12 icon-grid">
                        <img src="imgs/template/Tank.svg" class="svg"/>
                        <h4>Cervejeiro Cigano</h4>
                        <p>Sou cigano e desejo compartilhar os custos de produ��o
                            da minha cerveja com os meus amigos do Facebook</p>
                    </div>
                </div>
            </div>

            <div class="container-fluid dark section no-padding margin-bottom-20"
                 ng-if="vm.isTipoUsuario(AppConstants.TIPO_CADASTRO.CERVEJARIA)">

                <div class="row">
                    <div class="col-sm-12 icon-grid">
                        <img src="imgs/template/3DSixPack.svg" class="svg"/>
                        <h4>Cervejaria</h4>
                        <p>Sou uma cervejaria artesanal licenciada e desejo
                            comercializar a minha produ��o de cerveja artesanal</p>
                    </div>
                </div>
            </div>

            <div class="container-fluid dark section no-padding margin-bottom-20"
                 ng-if="vm.isTipoUsuario(AppConstants.TIPO_CADASTRO.BREWPUB)">

                <div class="row">
                    <div class="col-sm-12 icon-grid">
                        <img src="imgs/template/Keg.svg" class="svg"/>
                        <h4>Brewpub</h4>
                        <p>Sou um brewpub e desejo comercializar a minha produ��o
                            de cerveja artesanal em meu pr�prio estebelecimento</p>
                    </div>
                </div>
            </div>

        </div>

        <!-- Topo da p�gina - Edi��o do usu�rio -->
        <div ng-if="vm.isEdicaoDoUsuario()">

            <div class="container-fluid dark section no-padding margin-bottom-20"
                 ng-if="vm.isTipoUsuario(AppConstants.TIPO_CADASTRO.CERVEJEIRO_CIGANO)">

                <div class="row">
                    <div class="col-sm-12 icon-grid">
                        <img src="imgs/template/Tank.svg" class="svg"/>
                        <h4>Cervejeiro Cigano</h4>
                        <p>Meus dados cadastrais</p>
                    </div>
                </div>
            </div>

            <div class="container-fluid dark section no-padding margin-bottom-20"
                 ng-if="vm.isTipoUsuario(AppConstants.TIPO_CADASTRO.CERVEJARIA)">

                <div class="row">
                    <div class="col-sm-12 icon-grid">
                        <img src="imgs/template/3DSixPack.svg" class="svg"/>
                        <h4>Cervejeiria</h4>
                        <p>Meus dados cadastrais</p>
                    </div>
                </div>
            </div>

            <div class="container-fluid dark section no-padding margin-bottom-20"
                 ng-if="vm.isTipoUsuario(AppConstants.TIPO_CADASTRO.BREWPUB)">

                <div class="row">
                    <div class="col-sm-12 icon-grid">
                        <img src="imgs/template/Keg.svg" class="svg"/>
                        <h4>Brewpub</h4>
                        <p>Meus dados cadastrais</p>
                    </div>
                </div>
            </div>

        </div>

        <!-- Topo da p�gina - Edi��o do barril -->
        <div ng-if="vm.isEdicaoDoBarril()">

            <div class="container-fluid dark section no-padding margin-bottom-20"
                 ng-if="vm.isTipoUsuario(AppConstants.TIPOS_CADASTRO_EMPRESARIAL)">

                <div class="row">
                    <div class="col-sm-12 icon-grid">
                        <img src="imgs/template/Keg.svg" class="svg"/>
                        <h4>Alterar</h4>
                        <p>dados do meu barril</p>
                    </div>
                </div>
            </div>

            <div class="container-fluid dark section no-padding margin-bottom-20"
                 ng-if="vm.isTipoUsuario(AppConstants.TIPO_CADASTRO.DEGUSTADOR)">

                <div class="row">
                    <div class="col-sm-12 icon-grid">
                        <img src="imgs/template/Chalice.svg" class="svg"/>
                        <h4>Alterar</h4>
                        <p>dados da minha festa</p>
                    </div>
                </div>
            </div>

        </div>

        <!-- Topo da p�gina - Cadastro do barril -->
        <div ng-if="vm.isCadastroDoBarril()">

            <div class="container-fluid dark section no-padding margin-bottom-20"
                 ng-if="vm.isTipoUsuario(AppConstants.TIPOS_CADASTRO_EMPRESARIAL)">

                <div class="row">
                    <div class="col-sm-12 icon-grid">
                        <img src="imgs/template/Keg.svg" class="svg"/>
                        <h4>Cadastrar</h4>
                        <p>meu barril</p>
                    </div>
                </div>
            </div>

            <div class="container-fluid dark section no-padding margin-bottom-20"
                 ng-if="vm.isTipoUsuario(AppConstants.TIPO_CADASTRO.DEGUSTADOR)">

                <div class="row">
                    <div class="col-sm-12 icon-grid">
                        <img src="imgs/template/Keg.svg" class="svg"/>
                        <h4>Festa</h4>
                        <p>Sou amante da cerveja e desejo organizar uma festa!</p>
                    </div>
                </div>
            </div>

        </div>

        <form name="mainForm"
              class="col-sm-8 col-sm-push-2 form form-horizontal"
              style="margin-bottom: 0;"
              ng-if="vm.tipoDoUsuario != null">

            <!-- Se��o de dados b�sicos - Cadastro de empresa -->
            <div class='row'
                 ng-if="vm.isTipoUsuario(AppConstants.TIPOS_CADASTRO_EMPRESARIAL) && !vm.isCadastroDoBarril() && !vm.isEdicaoDoBarril()">

                <div class='col-sm-12'>

                    <header class="centred" ng-if="vm.isTipoUsuario([AppConstants.TIPO_CADASTRO.BREWPUB])">
                        <h2>Informe</h2>
                        <h4>os dados do seu Brewpub</h4>
                    </header>

                    <header class="centred"
                            ng-if="vm.isTipoUsuario([AppConstants.TIPO_CADASTRO.CERVEJEIRO_CIGANO, AppConstants.TIPO_CADASTRO.CERVEJARIA])">
                        <h2>Informe</h2>
                        <h4>os dados da sua cervejaria</h4>
                    </header>

                    <div class='box'>
                        <div class='box-content'>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldRazaoSocial)">

                                <label class='col-md-3 control-label' for='fieldRazaoSocial'>Raz�o Social</label>

                                <div class='col-md-6'>

                                    <input type="text"
                                           ng-model="vm.formDataCervejeiro.razaoSocial"
                                           class="form-control"
                                           ng-disabled="vm.disabledFields.fieldRazaoSocial"
                                           ng-required="vm.requiredFields.fieldRazaoSocial"
                                           name="fieldRazaoSocial"
                                           id="fieldRazaoSocial"
                                           placeholder=""
                                           omega-control-type="TextInput"
                                           maxlength="255"/>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldRazaoSocial', mainForm , {maxlength: 'O tamanho m�ximo da raz�o social deve ser de 100 caracteres.', required: 'A raz�o social � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldRazaoSocial', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>
                            </div>


                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldNome)">

                                <label class='col-md-3 control-label' for='fieldNome'>Nome Fantasia</label>

                                <div class='col-md-6'>

                                    <input type="text"
                                           ng-model="vm.formDataCervejeiro.nome"
                                           class="form-control"
                                           ng-disabled="vm.disabledFields.fieldNome"
                                           ng-required="vm.requiredFields.fieldNome"
                                           name="fieldNome"
                                           id="fieldNome"
                                           placeholder=""
                                           omega-control-type="TextInput"
                                           maxlength="255"/>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldNome', mainForm , {maxlength: 'O tamanho m�ximo do nome fantasia deve ser de 100 caracteres.', required: 'O nome fantasia � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldNome', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>
                            </div>


                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldCnpj)">

                                <label class='col-md-3 control-label' for='fieldCnpj'>CNPJ</label>

                                <div class='col-md-6'>

                                    <input class='form-control'
                                           ng-disabled="vm.disabledFields.fieldCnpj"
                                           ng-required="vm.requiredFields.fieldCnpj"
                                           placeholder=''
                                           ui-br-cnpj-mask
                                           name="fieldCnpj"
                                           ng-model="vm.formDataCervejeiro.cnpj"
                                           type='text'>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldCnpj', mainForm , {cnpj: 'CNPJ inv�lido', required: 'O CNPJ � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldCnpj', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>
                            </div>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldDataAberturaCervejaria)">

                                <label class='col-md-3 control-label'
                                       for='fieldDataAberturaCervejaria'>Data de abertura da cervejaria</label>

                                <div class='col-md-6'>

                                    <p class="input-group">

                                        <input type="text"
                                               class="form-control"
                                               ui-date-mask
                                               name="fieldDataAberturaCervejaria"
                                               uib-datepicker-popup="{{defaultDateFormat}}"
                                               ng-model="vm.formDataCervejeiro.dataAberturaCervejaria"
                                               is-open="vm.fieldsParameters.fieldDataAberturaCervejaria.popupAberto"
                                               datepicker-options="vm.fieldsParameters.fieldDataAberturaCervejaria.datePickerParams"
                                               ng-required="vm.requiredFields.fieldDataAberturaCervejaria"
                                               ng-disabled="vm.disabledFields.fieldDataAberturaCervejaria"
                                               readonly="readonly"
                                               current-text="Hoje"
                                               close-text="Fechar"
                                               clear-text="Limpar"
                                               ng-click="vm.fieldsParameters.fieldDataAberturaCervejaria.popupAberto = true"
                                        />

                                        <span class="input-group-btn">
                                                <button type="button" class="btn btn-default btn-calendar"
                                                        ng-click="vm.fieldsParameters.fieldDataAberturaCervejaria.popupAberto = true">
                                                    <i class="glyphicon glyphicon-calendar"></i></button>
                                            </span>

                                    </p>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldDataAberturaCervejaria', mainForm , {required: 'A data de abertura da cervejaria � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldDataAberturaCervejaria', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>
                            </div>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldComentariosCervejaria)">

                                <label class='col-md-3 control-label' for='fieldComentariosCervejaria'>Conte-nos
                                    mais</label>

                                <div class='col-md-6'>

                                        <textarea
                                                style="height: 180px;"
                                                class='form-control'
                                                name="fieldComentariosCervejaria"
                                                ng-disabled="vm.disabledFields.fieldComentariosCervejaria"
                                                ng-required="vm.requiredFields.fieldComentariosCervejaria"
                                                ng-model="vm.formDataCervejeiro.comentariosCervejaria"
                                                placeholder="Breve descritivo da sua cervejaria"
                                                ng-maxlength="250"></textarea>

                                    {{vm.getLimiteCaracteresRestante(vm.formDataCervejeiro.comentariosCervejaria, 250)}}

                                    <span class='help-block'
                                          ng-show="showValidationMessage('fieldComentariosCervejaria', mainForm, { maxlength: 'O tamanho m�ximo � de 250 caracteres', required: 'Campo Obrigat�rio'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldComentariosCervejaria', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>
                            </div>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldEmail)">

                                <label class='col-md-3 control-label' for='fieldEmail'>E-mail de Contato</label>

                                <div class='col-md-6'>

                                    <input class='form-control'
                                           ng-disabled="vm.disabledFields.fieldEmail"
                                           ng-required="vm.requiredFields.fieldEmail"
                                           placeholder=''
                                           ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/"
                                           name="fieldEmail"
                                           ng-model="vm.formDataCervejeiro.email"
                                           type='text'>

                                    <span class='help-block'
                                          ng-show="showValidationMessage('fieldEmail', mainForm, { 'pattern': 'O e-mail � inv�lido', required: 'Campo Obrigat�rio'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldEmail', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>
                            </div>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldTelefone)">

                                <label class='col-md-3 control-label' for='fieldTelefone'>Telefone de Contato</label>

                                <div class='col-md-6'>

                                    <input class='form-control'
                                           ng-disabled="vm.disabledFields.fieldTelefone"
                                           ng-required="vm.requiredFields.fieldTelefone"
                                           placeholder=''
                                           ui-br-phone-number
                                           name="fieldTelefone"
                                           ng-model="vm.formDataCervejeiro.telefone"
                                           type='text'>

                                    <span class='help-block'
                                          ng-show="showValidationMessage('fieldTelefone', mainForm, { 'br-phone-number': 'O telefone � inv�lido', required: 'Campo Obrigat�rio'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldTelefone', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <!-- Se��o de redes sociais - Cadastro de empresa -->
            <div class='row'
                 ng-if="vm.isTipoUsuario(AppConstants.TIPOS_CADASTRO_EMPRESARIAL) && (vm.isCadastroDoUsuario() || vm.isEdicaoDoUsuario())">

                <div class='col-sm-12'>

                    <header class="centred"
                            ng-if="vm.isTipoUsuario([AppConstants.TIPO_CADASTRO.CERVEJEIRO_CIGANO, AppConstants.TIPO_CADASTRO.CERVEJARIA])">

                        <h2>Redes Sociais</h2>
                        <h4>da sua cervejaria</h4>

                    </header>

                    <header class="centred"
                            ng-if="vm.isTipoUsuario([AppConstants.TIPO_CADASTRO.BREWPUB])">

                        <h2>Redes Sociais</h2>
                        <h4>do seu Brewpub</h4>

                    </header>

                    <div class='box'>
                        <div class='box-content'>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldInstagram)">

                                <label class='col-md-3 control-label' for='fieldInstagram'>Instagram</label>

                                <div class='col-md-6'>

                                    <input type="text"
                                           ng-model="vm.formDataCervejeiro.instagram"
                                           class="form-control"
                                           ng-disabled="vm.disabledFields.fieldInstagram"
                                           ng-required="vm.requiredFields.fieldInstagram"
                                           name="fieldInstagram"
                                           id="fieldInstagram"
                                           placeholder=""
                                           omega-control-type="TextInput"
                                           maxlength="255"/>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldInstagram', mainForm , {maxlength: 'O tamanho m�ximo do Instragram deve ser de 100 caracteres.', required: 'O Instagram � de preenchimento obrigat�rio.'})">
                                            <p ng-repeat="messages in getValidationMessages('fieldInstagram', mainForm)">
                                                {{messages}}
                                            </p>
                                        </span>

                                </div>
                            </div>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldTwitter)">

                                <label class='col-md-3 control-label' for='fieldTwitter'>Twitter</label>

                                <div class='col-md-6'>

                                    <input type="text"
                                           ng-model="vm.formDataCervejeiro.twitter"
                                           class="form-control"
                                           ng-disabled="vm.disabledFields.fieldTwitter"
                                           ng-required="vm.requiredFields.fieldTwitter"
                                           name="fieldTwitter"
                                           id="fieldTwitter"
                                           placeholder=""
                                           omega-control-type="TextInput"
                                           maxlength="255"/>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldTwitter', mainForm , {maxlength: 'O tamanho m�ximo do Twitter deve ser de 100 caracteres.', required: 'O Twitter � de preenchimento obrigat�rio.'})">
                                            <p ng-repeat="messages in getValidationMessages('fieldTwitter', mainForm)">
                                                {{messages}}
                                            </p>
                                        </span>

                                </div>
                            </div>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldSnapchat)">

                                <label class='col-md-3 control-label' for='fieldSnapchat'>Snapchat</label>

                                <div class='col-md-6'>

                                    <input type="text"
                                           ng-model="vm.formDataCervejeiro.snapchat"
                                           class="form-control"
                                           ng-disabled="vm.disabledFields.fieldSnapchat"
                                           ng-required="vm.requiredFields.fieldSnapchat"
                                           name="fieldSnapchat"
                                           id="fieldSnapchat"
                                           placeholder=""
                                           omega-control-type="TextInput"
                                           maxlength="255"/>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldSnapchat', mainForm , {maxlength: 'O tamanho m�ximo do Snapchat deve ser de 100 caracteres.', required: 'O Snapchat � de preenchimento obrigat�rio.'})">
                                            <p ng-repeat="messages in getValidationMessages('fieldSnapchat', mainForm)">
                                                {{messages}}
                                            </p>
                                        </span>

                                </div>
                            </div>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldCanalYoutube)">

                                <label class='col-md-3 control-label' for='fieldCanalYoutube'>Canal no Youtube</label>

                                <div class='col-md-6'>

                                    <input type="text"
                                           ng-model="vm.formDataCervejeiro.canalYoutube"
                                           class="form-control"
                                           ng-disabled="vm.disabledFields.fieldCanalYoutube"
                                           ng-required="vm.requiredFields.fieldCanalYoutube"
                                           name="fieldCanalYoutube"
                                           id="fieldCanalYoutube"
                                           placeholder=""
                                           omega-control-type="TextInput"
                                           maxlength="255"/>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldCanalYoutube', mainForm , {maxlength: 'O tamanho m�ximo do Canal no Youtube deve ser de 100 caracteres.', required: 'O Canal no Youtube � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldCanalYoutube', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <!-- Se��o de endere�o - Cadastro de empresa, degustador, barril -->
            <div class='row'>

                <!--Defini��o do template do dialog exibido caso tenha problema no endereco -->
                <script type="text/ng-template" id="dialog-problema-endereco-informado.html">
                    <div class="modal-header">
                        <h4 class="modal-title">
                            <span class="glyphicon"></span>
                            <?= I18N::getExpression("Endere�o inv�lido"); ?>
                        </h4>
                    </div>
                    <div class="modal-body">
                        <span class="help-block"><?= I18N::getExpression("Deseja informar o endere�o manualmente?"); ?></span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" ng-click="closeMessageDialog()">
                            <?= I18N::getExpression("N�o"); ?>
                        </button>
                        <button type="button" class="btn btn-success" ng-click="vm.informarEnderecoManualmente()">
                            <?= I18N::getExpression("Sim"); ?>
                        </button>
                    </div>
                </script>

                <div class='col-sm-12'
                     ng-if="(vm.isTipoUsuario(AppConstants.TIPO_CADASTRO.DEGUSTADOR) && (vm.isCadastroDoBarril() || vm.isEdicaoDoBarril())) || (vm.isTipoUsuario(AppConstants.TIPOS_CADASTRO_EMPRESARIAL) && (vm.isCadastroDoUsuario() || vm.isEdicaoDoUsuario()))">

                    <header class="centred"
                            ng-if="vm.isTipoUsuario([AppConstants.TIPO_CADASTRO.BREWPUB])">

                        <h2>Local</h2>
                        <h4>do seu estabelecimento</h4>

                    </header>

                    <header class="centred"
                            ng-if="vm.isTipoUsuario([AppConstants.TIPO_CADASTRO.CERVEJARIA])">

                        <h2>Local</h2>
                        <h4>da sua cervejaria</h4>

                    </header>

                    <header class="centred"
                            ng-if="vm.isTipoUsuario([AppConstants.TIPO_CADASTRO.CERVEJEIRO_CIGANO])">

                        <h2>Local</h2>
                        <h4>de produ��o da sua cerveja</h4>

                    </header>

                    <header class="centred"
                            ng-if="vm.isTipoUsuario([AppConstants.TIPO_CADASTRO.DEGUSTADOR])">

                        <h2>Local</h2>
                        <h4>da sua festa</h4>

                    </header>

                    <div class='box'>
                        <div class='box-content'>

                            <div class='form-group'
                                 ng-if="vm.isTipoUsuario([AppConstants.TIPO_CADASTRO.DEGUSTADOR])"
                                 ng-class="getFormControlCssClass(mainForm.fieldDataDaFesta)">

                                <label class='col-md-3 control-label'
                                       for='fieldDataDaFesta'>Data da festa</label>

                                <div class='col-md-6'>

                                    <p class="input-group">

                                        <input type="text"
                                               class="form-control"
                                               ui-date-mask
                                               name="fieldDataDaFesta"
                                               uib-datepicker-popup="{{defaultDateFormat}}"
                                               ng-model="vm.formDataCervejeiro.dataDaFesta"
                                               is-open="vm.fieldsParameters.fieldDataDaFesta.popupAberto"
                                               datepicker-options="vm.fieldsParameters.fieldDataDaFesta.datePickerParams"
                                               ng-required="vm.requiredFields.fieldDataDaFesta"
                                               ng-disabled="vm.disabledFields.fieldDataDaFesta"
                                               readonly="readonly"
                                               current-text="Hoje"
                                               close-text="Fechar"
                                               clear-text="Limpar"
                                               ng-click="vm.fieldsParameters.fieldDataDaFesta.popupAberto = true"
                                        />

                                        <span class="input-group-btn">
                                                <button type="button" class="btn btn-default btn-calendar"
                                                        ng-click="vm.fieldsParameters.fieldDataDaFesta.popupAberto = true">
                                                    <i class="glyphicon glyphicon-calendar"></i></button>
                                            </span>

                                    </p>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldDataDaFesta', mainForm , {required: 'A data que vai rolar a festa � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldDataDaFesta', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>
                            </div>

                            <div class='form-group'
                                 ng-if="vm.showCampoEnderecoAutocomplete"
                                 ng-class="vm.showMensagemEnderecoInvalido ? 'has-error' : ''">

                                <label class='col-md-3 control-label' for='fieldEnderecoAutocomplete'>Informe o
                                    Endere�o</label>

                                <div class='col-md-6'>

                                    <input class='form-control'
                                           ng-disabled="vm.disabledFields.fieldEnderecoAutocomplete"

                                           placeholder=''
                                           name="fieldEnderecoAutocomplete"
                                           id="fieldEnderecoAutocomplete"
                                           type='text'>

                                    <span class='help-block' ng-if="vm.showMensagemEnderecoInvalido">
                                        <p>Endere�o Inv�lido</p>
                                    </span>

                                </div>
                            </div>

                            <div class='form-group'
                                 ng-if="vm.isEnderecoGooglePlacesSelecionado"
                                 ng-class="getFormControlCssClass(mainForm.fieldEnderecoLogradouro)">

                                <label class='col-md-3 control-label' for='fieldEnderecoLogradouro'>Logradouro</label>

                                <div class='col-md-6'>

                                    <input class='form-control'
                                           ng-disabled="vm.disabledFields.fieldEnderecoLogradouro"

                                           placeholder=''
                                           name="fieldEnderecoLogradouro"
                                           ng-model="vm.formDataEndereco.logradouro"
                                           maxlength="255"
                                           type='text'>

                                    <span class='help-block'
                                          ng-show="showValidationMessage('fieldLogradouro', mainForm, { maxlength: 'O tamanho m�ximo do logradouro deve ser de 255 caracteres.', required: 'Campo Obrigat�rio'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldLogradouro', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>

                            </div>

                            <div class='form-group'
                                 ng-if="vm.isEnderecoGooglePlacesSelecionado"
                                 ng-class="getFormControlCssClass(mainForm.fieldEnderecoNumero)">

                                <label class='col-md-3 control-label' for='fieldEnderecoLogradouro'>N�mero</label>

                                <div class='col-md-6'>

                                    <input type="text"
                                           ng-model="vm.formDataEndereco.numero"
                                           class="form-control"
                                           ng-disabled="vm.disabledFields.fieldEnderecoNumero"

                                           name="fieldEnderecoNumero"
                                           id="fieldEnderecoNumero"
                                           placeholder=""
                                           omega-control-type="TextInput"
                                           maxlength="20"/>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldEnderecoNumero', mainForm , {maxlength: 'O tamanho m�ximo do n�mero deve ser de 20 caracteres.', required: 'O N�mero � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldEnderecoNumero', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>

                            </div>

                            <div class='form-group'
                                 ng-if="vm.isEnderecoGooglePlacesSelecionado"
                                 ng-class="getFormControlCssClass(mainForm.fieldEnderecoComplemento)">

                                <label class='col-md-3 control-label' for='fieldEnderecoComplemento'>Complemento</label>

                                <div class='col-md-6'>

                                    <input type="text"
                                           ng-model="vm.formDataEndereco.complemento"
                                           class="form-control"
                                           ng-disabled="vm.disabledFields.fieldEnderecoComplemento"

                                           name="fieldEnderecoComplemento"
                                           id="fieldEnderecoComplemento"
                                           placeholder=""
                                           omega-control-type="TextInput"
                                           maxlength="20"/>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldEnderecoComplemento', mainForm , {maxlength: 'O tamanho m�ximo do complemento deve ser de 20 caracteres.', required: 'O N�mero � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldEnderecoComplemento', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>

                            </div>

                            <div class='form-group'
                                 ng-if="vm.isEnderecoGooglePlacesSelecionado"
                                 ng-class="getFormControlCssClass(mainForm.fieldEnderecoBairro)">

                                <label class='col-md-3 control-label' for='fieldEnderecoBairro'>Bairro</label>

                                <div class='col-md-6'>

                                    <input type="text"
                                           ng-model="vm.formDataEndereco.bairro"
                                           class="form-control"
                                           ng-disabled="vm.disabledFields.fieldEnderecoBairro"

                                           name="fieldEnderecoBairro"
                                           id="fieldEnderecoBairro"
                                           placeholder=""
                                           omega-control-type="TextInput"
                                           maxlength="100"/>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldEnderecoBairro', mainForm , {maxlength: 'O tamanho m�ximo do bairro deve ser de 100 caracteres.', required: 'O N�mero � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldEnderecoBairro', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>

                            </div>

                            <div class='form-group'
                                 ng-if="vm.isEnderecoGooglePlacesSelecionado"
                                 ng-class="getFormControlCssClass(mainForm.fieldEnderecoMunicipio)">

                                <label class='col-md-3 control-label' for='fieldEnderecoMunicipio'>Munic�pio</label>

                                <div class='col-md-6'>

                                    <input type="text"
                                           ng-model="vm.formDataEndereco.municipio"
                                           class="form-control"
                                           ng-disabled="vm.disabledFields.fieldEnderecoMunicipio"

                                           name="fieldEnderecoMunicipio"
                                           id="fieldEnderecoMunicipio"
                                           placeholder=""
                                           omega-control-type="TextInput"
                                           maxlength="100"/>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldEnderecoMunicipio', mainForm , {maxlength: 'O tamanho m�ximo do munic�pio deve ser de 100 caracteres.', required: 'O N�mero � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldEnderecoMunicipio', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>

                            </div>

                            <div class='form-group'
                                 ng-if="vm.isEnderecoGooglePlacesSelecionado"
                                 ng-class="getFormControlCssClass(mainForm.fieldEnderecoEstado)">

                                <label class='col-md-3 control-label' for='fieldEnderecoEstado'>Estado</label>

                                <div class='col-md-6'>

                                    <input type="text"
                                           ng-model="vm.formDataEndereco.estado"
                                           class="form-control"
                                           ng-disabled="vm.disabledFields.fieldEnderecoEstado"

                                           name="fieldEnderecoEstado"
                                           id="fieldEnderecoEstado"
                                           placeholder=""
                                           omega-control-type="TextInput"
                                           maxlength="50"/>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldEnderecoEstado', mainForm , {maxlength: 'O tamanho m�ximo do estado deve ser de 50 caracteres.', required: 'O N�mero � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldEnderecoEstado', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <!-- Se��o de cadastrar ou editar barril - Apenas empresa -->
            <div class='row'
                 ng-if="vm.isTipoUsuario(AppConstants.TIPOS_CADASTRO_EMPRESARIAL) && !vm.isEdicaoDoUsuario()"
                 ng-repeat="recordBarril in vm.formDataBarril track by $index">

                <div class='col-sm-12'>

                    <header class="centred" ng-if="vm.isCadastroDoUsuario()">
                        <h2>Cadastrar</h2>
                        <h4>Cerveja</h4>
                    </header>

                    <div class='box'>
                        <div class='box-content'>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm['fieldTipoCerveja' + $index])">
                                <label class='col-md-3 control-label' for="{{'fieldTipoCerveja' + $index }}">Tipo de
                                    Cerveja</label>

                                <div class='col-md-6'>

                                    <select class='form-control'
                                            ng-disabled="vm.disabledFields.fieldTipoCerveja"
                                            ng-required="vm.requiredFields.fieldTipoCerveja"
                                            placeholder=''
                                            name="{{'fieldTipoCerveja' + $index }}"
                                            ng-model="recordBarril.tipoCerveja"
                                            type='text'>

                                        <option ng-repeat="recordCerveja in vm.tiposCerveja track by $index"
                                                value="{{recordCerveja.id}}">{{recordCerveja.nome}}
                                        </option>

                                    </select>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldTipoCerveja' + $index, mainForm , {required: 'O tipo de cerveja � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldTipoCerveja' + $index, mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>
                            </div>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm['fieldVolumeTotalProducao' + $index])">
                                <label class='col-md-3 control-label' for="{{'fieldVolumeTotalProducao' + $index}}">Volume
                                    de Produ��o (em litros)</label>

                                <div class='col-md-6'>

                                    <input class='form-control'
                                           ng-disabled="vm.disabledFields.fieldVolumeTotalProducao"
                                           ng-required="vm.requiredFields.fieldVolumeTotalProducao"
                                           placeholder=""
                                           ui-number-mask="0"
                                           min="{{vm.getCapacidadeMinimaDoBarril()}}"
                                           max="{{vm.getCapacidadeMaximaDoBarril()}}"
                                           name="{{'fieldVolumeTotalProducao' + $index }}"
                                           ng-model="recordBarril.volumeTotalProducao"
                                           type='text'>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldVolumeTotalProducao' + $index, mainForm , {min: 'O valor m�nimo deve ser de ' + vm.getCapacidadeMinimaDoBarril() + ' litros', max: 'O valor m�ximo deve ser de ' + vm.getCapacidadeMaximaDoBarril() + ' litros', required: 'O volume total de produ��o � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldVolumeTotalProducao' + $index, mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>
                            </div>

                            <div class='form-group' ng-class="getFormControlCssClass(mainForm['fieldIBU' + $index])">
                                <label class='col-md-3 control-label' for="{{'fieldIBU' + $index}}">IBU</label>

                                <div class='col-md-6'>

                                    <input class='form-control'
                                           ng-disabled="vm.disabledFields.fieldIBU"
                                           ng-required="vm.requiredFields.fieldIBU"
                                           placeholder=''
                                           min="0"
                                           max="120"
                                           ui-number-mask="0"
                                           name="{{'fieldIBU' + $index }}"
                                           ng-model="recordBarril.IBU"
                                           type='text'>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldIBU' + $index, mainForm , {min: 'O valor m�nimo permitido � 0 (zero)', max: 'O valor m�ximo permitido � 120', required: 'O IBU da cerveja � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldIBU' + $index, mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>
                            </div>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm['fieldPercentualAlcool' + $index])">
                                <label class='col-md-3 control-label' for="{{'fieldPercentualAlcool' + $index}}">Percentual
                                    de Alcool (%)</label>

                                <div class='col-md-6'>

                                    <input class='form-control'
                                           ng-disabled="vm.disabledFields.fieldPercentualAlcool"
                                           ng-required="vm.requiredFields.fieldPercentualAlcool"
                                           placeholder=''
                                           ui-percentage-mask
                                           max="1"
                                           name="{{'fieldPercentualAlcool' + $index}}"
                                           ng-model="recordBarril.percentualAlcool"
                                           type='text'>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldPercentualAlcool' + $index, mainForm , {max: 'O valor m�ximo deve ser de 100%', required: 'O Percentual de alcool da cerveja � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldPercentualAlcool' + $index, mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>
                            </div>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm['fieldFotoInstagram' + $index])">
                                <label class='col-md-3 control-label' for="{{'fieldFotoInstagram' + $index}}">Foto no
                                    Instagram</label>

                                <div class='col-md-6'>

                                    <input type="text"
                                           ng-model="recordBarril.fotoInstagram"
                                           class="form-control"
                                           ng-disabled="vm.disabledFields.fieldFotoInstagram"
                                           ng-required="vm.requiredFields.fieldFotoInstagram"
                                           name="{{'fieldFotoInstagram' + $index}}"
                                           id="fieldFotoInstagram"
                                           placeholder=""
                                           omega-control-type="TextInput"
                                           maxlength="255"/>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldFotoInstagram' + $index, mainForm , {maxlength: 'O tamanho m�ximo do Instragram deve ser de 100 caracteres.', required: 'O Instagram � de preenchimento obrigat�rio.'})">
                                            <p ng-repeat="messages in getValidationMessages('fieldFotoInstagram' + $index, mainForm)">
                                                {{messages}}
                                            </p>
                                        </span>

                                </div>
                            </div>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm['fieldComentariosCerveja' + $index])">
                                <label class='col-md-3 control-label' for="{{'fieldComentariosCerveja' + $index}}">Detalhes
                                    da sua cerveja</label>

                                <div class='col-md-6'>

                                            <textarea
                                                    style="height: 180px;"
                                                    class='form-control'
                                                    ng-model="recordBarril.comentariosCerveja"
                                                    name="{{'fieldComentariosCerveja' + $index}} "
                                                    ng-disabled="vm.disabledFields.fieldComentariosCerveja"
                                                    ng-required="vm.requiredFields.fieldComentariosCerveja"
                                                    placeholder='Breve descritivo da cerveja produzida'
                                                    ng-maxlength="250">
                                            </textarea>

                                    <span class='help-block'
                                          ng-show="showValidationMessage('fieldComentariosCerveja' + $index, mainForm, { maxlength: 'O tamanho m�ximo � de 250 caracteres', required: 'Campo Obrigat�rio'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldComentariosCerveja' + $index, mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>
                            </div>

                            <div class='form-group'>
                                <label class='col-md-3 control-label'>Faz envazamento em garrafas?</label>

                                <div class='col-md-6'>

                                    <div class="scrollable-area dataTables_wrapper">
                                        <table class="table table-bordered table-striped omega-list-table">
                                            <thead>
                                            <tr>
                                                <th class="omega-list-checkbox-th">
                                                    &nbsp;
                                                </th>

                                                <th class="omega-list-record-th">
                                                    Capacidade da Garrafa
                                                </th>

                                                <th class="omega-list-record-th">
                                                    Pre�o
                                                </th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr ng-repeat="recordEmbalagem in recordBarril.tiposEmbalagensGarrafa track by $index">
                                                <td class="omega-list-checkbox-td">
                                                    <input type="checkbox" ng-model="recordEmbalagem.isProduzida">
                                                </td>

                                                <td class="omega-list-record-content-td">
                                                    {{recordEmbalagem.descricao}}
                                                </td>
                                                <td class="omega-list-record-content-td">
                                                    <input class='form-control'
                                                           ng-disabled="!recordEmbalagem.isProduzida"
                                                           ng-required="recordEmbalagem.isProduzida"
                                                           placeholder=''
                                                           ui-money-mask="2"
                                                           name="{{'campoPreco' + $index}}"
                                                           ng-model="recordEmbalagem.preco"
                                                           type='text'>
                                                </td>

                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>

                            </div>

                            <div class='form-group'>
                                <label class='col-md-3 control-label'>Faz envazamento em keg?</label>

                                <div class='col-md-6'>

                                    <div class="scrollable-area dataTables_wrapper">
                                        <table class="table table-bordered table-striped omega-list-table">
                                            <thead>
                                            <tr>
                                                <th class="omega-list-checkbox-th">
                                                    &nbsp;
                                                </th>

                                                <th class="omega-list-record-th">
                                                    Capacidade do Keg
                                                </th>

                                                <th class="omega-list-record-th">
                                                    Pre�o
                                                </th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr ng-repeat="recordEmbalagem in recordBarril.tiposEmbalagensKeg track by $index">
                                                <td class="omega-list-checkbox-td">
                                                    <input type="checkbox" ng-model="recordEmbalagem.isProduzida">
                                                </td>

                                                <td class="omega-list-record-content-td">
                                                    {{recordEmbalagem.descricao}}
                                                </td>
                                                <td class="omega-list-record-content-td">
                                                    <input class='form-control'
                                                           ng-disabled="!recordEmbalagem.isProduzida"
                                                           ng-required="recordEmbalagem.isProduzida"
                                                           placeholder=''
                                                           ui-money-mask="2"
                                                           name="{{'campoPreco' + $index}}"
                                                           ng-model="recordEmbalagem.preco"
                                                           type='text'>
                                                </td>

                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <!-- Se��o de cadastrar ou editar festa - Apenas degustador -->
            <div class='row'
                 ng-if="(vm.isCadastroDoBarril() || vm.isEdicaoDoBarril()) && vm.isTipoUsuario(AppConstants.TIPO_CADASTRO.DEGUSTADOR)">

                <div class='col-sm-12'>

                    <header class="centred">
                        <h2>Cerveja</h2>
                        <h4>da festa</h4>
                    </header>

                    <div class='box'>
                        <div class='box-content'>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldCervejaFestaValorPorLitro)">
                                <label class='col-md-3 control-label' for="fieldCervejaFestaValorPorLitro">Valor por
                                    Litro</label>

                                <div class='col-md-6'>

                                    <input class='form-control'
                                           ng-disabled="vm.disabledFields.fieldCervejaFestaValorPorLitro"
                                           ng-required="vm.requiredFields.fieldCervejaFestaValorPorLitro"
                                           placeholder=''
                                           ui-money-mask="2"
                                           min="{{vm.getCervejaFestaValorMinimo()}}"
                                           max="{{vm.getCervejaFestaValorMaximo()}}"
                                           name="fieldCervejaFestaValorPorLitro"
                                           ng-model="vm.formDataFesta.valorPorLitro"
                                           maxlength="8"
                                           type='text'>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldCervejaFestaValorPorLitro', mainForm , {min: 'O valor m�nimo � de ' + vm.getCervejaFestaValorMinimo() + ' reais.', max: 'O valor m�ximo � de ' + vm.getCervejaFestaValorMaximo() + ' reais.', required: 'O valor do litro de cerveja � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldCervejaFestaValorPorLitro', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>
                            </div>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldCervejaFestaVolumeTotalEmLitros)">
                                <label class='col-md-3 control-label' for="fieldCervejaFestaVolumeTotalEmLitros">Quantidade
                                    de cerveja (em litros)</label>

                                <div class='col-md-6'>

                                    <input class='form-control'
                                           ng-disabled="vm.disabledFields.fieldCervejaFestaVolumeTotalEmLitros"
                                           ng-required="vm.requiredFields.fieldCervejaFestaVolumeTotalEmLitros"
                                           placeholder=""
                                           ui-number-mask="0"
                                           min="{{vm.getCervejaFestaVolumeMinimo()}}"
                                           max="{{vm.getCervejaFestaVolumeMaximo()}}"
                                           name="fieldCervejaFestaVolumeTotalEmLitros"
                                           ng-model="vm.formDataFesta.volumeTotalEmLitros"
                                           maxlength="6"
                                           type='text'>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldCervejaFestaVolumeTotalEmLitros', mainForm , {min: 'A quantidade m�nima de cerveja da festa deve ser de ' + vm.getCapacidadeMinimaDoBarril() + ' litros', max: 'A quantidade m�xima de cerveja da festa deve ser de ' + vm.getCapacidadeMaximaDoBarril() + ' litros', required: 'A quantidade de cerveja da festa � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldCervejaFestaVolumeTotalEmLitros', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>
                            </div>

                            <div class='form-group'
                                 ng-if="vm.showSimulacaoValores()">

                                <label class='col-md-3 control-label'>Simula��o de Valor</label>

                                <div class='col-md-6'>

                                    <div class="scrollable-area dataTables_wrapper">
                                        <table class="table table-bordered table-striped omega-list-table">
                                            <thead>
                                            <tr>

                                                <th class="omega-list-record-th">
                                                    Embalagem
                                                </th>

                                                <th class="omega-list-record-th"
                                                    ng-if="!isNullOrEmpty(vm.formDataFesta.valorPorLitro) && vm.formDataFesta.valorPorLitro > 0">
                                                    Valor
                                                </th>

                                                <th class="omega-list-record-th"
                                                    ng-if="!isNullOrEmpty(vm.formDataFesta.volumeTotalEmLitros) && vm.formDataFesta.volumeTotalEmLitros > 0">
                                                    Quantidade
                                                </th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr ng-repeat="recordEmbalagem in vm.tiposEmbalagensDegustador track by $index">

                                                <td class="omega-list-record-content-td">
                                                    {{recordEmbalagem.descricao}}
                                                </td>
                                                <td class="omega-list-record-content-td"
                                                    ng-if="!isNullOrEmpty(vm.formDataFesta.valorPorLitro) && vm.formDataFesta.valorPorLitro > 0">
                                                    {{vm.getPrecoSimuladoEmbalagem(recordEmbalagem)}}
                                                </td>

                                                <td class="omega-list-record-th"
                                                    ng-if="!isNullOrEmpty(vm.formDataFesta.volumeTotalEmLitros) && vm.formDataFesta.volumeTotalEmLitros > 0">
                                                    {{vm.getQuantidadeSimuladaEmbalagem(recordEmbalagem)}}
                                                </td>

                                            </tr>

                                            </tbody>

                                        </table>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <!-- Se��o de cadastrar ou editar dados banc�rios - Apenas degustador -->
            <div class='row'
                 ng-if="(vm.isCadastroDoBarril() || vm.isEdicaoDoBarril()) && vm.isTipoUsuario(AppConstants.TIPO_CADASTRO.DEGUSTADOR)">

                <div class='col-sm-12'>

                    <header class="centred">
                        <h2>Dados Banc�rios</h2>
                        <h4>para receber a vaquinha</h4>
                    </header>

                    <div class='box'>
                        <div class='box-content'>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldDadosBancariosBanco)">

                                <label class='col-md-3 control-label' for='fieldDadosBancariosBanco'>Banco</label>

                                <div class='col-md-6'>

                                    <select class='form-control'
                                            ng-disabled="vm.disabledFields.fieldDadosBancariosBanco"

                                            placeholder=''
                                            name="fieldDadosBancariosBanco"
                                            ng-model="vm.formDataFesta.dadosBancarios.banco"
                                            ng-change="vm.onChangeBanco()"
                                            type='text'>

                                        <option ng-repeat="recordBancoFebraban in vm.bancosFebraban track by $index"
                                                value="{{recordBancoFebraban.Codigo}}">
                                            {{recordBancoFebraban.Nome}} (C�digo {{recordBancoFebraban.Codigo}})
                                        </option>

                                    </select>

                                    <span class='help-block'
                                          ng-show="showValidationMessage('fieldDadosBancariosBanco', mainForm, { required: 'O banco � de preenchimento obrigat�rio'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldDadosBancariosBanco', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>

                            </div>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldDadosBancariosTitular)">

                                <label class='col-md-3 control-label' for='fieldDadosBancariosTitular'>Nome do Titular
                                    da Conta</label>

                                <div class='col-md-6'>

                                    <input class='form-control'
                                           ng-disabled="vm.disabledFields.fieldDadosBancariosTitular"

                                           placeholder=''
                                           name="fieldDadosBancariosTitular"
                                           ng-model="vm.formDataFesta.dadosBancarios.titular"
                                           maxlength="255"
                                           type='text'>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldDadosBancariosTitular', mainForm , { maxlength: 'O nome do titular deve ter no m�ximo 255 caracteres.', required: 'O titular da conta � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldDadosBancariosTitular', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>
                            </div>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldDadosBancariosCpf)">

                                <label class='col-md-3 control-label' for='fieldDadosBancariosCpf'>CPF do
                                    Titular</label>

                                <div class='col-md-6'>

                                    <input class='form-control'
                                           ng-disabled="vm.disabledFields.fieldDadosBancariosCpf"

                                           placeholder=''
                                           ui-br-cpf-mask
                                           name="fieldDadosBancariosCpf"
                                           ng-model="vm.formDataFesta.dadosBancarios.cpf"
                                           type='text'>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldDadosBancariosCpf', mainForm , {cpf: 'CPF inv�lido', required: 'O CPF � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldDadosBancariosCpf', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>
                            </div>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldDadosBancariosAgencia)">

                                <label class='col-md-3 control-label' for='fieldDadosBancariosAgencia'>Ag�ncia</label>

                                <div class='col-md-6'>

                                    <input type="text"
                                           ng-model="vm.formDataFesta.dadosBancarios.agencia"
                                           class="form-control"
                                           ng-disabled="vm.disabledFields.fieldDadosBancariosAgencia"

                                           name="fieldDadosBancariosAgencia"
                                           id="fieldDadosBancariosAgencia"
                                           placeholder=""
                                           ui-number-mask="0"
                                           ui-hide-group-sep
                                           min="1"
                                           max="99999"
                                           maxlength="5"
                                    />

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldDadosBancariosAgencia', mainForm , {min: 'Ag�ncia inv�lida', max: 'O n�mero da ag�ncia possui de 1 a 5 d�gitos num�ricos', required: 'O n�mero da ag�ncia � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldDadosBancariosAgencia', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>

                            </div>

                            <div class='form-group'
                                 ng-if="vm.liberarCampoDadosBancariosTipoConta()"
                                 ng-class="getFormControlCssClass(mainForm.fieldDadosBancariosTipoConta)">

                                <label class='col-md-3 control-label' for='fieldDadosBancariosTipoConta'>Tipo de
                                    Conta</label>

                                <div class='col-md-6'>

                                    <select class='form-control'
                                            ng-disabled="vm.disabledFields.fieldDadosBancariosTipoConta"

                                            placeholder=''
                                            name="fieldDadosBancariosTipoConta"
                                            ng-model="vm.formDataFesta.dadosBancarios.tipoConta"
                                            type='text'>

                                        <option ng-repeat="recordTipoContaBancaria in vm.tiposContasBancarias track by $index"
                                                value="{{recordTipoContaBancaria.chave}}">
                                            {{recordTipoContaBancaria.descricao}}
                                        </option>

                                    </select>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldDadosBancariosTipoConta', mainForm , { required: 'O tipo de conta � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldDadosBancariosTipoConta', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>
                            </div>

                            <div class='form-group'
                                 ng-if="vm.liberarCampoDadosBancariosOperacao()"
                                 ng-class="getFormControlCssClass(mainForm.fieldDadosBancariosOperacao)">

                                <label class='col-md-3 control-label' for='fieldDadosBancariosOperacao'>Opera��o</label>

                                <div class='col-md-6'>

                                    <select class='form-control'
                                            ng-disabled="vm.disabledFields.fieldDadosBancariosOperacao"

                                            placeholder=''
                                            name="fieldDadosBancariosOperacao"
                                            ng-model="vm.formDataFesta.dadosBancarios.operacao"
                                            type='text'>

                                        <option ng-repeat="recordOperacaoBancaria in vm.operacoesBancarias track by $index"
                                                value="{{recordOperacaoBancaria.Codigo}}">
                                            {{recordOperacaoBancaria.Nome}} (C�digo {{recordOperacaoBancaria.Codigo}})
                                        </option>

                                    </select>

                                    <span class='help-block'
                                          ng-show="showValidationMessage('fieldDadosBancariosOperacao', mainForm, { required: 'Campo Obrigat�rio'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldDadosBancariosOperacao', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>

                            </div>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldDadosBancariosConta)">

                                <label class='col-md-3 control-label' for='fieldDadosBancariosConta'>N�mero da
                                    conta</label>

                                <div class='col-md-6'>

                                    <input type="text"
                                           ng-model="vm.formDataFesta.dadosBancarios.conta"
                                           class="form-control"
                                           ng-disabled="vm.disabledFields.fieldDadosBancariosConta"

                                           name="fieldDadosBancariosConta"
                                           id="fieldDadosBancariosConta"
                                           placeholder=""
                                           ui-number-mask="0"
                                           ui-hide-group-sep
                                           min="1"
                                           max="99999999999999999999"
                                           maxlength="20"/>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldDadosBancariosConta', mainForm , {min: 'O n�mero da conta-corrente � inv�lido', max: 'O n�mero da conta-corrente � inv�lido', required: 'O n�mero da conta � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldDadosBancariosConta', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>

                            </div>

                            <div class='form-group'
                                 ng-class="getFormControlCssClass(mainForm.fieldDadosBancariosConta)">

                                <label class='col-md-3 control-label' for='fieldDadosBancariosConta'>D�gito verificador
                                    da conta</label>

                                <div class='col-md-6'>

                                    <input type="text"
                                           ng-model="vm.formDataFesta.dadosBancarios.digitoVerificadorConta"
                                           class="form-control"
                                           ng-disabled="vm.disabledFields.fieldDadosBancariosDigitoVerificadorConta"

                                           name="fieldDadosBancariosDigitoVerificadorConta"
                                           id="fieldDadosBancariosDigitoVerificadorConta"
                                           placeholder=""
                                           maxlength="1"/>

                                    <span class="help-block"
                                          ng-show="showValidationMessage('fieldDadosBancariosDigitoVerificadorConta', mainForm , {maxlength: 'D�gito verificador da conta inv�lido', required: 'O d�gito verificador da conta corrente � de preenchimento obrigat�rio.'})">
                                        <p ng-repeat="messages in getValidationMessages('fieldDadosBancariosDigitoVerificadorConta', mainForm)">
                                            {{messages}}
                                        </p>
                                    </span>

                                </div>

                            </div>


                        </div>

                    </div>

                </div>

            </div>

            <!-- Bot�o de gravar dados -->
            <div class='form-actions form-actions-padding-sm btn-wrap'>
                <div class='row'>
                    <div class='col-md-10 col-md-offset-2'>
                        <button class='btn btn-default' type='submit' ng-click="vm.submeterDados(mainForm)">
                            <i class='icon-save'></i>
                            Prosseguir
                        </button>
                    </div>
                </div>
            </div>

        </form>

    </div>
</div>

<!--Defini��o do Controller Angular JS -->
<script type="text/javascript" src="angular/app/cadastro.cervejeiro.controller.js"></script>
