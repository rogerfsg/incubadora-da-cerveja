<script type="text/javascript">

    <?php

    echo "var idBarrilPedido = " . Helper::POSTGET("idDoBarril");

    ?>

</script>

<div id='content-wrapper' ng-app="omegaApp">
    <table class="tsel" ng-cloak border="0" width="100%" ng-controller="exibirCameraQrCodeController as vm">
        <tr>
            <td valign="top" align="center" width="50%">
                <table class="tsel" border="0">
                    <tr ng-show="vm.getEstado() == vm.ESTADO_RASTREANDO">

                        <td>
                            <p>
                            <h4>
                                <img src="imgs/especifico/loading.gif"
                                     style="height:25px; width: 25px; margin-left: 20px; margin-right: 15px;"/>
                                Aponte o QR Code do convite na camera...
                            </h4>
                            </p>
                            <div id="result">
                            </div>
                        </td>
                    </tr>

                    <tr ng-hide="vm.estadoSeq != vm.ESTADO_RASTREANDO">
                        <td colspan="2" align="center">

                            <div id="outdiv">
                            </div>
                        </td>
                    </tr>
                    <tr ng-hide="vm.estadoSeq != vm.ESTADO_RASTREANDO">
                        <td colspan="2" align="center">
                            <p>
                            <h4>
                                <button class="btn btn-sm btn-info"
                                        type="button"
                                        id="webcamimg"
                                        onclick="setwebcam()">
                                    <i class="glyphicon glyphicon-send "></i>
                                    &nbsp;
                                    Trocar camera
                                </button>
                            </h4>
                            </p>
                        </td>
                    </tr>
                    <tr ng-hide="vm.estadoSeq != vm.ESTADO_QUANTIDADE_ML">
                        <td colspan="2">

                            <form name="mainForm"
                                  class="col-sm-8 col-sm-push-2 form form-horizontal"
                                  style="margin-bottom: 0;">

                                <div class='row'>
                                    <div class='col-sm-12'>

                                        <header class="centred">
                                            <h1>Cerveja</h1>
                                            <h2>Quantos ml?</h2>
                                        </header>

                                        <div class='box'>
                                            <div class='box-content'>

                                                <div class='form-group' ng-hide="vm.qtdMlSaldoAtual == null">

                                                    <label class='col-md-6 control-label' for='fieldEmail'>
                                                        <h4>Saldo atual</h4></label>

                                                    <div class='col-md-6'>

                                                        <span ng-bind="vm.qtdMlSaldoAtual"></span>
                                                    </div>
                                                </div>
                                                <div class='form-group'
                                                     ng-class="getFormControlCssClass(mainForm.fieldQtdMlCerveja)">

                                                    <label class='col-md-6 control-label' for='fieldEmail'>
                                                        <h4>Quantidade de cerveja (ml)</h4></label>

                                                    <div class='col-md-6'>

                                                        <input class='form-control'
                                                               placeholder=''
                                                               ng-model="vm.qtdMlEscolhida"
                                                               type='number'>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class='form-actions form-actions-padding-sm btn-wrap'>
                                    <div class='row'>
                                        <div class='col-md-8 col-md-offset-1'>
                                            <button class='btn btn-success' type='submit'
                                                    ng-click="vm.requisitaAprovacaoPedido()">
                                                <i class='icon-save'></i>
                                                Pedir
                                            </button>
                                        </div>


                                        <div class='col-md-10'>
                                            <button class='btn btn-default btn-sm' ng-click="vm.concluir()">
                                                <i class='glyphicon-remove'></i>
                                                Retornar
                                            </button>
                                        </div>
                                    </div>

                                </div>

                            </form>

                        </td>
                    </tr>

                    <tr ng-show="vm.estadoSeq == vm.ESTADO_REQUISITAR_PEDIDO">
                        <td colspan="2">
                            <img src="imgs/especifico/loading.gif" style="height:42px; width: 42px;"/>
                        </td>
                    </tr>

                    <tr ng-show="vm.estadoSeq == vm.ESTADO_COMPROVANTE">
                        <td colspan="2">

                            <form name="mainForm"
                                  class="col-sm-8 col-sm-push-2 form form-horizontal"
                                  style="margin-bottom: 0;">

                                <div class='row'>
                                    <div class='col-sm-12'>

                                        <header class="centred">
                                            <h1>Finaliza��o pedido</h1>
                                            <h2>Segue o status do pedido</h2>
                                        </header>

                                        <div class='box'>
                                            <div class='box-content'>

                                                <div class='form-group' ng-hide="vm.qtdMlSaldoAtual == null">

                                                    <label class='col-md-6 control-label' for='fieldEmail'>
                                                        <h4>Saldo atual</h4></label>

                                                    <div class='col-md-6'>

                                                        <h4 ng-bind="vm.qtdMlSaldoAtual"></h4>
                                                    </div>
                                                </div>
                                                <div ng-class="getFormControlCssClass(mainForm.fieldQtdMlCerveja)">

                                                    <label class='col-md-6 control-label' for='fieldEmail'>
                                                        <h4>Resultado</h4></label>

                                                    <div class='col-md-6'>

                                                        <h4 ng-bind="vm.resultadoOperacao"></h4>
                                                    </div>
                                                </div>
                                                <div class='form-actions form-actions-padding-sm btn-wrap'>
                                                    <div class='row'>

                                                        <div class='col-md-8 col-md-offset-2'>
                                                            <button class='btn btn-success btn-sm' type='button'
                                                                    ng-click="vm.concluir()">
                                                                <i class='icon-save'></i>
                                                                Pr�ximo
                                                            </button>
                                                        </div>

                                                        <div class='col-md-8'
                                                             ng-show="vm.requisicaoPedidoRealizadaComSucesso == true">
                                                            <button class='btn btn-danger btn-sm'
                                                                    ng-click="vm.cancelarPedido()">
                                                                <i class='glyphicon-remove'></i>
                                                                Cancelar pedido
                                                            </button>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </form>

                        </td>

                    </tr>

                </table>

            </td>

        </tr>

    </table>

</div>&nbsp;

<canvas id="qr-canvas" width="800" height="600" style="display: none;"></canvas>
<script type="text/javascript">

    // QRCODE reader Copyright 2011 Lazar Laszlo
    // http://www.webqr.com
    var gCtx = null;
    var gCanvas = null;
    var c = 0;
    var stype = 0;
    var gUM = false;
    var webkit = false;
    var moz = false;
    var v = null;
    var imghtml = '<div id="qrfile"><canvas id="out-canvas" width="320" height="240"></canvas>' +
        '<div id="imghelp">drag and drop a QRCode here' +
        '<br>or select a file' +
        '<input type="file" onchange="handleFiles(this.files)"/>' +
        '</div>' +
        '</div>';
    var vidhtml = '<video id="v" autoplay></video>';
    function initCanvas(w, h)
    {
        gCanvas = document.getElementById("qr-canvas");
        gCanvas.style.width = w + "px";
        gCanvas.style.height = h + "px";
        gCanvas.width = w;
        gCanvas.height = h;
        gCtx = gCanvas.getContext("2d");
        gCtx.clearRect(0, 0, w, h);
    }
    function captureToCanvas()
    {
        if (stype != 1)
        {
            return;
        }
        if (gUM)
        {
            try
            {
                gCtx.drawImage(v, 0, 0);
                try
                {
                    qrcode.decode();
                }
                catch (e)
                {
                    console.log(e);
                    setTimeout(captureToCanvas, 500);
                }
            }
            catch (e)
            {
                console.log(e);
                setTimeout(captureToCanvas, 500);
            }
        }
    }
    function htmlEntities(str)
    {
        return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
    }
    var reading = false;
    function read(a)
    {
        if (reading)
        {
            return;
        }
        else
        {
            try
            {
                reading = true;
                setTimeout(audioAbrirLata, 500);
                var controllerScope = getCurrentControllerAngular();
                if (controllerScope == null)
                {
                    return null;
                }
                else
                {
                    var $rootScope = controllerScope.$root;
                    $rootScope.$emit('qrCodeRead', {
                        message: 'Qr Code',
                        a: a,
                        idBarrilPedido: idBarrilPedido
                    });
                }
            }
            finally
            {
                reading = false;
            }
        }
    }
    var onceAudio = false;
    async function audioAbrirLata()
    {
        if (onceAudio)
        {
            return;
        }
        else
        {
            try
            {
                onceAudio = true;
                var audio = new Audio('audio/beep.wav');
                audio.play();
                await sleep(1000);
            }
            finally
            {
                onceAudio = false;
            }
        }
    }
    function isCanvasSupported()
    {
        var elem = document.createElement('canvas');
        return !!(elem.getContext && elem.getContext('2d'));
    }
    function success(stream)
    {
        v.srcObject = stream;
        v.play();
        gUM = true;
        setTimeout(captureToCanvas, 500);
    }
    function error(error)
    {
        gUM = false;
        return;
    }
    function load()
    {
        if (isCanvasSupported() && window.File && window.FileReader)
        {
            initCanvas(600, 400);
            qrcode.callback = read;
            document.getElementById("content-wrapper").style.display = "inline";
            setwebcam();
        }
        else
        {
            document.getElementById("content-wrapper").style.display = "inline";
            document.getElementById("content-wrapper").innerHTML = '<p id="mp1">Browser n�o � compart�vel</p><br>' +
                '<br><p id="mp2">Desculpe apenas browser que suportam HTML5</p><br><br>';
        }
    }
    function setwebcam()
    {
        var options = true;
        if (navigator.mediaDevices && navigator.mediaDevices.enumerateDevices)
        {
            try
            {
                navigator.mediaDevices.enumerateDevices()
                    .then(function (devices)
                    {
                        devices.forEach(function (device)
                        {
                            if (device.kind === 'videoinput')
                            {
                                if (device.label.toLowerCase().search("back") > -1)
                                {
                                    options = {'deviceId': {'exact': device.deviceId}, 'facingMode': 'environment'};
                                }
                            }
                            console.log(device.kind + ": " + device.label + " id = " + device.deviceId);
                        });
                        setwebcam2(options);
                    });
            }
            catch (e)
            {
                console.log(e);
            }
        }
        else
        {
            console.log("no navigator.mediaDevices.enumerateDevices");
            setwebcam2(options);
        }
    }
    function setwebcam2(options)
    {
        console.log(options);
        if (stype == 1)
        {
            setTimeout(captureToCanvas, 500);
            return;
        }
        var n = navigator;
        document.getElementById("outdiv").innerHTML = vidhtml;
        v = document.getElementById("v");
        if (n.mediaDevices.getUserMedia)
        {
            n.mediaDevices.getUserMedia({video: options, audio: false}).then(function (stream)
            {
                success(stream);
            }).catch(function (error)
            {
                error(error)
            });
        }
        else
        {
            if (n.getUserMedia)
            {
                webkit = true;
                n.getUserMedia({video: options, audio: false}, success, error);
            }
            else
            {
                if (n.webkitGetUserMedia)
                {
                    webkit = true;
                    n.webkitGetUserMedia({video: options, audio: false}, success, error);
                }
            }
        }
        document.getElementById("webcamimg").style.opacity = 1.0;
        stype = 1;
        setTimeout(captureToCanvas, 500);
    }
    function setimg()
    {
        document.getElementById("result").innerHTML = "";
        if (stype == 2)
        {
            return;
        }
        document.getElementById("outdiv").innerHTML = imghtml;
        //document.getElementById("webcamimg").src="webcam2.png";
        document.getElementById("webcamimg").style.opacity = 0.2;
        stype = 2;
    }
    function sleep(ms)
    {
        return new Promise(resolve => setTimeout(resolve, ms));

    }
    load();

</script>
