<!--Defini��o da View Angular JS -->
<div class='row' id='content-wrapper'>
    <div class='col-sm-12 no-padding'>

        <div class="container-fluid dark section no-padding margin-bottom-20">
            <div class="row">
                <div class="col-sm-12 icon-grid">
                    <img src="imgs/template/Hops.svg" class="svg"/>
                    <h4>Pol�tica de Uso</h4>

                </div>
            </div>
        </div>


        <div
                class="col-sm-8 col-sm-push-2 form form-horizontal"
                style="margin-bottom: 0;">


            <div class='box'>
                <div class='box-content'>
                    <div novalidate>


                        <div class='box-content'>
                            <h4 class='col-form-label'>Pol�tica de Privacidade e Termos de Uso - Facebook</h4>
                            <p>
                                26 de mar�o de 2018 �s 18:50
                            </p>
                            <p>
                                O usu�rio da p�gina da INCUBADORA DA CERVEJA, ao acess�-la,reconhece e aceita que
                                concorda com todas as condi��es e regras desta Pol�tica de Privacidade e da Pol�tica do
                                Facebook (https://www.facebook.com/about/privacy/), implicando, dessa forma, na
                                presun��o de conhecimento e aceita��o integral e irrestrita destas Pol�ticas.
                            </p>
                            <p>
                                O usu�rio se responsabilizar� exclusiva e integralmente pelos dados inseridos na p�gina
                                acima mencionada, isentando a INCUBADORA DA CERVEJA de qualquer responsabilidade pela
                                veracidade e/ou eventuais incorre��es nos dados inseridos.
                            </p>
                            <p>
                                O usu�rio da p�gina INCUBADORA DA CERVEJA tamb�m assume responsabilidade integral e
                                exclusiva por eventuais viola��es a direitos de imagem,a direitos autorais de terceiros
                                ou a direitos de propriedade industrial,especialmente no que diz respeito �s marcas da
                                INCUBADORA DA CERVEJA, n�o podendo utilizar dados e/ou imagens que n�o sejam de sua
                                propriedade ou que representem uma infra��o a direitos de terceiros, tampouco que violem
                                a moral e os bons costumes.
                            </p>
                            <p>
                                Existem 4 tipos de perfil na INCUBADORA DA CERVEJA, s�o eles:
                            </p>
                            <ul>
                                <li>
                                    <strong>Degustador</strong>
                                    <p>Deve possuir uma conta no facebook</p>
                                </li>
                                <li>
                                    <strong>Cervejaria</strong>
                                    <p>Deve possuir CNPJ ativo e uma conta no facebook
                                    </p>
                                </li>
                                <li>
                                    <strong>Cigano</strong>
                                    <p>Deve possuir CNPJ ativo e uma conta no facebook
                                    </p>
                                </li>
                                <li>
                                    <strong>Brew Pub</strong>
                                    <p>Deve possuir CNPJ ativo e uma conta no facebook
                                    </p>
                                </li>
                            </ul>
                            <p>
                                A sua conta do facebook s� pode estar vinculado a um perfil por vez.
                            </p>
                            <p>
                                O perfil Degustador s� pode utilizar o cadastramento de barril para fim de festa entre
                                amigos. O barril n�o pode ser utilizado para mecanismo de venda. Caso haja contraven��o
                                dessa regra, o barril pode ser removido, e o usu�rio bloqueado.
                            </p>
                            <p>
                                Os perfis de empresa, no caso: Cervejaria, Cigano e Brew Pub; ap�s o cadastramento no
                                site incubadoradacerveja.com.br, ser�o submetidos a uma an�lise interna para comprovar a
                                veracidade da empresa antes da libera��o do acesso ao site.
                            </p>
                            <p>
                                No mais, a INCUBADORA DA CERVEJA se reserva ao direito de remover a qualquer momento e
                                independentemente de aviso pr�vio, qualquer coment�rio e/ou barril publicado na p�gina
                                INCUBADORA DA CERVEJA que contenha:
                            </p>
                            <ul>
                                <li>
                                    termos de baixo cal�o, material agressivo, obsceno e/ou pornogr�fico;
                                </li>
                                <li>
                                    material que possa causar danos a terceiros, atrav�s de difama��o, inj�ria ou
                                    cal�nia;
                                </li>
                                <li>
                                    informa��es que constituam ou possam constituir crime ou contraven��o penal, ou que
                                    possam ser entendidas como apologia a pr�tica de crimes ou contraven��o penal;
                                </li>
                                <li>
                                    dados ou informa��es de cunho discriminat�rio;
                                </li>
                                <li>
                                    o incentivo ao uso de �lcool, drogas, tabaco ou armas de fogo;
                                </li>
                                <li>
                                    propaganda eleitoral ou opini�o referente a partido pol�tico ou candidato;
                                </li>
                                <li>
                                    informa��es confidenciais ou sujeitas a contratos de confidencialidade;
                                </li>
                                <li>
                                    conte�do il�cito e contr�rio � legisla��o.

                                </li>

                            </ul>
                        </div>

                        <div class='box-content'>
                            <h4 class='col-form-label'>COLETA E USO DE DADOS PESSOAIS</h4>
                            <p>
                                Dados pessoais s�o informa��es que permitem identific�-lo,tais como nome, e-mail ou
                                endere�o postal. A INCUBADORA DA CERVEJA compromete-se an�o divulgar seus dados
                                pessoais, n�o transferi-los a terceiros ou utiliz�-los para qualquer finalidade n�o
                                permitida pelas regras desta Pol�tica dePrivacidade e/ou da Pol�tica do Facebook, a
                                menos que voc� nos autorize ou nos forne�a espontaneamente (p.ex., quando se inscreve
                                para receber newsletters, participa de uma pesquisa, concurso ou sorteios, pedidos de
                                amostra ou brochuras, ou requisi��o de informa��es), consentindo, portanto, com seu uso.

                            </p>
                            <p>
                                Dados sobre o gosto de cerveja do perfil Degustador, como por exemplo os tipos de
                                cerveja: Pale Ale e Pilsen. Poder�o ser repassados a usu�rios dos perfis: Cervejaria,
                                Brew Pub e Cigano. J� no caso do perfil Degustador dono da festa, os dados dos
                                convidados do perfil Degustador ficar�o dispon�veis se eles confirmarem a presen�a na
                                festa.

                            </p>
                            <p>
                                Ficam excepcionados das condi��es de uso de dados pessoais, acima mencionadas, os casos
                                em que haja requerimento de autoridade administrativa ou judicial para o fornecimento
                                desses dados.
                            </p>
                        </div>

                        <div class='box-content'>
                            <h4 class='col-form-label'>USO DE INFORMA��ES COMPARTILHADAS</h4>
                            <p>
                                As informa��es compartilhadas na p�gina INCUBADORA DA CERVEJA abrangem as publica��es
                                feitas pelos usu�rios diretamente na p�gina INCUBADORA DA CERVEJA, as atualiza��es de
                                status dos usu�rios nas quais a p�gina INCUBADORA DA CERVEJA � marcada, as fotos e
                                imagens carregadas pelos usu�rios diretamente na p�gina INCUBADORA DA CERVEJA ou nas
                                quais a p�gina INCUBADORA DA CERVEJA � marcada ou, ainda, coment�rios e/ou barris feitos
                                pelos usu�rios em publica��es da pr�pria p�gina INCUBADORA DA CERVEJA, ou que remetam �
                                p�gina INCUBADORA DA CERVEJA, incluindo, mas n�o se limitando ao uso de hashtags.


                            </p>
                            <p>
                                Uma vez que a p�gina INCUBADORA DA CERVEJA � uma p�gina p�blica, as informa��es e
                                conte�dos compartilhados com e na p�gina INCUBADORA DA CERVEJA tamb�m s�o informa��es
                                p�blicas.

                            </p>
                            <p>
                                Ao inserir dados, informa��es, imagens e conte�dos na p�gina INCUBADORA DA CERVEJA, o
                                usu�rio expressamente declara possuir todos os direitos, licen�as e autoriza��es que
                                possam ser necess�rias para a inclus�o de tais dados e informa��es, imagens e conte�do,
                                incluindo, mas sem se limitar a direitos autorais e direito de imagem relacionados a
                                tais dados, informa��es e conte�do.
                            </p>
                            <p>
                                Ao compartilhar informa��es e conte�do na p�gina da INCUBADORA DA CERVEJA, por meio de
                                marca��es da p�gina INCUBADORA DA CERVEJA, por meio do uso de hashtags, ou por outras
                                formas que remetam � p�gina INCUBADORA DA CERVEJA, o usu�rio expressamente autoriza e
                                concorda que tais informa��es e conte�do compartilhados possam ser utilizados e exibidos
                                publicamente pela INCUBADORA DA CERVEJA como publica��es da pr�pria p�gina INCUBADORA DA
                                CERVEJA ou de outras maneiras para a promo��o da p�gina INCUBADORA DA CERVEJA ou das
                                atividades e produtos distinguidos pela marca INCUBADORA DA CERVEJA.
                            </p>
                            <p>
                                A INCUBADORA DA CERVEJA poder�, ainda, coletar e utilizar informa��es, conte�dos, fotos
                                e imagens compartilhadas e que sejam acess�veis por meio do uso de hashtags compostas
                                por palavras que remetam � p�gina INCUBADORA DA CERVEJA.
                            </p>
                        </div>
                        <div class='box-content'>
                            <h4 class='col-form-label'>CONTRIBUI��ES</h4>
                            <p>
                                Ao enviar ideias, sugest�es,documentos e/ou propostas (?Contribui��es?) � p�gina
                                INCUBADORA DA CERVEJA,solicitadas ou n�o, o usu�rio entende e concorda que:
                            </p>
                            <ul>
                                <li>suas Contribui��es n�o cont�m informa��es confidenciais ou de propriedade de
                                    terceiros;
                                </li>
                                <li>a INCUBADORA DA CERVEJA n�o est� sob nenhuma obriga��o de confidencialidade,
                                    impl�cita ou expl�cita, com refer�ncia a tais Contribui��es;
                                </li>
                                <li>a INCUBADORA DA CERVEJA ter� a prerrogativa de usar ou tornar p�blica suas
                                    Contribui��es para qualquer prop�sito, inclusive com car�ter comercial, de qualquer
                                    maneira, em qualquer tipo de m�dia;
                                </li>
                                <li> a INCUBADORA DA CERVEJA pode j� ter algo similar �s Contribui��es em elabora��o
                                    e/ou em desenvolvimento;
                                </li>
                                <li>as Contribui��es do usu�rio ser�o de propriedade da INCUBADORA DA CERVEJA e n�o
                                    gerar�o qualquer obriga��o por parte da INCUBADORA DA CERVEJA em rela��o ao Usu�rio;
                                </li>
                                <li>e o Usu�rio n�o ter� direito a receber pagamento,compensa��o, reembolso ou qualquer
                                    valor de qualquer tipo por parte da INCUBADORA DA CERVEJA, em nenhuma circunst�ncia.
                                </li>
                            </ul>
                        </div>

                        <div class='box-content'>
                            <h4 class='col-form-label'>CONDUTA E RESPONSABILIDADES</h4>
                            <p>
                                O usu�rio reconhece e concorda que n�o poder� utilizar a p�gina INCUBADORA DA CERVEJA
                                para:
                            </p>
                            <ul>
                                <li>divulgar, exibir, enviar ou de qualquer forma tornar dispon�vel conte�do ilegal,
                                    incluindo, mas n�o se limitando, que seja ofensivo � honra, que invada a privacidade
                                    de terceiros, que seja amea�ador, vulgar,obsceno, preconceituoso, racista ou de
                                    qualquer forma censur�vel, ou ainda sem que tenha o direito de faz�-lo de acordo com
                                    a lei ou por for�a de contrato ou de rela��o de confian�a;
                                </li>
                                <li>assumir a personalidade de outra pessoa;</li>
                                <li>divulgar, exibir, enviar ou de qualquer forma tornar dispon�vel conte�do que viole
                                    qualquer patente, marca, segredo de neg�cio,direito autoral, direitos de propriedade
                                    intelectual ou qualquer outro direito de terceiro;
                                </li>
                                <li>divulgar, exibir, enviar ou de qualquer forma tornar dispon�vel qualquer conte�do
                                    que contenha v�rus ou qualquer outro c�digo,arquivo ou programa de computador com o
                                    prop�sito de interromper, destruir ou limitar a funcionalidade de qualquer software,
                                    hardware ou equipamento de telecomunica��es;
                                </li>
                                <li>e violar, seja intencionalmente ou n�o, qualquer norma legal municipal, estadual,
                                    nacional ou internacional que seja integrada ao ordenamento jur�dico brasileiro.
                                </li>
                            </ul>
                        </div>

                        <div class='box-content'>
                            <h4 class='col-form-label'>ATUALIZA��ES E MUDAN�AS</h4>
                            <p>
                                Podemos alterar ou atualizar partes desta Pol�tica a qualquer momento e sem aviso
                                pr�vio. Por favor, volte a verific�-la de tempos em tempos para saber quais mudan�as ou
                                atualiza��es foram feitas.
                            </p>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>