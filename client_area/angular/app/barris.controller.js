omegaApp.controller('barrisController',
    ['$scope', '$rootScope', '$element', '$window', '$interval', '$timeout', '$http', 'barrilService', 'facebookService',
     'comumService',
     function ($scope, $rootScope, $element, $window, $interval, $timeout, $http, barrilService, facebookService, comumService) {
         try
         {
             var vm = this;
             vm.barris = null;
             $rootScope.$on('facebook', function (event, args) {
                 // .. Do whatever you need to do.
                 if (args.message === 'online')
                 {
                     vm.getDadosTelaPrincipal(facebookService.getSessaoFacebook());
                 }
                 else
                 {
                     vm.getDadosTelaPrincipal(null);
                 }
             });
             vm.abrirBarril = function (barril) {
                 barrilService.selecionaBarril(barril.idBarril, facebookService.getSessaoFacebook(), null);
                 var idCervejeiroLogado = barrilService.getIdCervejeiroLogado();
                 var idBarril = barrilService.getBarrilSelecionado();
                 if (barril.idCervejeiro === barrilService.getIdCervejeiroLogado())
                 {
                     $window.location.href = 'index.php?tipo=paginas&page=acompanhamento.festa.degustador&idDoBarril=' +
                         idBarril;
                 }
                 else
                 {
                     if (!comumService.isMobileBrowser())
                     {
                         angular.element('#bt-modal-convite-degustador').trigger('click');
                     }
                     else
                     {
                         $window.location.href = 'index.php?tipo=paginas&page=barril&idDoBarril=' +
                             idBarril;
                     }
                 }
             };
             vm.criarBarril = function (idBarril, dadosBarril) {
                 $timeout(function () {

                     //AppUtils.criarBarril(idBarril, (dadosBarril.totalMlVendido/dadosBarril.totalMl)*100, 50);
                     AppUtils.criarBarril(idBarril, 100, 50);
                 });
             };
             vm.abrirFormularioCadastroBarril = function () {
                 // if(facebookService.getSessaoFacebook() === null || facebookService.getUsrLogadoCookie() === null){
                 //     window.location.href="index.php?tipo=paginas&page=cadastro.cervejeiro";
                 // }else{
                 window.location.href = "index.php?tipo=paginas&page=cadastro.cervejeiro&isCadastroDoBarril=true";
                 // }
             };
             vm.criarBarrilCadastro = function () {
                 $timeout(function () {
                     AppUtils.criarBarril('barril-cadastro', 75, 50);
                 });
             };
             vm.getDadosTelaPrincipal = function (sessaoFacebook) {
                 if (sessaoFacebook == null)
                 {
                     vm.barris = null;
                     vm.barris = null;
                     return;
                 }
                 $http.post(
                     "webservice.php?class=Servicos_web&action=getDadosTelaPrincipal",
                     {
                         sessaoFacebook: sessaoFacebook,
                         hideLoading: true
                     })
                     .then(function (result) {
                             try
                             {
                                 var retorno = arguments[0].data;
                                 if (retorno.mCodRetorno == -1)
                                 {
                                     //AppUtils.criarBarril('barril-{{$index}}', {{(b.totalMlVendido/b.totalMl)*100}},
                                     // 50);
                                     vm.barris = result.data.mObj;
                                     barrilService.setIdCervejeiroLogado(vm.barris.idCervejeiroLogado);
                                     $scope.$applyAsync();
                                 }
                                 else
                                 {
                                     RemoteDataUtil.defaultRemoteErrorHandling(retorno, vm);
                                 }
                             }
                             catch (err)
                             {
                                 ApplicationUtil.showErrorMessage(err, 'Erro', vm);
                                 return false;
                             }
                             finally
                             {
                             }
                         },
                         function (error) {
                             ApplicationUtil.showErrorMessage(err, 'Erro', vm);
                         });
             };
             vm.clickCervejaria = function () {
                 $window.location.href = 'index.php?tipo=paginas&page=cadastro.cervejeiro&tipoCadastro=cervejaria';
             };
         }
         catch (ex)
         {
             ApplicationUtil.showErrorMessage(ex.message, 'Erro no controller barrisController', vm);
         }
     }]
);