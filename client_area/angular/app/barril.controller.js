omegaApp.controller('barrilController',

    ['$scope', '$rootScope', '$element', '$window', '$interval', '$timeout', '$http', '$location',
        'barrilService', 'facebookService',

        function ($scope, $rootScope, $element, $window, $interval, $timeout, $http, $location,
                  barrilService, facebookService)
        {
            try{
                var vm = this;
                vm.qtdLitroNovoPedido=0;

                vm.get = $location.search();
                vm.online= false;
                vm.onlineFacebook= false;
                $rootScope.$on('facebook', function(event, args) {


                    if(args.message === 'onine') {
                        console.log('barril::facebook - online');
                        vm.onlineFacebook=true;
                    }
                    else{
                        console.log('barril::facebook - offline');
                        vm.onlineFacebook=false;
                    }
                });

                vm.isOnlineFacebook = function(){return vm.onlineFacebook;};

                $rootScope.$on('usrLogado', function(event, args) {
                    // .. Do whatever you need to do.
                    if(args.message === 'online'){
                        console.log('barril::usrLogado - online');
                        vm.online = true;
                        if(typeof idDoBarril !== 'undefined' && idDoBarril !== null){
                            vm.idDoBarril = idDoBarril;
                            vm.getDadosBarril({ idBarril: idDoBarril });
                        } else if(vm.get.idDoBarril !== null){
                            vm.getDadosBarril({ idBarril: vm.get.idDoBarril });
                        }
                    } else{
                        console.log('barril::usrLogado - offline');
                        if(typeof idDoBarril === 'undefined' || idDoBarril === null){
                            //$window.location.href='index.php';
                        }else{
                            //se ele recebeu um convite e ainda nao esta cadastrado
                            if(facebookService.getSessaoFacebook() !== null){

                                var url1 = 'index.php?tipo=paginas&page=cadastro.cervejeiro';
                                if(typeof vm.get.convite !== 'undefined'){
                                    url1 += '&convite=' + vm.get.convite;
                                }
                                $window.location.href=url1;
                            }
                            //disponibiliza a opcao de login do facebook
                        }
                    }
                    $scope.$apply();
                });


                vm.getDadosBarril = function ( barril) {

                    if(barril === null || typeof  barril === 'undefined'){
                        barrilService.setQrCode(null);
                    }else {
                        barrilService.selecionaBarril(
                            barril.idBarril,
                            facebookService.getSessaoFacebook(),
                            null);
                    }
                };
                vm.comentarClick = function (){
                    var idBarril = barrilService.getBarrilSelecionado();
                    var dadosBarril = barrilService.getDadosBarrilSelecionado();
                    FB.ui({
                        method: 'send',
                        link: 'https://incubadoradacerveja.com.br?tipo=paginas&page=barril&origem=facebook&contexto=beber&id=' + idBarril,
                        to: dadosBarril.facebookId,
                        redirect_uri: 'https://incubadoradacerveja.com.br'
                    });
                }
                vm.beberClick = function (vaiBeber) {
                    var idBarril = barrilService.getBarrilSelecionado();
                    var valor = vm.calculaCustoDaBebida(vm.qtdLitroNovoPedido);
                    var dadosBarril = barrilService.getDadosBarrilSelecionado();
                    if (vm.validateForm()) {
                        $http
                            .post(
                                "webservice.php?class=BO_Convite&action=beberClick",
                                {
                                    convite: typeof vm.get.convite !== 'undefined' ? vm.get.convite : null,
                                    valorNovoPedido: valor,
                                    qtdLitroNovoPedido: vm.qtdLitroNovoPedido,
                                    idBarril: idBarril,
                                    sessaoFacebook: facebookService.getSessaoFacebook(),
                                    vaiBeber: vaiBeber

                                })
                            .then(function (result) {
                                    try {
                                        var retorno = result.data;
                                        if (retorno.mCodRetorno == OPERACAO_REALIZADA_COM_SUCESSO) {

                                            //$.jGrowl(retorno.mMensagem);


                                            RemoteDataUtil.defaultRemoteErrorHandling(retorno, vm);
                                        }
                                        else {
                                            RemoteDataUtil.defaultRemoteErrorHandling(result, vm);
                                        }

                                    } catch (err) {

                                        ApplicationUtil.showErrorMessage(err, 'Erro', vm);
                                        return false;
                                    } finally {

                                    }

                                },
                                function (error) {
                                    ApplicationUtil.showErrorMessage(error, 'Erro', vm);
                                });
                    }
                };

                vm.saldoNaoConfirmado = function(){
                    return barrilService.saldoNaoConfirmado ();
                };

                vm.saldoConfirmado = function(){
                    return barrilService.saldoConfirmado ();
                };

                vm.precoLitro= function(){
                    return barrilService.precoLitro();

                };

                vm.calculaCustoDaBebida = function() {
                    return barrilService.calculaCustoDaBebida (vm.qtdLitroNovoPedido);
                };

                vm.mensagemLimite = function(){

                    var max = barrilService.getTotalRestante();
                    if(max < 0 ){ max = 0;}
                    return "O m�ximo que ainda d� para pedir � " + max;
                };
                vm.getTotalRestante = function(){
                    return barrilService.getTotalRestante();
                };

                vm.checaLimiteVolume = function(){
                    return barrilService.checaLimiteVolume(vm.qtdLitroNovoPedido);
                };


                vm.barrilEstaCheio = function(){
                    return barrilService.barrilEstaCheio();
                };


                vm.validateForm = function() {
                    var dadosBarril= barrilService.getDadosBarrilSelecionado();
                    if(typeof (dadosBarril) === 'undefined') {
                        return false;
                    }
                    var validade = true;
                    if(vm.qtdLitroNovoPedido === null ||
                        vm.qtdLitroNovoPedido == 0){
                        $.jGrowl("Selecione a quantidade de litros que voc� ir� beber");
                        return false;
                    }
                    if(!vm.checaLimiteVolume(vm.qtdLitroNovoPedido)){
                        validade = false;
                        $.jGrowl(vm.mensagemLimite());
                    }

                    return validade;
                };





                vm.downloadQrCode = function(){
                    html2canvas(document.querySelector("#capture-convite")).then(canvas => {
                        vm.downloadCanvas(canvas);

                });
                    // var src= document.querySelector('#qrcode img').getAttribute("src");
                    // var url = src.replace(/^data:image\/[^;]+/, 'data:application/octet-stream;headers=Content-Disposition%3A%20attachment%3B%20qrcodeconvite.png;');
                    // window.open(url);

                };

                vm.downloadCanvas= function(canvas){
                    var a = document.createElement('a');
                    // toDataURL defaults to png, so we need to request a jpeg, then convert for file download.
                    a.href = canvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");
                    a.download = 'convite_' + barrilService.getBarrilSelecionado() + '.jpg';

                    a.click();
                };

                vm.getEstadoBarril = function(){
                    var dados = barrilService.getDadosBarrilSelecionado();
                    if(dados===null){ return null;}
                    return  dados.estadoBarril;
                };

                vm.getChaveBarril = function(){
                    var dados = barrilService.getDadosBarrilSelecionado();
                    if(dados===null){ return null;}
                    return  dados.chave;
                };

                vm.getChaveBarril = function(){
                    var dados = barrilService.getDadosBarrilSelecionado();
                    if(dados===null){ return null;}
                    return  dados.chave;
                };

                vm.getNomeCervejeiro= function(){
                    var dados = barrilService.getDadosBarrilSelecionado();
                    if(dados===null){ return null;}
                    return  dados.nomeCervejeiro;
                };

                vm.getEnderecoCervejeiro= function(){
                    var dados = barrilService.getDadosBarrilSelecionado();
                    if(dados===null){ return null;}
                    return  dados.endereco;
                };

                vm.getDataFesta= function(){
                    var dados = barrilService.getDadosBarrilSelecionado();
                    if(dados===null){ return null;}
                    return  dados.dataFesta;
                };

                vm.getDataConfirmacao= function(){
                    var dados = barrilService.getDadosBarrilSelecionado();
                    if(dados===null){ return null;}
                    return  dados.dataConfirmacao;
                };

                vm.getDadosBancarios = function(){
                    var dados = barrilService.getDadosBarrilSelecionado();
                    if(dados===null){ return null;}
                    return  dados.dadosBancarios;
                };

                vm.getPedidos = function(){
                    var dados = barrilService.getDadosBarrilSelecionado();
                    if(dados===null){ return null;}
                    return  dados.pedidos;
                };

                vm.getIdEstadoBarril = function(){
                    var dados = barrilService.getDadosBarrilSelecionado();
                    if(dados===null){ return null;}
                    return  dados.idEstadoBarril;
                };

                vm.getIdEstadoConvite = function(){
                    var dados = barrilService.getDadosBarrilSelecionado();
                    if(dados===null){ return null;}
                    return  dados.idEstadoConvite;
                };

                vm.gerarQrCode = function( ){
                    return barrilService.gerarQrCode(false);
                };

                vm.isOnline = function(){
                    return vm.online;
                };
            }
            catch (ex)
            {
                ApplicationUtil.showErrorMessage(ex.message, 'Erro no controller barrisController', vm);
            }

        }]
);