omegaApp.service('comumService',
    function ($rootScope, $window, $cookies, $http, $timeout) {
        try
        {
            var vm = this;
            vm.isMobileBrowser = function () {
                if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            };
        }
        catch (ex)
        {
            ApplicationUtil.showErrorMessage(ex.message, 'Erro no controller facebookService', vm);
        }
    }
);