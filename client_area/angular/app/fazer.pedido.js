omegaApp.controller('fazerPedidoController',

    ['$scope', '$rootScope', '$element', '$window', '$interval', '$timeout', '$http',

        function ($scope, $rootScope, $element, $window, $interval, $timeout, $http)
        {
            var vm = this;

            try
            {
                var dataService = $http;
                vm.getSaldoAtual = function(sessaoFacebook){

                    dataService
                        .post(
                            "webservice.php?class=BO_Pedido&action=getDadosFormularioPedir",
                            {
                                sessaoFacebook: sessaoFacebook
                            }
                        )
                        .then(function (result) {
                                try {
                                    var retorno = arguments[0].data;
                                    if (retorno.mCodRetorno == -1) {
                                        vm.tiposCerveja = result.data.mObj;
                                    }
                                    else {
                                        RemoteDataUtil.defaultRemoteErrorHandling(result, vm);
                                    }
                                } catch (err) {

                                    ApplicationUtil.showErrorMessage(err, 'Erro', vm);
                                    return false;
                                }
                            },
                            function (error) {
                                ApplicationUtil.showErrorMessage(error, 'Erro', vm);
                            });
                };

                vm.gerarQrCodePedido = function(sessaoFacebook){
                    var x = {
                        precoTotal: vm.precoTotal
                        , qtdMlEscolhida: vm.qtdMlEscolhida
                        , accessToken: sessaoFacebook.accessToken
                    };
                    var json= JSON.stringify(x);
                    geraQrCode(json);
                    // dataService
                    //     .post(
                    //         "webservice.php?class=BO_Cervejeiro_degustador&action=gerarQrCode"
                    //         , {
                    //             precoTotal: vm.precoTotal
                    //             , qtdMlEscolhida: vm.qtdMlEscolhida
                    //             , sessaoFacebook: sessaoFacebook
                    //         }
                    //     )
                    //     .then(function (result) {
                    //             try {
                    //                 var retorno = arguments[0].data;
                    //                 if (retorno.mCodRetorno == -1) {
                    //                     $.jGrowl(retorno.mMensagem);
                    //                 }
                    //                 else {
                    //                     RemoteDataUtil.defaultRemoteErrorHandling(retorno, vm);
                    //                 }
                    //             } catch (err) {
                    //
                    //                 ApplicationUtil.showErrorMessage(err, 'Erro', vm);
                    //                 return false;
                    //             }
                    //
                    //         },
                    //         function (error) {
                    //             ApplicationUtil.showErrorMessage(err, 'Erro', vm);
                    //         });
                }

                vm.afterLoginFacebook = function(sessaoFacebook){


                };



                vm.getTiposCerveja();

            }
            catch (ex)
            {
                ApplicationUtil.showErrorMessage(ex.message, 'Erro no controller formCadastroDegustador', vm);
            }

        }]
);