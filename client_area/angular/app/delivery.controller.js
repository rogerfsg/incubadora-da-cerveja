omegaApp.controller('deliveryController',

    ['$scope', '$rootScope', '$element', '$window', '$interval', '$timeout', '$http', 'facebookService',

        function ($scope, $rootScope, $element, $window, $interval, $timeout, $http, facebookService)
        {
            var vm = this;
            vm.email='';
            try
            {
                vm.cadastrar = function(){

                    var d = new Date();
                    var n = d.getTime();
                    $http
                        .post(
                            "webservice.php?class=BO_Newsletter&action=cadastrar",
                            {
                                email: vm.email
                            }
                        )
                        .then(function (result)
                        {
                            try
                            {
                                var retorno = result.data;
                                RemoteDataUtil.defaultRemoteErrorHandling(retorno, vm);

                            }
                            catch (err)
                            {
                                ApplicationUtil.showErrorMessage(err, 'Erro...', vm);
                                return false;
                            }
                        },
                        function (error)
                        {
                            ApplicationUtil.showErrorMessage(error, 'Erro...', vm);
                            return false;
                        });

                };

            }
            catch (ex)
            {
                ApplicationUtil.showErrorMessage(ex.message, 'Erro no controller deliveryController', vm);
                vm.estadoSeq = vm.ESTADO_RASTREANDO;
                $scope.$apply();

            }

        }
    ]
);