omegaApp.controller('fazerPedidoSimplesController',

    ['$scope', '$rootScope', '$element', '$window', '$interval', '$timeout', '$http',

        function ($scope, $rootScope, $element, $window, $interval, $timeout, $http)
        {
            var vm = this;

            var cervejaDisponiveisVenda = {

                americanLager: {

                    identificador: 1,
                    triggerModal: '#trigger-modal-american-lager',
                    descricao: 'Saint Stello American Lager',
                    imagem: 'americagr.png',
                    embalagem: 'Garrafa de 500ml',
                    preco: 18.00

                },

                sessionIpa: {

                    identificador: 2,
                    triggerModal: '#trigger-modal-session-ipa',
                    descricao: 'Saint Stello Session IPA',
                    imagem: 'ipagr.png',
                    embalagem: 'Garrafa de 500ml',
                    preco: 18.00

                },

                witbier: {

                    identificador: 3,
                    triggerModal: '#trigger-modal-witbier',
                    descricao: 'Saint Stello Witbier',
                    imagem: 'witgr.png',
                    embalagem: 'Garrafa de 500ml',
                    preco: 18.00

                },

                bohemianPilsner: {

                    identificador: 4,
                    triggerModal: '#trigger-modal-bohemian-pilsner',
                    descricao: 'Saint Stello Bohemian Pilsner',
                    imagem: 'bohemiagr.png',
                    embalagem: 'Garrafa de 500ml',
                    preco: 18.00

                },

                weiss: {

                    identificador: 5,
                    triggerModal: '#trigger-modal-weiss',
                    descricao: 'Saint Stello Weiss',
                    imagem: 'logo.png',
                    embalagem: 'Garrafa de 500ml',
                    preco: 18.00

                },

                stout: {

                    identificador: 6,
                    triggerModal: '#trigger-modal-stout',
                    descricao: 'Saint Stello Stout',
                    imagem: 'logo.png',
                    embalagem: 'Garrafa de 500ml',
                    preco: 18.00

                }

            };

            try
            {
                var dataService = $http;

                vm.formDataDadosCliente =
                {
                    nomeCompleto: null,
                    cpf: null,
                    email: null,
                    telefone: null,
                    instagram: null
                };

                vm.formDataEndereco =
                {
                    logradouro: null,
                    numero: null,
                    complemento: null,
                    bairro: null,
                    municipio: null,
                    estado: null,
                    cep: null
                };

                vm.disabledFields =
                {
                    fieldNomeCompleto: false,
                    fieldCpf: false,
                    fieldEmail: false,
                    fieldTelefone: false,
                    fieldInstagram: false,

                    fieldEnderecoAutocomplete: false,
                    fieldEnderecoLogradouro: true,
                    fieldEnderecoNumero: true,
                    fieldEnderecoComplemento: false,
                    fieldEnderecoBairro: true,
                    fieldEnderecoMunicipio: true,
                    fieldEnderecoEstado: true,
                    fieldEnderecoCep: true
                };

                vm.requiredFields =
                {
                    fieldNomeCompleto: true,
                    fieldCpf: true,
                    fieldEmail: true,
                    fieldTelefone: false,
                    fieldInstagram: true,

                    fieldEnderecoAutocomplete: true,
                    fieldEnderecoLogradouro: true,
                    fieldEnderecoNumero: true,
                    fieldEnderecoComplemento: false,
                    fieldEnderecoBairro: true,
                    fieldEnderecoMunicipio: true,
                    fieldEnderecoEstado: true,
                    fieldEnderecoCep: true
                };

                vm.showMensagemEnderecoInvalido = false;
                vm.isEnderecoGooglePlacesSelecionado = false;
                var getTipoClienteLogado = function()
                {
                    return 'pessoa-fisica';
                };

                vm.itensPedido = {};
                vm.getLimiteMaximoDoProdutoPorPedido = function()
                {
                    var tipoClienteLogado = getTipoClienteLogado();

                    switch(tipoClienteLogado)
                    {
                        case 'pessoa-fisica':
                            return 1;

                        case 'pessoa-juridica':
                            return 3;
                    }

                };

                var irParaSecaoPedido = function()
                {
                    setTimeout(function(){

                        $('html, body').animate({
                            scrollTop: $("#pedido-simples-lista-produtos").offset().top - 200
                        }, 500);

                    }, 500);
                };

                var irParaSecaoEndereco = function()
                {
                    setTimeout(function(){

                        $('html, body').animate({
                            scrollTop: $("#pedido-simples-secao-endereco").offset().top - 200
                        }, 500);

                    }, 500);
                };

                vm.irParaListaDeProdutos = function()
                {
                    setTimeout(function(){

                        $('html, body').animate({
                            scrollTop: $("#lista-produtos-disponiveis-compra").offset().top
                        }, 500);

                    }, 500);
                };

                vm.secaoEnderecoVisivel = false;
                vm.exibirSecaoEndereco = function()
                {
                    setTimeout(function(){

                        //inicializa campo de endere�o
                        initEnderecoAutocomplete();

                    }, 100);

                    vm.secaoEnderecoVisivel = true;
                    irParaSecaoEndereco();

                };

                vm.getValorFretePedido = function()
                {
                    return 6.00;
                };

                vm.getValorTotalPedido = function()
                {
                    var valorTotalPedido = 0;
                    if(!GeneralUtil.isNullOrEmpty(vm.itensPedido))
                    {
                        for (var tipoCerveja in vm.itensPedido)
                        {
                            valorTotalPedido += vm.itensPedido[tipoCerveja].quantidade*vm.itensPedido[tipoCerveja].preco;
                        }
                        valorTotalPedido += vm.getValorFretePedido();
                    }

                    return valorTotalPedido;

                };

                vm.verDetalhesProdutoCarrinho = function(objCerveja)
                {
                    if(!GeneralUtil.isNullOrEmpty(objCerveja)
                        && !GeneralUtil.isNullOrEmpty(objCerveja.triggerModal))
                    {
                        $(objCerveja.triggerModal).click();
                    }
                };

                vm.adicionarProdutoAoPedido = function(chaveTipoCerveja, idModal)
                {
                    if(!GeneralUtil.isNullOrEmpty(chaveTipoCerveja))
                    {
                        if(GeneralUtil.isNullOrEmpty(vm.itensPedido[chaveTipoCerveja]))
                        {
                            vm.itensPedido[chaveTipoCerveja] = angular.copy(cervejaDisponiveisVenda[chaveTipoCerveja]);
                            vm.itensPedido[chaveTipoCerveja].quantidade = 0;
                        }

                        var quantidadeAtual = vm.itensPedido[chaveTipoCerveja].quantidade;
                        var limitePorProduto = vm.getLimiteMaximoDoProdutoPorPedido();
                        if(quantidadeAtual + 1 > limitePorProduto)
                        {
                            var complementoMensagem = limitePorProduto == 1 ? "1 item de cada produto": "at� " + limitePorProduto + " itens de cada produto";
                            alert("O limite para compra � de " + complementoMensagem);
                        }
                        else
                        {
                            vm.itensPedido[chaveTipoCerveja].quantidade += 1;
                        }

                        if(vm.itensPedido[chaveTipoCerveja].quantidade == 0)
                        {
                            delete vm.itensPedido[chaveTipoCerveja];
                        }

                    }

                    if(!GeneralUtil.isNullOrEmpty(idModal))
                    {
                        $('#' + idModal).find('.modal-header button.close').click();
                        irParaSecaoPedido();
                    }

                };

                vm.removerProdutoDoPedido = function(chaveTipoCerveja)
                {
                    if(!GeneralUtil.isNullOrEmpty(vm.itensPedido[chaveTipoCerveja]))
                    {
                        var quantidadeAtual = vm.itensPedido[chaveTipoCerveja].quantidade;
                        if(quantidadeAtual == 1)
                        {
                            delete vm.itensPedido[chaveTipoCerveja];
                        }
                        else
                        {
                            vm.itensPedido[chaveTipoCerveja].quantidade -= 1;
                        }
                    }
                };

                var autoCompleteField;
                vm.showCampoEnderecoAutocomplete = true;
                vm.isEnderecoGooglePlacesSelecionado = false;

                var initEnderecoAutocomplete = function()
                {
                    if(!GeneralUtil.isNullOrEmpty(document.getElementById("fieldEnderecoAutocomplete")))
                    {
                        autoCompleteField = new google.maps.places.Autocomplete(
                            (document.getElementById('fieldEnderecoAutocomplete')),
                            {types: ['geocode'], componentRestrictions: {country: "br"}});

                        //adiciona listener
                        autoCompleteField.addListener('place_changed', preencherCamposDeEndereco);

                        //ponto central da regi�o de BH atendida pelo Rappi
                        var geolocation = {
                            lat: -19.935297,
                            lng: -43.941081
                        };

                        //raio de 2,6 Km, calculado no Google Earth
                        var areaAbrangenciaEntrega = new google.maps.Circle({
                            center: geolocation,
                            radius: 2600
                        });

                        autoCompleteField.setBounds(areaAbrangenciaEntrega.getBounds());

                        google.maps.event.addDomListener(document.getElementById("fieldEnderecoAutocomplete"), 'blur', function()
                        {
                            if(GeneralUtil.isNullOrEmpty($('#fieldEnderecoAutocomplete').val()))
                            {

                            }
                            else if ($('.pac-item:hover').length === 0 )
                            {
                                $rootScope.showDialog(vm, 'dialog-problema-endereco-informado.html');
                                vm.showMensagemEnderecoInvalido = true;
                            }
                        });

                    }

                };

                var componentForm = {
                    street_number: 'short_name',
                    route: 'long_name',
                    sublocality_level_1: 'long_name',
                    locality: 'long_name',
                    administrative_area_level_2: 'long_name',
                    administrative_area_level_1: 'short_name',
                    country: 'long_name',
                    postal_code: 'short_name'
                };


                var preencherCamposDeEndereco = function()
                {
                    var place = autoCompleteField.getPlace();
                    if(!GeneralUtil.isNullOrEmpty(place) && !GeneralUtil.isNullOrEmpty(place.address_components))
                    {
                        //itera sobre os elementos retornado pelo Google Places
                        for (var i = 0; i < place.address_components.length; i++)
                        {
                            var addressType = place.address_components[i].types[0];
                            if (componentForm[addressType])
                            {
                                var valor = place.address_components[i][componentForm[addressType]];

                                switch (addressType)
                                {
                                    case 'route':
                                        vm.formDataEndereco.logradouro = valor;
                                        vm.disabledFields.fieldEnderecoLogradouro = true;
                                        break;

                                    case 'street_number':
                                        vm.formDataEndereco.numero = valor;
                                        vm.disabledFields.fieldEnderecoNumero = true;
                                        break;

                                    case 'sublocality_level_1':
                                        vm.formDataEndereco.bairro = valor;
                                        vm.disabledFields.fieldEnderecoBairro = true;
                                        break;

                                    case 'locality':
                                    case 'administrative_area_level_2':
                                        vm.formDataEndereco.municipio = valor;
                                        vm.disabledFields.fieldEnderecoMunicipio = true;
                                        break;

                                    case 'administrative_area_level_1':
                                        vm.formDataEndereco.estado = valor;
                                        vm.disabledFields.fieldEnderecoEstado = true;
                                        break;

                                    case 'postal_code':
                                        vm.formDataEndereco.cep = valor;
                                        vm.disabledFields.fieldEnderecoCep = true;
                                        break;

                                }

                            }

                        }

                        if(GeneralUtil.isNullOrEmpty(vm.formDataEndereco.numero))
                        {
                            ApplicationUtil.showErrorMessage('Voc� deve informar o n�mero de sua resid�ncia no campo de busca.', 'Erro', vm);
                        }
                        else
                        {
                            vm.disabledFields.fieldEnderecoMunicipio = true;
                            vm.disabledFields.fieldEnderecoEstado = true;
                            vm.isEnderecoGooglePlacesSelecionado = true;
                            vm.showMensagemEnderecoInvalido = false;
                        }
                    }
                    else
                    {
                        vm.showMensagemEnderecoInvalido = true;
                    }

                    $scope.$apply();

                };

                vm.validarDadosAndFinalizarPedido = function(formInstance)
                {
                    if(GeneralUtil.isNullOrEmpty(vm.itensPedido) || vm.getValorTotalPedido() == 0)
                    {
                        ApplicationUtil.showErrorMessage('Dados inv�lidos. Atualize a p�gina e refa�a seu pedido para continuar.', 'Erro', vm);
                    }
                    else if(!isEnderecoValidoParaFinalizarPedido())
                    {
                        ApplicationUtil.showErrorMessage('Endere�o de entrega inv�lido. Corrija o endere�o informado e tente novamente.', 'Erro', vm);
                    }
                    else if ($rootScope.validateForm(formInstance, vm))
                    {
                        finalizarPedido();
                    }

                };

                var isEnderecoValidoParaFinalizarPedido = function()
                {
                    return !GeneralUtil.isNullOrEmpty(vm.formDataEndereco.logradouro)
                            && !GeneralUtil.isNullOrEmpty(vm.formDataEndereco.numero)
                                && !GeneralUtil.isNullOrEmpty(vm.formDataEndereco.bairro)
                                    && !GeneralUtil.isNullOrEmpty(vm.formDataEndereco.municipio)
                                        && !GeneralUtil.isNullOrEmpty(vm.formDataEndereco.estado)
                                            && !GeneralUtil.isNullOrEmpty(vm.formDataEndereco.cep);

                };


                var finalizarPedido = function()
                {
                    dataService
                        .post(
                            "webservice.php?class=BO_Pedido_simples&action=gerarUrlDoMercadoPagoParaPagamento",
                            {
                                itensPedido: vm.itensPedido,
                                dadosUsuarioLogado: vm.formDataDadosCliente,
                                enderecoEntrega: vm.formDataEndereco
                            }
                        )
                        .then(function (result)
                        {
                            try
                            {
                                var retorno = result.data;
                                if (retorno.mCodRetorno == -1)
                                {
                                    if(!GeneralUtil.isNullOrEmpty(result.data.mObj))
                                    {
                                        vm.urlPagamentoMercadoPago = result.data.mObj;
                                        document.location.href = vm.urlPagamentoMercadoPago;
                                    }
                                    else
                                    {
                                        ApplicationUtil.showErrorMessage('Ocorreu um erro ao prosseguir para o pagamento.', 'Erro', vm);
                                    }
                                }
                                else
                                {
                                    RemoteDataUtil.defaultRemoteErrorHandling(result, vm);
                                }
                            }
                            catch (err)
                            {
                                ApplicationUtil.showErrorMessage(err, 'Erro', vm);
                                return false;
                            }
                        },
                        function (error)
                        {
                            ApplicationUtil.showErrorMessage(error, 'Erro', vm);
                        }
                    );




                };

            }
            catch (ex)
            {
                ApplicationUtil.showErrorMessage(ex.message, 'Erro no controller fazerPedidoSimplesController', vm);
            }

        }]
);