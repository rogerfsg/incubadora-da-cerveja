omegaApp.service('facebookService',
    function ($rootScope, $window, $cookies, $http, $timeout) {
        try {
            var vm = this;
            vm.sessaoFacebook = null;

            vm.broadcastUsrLogado = function(usrLogado) {
                var str = usrLogado === null || typeof usrLogado === 'undefined' ? 'offline' : 'online';
                $rootScope.$emit('usrLogado', {
                    message: str,
                    usrLogado: usrLogado
                });
            };
            vm.broadcastFacebook = function(onlineOuOffline ) {
                $rootScope.$emit('facebook', {
                    message: onlineOuOffline,
                    sessaoFacebook: vm.sessaoFacebook
                });
            };
            $rootScope.$on('loginFacebookOutsideAngularJs', function(event, args) {
                // .. Do whatever you need to do.
                FB.getLoginStatus(function(response) {
                    vm.statusChangeCallback(response);
                });
            });

            vm.initialized = false;
            $window.fbAsyncInit = function() {
                vm.__fbAsyncInit();
            };

            vm.__fbAsyncInit = function(){
                if(vm.initialized)
                    return;
                else if (typeof FB == 'undefined')
                    return;

                vm.initialized = true;
                console.log('facebookService::fbAsyncInit');
                FB.init({
                    appId      : '173030516679427',
                    cookie     : true,
                    xfbml      : false,
                    version    : 'v2.12'
                    //version    : 'v2.4'
                });

                FB.AppEvents.logPageView();

                FB.getLoginStatus(function(response) {
                    console.log('facebookService::FB.getLoginStatus');
                    vm.statusChangeCallback(response);
                });
            };

            vm.runCache = function(){
                vm.__fbAsyncInit();
            }

            //Toda vez que o login muda de status ele passa aqui
            vm.statusChangeCallback = function(response){
                if(response.status === "connected"){
                    console.log('facebookService::statusChangeCallback - connected');
                    //authResponse: {accessToken: "EAACdXs57YwMBALRnDnPJcADCJqSZCEVn4Tp7HMH5kSWPIm78e?74pXLev8dGeaF0oZCdsKZBvfa93
                    // ZBumO56QrH5E2AJ1SAZDZD", userID: "891658891013315", expiresIn: 5333, signedRequest: "OwB1m_igKVjw
                    // Nuz3YFi4ZQrQwy8Xse_oUwJ_WO6rjUs.eyJhbG?yMDM4NjI2NywidXNlcl9pZCI6Ijg5MTY1ODg5MTAxMzMxNSJ9"}
                    // status: "connected"__proto__: Object
                    angular.element('.only-public-facebook-area').css('display', 'none');
                    angular.element('.only-private-facebook-area').css('display', 'inherit');
                    vm.sessaoFacebook = response.authResponse;
                    vm.checkCookieUser(response.authResponse);

                    vm.broadcastFacebook('online');

                } else {
                    console.log('facebookService::statusChangeCallback - offline');
                    angular.element('.only-public-facebook-area').css('display', 'inherit');
                    angular.element('.only-private-facebook-area').css('display', 'none');
                    vm.sessaoFacebook =null;
                    vm.checkCookieUser(null);

                    vm.broadcastFacebook('offline');
                }
            };

            vm.checkCookieUser = function(sessaoFacebook){

                //se sessaoFacebook for nulo eh porque nao teve login, ou seja, area publica
                var jsonUsrLogado= $cookies.get('usrLogado');
                jsonUsrLogado = typeof jsonUsrLogado==='undefined'?null:jsonUsrLogado;
                var usrLogado = jsonUsrLogado !== null ? JSON.parse(jsonUsrLogado) : null;
                if(sessaoFacebook === null ){
                    //realiza logoff
                    $cookies.put('usrLogado', null);
                    vm.refreshStateView(true);
                } else if(usrLogado === null || usrLogado.idFacebook !== sessaoFacebook.userID){
                    vm.getDadosLoginCervejeiro(sessaoFacebook);
                } // else se for o usuario que ja esta salvo, nao persiste novamente
                else {
                    vm.refreshStateView(true);
                }
            };

            vm.getUsrLogadoCookie = function(){
                var jsonUsrLogado= $cookies.get('usrLogado');
                jsonUsrLogado = typeof jsonUsrLogado==='undefined'?null:jsonUsrLogado;
                if(jsonUsrLogado === null){return null;}
                var usrLogado = jsonUsrLogado !== null ? JSON.parse(jsonUsrLogado) : null;
                return usrLogado;
            };

            vm.refreshStateView  = function(sleep){
                // if(sleep){
                //     $timeout(function ()
                //     {
                //         vm.__refreshStateView();
                //     }, 1000);
                // }else{
                //     vm.__refreshStateView();
                // }
                vm.__refreshStateView();
            };

            vm.__refreshStateView = function(){
                var usrLogado =  vm.getUsrLogadoCookie();
                vm.broadcastUsrLogado(usrLogado);
                if(usrLogado !== null ){
                    angular.element('.only-public-area').css('display', 'none');
                    angular.element('.only-private-area').css('display', 'inherit');
                    if(usrLogado.tipoCervejeiro === "degustador"){
                        angular.element('.only-empresa-area').css('display', 'none');
                        angular.element('.only-degustador-area').css('display', 'inherit');
                    } else {
                        angular.element('.only-degustador-area').css('display', 'none');
                        angular.element('.only-empresa-area').css('display', 'inherit');
                    }
                } else {

                    angular.element('.only-public-area').css('display', 'inherit');
                    angular.element('.only-private-area').css('display', 'none');

                    angular.element('.only-empresa-area').css('display', 'none');
                    angular.element('.only-degustador-area').css('display', 'none');
                }
            };

            vm.getDadosLoginCervejeiro = function(sessaoFacebook){

                $http
                    .post(
                        "webservice.php?class=BO_Cervejeiro&action=getDadosLoginCervejeiro",
                        {
                            sessaoFacebook: sessaoFacebook
                        }
                    )
                    .then(function (result) {
                            try {
                                var retorno = arguments[0].data;
                                if (retorno.mCodRetorno === OPERACAO_REALIZADA_COM_SUCESSO) {
                                    vm.dadosLogin = result.data.mObj;
                                    vm.dadosLogin.idFacebook = sessaoFacebook.userID;
                                    $cookies.put("usrLogado", JSON.stringify(vm.dadosLogin));
                                    vm.refreshStateView(false);

                                } else {
                                    $cookies.put('usrLogado', null);
                                    vm.refreshStateView(false);
                                }
                            } catch (err) {
                                ApplicationUtil.showErrorMessage(err, 'Erro', vm);
                                return false;
                            }

                        },
                        function (error) {
                            ApplicationUtil.showErrorMessage(error, 'Erro', vm);
                        });
            };
            vm.getSessaoFacebook = function () {
                return vm.sessaoFacebook;
            };

            vm.setSessaoFacebook = function (sessaoFacebook) {
                vm.sessaoFacebook = sessaoFacebook;
            };


        }
        catch (ex) {
            ApplicationUtil.showErrorMessage(ex.message, 'Erro no controller facebookService', vm);
        }
    }
);