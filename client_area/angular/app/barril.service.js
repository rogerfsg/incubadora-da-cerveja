omegaApp.service('barrilService',
    function ($rootScope, $window, $location, $http) {
        try{
            var vm = this;
            vm.get = $location.search();
            vm.qrcode = null;

            vm.idBarril = null;
            if(vm.get !== null &&
                vm.get.idDoBarril !== null ){
                vm.idBarril = vm.get.idDoBarril ;
            }
            vm.dadosBarril= null;
            vm.idCervejeiroLogado = null;



            $rootScope.$on('facebook', function(event, args) {
                // .. Do whatever you need to do.
                if(args.message === 'online'){
                    if(vm.getBarrilSelecionado() !== null){
                        vm.selecionaBarril( vm.getBarrilSelecionado(), args.sessaoFacebook, null);

                    } else {
                        // nao faz nada pois j� temos um barril selecionado
                        vm.selecionaBarril(null,null,null);
                    }
                } else {
                    vm.idBarril = null;
                    vm.dadosBarril= null;
                    vm.idCervejeiroLogado = null;
                }
            });

            vm.getQrCode = function(){
                return vm.qrcode ;
            };

            vm.setQrCode = function(qrcode){
                vm.qrcode =qrcode;
            };

            vm.getIdCervejeiroLogado = function(){
                return vm.idCervejeiroLogado;
            };

            vm.setIdCervejeiroLogado = function(idCervejeiroLogado){
                vm.idCervejeiroLogado =   idCervejeiroLogado;
            };


            vm.getBarrilSelecionado = function(){
                return vm.idBarril ;
            };

            vm.getDadosBarrilSelecionado = function(){
                return vm.dadosBarril;
            };

            vm.setDadosBarrilSelecionado = function(dadosBarril){
                vm.dadosBarril = dadosBarril;
            };

            vm.selecionaBarril = function(idBarril, sessaoFacebook, funcaoSucesso){
                vm.idBarril= idBarril;
                if(typeof idBarril === 'undefined' ||
                    idBarril === null) {
                    return false;
                }

                $http
                    .post(
                        "webservice.php?class=Servicos_web&action=getDadosBarril",
                        {
                            idBarril: idBarril,
                            sessaoFacebook: sessaoFacebook
                        })
                    .then(function (result) {
                            try {
                                var retorno = result.data;
                                if (retorno.mCodRetorno == OPERACAO_REALIZADA_COM_SUCESSO) {
                                    var dadosBarril = result.data.mObj;
                                    vm.dadosBarril = dadosBarril;
									if(dadosBarril.totalLitrosComprometidos === null){ dadosBarril.totalLitrosComprometidos=0; }
                                    vm.gerarQrCode(true);
                                    if(funcaoSucesso!==null){funcaoSucesso(dadosBarril);}
                                }
                                else {
                                    RemoteDataUtil.defaultRemoteErrorHandling(retorno, vm);
                                }

                            } catch (err) {

                                ApplicationUtil.showErrorMessage(err, 'Erro', vm);
                                return false;
                            }

                        },
                        function (error) {
                            ApplicationUtil.showErrorMessage(error, 'Erro', vm);
                        });

            };

            vm.saldoNaoConfirmado = function(){
                if(typeof (vm.dadosBarril) === 'undefined' || vm.dadosBarril === null){
                    return 0;
                }
                return vm.dadosBarril.totalLitrosNaoConfirmados + " litros";
            };

            vm.saldoConfirmado = function(){
                if(typeof (vm.dadosBarril) === 'undefined' || vm.dadosBarril === null) {
                    return 0;
                }
                return vm.dadosBarril.totalLitrosConfirmados +" litros";
            };

            vm.precoLitro= function(){
                if(typeof (vm.dadosBarril) === 'undefined' || vm.dadosBarril === null) {
                    return 0;
                }
                return vm.dadosBarril.precoLitro;
            };

            vm.calculaCustoDaBebida = function(qtdLitroNovoPedido) {
                // $dadosBarril->precoLitro = 3.6;
                // $dadosBarril->qtdLitroNovoPedido = 0;
                if(typeof (vm.dadosBarril) === 'undefined'  || vm.dadosBarril === null) {
                    return 0;
                }
                return vm.dadosBarril.precoLitro * qtdLitroNovoPedido;
            };

            vm.checaLimiteVolume = function(qtdLitroNovoPedido){
                if(typeof (vm.dadosBarril) === 'undefined'  || vm.dadosBarril === null) {
                    return false;
                }
                var tot = parseFloat( vm.dadosBarril.totalLitrosComprometidos);
                var x = qtdLitroNovoPedido + tot;
                var validade = x <= vm.dadosBarril.volumeTotal;
                return validade;
            };

            vm.barrilEstaCheio = function(){
                if(typeof (vm.dadosBarril) === 'undefined'  || vm.dadosBarril === null) {
                    return true;
                }
                var tot = parseFloat( vm.dadosBarril.totalLitrosComprometidos);
                return tot  >= vm.dadosBarril.volumeTotal;
            };
            vm.gerarQrCode = function(forcar){

                if(vm.dadosBarril === null){
                    vm.qrCode = null;

                    return null;
                } else if(
                    vm.qrCode !== null &&
                    !forcar){
                    return vm.qrCode;
                }

                var texto = vm.dadosBarril.chave;
                if(vm.qrCode=== null) {
                    vm.qrCode= new QRCode(
                        document.getElementById("qrcode-convite"),
                        {
                            text: texto,
                            width: 128,
                            height: 128,
                            colorDark: "#000000",
                            colorLight: "#ffffff",
                            correctLevel: QRCode.CorrectLevel.H
                        });
                }
                else {
                    vm.qrCode.clear(); // clear the code.
                    vm.qrCode.makeCode(texto); // make another code.
                }
                return vm.qrCode;
            };

            vm.getTotalRestante = function(){
                var dadosBarril= vm.dadosBarril;
                if(typeof (dadosBarril) === 'undefined' || dadosBarril === null){
                    return 0;
                }
				if(dadosBarril.totalLitrosComprometidos === null){ dadosBarril.totalLitrosComprometidos=0; }
                var tot = parseFloat( dadosBarril.totalLitrosComprometidos);
                var max = dadosBarril.volumeTotal - tot ;
				
                return max;
            };



        }
        catch (ex)
        {
            ApplicationUtil.showErrorMessage(ex.message, 'Erro no controller barrilService', vm);
        }

    });