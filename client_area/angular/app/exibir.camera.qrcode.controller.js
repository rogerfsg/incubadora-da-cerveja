omegaApp.controller('exibirCameraQrCodeController',

    ['$scope', '$rootScope', '$element', '$window', '$interval', '$timeout', '$http', 'facebookService',

        function ($scope, $rootScope, $element, $window, $interval, $timeout, $http, facebookService)
        {
            var vm = this;

            vm.ESTADO_RASTREANDO = 1;
            vm.ESTADO_QUANTIDADE_ML = 2;
            vm.ESTADO_REQUISITAR_PEDIDO = 3;

            vm.ESTADO_COMPROVANTE = 4;
            vm.estadoSeq = 1;
            vm.qtdMlSaldoAtual = null;
            vm.qtdMlEscolhida = 0;
            vm.requisicaoPedidoRealizadaComSucesso = null;
            vm.chaveConvidado = null;
            vm.idBarrilConvidado = null;
            vm.idUltimoPedido=null;
            try
            {
                

                vm.getEstado = function(){
                    return vm.estadoSeq;
                };

                vm.perguntaQuantidadeMl = function(sessaoFacebook, chaveConvidado, idBarril) {
                    vm.estadoSeq = vm.ESTADO_QUANTIDADE_ML;

                    vm.chaveConvidado = chaveConvidado;
                    vm.idBarrilConvidado = idBarril;
                    $scope.$apply();
                }
                vm.requisitaAprovacaoPedido = function(){
                    vm.estadoSeq = vm.ESTADO_REQUISITAR_PEDIDO;
                    var d = new Date();
                    var n = d.getTime();
                    $http
                        .post(
                            "webservice.php?class=BO_Pedido_debito&action=requisitaAprovacaoPedido",
                            {
                                sessaoFacebook: facebookService.getSessaoFacebook()
                                , chaveConvidado: vm.chaveConvidado
                                , idBarril: vm.idBarrilConvidado
                                , qtdMlEscolhida: vm.qtdMlEscolhida
                                , identificadorChamada: n
                            }
                        )
                        .then(function (result) {
                                try {
                                    var retorno = result.data;
                                    vm.resultadoOperacao = retorno.mMensagem;
                                    vm.estadoSeq = vm.ESTADO_COMPROVANTE;
                                    if (retorno.mCodRetorno === -1) {
                                        vm.requisicaoPedidoRealizadaComSucesso = true;
                                        vm.qtdMlSaldoAtual = retorno.mObj;
                                    }
                                    else {
                                        vm.requisicaoPedidoRealizadaComSucesso = false;
                                        if(typeof retorno.mObj !== 'undefined') {
                                            var objRet = retorno.mObj;
                                            vm.qtdMlSaldoAtual = objRet.saldoAtual;
                                        }
                                    }
                                } catch (err) {
                                    vm.estadoSeq = vm.ESTADO_RASTREANDO;
                                    ApplicationUtil.showErrorMessage(err, 'Erro', vm);


                                    return false;
                                }
                            },
                            function (error) {
                                vm.estadoSeq = vm.ESTADO_RASTREANDO;
                                ApplicationUtil.showErrorMessage(error, 'Erro', vm);

                                return false;
                            });

                };

                vm.cancelarPedido = function(){


                    vm.estadoSeq = vm.ESTADO_REQUISITAR_PEDIDO;
                    var d = new Date();
                    var n = d.getTime();
                    $http
                        .post(
                            "webservice.php?class=BO_Pedido_debito&action=cancelarPedido",
                            {
                                sessaoFacebook: facebookService.getSessaoFacebook(),
                                chaveConvidado: vm.chaveConvidado,
                                idPedido: vm.idUltimoPedido,
                                identificadorChamada: n
                            }
                        )
                        .then(function (result) {
                                try {
                                    var retorno = result.data;
                                    vm.resultadoOperacao = retorno.mMensagem;
                                    vm.estadoSeq = vm.ESTADO_COMPROVANTE;
                                    if (retorno.mCodRetorno === -1) {

                                    }
                                    else {

                                        if(typeof retorno.mObj !== 'undefined') {
                                            var objRet = retorno.mObj;
                                            vm.qtdMlSaldoAtual = objRet.saldoAtual;

                                        }
                                    }
                                } catch (err) {
                                    vm.estadoSeq = vm.ESTADO_RASTREANDO;
                                    ApplicationUtil.showErrorMessage(err, 'Erro', vm);


                                    return false;
                                }
                            },
                            function (error) {
                                vm.estadoSeq = vm.ESTADO_RASTREANDO;
                                ApplicationUtil.showErrorMessage(error, 'Erro', vm);

                                return false;
                            });

                };

                vm.concluir=function(){
                    vm.estadoSeq = vm.ESTADO_RASTREANDO;
                    if(typeof captureToCanvas !== 'undefined')
                        setTimeout(captureToCanvas, 500);
                    $scope.$apply();
                };

                $rootScope.$on('qrCodeRead', function(event, args) {

                    vm.perguntaQuantidadeMl(facebookService.getSessaoFacebook(), args.a, args.idBarrilPedido);
                });
            }
            catch (ex)
            {
                ApplicationUtil.showErrorMessage(ex.message, 'Erro no controller formCadastroDegustador', vm);
                vm.estadoSeq = vm.ESTADO_RASTREANDO;
                $scope.$apply();

            }

        }]
);