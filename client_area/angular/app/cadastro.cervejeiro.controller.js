omegaApp.controller('cadastroCervejeiroController',

    ['$scope', '$rootScope', '$element', '$window', '$interval', '$timeout', '$http', '$location', 'facebookService',

        function ($scope, $rootScope, $element, $window, $interval, $timeout, $http, $location, facebookService)
        {
            var vm = this;
            vm.messages = {};
            vm.isEnderecoGooglePlacesSelecionado = false;

            try
            {
                var dataService = $http;

                vm.idDoCervejeiro = null;
                vm.idDoBarril = null;

                vm.tipoDoUsuario = null;
                vm.get = $location.search();

                vm.tipoDoUsuario = (typeof vm.get.tipoDoUsuario !== 'undefined') ? vm.get.tipoDoUsuario : null;
                vm.idDoBarril = (typeof vm.get.idBarril !== 'undefined') ? vm.get.idBarril : null;
                vm.isFormularioDeCadastroDoBarril = (typeof vm.get.isCadastroDoBarril !== 'undefined') && vm.get.isCadastroDoBarril==='true'? true : false;

                //mostra loading inicial
                //HtmlUtil.mostrarLoading();

                //espera evento de broadcast do facebook para iniciar carregamento dos dados
                $rootScope.$on('usrLogado', function(event, args)
                {
                    //esconde o loading
                    HtmlUtil.esconderLoading();

                    if(args.message === 'online')
                    {

                        $timeout(function()
                        {
                            //carrega os dados do usu�rio logado
                            getDadosBasicosDoUsuario();

                        }, 0);
                    }
                    else
                    {

                        //abrir login facebook - obrigat�rio estar online em qualquer caso de uso da tela
                        console.log("TODO: A��o ao acessar a p�gina sem estar logado");
                    }

                });

                //recupera dados inicias do servidor (em caso de edi��o)
                var getDadosCadastraisIniciais = function()
                {
                    var idUsuario = vm.idDoCervejeiro;

                    //trata edi��o do barril de empresas
                    if(vm.isEdicaoDoBarril() && vm.isTipoUsuario(AppConstants.TIPOS_CADASTRO_EMPRESARIAL))
                    {
                        //adiciona barril inicial no array
                        vm.adicionarBarril();

                        //recupera lista de tipos de cerveja do servidor
                        getTiposDeCerveja();

                        //recupera dados do barril
                        getDadosBarrilEmpresa();
                    }
                    else if(vm.isCadastroDoBarril() && vm.isTipoUsuario(AppConstants.TIPOS_CADASTRO_EMPRESARIAL))
                    {
                        //adiciona barril inicial no array
                        vm.adicionarBarril();

                        //recupera lista de tipos de cerveja do servidor
                        getTiposDeCerveja();
                    }
                    //trata edi��o de festa
                    else if(vm.isEdicaoDoBarril() && vm.isTipoUsuario(AppConstants.TIPO_CADASTRO.DEGUSTADOR))
                    {
                        //recupera dados da festa do servidor
                        getDadosFestaDegustador();
                    }
                    else
                    {
                        //cadastro de usu�rio - fluxo padr�o
                        if(GeneralUtil.isNullOrEmpty(idUsuario))
                        {
                            //adiciona barril inicial (�nico inicialmente)
                            vm.adicionarBarril();

                            //recupera lista de tipos de cerveja do servidor
                            getTiposDeCerveja();
                        }
                        //edi��o de usu�rio
                        else
                        {
                            getDadosCompletosUsuario();
                        }

                    }

                };

                var getDadosBasicosDoUsuario = function()
                {
                    var idUsuarioFacebook = facebookService.getSessaoFacebook().userID;
                    dataService.get(
                        "webservice.php?class=BO_Cervejeiro&action=getDadosBasicosUsuario&idFacebook=" + idUsuarioFacebook
                    )
                    .then(

                        function (returnContent)
                        {
                            if (RemoteDataUtil.validateRemoteResponse(returnContent))
                            {
                                var dadosRemotos = returnContent.data;
                                if(!GeneralUtil.isNullOrEmpty(dadosRemotos) && !GeneralUtil.isNullOrEmpty(dadosRemotos.mObj) && !GeneralUtil.isNullOrEmpty(dadosRemotos.mObj.idCervejeiro))
                                {
                                    vm.idDoCervejeiro = dadosRemotos.mObj.idCervejeiro;
                                    vm.idDoTipoDoUsuario = dadosRemotos.mObj.idTipoCervejeiro;

                                    vm.tipoDoUsuario = getTipoDeUsuarioFromId(vm.idDoTipoDoUsuario);
                                    if(vm.tipoDoUsuario === AppConstants.TIPO_CADASTRO.DEGUSTADOR
                                        && vm.isFormularioDeCadastroDoBarril !== true){
                                        $window.location.href = 'index.php?tipo=paginas&page=cadastro.degustador';
                                    }
                                    //inicializa campo de endere�o
                                    initEnderecoAutocomplete();

                                    //m�todo que carrega dos dados de edi��o de entidade de acordo com o tipo em quest�o
                                    getDadosCadastraisIniciais();

                                    //aplica regras de obrigatoriedade dependendo do tipo de cadastro corrente
                                    refreshRequiredFields();

                                    $scope.$apply();
                                }
                                //usu�rio logado no facebook, mas n�o cadastrado no site
                                else if(!GeneralUtil.isNullOrEmpty(dadosRemotos) && dadosRemotos.mCodRetorno == PROTOCOLO_SISTEMA.RESULTADO_VAZIO)
                                {
                                    //mant�m vm.tipoDoUsuario vindo da view atrav�s do ng-init
                                    if(GeneralUtil.isNullOrEmpty(vm.tipoDoUsuario))
                                    {
                                        //exibe op��es para cadastro
                                        vm.showOpcoesCadastro = true;
                                    }
                                    else
                                    {
                                        //inicializa campo de endere�o
                                        initEnderecoAutocomplete();

                                        //m�todo que carrega dos dados de edi��o de entidade de acordo com o tipo em quest�o
                                        getDadosCadastraisIniciais();

                                        //aplica regras de obrigatoriedade dependendo do tipo de cadastro corrente
                                        refreshRequiredFields();
                                    }

                                }

                            }
                            else
                            {
                                //exibe dialog do erro
                                RemoteDataUtil.defaultRemoteErrorHandling(returnContent, vm);
                            }

                        },
                        function (error)
                        {
                            //exibe dialog do erro
                            ApplicationUtil.showErrorMessage(error, 'Erro', vm);
                        }
                    );

                };

                var getDadosCompletosUsuario = function()
                {
                    var idCervejeiro = vm.idDoCervejeiro;
                    dataService.get(
                        "webservice.php?class=BO_Cervejeiro&action=getDadosCompletosUsuario&idCervejeiro=" + idCervejeiro
                    )
                    .then(

                        function (returnContent)
                        {
                            if (RemoteDataUtil.validateRemoteResponse(returnContent))
                            {
                                var dadosRemotos = returnContent.data;
                                if(!GeneralUtil.isNullOrEmpty(dadosRemotos) &&
                                        !GeneralUtil.isNullOrEmpty(dadosRemotos.mObj) &&
                                            !GeneralUtil.isNullOrEmpty(dadosRemotos.mObj.idCervejeiro) &&
                                                !GeneralUtil.isNullOrEmpty(dadosRemotos.mObj.dadosFormulario))
                                {
                                    var dadosCervejeiro = angular.copy(dadosRemotos.mObj.dadosFormulario);

                                    vm.idDoCervejeiro = dadosRemotos.mObj.idCervejeiro;

                                    vm.formDataCervejeiro = dadosCervejeiro;
                                    vm.formDataEndereco = dadosCervejeiro.endereco;

                                    //remove estrutura de endere�o do formDataCervejeiro
                                    delete vm.formDataCervejeiro.endereco;

                                    //faz convers�o da data proveniente do servidor
                                    vm.formDataCervejeiro.dataAberturaCervejaria = new Date(vm.formDataCervejeiro.dataAberturaCervejaria);

                                    //desabilita edi��o do campo de cnpj
                                    vm.disabledFields.fieldCnpj = true;

                                    //esconde campo google autocomplete do Google Places
                                    //e exibe campos separados para edi��o do endere�o
                                    vm.showCampoEnderecoAutocomplete = false;
                                    vm.isEnderecoGooglePlacesSelecionado = true;
                                }
                                else
                                {
                                    //exibe dialog do erro
                                    RemoteDataUtil.showErrorMessage("Falha ao recuperar dados", "Erro", vm);
                                }

                            }
                            else
                            {
                                //exibe dialog do erro
                                RemoteDataUtil.defaultRemoteErrorHandling(returnContent, vm);
                            }

                        },
                        function (error)
                        {
                            //exibe dialog do erro
                            ApplicationUtil.showErrorMessage(error, 'Erro', vm);
                        }
                    );
                };

                var getDadosBarrilEmpresa = function()
                {
                    var dadosPost =
                    {
                        idBarril: vm.idDoBarril,
                        sessaoFacebook: facebookService.getSessaoFacebook()
                    };

                    dataService.post(
                        "webservice.php?class=BO_Cervejeiro&action=getDadosBarrilEmpresa",
                        dadosPost
                    )
                    .then(

                        function (returnContent)
                        {
                            if (RemoteDataUtil.validateRemoteResponse(returnContent))
                            {
                                if(!GeneralUtil.isNullOrEmpty(returnContent.data) && !GeneralUtil.isNullOrEmpty(returnContent.data.mObj))
                                {
                                    var dadosEdicaoBarril = angular.copy(returnContent.data.mObj);
                                    vm.formDataBarril = [];

                                    if(!GeneralUtil.isNullOrEmpty(dadosEdicaoBarril.embalagensDisponiveis) &&
                                        dadosEdicaoBarril.embalagensDisponiveis.length > 0)
                                    {
                                        dadosEdicaoBarril.tiposEmbalagensGarrafa = angular.copy(tiposEmbalagensCervejariaGarrafa);
                                        dadosEdicaoBarril.tiposEmbalagensKeg = angular.copy(tiposEmbalagensCervejariaKeg);

                                        loopEmbalagensRemotas:
                                        for(var i=0; i < dadosEdicaoBarril.embalagensDisponiveis.length; i++)
                                        {
                                            var embalagemCorrente = dadosEdicaoBarril.embalagensDisponiveis[i];
                                            for(var j=0; j < dadosEdicaoBarril.tiposEmbalagensGarrafa.length; j++)
                                            {
                                                if(dadosEdicaoBarril.tiposEmbalagensGarrafa[j].chave == embalagemCorrente.chave)
                                                {
                                                    var embalagemGarrafaCorrente = dadosEdicaoBarril.tiposEmbalagensGarrafa[j];

                                                    embalagemGarrafaCorrente.isProduzida = true;
                                                    embalagemGarrafaCorrente.preco = embalagemCorrente.preco;
                                                    continue loopEmbalagensRemotas;
                                                }
                                            }

                                            for(var j=0; j < dadosEdicaoBarril.tiposEmbalagensKeg.length; j++)
                                            {
                                                if(dadosEdicaoBarril.tiposEmbalagensKeg[j].chave == embalagemCorrente.chave)
                                                {
                                                    var embalagemKedCorrente = dadosEdicaoBarril.tiposEmbalagensKeg[j];

                                                    embalagemKedCorrente.isProduzida = true;
                                                    embalagemKedCorrente.preco = embalagemCorrente.preco;
                                                    continue loopEmbalagensRemotas;
                                                }
                                            }
                                        }

                                        delete dadosEdicaoBarril.embalagensDisponiveis;
                                        vm.formDataBarril.push(dadosEdicaoBarril);
                                    }

                                }
                                else
                                {
                                    //exibe dialog do erro
                                    ApplicationUtil.showErrorMessage("Dados inv�lidos.", 'Erro', vm);
                                }
                            }
                            else
                            {
                                //exibe dialog do erro
                                RemoteDataUtil.defaultRemoteErrorHandling(returnContent, vm);
                            }

                        },
                        function (error)
                        {
                            //exibe dialog do erro
                            ApplicationUtil.showErrorMessage(error, 'Erro', vm);
                        }
                    );
                };

                var getDadosFestaDegustador = function()
                {
                    var dadosPost =
                    {
                        idBarril: vm.idDoBarril,
                        sessaoFacebook: facebookService.getSessaoFacebook()
                    };

                    dataService.post(
                        "webservice.php?class=BO_Cervejeiro&action=getDadosFesta",
                        dadosPost
                    )
                    .then(

                        function (returnContent)
                        {
                            if (RemoteDataUtil.validateRemoteResponse(returnContent))
                            {
                                if(!GeneralUtil.isNullOrEmpty(returnContent.data) && !GeneralUtil.isNullOrEmpty(returnContent.data.mObj))
                                {
                                    var dadosRemotos = returnContent.data.mObj;

                                    vm.formDataEndereco = dadosRemotos.endereco;
                                    vm.formDataFesta = angular.copy(dadosRemotos);

                                    delete vm.formDataFesta.endereco;

                                    //esconde campo google autocomplete do Google Places
                                    //e exibe campos separados para edi��o do endere�o
                                    vm.showCampoEnderecoAutocomplete = false;
                                    vm.isEnderecoGooglePlacesSelecionado = true;
                                }
                                else
                                {
                                    //exibe dialog do erro
                                    ApplicationUtil.showErrorMessage("Dados inv�lidos.", 'Erro', vm);
                                }
                            }
                            else
                            {
                                //exibe dialog do erro
                                RemoteDataUtil.defaultRemoteErrorHandling(returnContent, vm);
                            }

                        },
                        function (error)
                        {
                            //exibe dialog do erro
                            ApplicationUtil.showErrorMessage(error, 'Erro', vm);
                        }

                    );

                };

                var getTiposDeCerveja = function()
                {
                    //carrega tipos de cerveja
                    dataService.get(
                        "webservice.php?class=BO_Tipo_cerveja&action=getTiposCerveja"
                    )
                    .then(
                        function (returnContent)
                        {
                            if (RemoteDataUtil.validateRemoteResponse(returnContent))
                            {
                                vm.tiposCerveja = returnContent.data.mObj;
                            }
                            else
                            {
                                //exibe dialog do erro
                                RemoteDataUtil.defaultRemoteErrorHandling(returnContent, vm);
                            }

                        },
                        function (error)
                        {
                            //exibe dialog do erro
                            ApplicationUtil.showErrorMessage(error, 'Erro', vm);
                        }

                    );

                };

                var getBancosFebraban = function()
                {
                    //carrega tipos de cerveja
                    dataService.get(
                        "webservice.php?class=BO_Bancos_febraban&action=getListaBancos"
                    )
                    .then(
                        function (returnContent)
                        {
                            if (RemoteDataUtil.validateRemoteResponse(returnContent))
                            {
                                vm.tiposCerveja = returnContent.data.mObj;
                            }
                            else
                            {
                                //exibe dialog do erro
                                RemoteDataUtil.defaultRemoteErrorHandling(returnContent, vm);
                            }

                        },
                        function (error)
                        {
                            //exibe dialog do erro
                            ApplicationUtil.showErrorMessage(error, 'Erro', vm);
                        }

                    );

                };

                //controla o estado 'disabled' dos campos
                vm.disabledFields =
                {
                    //cervejaria, brewpub e cigano
                    fieldRazaoSocial: false,
                    fieldNome: false,
                    fieldCnpj: false,
                    fieldDataAberturaCervejaria: false,
                    fieldComentariosCervejaria: false,
                    fieldEmail: false,
                    fieldTelefone: false,

                    //distribuidor
                    //TODO: a definir

                    //cervejaria, brewpub e cigano
                    fieldInstagram: false,
                    fieldTwitter: false,
                    fieldSnapchat: false,
                    fieldCanalYoutube: false,

                    //Barril - cervejaria, brewpub e cigano
                    fieldTipoCerveja: false,
                    fieldVolumeTotalProducao: false,
                    fieldIBU: false,
                    fieldPercentualAlcool: false,
                    fieldFotoInstagram: false,
                    fieldComentariosCerveja: false,

                    //Barril - apenas degustador
                    fieldCervejaFestaValorPorLitro: false,
                    fieldCervejaFestaVolumeTotalEmLitros: false,

                    //apenas degustador
                    fieldDadosBancariosBanco: false,
                    fieldDadosBancariosCpf: false,
                    fieldDadosBancariosTitular: false,
                    fieldDadosBancariosAgencia: false,
                    fieldDadosBancariosTipoConta: false,
                    fieldDadosBancariosOperacao: false,
                    fieldDadosBancariosConta: false,
                    fieldDadosBancariosDigitoVerificadorConta: false,

                    //todos - Local (ou da festa do degustador ou do estabelecimento do cervejeiro)
                    fieldDataDaFesta: false,
                    fieldEnderecoLogradouro: false,
                    fieldEnderecoNumero: false,
                    fieldEnderecoComplemento: false,
                    fieldEnderecoBairro: false,
                    fieldEnderecoMunicipio: false,
                    fieldEnderecoEstado: false

                };

                //controla o estado 'required' dos campos
                vm.requiredFields =
                {
                    //cervejaria, brewpub e cigano
                    fieldRazaoSocial: false,
                    fieldNome: false,
                    fieldCnpj: false,
                    fieldDataAberturaCervejaria: false,
                    fieldDataDaFesta: false,
                    fieldComentariosCervejaria: false,
                    fieldEmail: false,
                    fieldTelefone: false,

                    //distribuidor
                    //TODO: a definir

                    //cervejaria, brewpub e cigano
                    fieldInstagram: false,
                    fieldTwitter: false,
                    fieldSnapchat: false,
                    fieldCanalYoutube: false,

                    //Barril - cervejaria, brewpub e cigano
                    fieldTipoCerveja: false,
                    fieldVolumeTotalProducao: false,
                    fieldIBU: false,
                    fieldPercentualAlcool: false,
                    fieldFotoInstagram: false,
                    fieldComentariosCerveja: false,

                    //Barril - apenas degustador
                    fieldCervejaFestaValorPorLitro: false,
                    fieldCervejaFestaVolumeTotalEmLitros: false,

                    //apenas degustador
                    fieldDadosBancariosBanco: false,
                    fieldDadosBancariosCpf: false,
                    fieldDadosBancariosTitular: false,
                    fieldDadosBancariosAgencia: false,
                    fieldDadosBancariosTipoConta: false,
                    fieldDadosBancariosOperacao: false,
                    fieldDadosBancariosConta: false,
                    fieldDadosBancariosDigitoVerificadorConta: false,

                    //todos - Local (ou da festa ou do estabelecimento)
                    fieldEnderecoLogradouro: true,
                    fieldEnderecoNumero: true,
                    fieldEnderecoComplemento: false,
                    fieldEnderecoBairro: true,
                    fieldEnderecoMunicipio: true,
                    fieldEnderecoEstado: true
                };

                //utilizado para empresas
                //pois o degustador possui formul�rio separado
                vm.formDataCervejeiro =
                {
                    //cervejaria, brewpub e cigano
                    razaoSocial: null,
                    nome: null,
                    cnpj: null,
                    dataAberturaCervejaria: null,
                    comentariosCervejaria: null,
                    email: null,
                    telefone: null,

                    //distribuidor
                    //TODO: a definir

                    //cervejaria, brewpub e cigano
                    instagram: null,
                    twitter: null,
                    snapchat: null,
                    canalYoutube: null
                };

                //cervejaria, brewpub e cigano
                //o degustador possui o endere�o associado ao barril (festa)
                vm.formDataEndereco =
                {
                    logradouro: null,
                    numero: null,
                    complemento: null,
                    bairro: null,
                    municipio: null,
                    estado: null
                };

                //dados do barril do cervejeiro (apenas empresa)
                vm.formDataBarril = [] ;

                //dados da festa (apenas degustador)
                vm.formDataFesta =
                {
                    volumeTotalEmLitros: null,
                    valorPorLitro: null,

                    dadosBancarios:
                    {
                        cpf: null,
                        titular: null,
                        banco: null,
                        agencia: null,
                        tipoConta: null,
                        operacao: null,
                        conta: null,
                        digitoVerificadorConta: null
                    }

                };

                var refreshRequiredFields = function()
                {
                    //cadastro de usu�rio (empresa)
                    if(vm.isEdicaoDoUsuario() || vm.isCadastroDoUsuario())
                    {
                        vm.requiredFields.fieldRazaoSocial = true;
                        vm.requiredFields.fieldNome = true;
                        vm.requiredFields.fieldCnpj = true;
                        vm.requiredFields.fieldDataAberturaCervejaria = true;
                        vm.requiredFields.fieldComentariosCervejaria = false;
                        vm.requiredFields.fieldEmail = true;
                        vm.requiredFields.fieldTelefone = true;

                        vm.requiredFields.fieldTipoCerveja = true;
                        vm.requiredFields.fieldVolumeTotalProducao = true;
                        vm.requiredFields.fieldIBU = true;
                        vm.requiredFields.fieldPercentualAlcool = true;
                        vm.requiredFields.fieldFotoInstagram = false;
                        vm.requiredFields.fieldComentariosCerveja = false;

                        vm.requiredFields.fieldCervejaFestaValorPorLitro = false;
                        vm.requiredFields.fieldCervejaFestaVolumeTotalEmLitros = false;

                        vm.requiredFields.fieldDadosBancariosTitular = false;
                        vm.requiredFields.fieldDadosBancariosCpf = false;
                        vm.requiredFields.fieldDadosBancariosBanco = false;
                        vm.requiredFields.fieldDadosBancariosAgencia = false;
                        vm.requiredFields.fieldDadosBancariosTipoConta = false;
                        vm.requiredFields.fieldDadosBancariosConta = false;
                        vm.requiredFields.fieldDadosBancariosDigitoVerificadorConta = false;
                    }
                    else
                    {
                        //cadastro de festa
                        if(vm.isCadastroDoBarril() && vm.isTipoUsuario(AppConstants.TIPO_CADASTRO.DEGUSTADOR))
                        {
                            vm.requiredFields.fieldRazaoSocial = false;
                            vm.requiredFields.fieldNome = false;
                            vm.requiredFields.fieldCnpj = false;
                            vm.requiredFields.fieldDataDaFesta = true;
                            vm.requiredFields.fieldDataAberturaCervejaria = false;
                            vm.requiredFields.fieldComentariosCervejaria = false;
                            vm.requiredFields.fieldEmail = false;
                            vm.requiredFields.fieldTelefone = false;

                            vm.requiredFields.fieldTipoCerveja = false;
                            vm.requiredFields.fieldVolumeTotalProducao = false;
                            vm.requiredFields.fieldIBU = false;
                            vm.requiredFields.fieldPercentualAlcool = false;
                            vm.requiredFields.fieldFotoInstagram = false;
                            vm.requiredFields.fieldComentariosCerveja = false;

                            vm.requiredFields.fieldCervejaFestaValorPorLitro = true;
                            vm.requiredFields.fieldCervejaFestaVolumeTotalEmLitros = true;

                            vm.requiredFields.fieldDadosBancariosTitular = false;
                            vm.requiredFields.fieldDadosBancariosCpf = false;
                            vm.requiredFields.fieldDadosBancariosBanco = false;
                            vm.requiredFields.fieldDadosBancariosAgencia = false;
                            vm.requiredFields.fieldDadosBancariosTipoConta = false;
                            vm.requiredFields.fieldDadosBancariosConta = false;
                            vm.requiredFields.fieldDadosBancariosDigitoVerificadorConta = false;
                        }
                        //cadastro de barril avulso
                        else if(vm.isCadastroDoBarril() && !vm.isTipoUsuario(AppConstants.TIPO_CADASTRO.DEGUSTADOR))
                        {
                            vm.requiredFields.fieldRazaoSocial = false;
                            vm.requiredFields.fieldNome = false;
                            vm.requiredFields.fieldCnpj = false;
                            vm.requiredFields.fieldDataAberturaCervejaria = false;
                            vm.requiredFields.fieldDataDaFesta = true;
                            vm.requiredFields.fieldComentariosCervejaria = false;
                            vm.requiredFields.fieldEmail = false;
                            vm.requiredFields.fieldTelefone = false;

                            vm.requiredFields.fieldDadosBancariosTitular = false;
                            vm.requiredFields.fieldDadosBancariosCpf = false;
                            vm.requiredFields.fieldDadosBancariosBanco = false;
                            vm.requiredFields.fieldDadosBancariosAgencia = false;
                            vm.requiredFields.fieldDadosBancariosTipoConta = false;
                            vm.requiredFields.fieldDadosBancariosConta = false;
                            vm.requiredFields.fieldDadosBancariosDigitoVerificadorConta = false;
                        }

                    }

                };

                //----------------------------------------------------
                //INICIO - Se��o para controle dos barris (empresas)
                //----------------------------------------------------

                var modelBarrilCervejaria =
                {
                    fotoInstagram: null,
                    tipoCerveja: null,
                    IBU: null,
                    percentualAlcool: null,
                    comentariosCerveja: null,
                    volumeTotalProducao: null
                };

                var tiposEmbalagensCervejariaGarrafa =
                [
                    {
                        isProduzida: false,
                        chave: 'garrafa500',
                        descricao: 'Garrafa de 500 ml',
                        capacidade: 500,
                        preco: null
                    },
                    {
                        isProduzida: false,
                        chave: 'garrafa600',
                        descricao: 'Garrafa de 600 ml',
                        capacidade: 600,
                        preco: null
                    }
                ];

                var tiposEmbalagensCervejariaKeg =
                [
                    {
                        isProduzida: false,
                        chave: 'keg1000',
                        descricao: 'Keg de 1 litro',
                        capacidade: 1000,
                        preco: null
                    },
                    {
                        isProduzida: false,
                        chave: 'keg8000',
                        descricao: 'Keg de 8 litros',
                        capacidade: 8000,
                        preco: null
                    }
                ];

                //m�todo para adicionar barril ao formul�rio
                //atualmente utilizado apenas 1 barril
                vm.adicionarBarril = function()
                {
                    var dadosBarril = angular.copy(modelBarrilCervejaria);
                    dadosBarril.tiposEmbalagensGarrafa = angular.copy(tiposEmbalagensCervejariaGarrafa);
                    dadosBarril.tiposEmbalagensKeg = angular.copy(tiposEmbalagensCervejariaKeg);

                    vm.formDataBarril.push(dadosBarril);
                };

                //limites por barril - m�nimo
                vm.getCapacidadeMinimaDoBarril = function()
                {
                    switch(vm.tipoDoUsuario)
                    {
                        case AppConstants.TIPO_CADASTRO.BREWPUB:
                            return 10;

                        case AppConstants.TIPO_CADASTRO.CERVEJEIRO_CIGANO:
                            return 20;

                        case AppConstants.TIPO_CADASTRO.CERVEJARIA:
                            return 50;

                        default:
                            return 10;
                    }

                };

                //limites por barril - m�ximo
                vm.getCapacidadeMaximaDoBarril = function()
                {
                    switch(vm.tipoDoUsuario)
                    {
                        case AppConstants.TIPO_CADASTRO.BREWPUB:
                            return 1000;

                        case AppConstants.TIPO_CADASTRO.CERVEJEIRO_CIGANO:
                            return 1000;

                        case AppConstants.TIPO_CADASTRO.CERVEJARIA:
                            return 10000;

                        default:
                            return 100;
                    }

                };

                //----------------------------------------------------
                //FIM - Se��o para controle dos barris (empresas)
                //----------------------------------------------------

                //----------------------------------------------------
                //INICIO - Se��o para controle das festas
                //----------------------------------------------------

                //limites de cerveja da festa - valor m�nimo (R$)
                vm.getCervejaFestaValorMinimo = function()
                {
                    return 0;
                };

                //limites de cerveja da festa - valor m�ximo (R$)
                vm.getCervejaFestaValorMaximo = function()
                {
                    return 100;
                };

                //limites de cerveja da festa - volume m�nimo
                vm.getCervejaFestaVolumeMinimo = function()
                {
                    return 1;
                };

                //limites de cerveja da festa - volume m�ximo
                vm.getCervejaFestaVolumeMaximo = function()
                {
                    return 500;
                };

                //embalagens para simula��o de valores (degustador)
                vm.tiposEmbalagensDegustador =
                [
                    {
                        descricao: 'Lata de 269 ml',
                        capacidade: 269
                    },
                    {
                        descricao: 'Lata de 350 ml',
                        capacidade: 350
                    },
                    {
                        descricao: 'Lat�o de 473 ml',
                        capacidade: 473
                    },
                    {
                        descricao: 'Super-lat�o de 473 ml',
                        capacidade: 550
                    },
                    {
                        descricao: 'Garrafa Long-neck de 355 ml',
                        capacidade: 355
                    },
                    {
                        descricao: 'Garrafa de 500 ml',
                        capacidade: 500
                    },
                    {
                        descricao: 'Garrafa de 600 ml',
                        capacidade: 600
                    },
                    {
                        descricao: 'Keg de 1 litro',
                        capacidade: 1000
                    },
                    {
                        descricao: 'Keg de 8 litros',
                        capacidade: 8000
                    }

                ];

                vm.showSimulacaoValores = function()
                {
                  return (!GeneralUtil.isNullOrEmpty(vm.formDataFesta.valorPorLitro) && vm.formDataFesta.valorPorLitro > 0) ||
                            (!GeneralUtil.isNullOrEmpty(vm.formDataFesta.volumeTotalEmLitros) && vm.formDataFesta.volumeTotalEmLitros > 0);
                };

                vm.getPrecoSimuladoEmbalagem = function(tipoEmbalagem)
                {
                    var valorPorMililitro = vm.formDataFesta.valorPorLitro / 1000;
                    return NumberUtil.getCurrencyFormattedString(tipoEmbalagem.capacidade * valorPorMililitro, "R$");
                };

                vm.getQuantidadeSimuladaEmbalagem = function(tipoEmbalagem)
                {
                    var volumeEmMililitros = vm.formDataFesta.volumeTotalEmLitros * 1000;
                    return Math.round(volumeEmMililitros / tipoEmbalagem.capacidade);
                };

                //----------------------------------------------------
                //FIM - Se��o para controle das festas
                //----------------------------------------------------

                //submete dados ao servidor
                vm.submeterDados = function(formInstance)
                {
                    if ($rootScope.validateForm(formInstance, vm))
                    {
                        //cadastro do barril
                        if(vm.isCadastroDoBarril())
                        {
                            if(vm.isTipoUsuario(AppConstants.TIPO_CADASTRO.DEGUSTADOR))
                            {
                                persistirCadastroOuEdicaoFesta();
                            }
                            else
                            {
                                persistirCadasroOuEdicaoBarrilEmpresa();
                            }
                        }
                        //cadastro isolado do usu�rio e barril
                        else if(vm.isCadastroDoUsuario())
                        {
                            persistirCadastroOuEdicaoCervejeiro();
                        }
                        else if(vm.isEdicaoDoBarril())
                        {
                            if(vm.isTipoUsuario(AppConstants.TIPO_CADASTRO.DEGUSTADOR))
                            {
                                persistirCadastroOuEdicaoFesta();
                            }
                            else
                            {
                                persistirCadasroOuEdicaoBarrilEmpresa();
                            }

                        }
                        else if(vm.isEdicaoDoUsuario())
                        {
                            persistirCadastroOuEdicaoCervejeiro();
                        }

                    }

                };

                var normalizarBarrilParaPostarAoServidor = function(dadosViewBarril)
                {
                    var barril = dadosViewBarril;
                    barril.embalagensDisponiveis = [];

                    for(var i=0; i < barril.tiposEmbalagensGarrafa.length; i++)
                    {
                        var recordEmbalagem = angular.copy (barril.tiposEmbalagensGarrafa[i]);
                        if(recordEmbalagem.isProduzida)
                        {
                            if(GeneralUtil.isNullOrEmpty(recordEmbalagem.preco) || recordEmbalagem.preco == 0)
                            {
                                ApplicationUtil.showErrorMessage('Os valores de todos os envazamentos selecionados devem ser preechidos', 'Aten��o', vm);
                                return false;
                            }

                            delete recordEmbalagem.isProduzida;
                            barril.embalagensDisponiveis.push(recordEmbalagem);
                        }
                    }

                    for(var i=0; i < barril.tiposEmbalagensKeg.length; i++)
                    {
                        var recordEmbalagem = angular.copy (barril.tiposEmbalagensKeg[i]);
                        if(recordEmbalagem.isProduzida)
                        {
                            if(GeneralUtil.isNullOrEmpty(recordEmbalagem.preco) || recordEmbalagem.preco == 0)
                            {
                                ApplicationUtil.showErrorMessage('Os valores de todos os envazamentos selecionados devem ser preechidos', 'Aten��o', vm);
                                return false;
                            }

                            delete recordEmbalagem.isProduzida;
                            barril.embalagensDisponiveis.push(recordEmbalagem);
                        }

                    }

                    delete barril.tiposEmbalagensGarrafa;
                    delete barril.tiposEmbalagensKeg;

                    return true;

                };

                var persistirCadastroOuEdicaoCervejeiro = function()
                {
                    //caso seja cadastro de empresas, persiste junto ao barril
                    //lembrando que o cadastro do usu�rio degustador � em outro formul�rio
                    if(vm.isTipoUsuario(AppConstants.TIPOS_CADASTRO_EMPRESARIAL))
                    {
                        var dadosPost = angular.copy(vm.formDataCervejeiro);

                        dadosPost.idDoTipoDoUsuario = getIdFromTipoDeUsuario(vm.tipoDoUsuario);
                        dadosPost.sessaoFacebook = facebookService.getSessaoFacebook();
                        dadosPost.endereco = angular.copy(vm.formDataEndereco);

                        var isEdicaoUsuario = !GeneralUtil.isNullOrEmpty(vm.idDoCervejeiro);
                        var metodoRemoto = isEdicaoUsuario ? 'editarCervejeiroEmpresa' : 'cadastrarCervejeiroEmpresa';

                        //caso seja cadastro
                        if(!isEdicaoUsuario)
                        {
                            dadosPost.barris = angular.copy(vm.formDataBarril);
                            for(var j=0; j < dadosPost.barris.length; j++)
                            {
                                var barril = dadosPost.barris[j];
                                var retornoValidacaoBarril = normalizarBarrilParaPostarAoServidor(barril);

                                if(!retornoValidacaoBarril)
                                {
                                    return false;
                                }
                            }

                        }
                        //caso seja edi��o
                        else
                        {
                            dadosPost.idCervejeiro = vm.idDoCervejeiro;
                        }

                        dataService.post(
                            "webservice.php?class=BO_Cervejeiro&action=" + metodoRemoto,
                            dadosPost
                        )
                        .then(
                            function (returnContent)
                            {
                                if (RemoteDataUtil.validateRemoteResponse(returnContent))
                                {
                                    var successMessage = RemoteDataUtil.getResponseMessageString(returnContent);
                                    ApplicationUtil.showSuccessMessage(successMessage, '', vm);

                                    //TODO: redirecionar usu�rio
                                }
                                else
                                {
                                    //exibe dialog do erro
                                    RemoteDataUtil.defaultRemoteErrorHandling(returnContent, vm);
                                }

                            },
                            function (error)
                            {
                                //exibe dialog do erro
                                ApplicationUtil.showErrorMessage(error, 'Erro', vm);
                            }
                        );

                    }
                    else
                    {
                        //exibe dialog do erro
                        ApplicationUtil.showErrorMessage('Opera��o n�o permitida.', 'Erro', vm);
                    }

                };

                var persistirCadastroOuEdicaoFesta = function()
                {
                    var dadosEnderecoFesta = vm.formDataEndereco;
                    var dadosGeraisFesta = vm.formDataFesta;
                    var isEdicaoFesta = !GeneralUtil.isNullOrEmpty(vm.idDoBarril);

                    if( !GeneralUtil.isNullOrEmpty(dadosGeraisFesta))
                    {
                        dadosGeraisFesta.endereco = dadosEnderecoFesta;

                        var dadosPost =
                        {
                            idCervejeiro: vm.idDoCervejeiro,
                            dadosFesta: dadosGeraisFesta,
                            sessaoFacebook: facebookService.getSessaoFacebook()
                        };

                        var metodoRemoto = 'cadastrarFesta';
                        if(isEdicaoFesta)
                        {
                            dadosPost.idBarril = vm.idDoBarril;
                            metodoRemoto = 'editarFesta';
                        }

                        var jsonParams = JSON.stringify(dadosGeraisFesta);
                        dataService.post(
                            "webservice.php?class=BO_Cervejeiro&action=" + metodoRemoto,
                            dadosPost
                        )
                        .then(
                            function (returnContent)
                            {
                                if (RemoteDataUtil.validateRemoteResponse(returnContent))
                                {
                                    var successMessage = RemoteDataUtil.getResponseMessageString(returnContent);
                                    ApplicationUtil.showSuccessMessage(successMessage, '', vm);
                                    if(metodoRemoto === 'cadastrarFesta'){
                                        var retorno = returnContent.data;
                                        $window.location.href = 'index.php?tipo=paginas&page=acompanhamento.festa.degustador&idDoBarril='+retorno.mObj;
                                    }
                                    //TODO: redirecionar usu�rio
                                }
                                else
                                {
                                    //exibe dialog do erro
                                    RemoteDataUtil.defaultRemoteErrorHandling(returnContent, vm);
                                }

                            },
                            function (error)
                            {
                                //exibe dialog do erro
                                ApplicationUtil.showErrorMessage(error, 'Erro', vm);
                            }
                        );

                    }

                };

                var persistirCadasroOuEdicaoBarrilEmpresa = function()
                {
                    var dadosGeraisBarril = vm.formDataBarril;
                    var isEdicaoBarril = !GeneralUtil.isNullOrEmpty(vm.idDoBarril);

                    if(!GeneralUtil.isNullOrEmpty(dadosGeraisBarril)
                            && ArrayUtil.isArray(dadosGeraisBarril) && dadosGeraisBarril.length > 0)
                    {
                        var dadosPost =
                        {
                            idCervejeiro: vm.idDoCervejeiro,
                            dadosBarril: angular.copy(vm.formDataBarril[0]),
                            sessaoFacebook: facebookService.getSessaoFacebook()
                        };

                        var retornoValidacaoBarril = normalizarBarrilParaPostarAoServidor(dadosPost.dadosBarril);
                        if(!retornoValidacaoBarril)
                        {
                            return false;
                        }

                        var metodoRemoto = isEdicaoBarril ? 'editarBarrilEmpresa' : 'cadastrarBarrilEmpresa';
                        if(isEdicaoBarril)
                        {
                            dadosPost.idBarril = vm.idDoBarril;
                        }

                        dataService.post(
                            "webservice.php?class=BO_Cervejeiro&action=" + metodoRemoto,
                            dadosPost
                        )
                        .then(
                            function (returnContent)
                            {
                                if (RemoteDataUtil.validateRemoteResponse(returnContent))
                                {
                                    var successMessage = RemoteDataUtil.getResponseMessageString(returnContent);
                                    ApplicationUtil.showSuccessMessage(successMessage, '', vm);

                                    //redirecionar usu�rio para p�gina inicial

                                }
                                else
                                {
                                    //exibe dialog do erro
                                    RemoteDataUtil.defaultRemoteErrorHandling(returnContent, vm);
                                }

                            },
                            function (error)
                            {
                                //exibe dialog do erro
                                ApplicationUtil.showErrorMessage(error, 'Erro', vm);
                            }
                        );

                    }
                    else
                    {
                        //exibe dialog do erro
                        ApplicationUtil.showErrorMessage('Opera��o n�o permitida.', 'Erro', vm);
                    }

                };

                //----------------------------------------------------
                // INICIO - M�todos para controle de estado
                //----------------------------------------------------

                vm.isTipoUsuario = function(arrTipoPessoa)
                {
                    if(GeneralUtil.isString(arrTipoPessoa))
                    {
                        arrTipoPessoa = [arrTipoPessoa];
                    }

                    return arrTipoPessoa.indexOf(vm.tipoDoUsuario) > -1;
                };

                vm.isCadastroDoUsuario = function()
                {
                    return !GeneralUtil.isNullOrEmpty(facebookService.getSessaoFacebook()) &&
                                GeneralUtil.isNullOrEmpty(vm.idDoCervejeiro) &&
                                    !vm.isCadastroDoBarril() &&
                                        !vm.isEdicaoDoBarril();
                };

                vm.isEdicaoDoUsuario = function()
                {
                    return !GeneralUtil.isNullOrEmpty(facebookService.getSessaoFacebook()) &&
                                !GeneralUtil.isNullOrEmpty(vm.idDoCervejeiro) &&
                                    !vm.isCadastroDoBarril() &&
                                        !vm.isEdicaoDoBarril();
                };

                vm.isCadastroDoBarril = function()
                {
                    return !GeneralUtil.isNullOrEmpty(vm.isFormularioDeCadastroDoBarril)
                        && (vm.isFormularioDeCadastroDoBarril === true)
                            && !vm.isEdicaoDoBarril();
                };

                vm.isEdicaoDoBarril = function()
                {
                    return !GeneralUtil.isNullOrEmpty(vm.idDoBarril);
                };

                //----------------------------------------------------
                // FIM - M�todos para controle de estado
                //----------------------------------------------------


                //----------------------------------------------------
                // INICIO - M�todos e estruturas auxiliares
                //----------------------------------------------------
                vm.validateMaxNumberOfLines = function(maxLines, text)
                {
                    return (text != null && typeof text != 'undefined' && text.split("\r").length-1 > maxLines);
                };

                vm.getLimiteCaracteresRestante = function(valorModel, limite)
                {
                    return GeneralUtil.isNullOrEmpty(valorModel) ? limite : limite - valorModel.length;
                };

                vm.filterFieldsParameters =
                {
                    fieldDataAberturaCervejaria: {
                        popupAberto: false,
                        datePickerParams: angular.copy(FieldsParametersUtil.defaultDatePickerParams)
                    },
                    fieldDataDaFesta: {
                        popupAberto: false,
                        datePickerParams: angular.copy(FieldsParametersUtil.defaultDatePickerParams)
                    }
                };

                var getTipoDeUsuarioFromId = function(idTipoUsuario)
                {
                    switch(idTipoUsuario)
                    {
                        case "1":
                            return AppConstants.TIPO_CADASTRO.DEGUSTADOR;

                        case "2":
                            return AppConstants.TIPO_CADASTRO.CERVEJEIRO_CIGANO;

                        case "3":
                            return AppConstants.TIPO_CADASTRO.CERVEJARIA;

                        case "4":
                            return AppConstants.TIPO_CADASTRO.BREWPUB;

                        case "5":
                            return AppConstants.TIPO_CADASTRO.DISTRIBUIDOR;

                    }

                };

                var getIdFromTipoDeUsuario = function(tipoUsuario)
                {
                    switch(tipoUsuario)
                    {
                        case AppConstants.TIPO_CADASTRO.DEGUSTADOR:
                            return 1;

                        case AppConstants.TIPO_CADASTRO.CERVEJEIRO_CIGANO:
                            return 2;

                        case AppConstants.TIPO_CADASTRO.CERVEJARIA:
                            return 3;

                        case AppConstants.TIPO_CADASTRO.BREWPUB:
                            return 4;

                        case AppConstants.TIPO_CADASTRO.DISTRIBUIDOR:
                            return 5;

                    }

                };

                vm.isTipoUsuarioIndefinido = function()
                {
                    return vm.tipoDoUsuario === null;
                };

                vm.cadastrarDegustador = function()
                {
                    var url1='index.php?tipo=paginas&page=cadastro.degustador';
                    if(typeof vm.get.convite !== 'undefined' && vm.get.convite !== null)
                    {
                        url1 += '&convite=' + vm.get.convite;
                    }
                    else
                    {
                        url1 += '&isCadastrarBarril=true';
                    }
                    $window.location.href = url1;
                };

                //----------------------------------------------------
                // FIM - M�todos e estruturas auxiliares
                //----------------------------------------------------

                //----------------------------------------------------
                //INICIO - Se��o referente ao endere�o
                //----------------------------------------------------

                var autoCompleteField;
                vm.showCampoEnderecoAutocomplete = true;

                function initEnderecoAutocomplete()
                {
                    if(!GeneralUtil.isNullOrEmpty(document.getElementById("fieldEnderecoAutocomplete")))
                    {
                        autoCompleteField = new google.maps.places.Autocomplete(
                            (document.getElementById('fieldEnderecoAutocomplete')),
                            {types: ['geocode'], componentRestrictions: {country: "br"}});

                        //adiciona listener
                        autoCompleteField.addListener('place_changed', preencherCamposDeEndereco);

                        google.maps.event.addDomListener(document.getElementById("fieldEnderecoAutocomplete"), 'blur', function()
                        {
                            if ($('.pac-item:hover').length === 0 )
                            {
                                $rootScope.showDialog(vm, 'dialog-problema-endereco-informado.html');
                                vm.showMensagemEnderecoInvalido = true;
                            }
                        });

                    }

                };


                vm.informarEnderecoManualmente = function()
                {
                    vm.showCampoEnderecoAutocomplete = false;

                    vm.isEnderecoGooglePlacesSelecionado = true;
                    vm.disabledFields.fieldEnderecoLogradouro = false;
                    vm.disabledFields.fieldEnderecoNumero = false;
                    vm.disabledFields.fieldEnderecoBairro = false;
                    vm.disabledFields.fieldEnderecoMunicipio = false;
                    vm.disabledFields.fieldEnderecoEstado = false;
                };

                var componentForm = {
                    street_number: 'short_name',
                    route: 'long_name',
                    sublocality_level_1: 'long_name',
                    locality: 'long_name',
                    administrative_area_level_2: 'long_name',
                    administrative_area_level_1: 'short_name',
                    country: 'long_name',
                    postal_code: 'short_name'
                };

                function preencherCamposDeEndereco()
                {
                    var place = autoCompleteField.getPlace();
                    if(!GeneralUtil.isNullOrEmpty(place) && !GeneralUtil.isNullOrEmpty(place.address_components))
                    {
                        //itera sobre os elementos retornado pelo Google Places
                        for (var i = 0; i < place.address_components.length; i++)
                        {
                            var addressType = place.address_components[i].types[0];
                            if (componentForm[addressType])
                            {
                                var valor = place.address_components[i][componentForm[addressType]];

                                switch (addressType)
                                {
                                    case 'route':
                                        vm.formDataEndereco.logradouro = valor;
                                        vm.disabledFields.fieldEnderecoLogradouro = false;
                                        break;

                                    case 'street_number':
                                        vm.formDataEndereco.numero = valor;
                                        vm.disabledFields.fieldEnderecoNumero = false;
                                        break;

                                    case 'sublocality_level_1':
                                        vm.formDataEndereco.bairro = valor;
                                        vm.disabledFields.fieldEnderecoBairro = false;
                                        break;

                                    case 'locality':
                                    case 'administrative_area_level_2':
                                        vm.formDataEndereco.municipio = valor;
                                        break;

                                    case 'administrative_area_level_1':
                                        vm.formDataEndereco.estado = valor;
                                        break;

                                }

                            }

                        }

                        vm.disabledFields.fieldEnderecoMunicipio = true;
                        vm.disabledFields.fieldEnderecoEstado = true;
                        vm.isEnderecoGooglePlacesSelecionado = true;

                        vm.showMensagemEnderecoInvalido = false;
                    }
                    else
                    {
                        vm.showMensagemEnderecoInvalido = true;
                    }

                    $scope.$apply();

                };

                //----------------------------------------------------
                //FIM - Se��o referente ao endere�o
                //----------------------------------------------------


                //----------------------------------------------------
                //INICIO - Se��o referente a conta banc�ria
                //----------------------------------------------------
                vm.tiposContasBancarias =
                [
                    {
                        chave: 'conta-corrente',
                        descricao: 'Conta Corrente'
                    },
                    {
                        chave: 'conta-poupan�a',
                        descricao: 'Conta Poupan�a'
                    }
                ];

                vm.onChangeBanco = function()
                {
                    if(vm.formDataFesta.dadosBancarios.banco == AppConstants.BANCO.CAIXA_ECONOMICA_FEDERAL)
                    {
                        vm.requiredFields.fieldDadosBancariosOperacao = true;
                        vm.requiredFields.fieldDadosBancariosTipoConta = false;
                    }
                    else
                    {
                        vm.requiredFields.fieldDadosBancariosOperacao = false;
                        vm.requiredFields.fieldDadosBancariosTipoConta = true;
                    }

                };

                vm.liberarCampoDadosBancariosOperacao = function()
                {
                    return vm.formDataFesta.dadosBancarios.banco == AppConstants.BANCO.CAIXA_ECONOMICA_FEDERAL;
                };

                vm.liberarCampoDadosBancariosTipoConta = function()
                {
                    return vm.formDataFesta.dadosBancarios.banco != AppConstants.BANCO.CAIXA_ECONOMICA_FEDERAL;
                };

                vm.operacoesBancarias =
                [
                    {
                        "Codigo": "001",
                        "Nome": "Conta Corrente de Pessoa F�sica"
                    },
                    {
                        "Codigo": "002",
                        "Nome": "Conta Simples de Pessoa F�sica"
                    },
                    {
                        "Codigo": "013",
                        "Nome": "Poupan�a de Pessoa F�sica"
                    },
                    {
                        "Codigo": "023",
                        "Nome": "Conta Caixa F�cil"
                    }
                ];

                vm.bancosFebraban =
                [
                    {
                        "Codigo": 654,
                        "Nome": "Banco A.J.Renner S.A."
                    },
                    {
                        "Codigo": 246,
                        "Nome": "Banco ABC Brasil S.A."
                    },
                    {
                        "Codigo": 121,
                        "Nome": "Banco Agibank S.A."
                    },
                    {
                        "Codigo": 25,
                        "Nome": "Banco Alfa S.A."
                    },
                    {
                        "Codigo": 641,
                        "Nome": "Banco Alvorada S.A."
                    },
                    {
                        "Codigo": 213,
                        "Nome": "Banco Arbi S.A."
                    },
                    {
                        "Codigo": 24,
                        "Nome": "Banco BANDEPE S.A."
                    },
                    {
                        "Codigo": 107,
                        "Nome": "Banco BBM S.A."
                    },
                    {
                        "Codigo": 96,
                        "Nome": "Banco BM&FBOVESPA de Servi�os de Liquida��o e Cust�dia S.A"
                    },
                    {
                        "Codigo": 318,
                        "Nome": "Banco BMG S.A."
                    },
                    {
                        "Codigo": 752,
                        "Nome": "Banco BNP Paribas Brasil S.A."
                    },
                    {
                        "Codigo": 36,
                        "Nome": "Banco Bradesco BBI S.A."
                    },
                    {
                        "Codigo": 122,
                        "Nome": "Banco Bradesco BERJ S.A."
                    },
                    {
                        "Codigo": 204,
                        "Nome": "Banco Bradesco Cart�es S.A."
                    },
                    {
                        "Codigo": 394,
                        "Nome": "Banco Bradesco Financiamentos S.A."
                    },
                    {
                        "Codigo": 237,
                        "Nome": "Banco Bradesco S.A."
                    },
                    {
                        "Codigo": 225,
                        "Nome": "Banco Brascan S.A."
                    },
                    {
                        "Codigo": 218,
                        "Nome": "Banco BS2 S.A."
                    },
                    {
                        "Codigo": 208,
                        "Nome": "Banco BTG Pactual S.A."
                    },
                    {
                        "Codigo": 412,
                        "Nome": "Banco Capital S.A."
                    },
                    {
                        "Codigo": 40,
                        "Nome": "Banco Cargill S.A."
                    },
                    {
                        "Codigo": 266,
                        "Nome": "Banco C�dula S.A."
                    },
                    {
                        "Codigo": 739,
                        "Nome": "Banco Cetelem S.A."
                    },
                    {
                        "Codigo": 233,
                        "Nome": "Banco Cifra S.A."
                    },
                    {
                        "Codigo": 745,
                        "Nome": "Banco Citibank S.A."
                    },
                    {
                        "Codigo": 241,
                        "Nome": "Banco Cl�ssico S.A."
                    },
                    {
                        "Codigo": 95,
                        "Nome": "Banco Confidence de C�mbio S.A."
                    },
                    {
                        "Codigo": 756,
                        "Nome": "Banco Cooperativo do Brasil S.A. - BANCOOB"
                    },
                    {
                        "Codigo": 748,
                        "Nome": "Banco Cooperativo Sicredi S.A."
                    },
                    {
                        "Codigo": 75,
                        "Nome": "Banco CR2 S.A."
                    },
                    {
                        "Codigo": 222,
                        "Nome": "Banco Credit Agricole Brasil S.A."
                    },
                    {
                        "Codigo": 505,
                        "Nome": "Banco Credit Suisse (Brasil) S.A."
                    },
                    {
                        "Codigo": 69,
                        "Nome": "Banco Crefisa S.A."
                    },
                    {
                        "Codigo": 3,
                        "Nome": "Banco da Amaz�nia S.A."
                    },
                    {
                        "Codigo": 83,
                        "Nome": "Banco da China Brasil S.A."
                    },
                    {
                        "Codigo": 707,
                        "Nome": "Banco Daycoval S.A."
                    },
                    {
                        "Codigo": 300,
                        "Nome": "Banco de La Nacion Argentina"
                    },
                    {
                        "Codigo": 495,
                        "Nome": "Banco de La Provincia de Buenos Aires"
                    },
                    {
                        "Codigo": 494,
                        "Nome": "Banco de La Republica Oriental del Uruguay"
                    },
                    {
                        "Codigo": 456,
                        "Nome": "Banco de Tokyo-Mitsubishi UFJ Brasil S.A."
                    },
                    {
                        "Codigo": 1,
                        "Nome": "Banco do Brasil S.A."
                    },
                    {
                        "Codigo": 47,
                        "Nome": "Banco do Estado de Sergipe S.A."
                    },
                    {
                        "Codigo": 37,
                        "Nome": "Banco do Estado do Par� S.A."
                    },
                    {
                        "Codigo": 41,
                        "Nome": "Banco do Estado do Rio Grande do Sul S.A."
                    },
                    {
                        "Codigo": 4,
                        "Nome": "Banco do Nordeste do Brasil S.A."
                    },
                    {
                        "Codigo": 370,
                        "Nome": "Banco Europeu para a America Latina (BEAL) S.A."
                    },
                    {
                        "Codigo": 265,
                        "Nome": "Banco Fator S.A."
                    },
                    {
                        "Codigo": 224,
                        "Nome": "Banco Fibra S.A."
                    },
                    {
                        "Codigo": 626,
                        "Nome": "Banco Ficsa S.A."
                    },
                    {
                        "Codigo": 473,
                        "Nome": "Banco Financial Portugu�s S.A."
                    },
                    {
                        "Codigo": 94,
                        "Nome": "Banco Finaxis S.A."
                    },
                    {
                        "Codigo": 346,
                        "Nome": "Banco Franc�s e Brasileiro S.A."
                    },
                    {
                        "Codigo": 612,
                        "Nome": "Banco Guanabara S.A."
                    },
                    {
                        "Codigo": 12,
                        "Nome": "Banco INBURSA de Investimentos S.A."
                    },
                    {
                        "Codigo": 258,
                        "Nome": "Banco Induscred S.A."
                    },
                    {
                        "Codigo": 604,
                        "Nome": "Banco Industrial do Brasil S.A."
                    },
                    {
                        "Codigo": 653,
                        "Nome": "Banco Indusval S.A."
                    },
                    {
                        "Codigo": 77,
                        "Nome": "Banco Inter S.A."
                    },
                    {
                        "Codigo": 630,
                        "Nome": "Banco Intercap S.A."
                    },
                    {
                        "Codigo": 249,
                        "Nome": "Banco Investcred S.A."
                    },
                    {
                        "Codigo": 184,
                        "Nome": "Banco Ita� BBA S.A."
                    },
                    {
                        "Codigo": 29,
                        "Nome": "Banco Ita� Consignado S.A."
                    },
                    {
                        "Codigo": 479,
                        "Nome": "Banco ItauBank S.A"
                    },
                    {
                        "Codigo": 376,
                        "Nome": "Banco J. P. Morgan S.A."
                    },
                    {
                        "Codigo": 74,
                        "Nome": "Banco J. Safra S.A."
                    },
                    {
                        "Codigo": 217,
                        "Nome": "Banco John Deere S.A."
                    },
                    {
                        "Codigo": 76,
                        "Nome": "Banco KDB S.A."
                    },
                    {
                        "Codigo": 757,
                        "Nome": "Banco KEB HANA do Brasil S.A."
                    },
                    {
                        "Codigo": 600,
                        "Nome": "Banco Luso Brasileiro S.A."
                    },
                    {
                        "Codigo": 243,
                        "Nome": "Banco M�xima S.A."
                    },
                    {
                        "Codigo": 720,
                        "Nome": "Banco Maxinvest S.A."
                    },
                    {
                        "Codigo": 389,
                        "Nome": "Banco Mercantil do Brasil S.A."
                    },
                    {
                        "Codigo": 755,
                        "Nome": "Banco Merrill Lynch S.A."
                    },
                    {
                        "Codigo": 746,
                        "Nome": "Banco Modal S.A."
                    },
                    {
                        "Codigo": 66,
                        "Nome": "Banco Morgan Stanley S.A."
                    },
                    {
                        "Codigo": 7,
                        "Nome": "Banco Nacional de Desenvolvimento Econ�mico e Social"
                    },
                    {
                        "Codigo": 735,
                        "Nome": "Banco Neon S.A."
                    },
                    {
                        "Codigo": 169,
                        "Nome": "Banco Ol� Bonsucesso Consignado S.A."
                    },
                    {
                        "Codigo": 79,
                        "Nome": "Banco Original do Agroneg�cio S.A."
                    },
                    {
                        "Codigo": 212,
                        "Nome": "Banco Original S.A."
                    },
                    {
                        "Codigo": 712,
                        "Nome": "Banco Ourinvest S.A."
                    },
                    {
                        "Codigo": 623,
                        "Nome": "Banco PAN S.A."
                    },
                    {
                        "Codigo": 65,
                        "Nome": "Banco Patagon S.A."
                    },
                    {
                        "Codigo": 611,
                        "Nome": "Banco Paulista S.A."
                    },
                    {
                        "Codigo": 613,
                        "Nome": "Banco Pec�nia S.A."
                    },
                    {
                        "Codigo": 643,
                        "Nome": "Banco Pine S.A."
                    },
                    {
                        "Codigo": 658,
                        "Nome": "Banco Porto Real S.A."
                    },
                    {
                        "Codigo": 747,
                        "Nome": "Banco Rabobank International Brasil S.A."
                    },
                    {
                        "Codigo": 633,
                        "Nome": "Banco Rendimento S.A."
                    },
                    {
                        "Codigo": 741,
                        "Nome": "Banco Ribeir�o Preto S.A."
                    },
                    {
                        "Codigo": 120,
                        "Nome": "Banco Rodobens S.A."
                    },
                    {
                        "Codigo": 422,
                        "Nome": "Banco Safra S.A."
                    },
                    {
                        "Codigo": 33,
                        "Nome": "Banco Santander (Brasil) S.A."
                    },
                    {
                        "Codigo": 743,
                        "Nome": "Banco Semear S.A."
                    },
                    {
                        "Codigo": 211,
                        "Nome": "Banco Sistema S.A."
                    },
                    {
                        "Codigo": 637,
                        "Nome": "Banco Sofisa S.A."
                    },
                    {
                        "Codigo": 464,
                        "Nome": "Banco Sumitomo Mitsui Brasileiro S.A."
                    },
                    {
                        "Codigo": 737,
                        "Nome": "Banco Theca S.A."
                    },
                    {
                        "Codigo": 82,
                        "Nome": "Banco Top�zio S.A."
                    },
                    {
                        "Codigo": 634,
                        "Nome": "Banco Tri�ngulo S.A."
                    },
                    {
                        "Codigo": 18,
                        "Nome": "Banco Tricury S.A."
                    },
                    {
                        "Codigo": 496,
                        "Nome": "Banco Uno - E Brasil S.A."
                    },
                    {
                        "Codigo": 655,
                        "Nome": "Banco Votorantim S.A."
                    },
                    {
                        "Codigo": 610,
                        "Nome": "Banco VR S.A."
                    },
                    {
                        "Codigo": 119,
                        "Nome": "Banco Western Union do Brasil S.A."
                    },
                    {
                        "Codigo": 124,
                        "Nome": "Banco Woori Bank do Brasil S.A."
                    },
                    {
                        "Codigo": 21,
                        "Nome": "BANESTES S.A. Banco do Estado do Esp�rito Santo"
                    },
                    {
                        "Codigo": 719,
                        "Nome": "Banif-Banco Internacional do Funchal (Brasil)S.A."
                    },
                    {
                        "Codigo": 81,
                        "Nome": "BBN Banco Brasileiro de Neg�cios S.A."
                    },
                    {
                        "Codigo": 250,
                        "Nome": "BCV - Banco de Cr�dito e Varejo S.A."
                    },
                    {
                        "Codigo": 144,
                        "Nome": "BEXS Banco de C�mbio S.A."
                    },
                    {
                        "Codigo": 17,
                        "Nome": "BNY Mellon Banco S.A."
                    },
                    {
                        "Codigo": 126,
                        "Nome": "BR Partners Banco de Investimento S.A."
                    },
                    {
                        "Codigo": 70,
                        "Nome": "BRB - Banco de Bras�lia S.A."
                    },
                    {
                        "Codigo": 104,
                        "Nome": "Caixa Econ�mica Federal"
                    },
                    {
                        "Codigo": 320,
                        "Nome": "China Construction Bank (Brasil) Banco M�ltiplo S.A."
                    },
                    {
                        "Codigo": 477,
                        "Nome": "Citibank N.A."
                    },
                    {
                        "Codigo": 163,
                        "Nome": "Commerzbank Brasil S.A. - Banco M�ltiplo"
                    },
                    {
                        "Codigo": 487,
                        "Nome": "Deutsche Bank S.A. - Banco Alem�o"
                    },
                    {
                        "Codigo": 725,
                        "Nome": "Finansinos S.A. - Cr�dito, Financ. e Investimento"
                    },
                    {
                        "Codigo": 64,
                        "Nome": "Goldman Sachs do Brasil Banco M�ltiplo S.A."
                    },
                    {
                        "Codigo": 135,
                        "Nome": "Gradual Corretora de C�mbio,T�tulos e Valores Mobili�rios S.A."
                    },
                    {
                        "Codigo": 78,
                        "Nome": "Haitong Banco de Investimento do Brasil S.A."
                    },
                    {
                        "Codigo": 62,
                        "Nome": "Hipercard Banco M�ltiplo S.A."
                    },
                    {
                        "Codigo": 63,
                        "Nome": "Ibibank S.A. - Banco M�ltiplo"
                    },
                    {
                        "Codigo": 132,
                        "Nome": "ICBC do Brasil Banco M�ltiplo S.A."
                    },
                    {
                        "Codigo": 492,
                        "Nome": "ING Bank N.V."
                    },
                    {
                        "Codigo": 139,
                        "Nome": "Intesa Sanpaolo Brasil S.A. - Banco M�ltiplo"
                    },
                    {
                        "Codigo": 341,
                        "Nome": "Ita� Unibanco S.A."
                    },
                    {
                        "Codigo": 488,
                        "Nome": "JPMorgan Chase Bank, National Association"
                    },
                    {
                        "Codigo": 128,
                        "Nome": "MS Bank S.A. Banco de C�mbio"
                    },
                    {
                        "Codigo": 14,
                        "Nome": "Natixis Brasil S.A. Banco M�ltiplo"
                    },
                    {
                        "Codigo": 753,
                        "Nome": "Novo Banco Continental S.A. - Banco M�ltiplo"
                    },
                    {
                        "Codigo": 254,
                        "Nome": "Paran� Banco S.A."
                    },
                    {
                        "Codigo": 751,
                        "Nome": "Scotiabank Brasil S.A. Banco M�ltiplo"
                    },
                    {
                        "Codigo": 129,
                        "Nome": "UBS Brasil Banco de Investimento S.A."
                    }
                ];

                //----------------------------------------------------
                //FIM - Se��o referente a conta banc�ria
                //----------------------------------------------------

            }
            catch (ex)
            {
                ApplicationUtil.showErrorMessage(ex.message, 'Erro no controller formBairroController', vm);
            }

        }]
);