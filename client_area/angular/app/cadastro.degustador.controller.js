omegaApp.controller('cadastroDegustadorController',

    ['$scope', '$rootScope', '$element', '$window', '$interval', '$timeout', '$http', 'facebookService', '$location',

        function ($scope, $rootScope, $element, $window, $interval, $timeout, $http, facebookService, $location)
        {
            var vm = this;
            vm.tiposCerveja = [];
            vm.cervejasQueGosta = [];
            vm.cervejasQueNaoGosta= [];
            vm.nome = null;
            vm.email = null;

            vm.get = $location.search();
            if(typeof vm.get.isCadastrarBarril === 'undefined'){
                vm.isCadastrarBarril = false;
            }
            else{ vm.isCadastrarBarril = vm.get.isCadastrarBarril; }

            try
            {

                $rootScope.$on('facebook', function(event, args) {
                    var sessaoFacebook = facebookService.getSessaoFacebook();
                    if(typeof sessaoFacebook === 'undefined') {
                        vm.getTiposCerveja();
                    }
                    else{
                        vm.getDadosFormularioCervejeiroDegustador(facebookService.getSessaoFacebook());
                    }
                });


                vm.getTiposCerveja = function(){

                    $http
                        .post(
                            "webservice.php?class=BO_Tipo_cerveja&action=getTiposCerveja")
                        .then(function (result) {
                                try {
                                    var retorno = arguments[0].data;
                                    if (retorno.mCodRetorno == -1) {
                                        vm.tiposCerveja = result.data.mObj;
                                    }
                                    else {
                                        RemoteDataUtil.defaultRemoteErrorHandling(retorno, vm);
                                    }
                                } catch (err) {

                                    ApplicationUtil.showErrorMessage(err, 'Erro', vm);
                                    return false;
                                }

                            },
                            function (error) {
                                ApplicationUtil.showErrorMessage(err, 'Erro', vm);
                            });
                };


                // Toggle selection for a given fruit by name
                vm.cervejaQueNaoGostaClick = function(tipoCerveja) {


                    // Is currently selected
                    if (vm.cervejasQueGosta !== 'undefined' &&
                        vm.cervejasQueGosta[tipoCerveja.id]) {
                        //vm.cervejasQueGosta.splice(tipoCerveja.id, 1);
                        delete vm.cervejasQueGosta[tipoCerveja.id];
                    }

                };

                // Toggle selection for a given fruit by name
                vm.cervejaQueGostaClick = function(tipoCerveja) {


                    // Is currently selected
                    if (typeof vm.cervejasQueNaoGosta !== 'undefined' &&
                        vm.cervejasQueNaoGosta[tipoCerveja.id]) {
                        //vm.cervejasQueNaoGosta.splice(tipoCerveja.id, 1);
                        delete vm.cervejasQueNaoGosta[tipoCerveja.id];
                    }


                };

                vm.salvar = function(){
                    

                    if(facebookService.getSessaoFacebook() === null){
                        FB.login(function(response){
                            if (response.status === 'connected') {

                                vm.salvarParte2(response.authResponse);
                            } else {
                                $.jGrowl('Para prosseguir � necess�rio realizar o login no facebook');

                            }
                        });
                    } else {

                        vm.salvarParte2(facebookService.getSessaoFacebook());
                    }
                };

                vm.salvarParte2 = function(sessaoFacebook){
                    $http
                        .post(
                            "webservice.php?class=BO_Cervejeiro_degustador&action=salvar",
                             {
                                cervejasQueGosta: vm.cervejasQueGosta,
                                cervejasQueNaoGosta: vm.cervejasQueNaoGosta,
                                email: vm.email,
                                telefone: vm.telefone,
                                sessaoFacebook: sessaoFacebook
                            }
                        )
                        .then(function (result) {
                                try {
                                    var retorno = arguments[0].data;

                                    RemoteDataUtil.defaultRemoteErrorHandling(retorno, vm);

                                    if(vm.isCadastrarBarril){
                                        $window.location.href='index.php?tipo=paginas&page=cadastro.cervejeiro&isCadastroDoBarril=true';
                                    }else if(typeof vm.get.convite !== 'undefined' && vm.get.convite !== null) {
                                        $window.location.href='index.php?tipo=paginas&page=barril&convite=' + vm.get.convite;
                                    }
                                } catch (err) {

                                    ApplicationUtil.showErrorMessage(err, 'Erro', vm);
                                    return false;
                                }

                            },
                            function (error) {
                                ApplicationUtil.showErrorMessage(err, 'Erro', vm);
                            });
                };


                vm.getDadosFormularioCervejeiroDegustador= function(sessaoFacebook){

                    $http
                        .post(
                            "webservice.php?class=BO_Cervejeiro_degustador&action=getDadosFormularioCervejeiroDegustador",
                            {
                                sessaoFacebook: sessaoFacebook
                            }
                        )
                        .then(function (result) {
                                try {
                                    var retorno = arguments[0].data;
                                    if (retorno.mCodRetorno === RESULTADO_VAZIO) {
                                        vm.tiposCerveja = result.data.mObj;
                                    } else if (retorno.mCodRetorno === OPERACAO_REALIZADA_COM_SUCESSO) {
                                        vm.tiposCerveja = result.data.mObj.tiposCerveja;
                                        vm.dadosCervejeiro = result.data.mObj.dadosCervejeiro;

                                        if(typeof  result.data.mObj.objJsonCervejeiro!=='undefined') {
                                            var objJsonCervejeiro = result.data.mObj.objJsonCervejeiro;
                                            vm.cervejasQueGosta = objJsonCervejeiro.cervejasQueGosta;
                                            vm.cervejasQueNaoGosta = objJsonCervejeiro.cervejasQueNaoGosta;
                                            vm.email = objJsonCervejeiro.email;
                                            vm.telefone = objJsonCervejeiro.telefone;
                                        }
                                        $scope.$applyAsync();
                                    }
                                    else {
                                        RemoteDataUtil.defaultRemoteErrorHandling(retorno, vm);
                                    }
                                } catch (err) {

                                    ApplicationUtil.showErrorMessage(err, 'Erro', vm);
                                    return false;
                                }

                            },
                            function (error) {
                                ApplicationUtil.showErrorMessage(err, 'Erro', vm);
                            });
                };





            }
            catch (ex)
            {
                ApplicationUtil.showErrorMessage(ex.message, 'Erro no controller formCadastroDegustador', vm);
            }

        }]
);