omegaApp.controller('acompanhamentoFestaDegustadorController',
    ['$scope', '$rootScope', '$element', '$window', '$interval', '$timeout', '$http', '$location', 'facebookService',
     function ($scope, $rootScope, $element, $window, $interval, $timeout, $http, $location, facebookService)
     {
         try
         {
             var vm = this;
             vm.get = $location.search();
             vm.idBarril = vm.get.idDoBarril;
             vm.barris = [];
             vm.pedidos = [];
             vm.pedidosDebito = [];
             $rootScope.$on('facebook', function (event, args)
             {
                 // .. Do whatever you need to do.
                 if (args.message === 'online')
                 {
                     vm.getDadosDaFestaDoDegustador(args.sessaoFacebook);
                 }
                 else
                 {
                     $window.location.href = 'index.php';
                 }
             });
             vm.getDadosDaFestaDoDegustador = function (sessaoFacebook)
             {
                 $http
                     .post(
                         "webservice.php?class=BO_Barril&action=getDadosDaFestaDoDegustador",
                         {
                             idBarril: vm.idBarril,
                             sessaoFacebook: sessaoFacebook
                         })
                     .then(function (result)
                         {
                             try
                             {
                                 var retorno = arguments[0].data;
                                 if (retorno.mCodRetorno == -1)
                                 {
                                     vm.dadosBarril = result.data.mObj;
                                     vm.dadosBarril.objJson = vm.dadosBarril.barril.objJson;
                                     vm.pedidos = vm.dadosBarril.pedidos;
                                     vm.pedidosDebito = vm.dadosBarril.pedidosDebito;
                                 }
                                 else
                                 {
                                     RemoteDataUtil.defaultRemoteErrorHandling(result, vm);
                                 }
                             }
                             catch (err)
                             {
                                 ApplicationUtil.showErrorMessage(err, 'Erro', vm);
                                 return false;
                             }
                             finally
                             {
                             }
                         },
                         function (error)
                         {
                             ApplicationUtil.showErrorMessage(error, 'Erro', vm);
                         });
             };
             vm.comentarPedidoClick = function (pedido)
             {
                 FB.ui({
                     method: 'send',
                     link: 'https://incubadoradacerveja.com.br?tipo=paginas&page=barril&origem=facebook&contexto=pagamento&idDoBarril=' + vm.idBarril + '&id_pedido=' + pedido.idPedido,
                     to: pedido.facebookId,
                     redirect_uri: 'https://incubadoradacerveja.com.br'
                 });
             };
             vm.confirmaPedidoClick = function (pagou, pedido)
             {
                 $http
                     .post(
                         "webservice.php?class=BO_Pedido&action=confirmaPedidoConvidado",
                         {
                             idBarril: vm.idBarril,
                             sessaoFacebook: facebookService.getSessaoFacebook(),
                             pagou: pagou,
                             idPedido: pedido.idPedido
                         })
                     .then(function (result)
                         {
                             try
                             {
                                 var retorno = arguments[0].data;
                                 if (retorno.mCodRetorno == -1)
                                 {
                                     var objRet = retorno.mObj;
                                     for (var i = 0; i < vm.pedidos.length; i++)
                                     {
                                         if (vm.pedidos[i].idPedido === pedido.idPedido)
                                         {
                                             var temp = vm.pedidos[i];
                                             temp.idEstadoConvite = objRet.idNovoEstadoConvite;
                                             temp.estadoConvite = objRet.novoEstado;
                                             vm.pedidos[i] = temp;
                                             break;
                                         }
                                     }
                                 }
                                 else
                                 {
                                     RemoteDataUtil.defaultRemoteErrorHandling(result, vm);
                                 }
                             }
                             catch (err)
                             {
                                 ApplicationUtil.showErrorMessage(err, 'Erro', vm);
                                 return false;
                             }
                             finally
                             {
                                 // $scope.$apply();
                                 // $scope.$apply();
                             }
                         },
                         function (error)
                         {
                             ApplicationUtil.showErrorMessage(error, 'Erro', vm);
                         });
             };
             vm.cancelarFestaClick = function ()
             {
                 $http
                     .post(
                         "webservice.php?class=BO_Pedido&action=cancelarFesta"
                         , {
                             idBarril: vm.idBarril,
                             sessaoFacebook: facebookService.getSessaoFacebook()
                         })
                     .then(function (result)
                         {
                             try
                             {
                                 var retorno = arguments[0].data;
                                 if (retorno.mCodRetorno == -1)
                                 {
                                     FB.ui({
                                         method: 'share',
                                         href: 'https://incubadoradacerveja.com.br',
                                         hashtag: '#IncubadoraDaCerveja',
                                         quote: 'Desculpa pessoal, festa cancelada!',
                                         redirect_uri: 'https://incubadoradacerveja.com.br?tipo=paginas&page=barril&origem=facebook&contexto=cancelamento_pedido&id=' + vm.idBarril
                                     });
                                 }
                                 else
                                 {
                                     RemoteDataUtil.defaultRemoteErrorHandling(result, vm);
                                 }
                             }
                             catch (err)
                             {
                                 ApplicationUtil.showErrorMessage(err, 'Erro', vm);
                                 return false;
                             }
                             finally
                             {
                             }
                         },
                         function (error)
                         {
                             ApplicationUtil.showErrorMessage(error, 'Erro', vm);
                         });
             };
             vm.cameraQrCode = function ()
             {
                 window.location.href = "index.php?tipo=paginas&page=exibir.camera.qrcode&idDoBarril=" + vm.idBarril;
             };
             vm.enviarConvite = function ()
             {
                 if(!GeneralUtil.isNullOrEmpty(vm.dadosBarril))
                 {
                     FB.ui({
                         method: 'send',
                         link: 'https://incubadoradacerveja.com.br/client_area/index.php?tipo=paginas&page=barril&convite=' + vm.dadosBarril.convite,
                         redirect_uri: 'https://incubadoradacerveja.com.br'
                     });
                 }
             };
             vm.getLinkConvite = function ()
             {
                 if(!GeneralUtil.isNullOrEmpty(vm.dadosBarril))
                 {
                     return 'https://incubadoradacerveja.com.br/client_area/index.php?tipo=paginas&page=barril&convite=' + vm.dadosBarril.convite;
                 }
                 else
                 {
                     return '';
                 }
             };
             vm.editarBarril = function ()
             {
                 $window.location.href = 'index.php?tipo=paginas&page=cadastro.cervejeiro&idBarril=' + vm.idBarril;
             };
             vm.getWhatsAppLinkConvite = function ()
             {
                 var link = vm.getLinkConvite();
                 var encodeLink = encodeURIComponent(link);
                 return "https://api.whatsapp.com/send?text=" + encodeLink;
             };
             vm.enviarConviteWhatsApp = function ()
             {
                 var link = vm.getWhatsAppLinkConvite();
                 $window.location.href = link;
             };
             vm.copyLinkConvite = function ()
             {
                 var copyText = document.getElementById('LinkConvite');
                 /* Select the text field */
                 copyText.select();
                 /* Copy the text inside the text field */
                 document.execCommand("Copy");
             };
         }
         catch (ex)
         {
             ApplicationUtil.showErrorMessage(ex, 'Erro no controller acompanhamentoFestaDegustadorController', vm);
         }
     }]
);