
<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <p><a href="https://incubadoradacerveja.com.br"><i class="fa fa-map-marker"></i> www.incubadoradacerveja.com.br</a></p>
                <p><a href="mailto:incubadoradacerveja@gmail.com"><i class="fa fa-envelope-o"></i> incubadoradacerveja@gmail.com</a></p>
                <p><a href="#"><i class="fa fa-instagram"></i> /incubadoradacerveja</a></p>
                <p><a href="#"><i class="fa fa-facebook-square"></i> /incubadoradacerveja</a></p>
                <p><a href="#"><i class="fa fa-twitter-square"></i> /incubadoradacerveja</a></p>
            </div>
        </div>
    </div>
</footer>

<script type="text/javascript">

    $(document).ready(function(){

        var footerHeight = $('footer').outerHeight();
        var contentHeight = $('#wrapper').outerHeight();
        var windowHeight = $(window).height();

        if(contentHeight + footerHeight < windowHeight)
        {
            $('#wrapper').css('min-height', (windowHeight-footerHeight) + 'px');
        }

    });

</script>