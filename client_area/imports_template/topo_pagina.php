<div class="navbar section no-padding" role="navigation">

    <div class="container" style="min-height: 28px;">

        <div class="navbar-header">

            <div class="fb-login-button"
                 data-max-rows="1"
                 data-size="medium"
                 data-button-type="login_with"
                 data-show-faces="false"
                 data-auto-logout-link="true"
                 data-use-continue-as="true"
                 style="color: white !important;"
                 onlogin="broadcastLoginAngularJs()"
                <?php echo HelperFacebook::PERMISSIONS; ?>
            ></div>


            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Menu</span>
                <span class="icon-bar top-bar"></span>
                <span class="icon-bar middle-bar"></span>
                <span class="icon-bar bottom-bar"></span>
            </button>
        </div>

        <script>

            function getCurrentControllerAngular()
            {
                var appElement = document.querySelector('[ng-app=omegaApp]');
                var appScope = angular.element(appElement).scope();
                if (typeof appScope === "undefined")
                {
                    return null;
                }
                var controllerScope = appScope.$$childHead;
                return controllerScope;
            }

            function broadcastLoginAngularJs()
            {
                var controler = getCurrentControllerAngular();
                if (typeof controler !== 'undefined')
                {
                    var $rootScope = controler.$root;
                    $rootScope.$emit('loginFacebookOutsideAngularJs', {
                        message: 'Check login'
                    });
                }
            }

        </script>

        <div class="navbar-collapse collapse">

            <ul id="menu-primary" class="nav navbar-nav">

                <li>
                    <span>
                        <a href="index.php">BARRIL</a>
                    </span>
                </li>
                <li class="only-public-area">
                    <span>
                        <a href="index.php?tipo=paginas&page=cadastro.degustador">
                            DEGUSTADOR</a>
                    </span>
                </li>

                <li class="only-public-area">
                    <span>
                        <a href="index.php?tipo=paginas&page=cadastro.cervejeiro&tipoCadastro=brewpub">
                            BREW PUB</a>
                    </span>
                </li>

                <li class="only-public-area">
                    <span>
                        <a href="index.php?tipo=paginas&page=cadastro.cervejeiro&tipoCadastro=cigano">
                            CIGANO</a>
                    </span>
                </li>
                <li class="only-public-area">
                    <span>
                        <a href="index.php?tipo=paginas&page=cadastro.cervejeiro&tipoCadastro=cervejaria">
                            CERVEJARIA</a>
                    </span>
                </li>
                <li class="only-empresa-area" style="display: none;">
                    <span>
                        <a href="index.php?tipo=paginas&page=cadastro.cervejeiro">
                            MEUS DADOS</a>
                    </span>
                </li>
                <li class="only-degustador-area" style="display: none;">
                    <span>
                        <a href="index.php?tipo=paginas&page=cadastro.degustador">
                            MEUS DADOS</a>
                    </span>
                </li>
                <li class="only-private-area">
                    <span>
                        <a href="index.php?tipo=paginas&page=perfis">
                            PERFIS</a>
                    </span>
                </li>
                <li>
                    <span>
                        <a href="index.php?tipo=paginas&page=faq">FAQ</a>
                    </span>
                </li>
                <li>
                    <span>
                        <a href="index.php?tipo=paginas&page=contato">CONTATO</a>
                    </span>
                </li>
            </ul>
        </div>
    </div>

    <div class="container-fluid light section separador-menu">

    </div>

</div>

