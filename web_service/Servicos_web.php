<?php

/**
 * Description of Servicos_web
 *
 * @author home
 */
class Servicos_web
{
    //Como chamar algum webservice da classe:
    //http://127.0.0.1/hospedagem/10001/adm/actions.php?class=Servicos_web&action=

    const PREFIXO = "INCCERV:";

    public function __construct()
    {
    }

    public static function factory()
    {
        return new Servicos_web();
    }

    public static function isServidorOnline()
    {
        echo "TRUE";
    }


    public function getDadosBarril()
    {
        $input = file_get_contents("php://input");
        $args = json_decode($input);
        $idBarril = $args->idBarril;

        $msg = null;
        if (!strlen($idBarril)) {
            return Mensagem::factoryParametroInvalido();
        }
        $boBarris = new  BO_Barris();
        $obj = $boBarris->getDadosBarril($idBarril, $args->sessaoFacebook);
        return $obj;
    }


    public function getDadosTelaPrincipal()
    {
        $input = file_get_contents("php://input");
        $args = json_decode($input);
        $objFacebook = null;
        if ($args != null) {
            $objFacebook = $args->sessaoFacebook;
        }
        $boBarris = new BO_Barris();
        return $boBarris->getDadosTelaPrincipal($objFacebook);
    }

    public function cadastrarCervejeiroAndPrimeiraProducao()
    {
        try {
            $args = json_decode(file_get_contents("php://input"));
            $bo = new  BO_Cervejeiro();

            return $bo->cadastrarV2($args);

        } catch (Exception $ex) {
            HelperLog::logException($ex, true);
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, null, $ex);
        }
    }
}



