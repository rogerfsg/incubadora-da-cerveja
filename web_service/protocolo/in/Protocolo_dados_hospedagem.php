<?php

    class Protocolo_dados_hospedagem extends Interface_protocolo
    {
        public $idAssinatura;
        public $dominio;
        public $nomeSite;

        public function factory($objJson)
        {
            $obj = new Protocolo_dados_hospedagem();
            $obj->idAssinatura = $objJson->idAssinatura;
            $obj->dominio = $objJson->dominio;
            $obj->nomeSite = $objJson->nomeSite;

            return $obj;
        }
    }

?>
