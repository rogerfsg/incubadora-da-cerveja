<?php

    class Protocolo_empresa_hosting extends Interface_protocolo
    {

        public $id_assinatura;
        public $id_hospedagem;
        public $corporacao;
        public $id_corporacao;
        public $id_sistema;

        public function __construct($id_assinatura = null, $id_hospedagem = null, $corporacao = null,
                                    $id_corporacao = null)
        {
            if ($id_assinatura != null)
            {
                $this->id_assinatura = $id_assinatura;
            }
            if ($id_hospedagem != null)
            {
                $this->id_hospedagem = $id_hospedagem;
            }
            if ($corporacao != null)
            {
                $this->corporacao = $corporacao;
            }
            if ($id_corporacao != null)
            {
                $this->id_corporacao = $id_corporacao;
            }
        }

        public static function constroi($pVetorValor)
        {
            if (is_array($pVetorValor))
            {
                $vObj = new Protocolo_empresa_hosting();
                $vObj->id_asinatura = $pVetorValor[0];
                $vObj->id_hospedagem = $pVetorValor[1];
                $vObj->id_sistema = $pVetorValor[2];

                return $vObj;
            }

            return null;
        }

        public function factory($objJson)
        {
            $obj = new Protocolo_empresa_hosting();
            $obj->id_assinatura = $objJson[0];
            $obj->id_hospedagem = $objJson[1];
            $obj->id_sistema = $objJson[2];

            return $obj;
        }

    }

?>
