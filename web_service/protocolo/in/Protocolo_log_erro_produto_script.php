<?php

    class Protocolo_log_erro_produto_script extends Interface_protocolo_entidade
    {
        public $id;
        public $classe;
        public $funcao;
        public $linha_INT;
        public $nome_arquivo;
        public $identificador_erro;
        public $descricao;
        public $stacktrace;
        public $data_visualizacao_DATETIME;
        public $data_ocorrida_DATETIME;
        public $sistema_tipo_log_erro_id_INT;
        public $sistema_projetos_versao_produto_id_INT;
        public $sistema_projetos_versao_id_INT;
        public $id_usuario_INT;
        public $id_corporacao_INT;
        public $script_comando_banco_id_INT;
        public $operacao_sistema_mobile_id_INT;
        public $mobile_conectado_id_INT;

        public function __construct()
        {
        }

        public function persiste()
        {
            $obj = new EXTDAO_Sistema_produto_log_erro();
            $obj->setClasse($this->classe);
            $obj->setFuncao($this->funcao);
            $obj->setLinha_INT($this->linha_INT);
            $obj->setNome_arquivo($this->nome_arquivo);
            $obj->setIdentificador_erro($this->identificador_erro);
            $obj->setDescricao($this->descricao);
            $obj->setStacktrace($this->stacktrace);
            $obj->setData_visualizacao_DATETIME($this->data_visualizacao_DATETIME);
            $obj->setData_ocorrida_DATETIME($this->data_ocorrida_DATETIME);
            $obj->setSistema_tipo_log_erro_id_INT($this->sistema_tipo_log_erro_id_INT);
            $obj->setSistema_projetos_versao_produto_id_INT($this->sistema_projetos_versao_produto_id_INT);
            $obj->setSistema_projetos_versao_id_INT($this->sistema_projetos_versao_id_INT);
            $obj->setScript_comando_banco_id_INT($this->script_comando_banco_id_INT);
            $obj->setId_usuario_INT($this->id_usuario_INT);

            $obj->setId_corporacao_INT($this->id_corporacao_INT);
            $obj->setScript_comando_banco_id_INT($this->script_comando_banco_id_INT);
            $obj->setOperacao_sistema_mobile_id_INT($this->operacao_sistema_mobile_id_INT);
            $obj->setTotal_ocorrencia_INT($this->total_ocorrencia_INT);
            $obj->setMobile_conectado_id_INT($this->mobile_conectado_id_INT);
            $obj->formatarParaSQL();
            $obj->insert();
        }

        public function factory($objJson)
        {
            $obj = new Protocolo_log_erro_produto();
            $obj->id = $objJson->id;
            $obj->classe = $objJson->classe;
            $obj->funcao = $objJson->funcao;
            $obj->linha_INT = $objJson->linha_INT;
            $obj->nome_arquivo = $objJson->nome_arquivo;
            $obj->identificador_erro = $objJson->identificador_erro;
            $obj->descricao = $objJson->descricao;
            $obj->stacktrace = $objJson->stacktrace;
            $obj->data_visualizacao_DATETIME = $objJson->data_visualizacao_DATETIME;
            $obj->data_ocorrida_DATETIME = $objJson->data_ocorrida_DATETIME;

            $obj->sistema_tipo_log_erro_id_INT = $objJson->sistema_tipo_log_erro_id_INT;
            $obj->sistema_projetos_versao_produto_id_INT = $objJson->sistema_projetos_versao_produto_id_INT;
            $obj->sistema_projetos_versao_id_INT = $objJson->sistema_projetos_versao_id_INT;

            $obj->id_usuario_INT = $objJson->id_usuario_INT;

            $obj->id_corporacao_INT = $objJson->id_corporacao_INT;
            $obj->script_comando_banco_id_INT = $objJson->script_comando_banco_id_INT;

            return $obj;
        }

    }

?>
