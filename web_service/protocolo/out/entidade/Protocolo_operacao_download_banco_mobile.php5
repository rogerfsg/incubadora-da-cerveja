<?php

    class Protocolo_operacao_download_banco_mobile extends Interface_protocolo_entidade
    {
        public $id;
        public $path_script_sql_banco;
        public $observacao;
        public $operacao_sistema_mobile_id_INT;
        public $popular_tabela_BOOLEAN;
        public $drop_tabela_BOOLEAN;
        public $destino_banco_id_INT;

        public function __construct($idOSMFilho)
        {
            $this->id = $idOSMFilho;
            $obj = new EXTDAO_Operacao_download_banco_mobile();
            $obj->select($idOSMFilho);

            $this->path_script_sql_banco = $obj->getPath_script_sql_banco();
            $this->observacao = $obj->getObservacao();
            $this->operacao_sistema_mobile_id_INT = $obj->getOperacao_sistema_mobile_id_INT();
            $this->popular_tabela_BOOLEAN = $obj->getPopular_tabela_BOOLEAN();
            $this->drop_tabela_BOOLEAN = $obj->getDrop_tabela_BOOLEAN();
            $this->destino_banco_id_INT = $obj->getDestino_banco_id_INT();
        }

        public function factory($objJson)
        {
        }

    }

?>
