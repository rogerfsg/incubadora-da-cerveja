<?php

    class Protocolo_operacao_sistema_mobile extends Interface_protocolo_entidade
    {

        public $id;
        public $tipo_operacao_sistema_id_INT;
        public $estado_operacao_sistema_mobile_id_INT;
        public $mobile_identificador_id_INT;
        public $data_abertura_DATETIME;
        public $data_processamento_DATETIME;
        public $data_conclusao_DATETIME;

        public $objOperacao;

        public function __construct($idOSM)
        {
            $obj = new EXTDAO_Operacao_sistema_mobile();
            $obj->select($idOSM);
            $this->id = $idOSM;

            $this->tipo_operacao_sistema_id_INT = $obj->getTipo_operacao_sistema_id_INT();
            $this->estado_operacao_sistema_mobile_id_INT = $obj->getEstado_operacao_sistema_mobile_id_INT();
            $this->mobile_identificador_id_INT = $obj->getMobile_identificador_id_INT();

            $this->data_abertura_DATETIME = $obj->getData_abertura_DATETIME();
            $this->data_processamento_DATETIME = $obj->getData_processamento_DATETIME();
            $this->data_conclusao_DATETIME = $obj->getData_conclusao_DATETIME();
        }

        public function factory($objJson)
        {
        }

    }

?>
