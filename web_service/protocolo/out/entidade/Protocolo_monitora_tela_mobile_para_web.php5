<?php

    class Protocolo_monitora_tela_mobile_para_web extends Interface_protocolo_entidade
    {

        public $id;                           //| int(11)      | NO   | PRI | NULL    | auto_increment |
        public $mobile_identificador_id_INT;       // | int(11)      | YES  |     | NULL    |                |
        public $operacao_sistema_mobile_id_INT; // | int(11)      | NO   |     | NULL    |                |
        public $coordenada_x_INT; //| int(11)      | YES  |     | NULL    |                |
        public $coordenada_y_INT; //| int(11)      | YES  |     | NULL    |                |
        public $tipo_operacao_monitora_tela_id_INT; //| int(11)      | YES  |     | NULL    |                |
        public $sequencia_operacao_INT; //| int(11)      | YES  |     | NULL    |                |
        public $data_ocorrencia_DATETIME; //| datetime     | YES  |     | NULL    |                |
        public $raio_circulo_INT; //| int(11)      | YES  |     | NULL    |                |
        public $largura_quadrado_INT; //| int(11)      | YES  |     | NULL    |                |
        public $altura_quadrado_INT; //| int(11)      | YES  |     | NULL    |                |
        public $cor_INT;                            //| int(11)      | YES  |     | NULL    |                |
        public $mensagem;

        public function __construct($idMTMPW)
        {
            $this->id = $idMTMPW;
            $obj = new EXTDAO_Monitora_tela_web_para_mobile();
            $obj->select($idMTMPW);

            $this->mobile_identificador_id_INT = $obj->getMobile_identificador_id_INT();
            $this->operacao_sistema_mobile_id_INT = $obj->getOperacao_sistema_mobile_id_INT();
            $this->coordenada_x_INT = $obj->getCoordenada_x_INT();
            $this->coordenada_y_INT = $obj->getCoordenada_y_INT();
            $this->tipo_operacao_monitora_tela_id_INT = $obj->getTipo_operacao_monitora_tela_id_INT();
            $this->sequencia_operacao_INT = $obj->getSequencia_operacao_INT();
            $this->data_ocorrencia_DATETIME = $obj->getData_ocorrencia_DATETIME();
            $this->raio_circulo_INT = $obj->getRaio_circulo_INT();
            $this->largura_quadrado_INT = $obj->getLargura_quadrado_INT();
            $this->altura_quadrado_INT = $obj->getAltura_quadrado_INT();
            $this->cor_INT = $obj->getCor_INT();
            $this->mensagem = $obj->getMensagem();
        }

        public function factory($objJson)
        {

        }

    }

?>
