<?php

    class Protocolo_executa_script extends Interface_protocolo_entidade
    {
        public $id;
        public $xml_script_sql_banco_ARQUIVO;

        public function __construct($idOSMFilho)
        {
            $this->id = $idOSMFilho;
            $obj = new EXTDAO_Operacao_executa_script();
            $obj->select($idOSMFilho);

            $this->operacao_sistema_mobile_id_INT = $obj->getOperacao_sistema_mobile_id_INT();
            $this->xml_script_sql_banco_ARQUIVO = $obj->getXml_script_sql_banco_ARQUIVO();
        }

        public function factory($objJson)
        {
        }

    }

?>
