<?php

    class Protocolo_caracteristicas_pacote extends Interface_protocolo
    {

        public $totalUsuario;
        public $numeroUsuarioOcupado;
        public $totalEspacoMB;
        public $totalClinica;
        public $totalEmailMensal;
        public $emailUsuarioPrincipal;
        public $idClienteAssinatura;

        public function gravaSessao($idCorporacao)
        {
            if (!strlen($idCorporacao))
            {
                return;
            }
            Helper::setSession("id_corporacao_pacote", $idCorporacao);
            Helper::setSession("total_usuario_pacote", $this->totalUsuario);
            Helper::setSession("total_espaco_mb_pacote", $this->totalEspacoMB);
            Helper::setSession("total_clinica_pacote", $this->totalClinica);
            Helper::setSession("total_email_mensal_pacote", $this->totalEmailMensal);
            Helper::setSession("email_usuario_principal_pacote", $this->emailUsuarioPrincipal);
            Helper::setSession("numero_usuario_ocupado", $this->numeroUsuarioOcupado);
            Helper::setSession("id_cliente_assinatura", $this->idClienteAssinatura);
        }

        public static function apagaSessao()
        {
            Helper::clearSession("id_corporacao_pacote");
            Helper::clearSession("total_usuario_pacote");
            Helper::clearSession("numero_usuario_ocupado");
            Helper::clearSession("total_espaco_mb_pacote");
            Helper::clearSession("total_clinica_pacote");
            Helper::clearSession("total_email_mensal_pacote");
            Helper::clearSession("email_usuario_principal_pacote");
            Helper::clearSession("id_cliente_assinatura");
        }

        public static function carregaSessao($idCorporacao)
        {
            if (Helper::SESSION("id_corporacao_pacote"))
            {
                $idCorporacaoSessao = Helper::SESSION("id_corporacao_pacote");
                if ($idCorporacaoSessao == $idCorporacao)
                {
                    $protocolo = new Protocolo_caracteristicas_pacote();
                    $protocolo->totalUsuario = Helper::SESSION("total_usuario_pacote");
                    $protocolo->totalEspacoMB = Helper::SESSION("total_espaco_mb_pacote");
                    $protocolo->totalClinica = Helper::SESSION("total_clinica_pacote");
                    $protocolo->totalEmailMensal = Helper::SESSION("total_email_mensal_pacote");
                    $protocolo->numeroUsuarioOcupado = Helper::SESSION("numero_usuario_ocupado");
                    $protocolo->emailUsuarioPrincipal = Helper::SESSION("email_usuario_principal_pacote");
                    $protocolo->idClienteAssinatura = Helper::SESSION("id_cliente_assinatura");

                    return $protocolo;
                }
            }

            return null;
        }

        public static function constroi($obj)
        {
            $protocolo = new Protocolo_caracteristicas_pacote();
            $protocolo->totalUsuario = $obj["total_usuario"];
            $protocolo->totalEspacoMB = $obj["total_espaco_mb"];
            $protocolo->totalClinica = $obj["total_clinica"];
            $protocolo->totalEmailMensal = $obj["total_email_mensal"];
            $protocolo->emailUsuarioPrincipal = $obj["email_usuario_principal"];
            $protocolo->numeroUsuarioOcupado = $obj["numero_usuario_ocupado"];
            $protocolo->idClienteAssinatura = $obj["id_cliente_assinatura"];

            return $protocolo;
        }

        public function factory($objJson)
        {
            $obj = new Protocolo_caracteristicas_pacote();
            $obj->totalUsuario = $objJson->totalUsuario;
            $obj->totalEspacoMB = $objJson->totalEspacoMB;
            $obj->totalClinica = $objJson->totalClinica;
            $obj->totalEmailMensal = $objJson->totalEmailMensal;
            $obj->emailUsuarioPrincipal = $objJson->emailUsuarioPrincipal;
            $obj->numeroUsuarioOcupado = $objJson->numeroUsuarioOcupado;
            $obj->idClienteAssinatura = $objJson->idClienteAssinatura;
            $obj->gravaSessao(Seguranca::getIdDaCorporacaoLogada());

            return $obj;
        }

    }

?>
