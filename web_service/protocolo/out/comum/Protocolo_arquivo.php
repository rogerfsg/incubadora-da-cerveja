<?php

    class Protocolo_arquivo extends Interface_protocolo
    {

        public $nomeArquivo;
        public $pathArquivo;

        public static function constroi($pVetorValor)
        {
            if (is_array($pVetorValor))
            {
                $vObj = new Protocolo_arquivo();
                $vObj->nomeArquivo = $pVetorValor[0];
                $vObj->pathArquivo = $pVetorValor[1];

                return $vObj;
            }

            return null;
        }

        public function factory($objJson)
        {
            //TODO
        }

    }

?>
