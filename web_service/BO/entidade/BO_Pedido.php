<?php


    class BO_Pedido
    {
        public function __construct()
        {
        }

        public static function factory()
        {
            return new BO_Pedido();
        }

        //funcao chamada da pagina http://localhost/incubadoradacerveja/IC10000/client_area/index.php?tipo=paginas&page=acompanhamento.festa.degustador&idDoBarril=7
        //para confirmar se o convidado pagou ou n�o
        //� acessada apenas pelo dono da festa
        public function confirmaPedidoConvidado()
        {

            $input = file_get_contents("php://input");
            $args = json_decode($input);
            $sessaoFacebook = null;
            $pagou = false;
            $idPedido = null;
            $idBarril = null;

            if ($args != null)
            {
                $sessaoFacebook = $args->sessaoFacebook;
                $pagou = $args->pagou;
                $idPedido = $args->idPedido;
                $idBarril = $args->idBarril;
            }
            else
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO);
            }

            $db = new Database();
            $boBarril = new BO_Barril();
            if (!$boBarril->barrilPertenceAoCervejeiroLogado($db, $idBarril, $sessaoFacebook))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO, "Area restrita");
            }

            $novoEstado = $pagou ? BO_Estado_pedido::pagamento_confirmado : BO_Estado_pedido::nao_pagou;

            $q = "UPDATE pedido
        SET estado_pedido_id_INT = " . $novoEstado . "
        WHERE barril_id_INT = $idBarril AND id={$idPedido}";

            $db->query($q);

            $pedido = new stdClass();
            $pedido->pagou = $pagou;
            $pedido->idNovoEstadoConvite = $novoEstado;
            $pedido->novoEstado = utf8_encode(BO_Estado_pedido::getLabelParaODono($novoEstado));

            return new Mensagem_generica($pedido);
        }

        public function getDadosBarrilParaODono($db, $idBarril)
        {
            $q = "SELECT b.cervejeiro_id_INT idCervejeiro
    , c.tipo_cervejeiro_id_INT idTipoCervejeiro
    , b.json jsonBarril
    , tc.id idTipoCerveja
    , tc.nome tipoCerveja
    , c.nome nomeCervejeiro
    , c.lat_INT
    , c.lng_INT 
    , b.estado_barril_id_INT idEstadoBarril";

            $q .= " FROM barril b 
  left join tipo_cerveja tc ON b.tipo_cerveja_id_INT = tc.id
  join cervejeiro c ON c.id = b.cervejeiro_id_INT ";
            $q .= " WHERE b.id = $idBarril ";
            $db->query($q);
            $obj = Helper::getResultSetToArrayDeObjetos($db->result);

            if (count($obj))
            {
                $dadosBarril = $obj[0];
                $jsonBarril = $dadosBarril->jsonBarril;
                $dadosBarril->volumeTotal = 0;
                if (strlen($jsonBarril))
                {
                    $objJsonBarril = json_decode($jsonBarril);
                    if (isset($objJsonBarril->volumeTotalProducao))
                    {
                        $dadosBarril->volumeTotal = $objJsonBarril->volumeTotalProducao;
                    }
                    else
                    {
                        $dadosBarril->volumeTotal = $objJsonBarril->volumeTotalEmLitros;
                    }
                }
                $boBarril = new BO_Barril();
                $dadosBarril->totalLitrosComprometidos = $boBarril->getTotalLitrosOcupados(
                    $idBarril
                    , array(
                        BO_Estado_pedido::pagamento_confirmado
                    , BO_Estado_pedido::disse_que_pagou)
                    , $db);

//            $dadosBarril->totalLitrosConfirmados = $totalLitrosConfirmados;
//            $dadosBarril->totalLitrosNaoConfirmados = $totalLitrosNaoConfirmados;
//            $dadosBarril->totalLitrosBloqueados = $totalLitrosBloqueados;

                $boBarril = new BO_Barril();
                $dadosBarril->totalLitrosComprometidos = $boBarril->getTotalLitrosOcupados(
                    $idBarril
                    , array(BO_Estado_pedido::disse_que_pagou, BO_Estado_pedido::pagamento_confirmado));

                unset($dadosBarril->jsonBarril);
                $dadosBarril->estadoBarril = utf8_encode(
                    BO_Estado_barril::getLabel(
                        $dadosBarril->idTipoCervejeiro, $dadosBarril->idEstadoBarril));

                return $dadosBarril;
            }
            else
            {
                return null;
            }
        }

        public function cancelarFesta()
        {
            $args = json_decode(file_get_contents("php://input"));
            $idBarril = $args->idBarril;
            if (!strlen($idBarril))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO, I18N::getExpression('�rea restrita'));
            }
            $db = new Database();
            $q = "SELECT b.id idBarril
                , co.id idConvite
                , ce.id idCervejeiro
                , ce.facebook_id idFacebook
            FROM barril b 
              JOIN convite co 
                ON b.id = co.barril_id_INT
              JOIN cervejeiro ce
                ON ce.id = co.convidado_cervejeiro_id_INT
            WHERE b.id = $idBarril AND co.estado_convite_id_INT != " . BO_Estado_convite::nao_vai;
            $db->query($q);
            $objs = Helper::getResultSetToMatrizObj($db->result);

            $qUpdate = "UPDATE barril
            SET estado_barril_id_INT = " . BO_Estado_barril::cancelado_pelo_cervejeiro
                . " WHERE  id = {$idBarril}";
            $db->query($qUpdate);

            return new Mensagem_generica($objs);
        }

        public function getDadosFormularioPedir()
        {
            $db = new Database();

            $args = json_decode(file_get_contents("php://input"));

            $boCervejeiro = new BO_Cervejeiro();
            $idCervejeiro = $boCervejeiro->getIdDoCervejeiroLogado($args->sessaoFacebook);
            if (!strlen($idCervejeiro))
            {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ACESSO_NEGADO
                    , I18N::getExpression("Realize o login no facebook para prosseguir."));
            }
            $boConvite = new BO_Convite();
            $msg = $boConvite->geraChaveQrCodeConvite(
                $db
                , $args->idBarril
                , $args->idCervejeiroConvidado);
            if (!Interface_mensagem::checkOk($msg))
            {
                return $msg;
            }
            $qrCode = $msg->mObj;
            $obj = new stdClass();
            $obj->chave = $qrCode;

            return new Mensagem_generica($qrCode);
        }

        public function getTotalLitrosDoConvidadoCervejeiro(
            $idBarril
            , $idConvidadoCervejeiro
            , $idsEstadoPedido
            , $db = null)
        {

            $in = Helper::arrayToString($idsEstadoPedido);

            $q = "SELECT SUM(qtd_litros_INT)
        FROM pedido
        WHERE estado_pedido_id_INT in ( $in ) 
          AND convidado_cervejeiro_id_INT = $idConvidadoCervejeiro
          AND barril_id_INT = $idBarril";
            if ($db == null)
            {
                $db = new Database();
            }
            $db->query($q);

            return $db->getPrimeiraTuplaDoResultSet(0);
        }

    }