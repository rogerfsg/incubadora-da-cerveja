<?php

    class BO_Pedido_simples
    {
        public $mercadoPagoInstance;

        public function __construct()
        {
            $this->loadMercadoPagoLibrary();

            //seta chaves da SDK do MercadoPago
            MercadoPago\SDK::setClientId(MERCADO_PAGO_CLIENT_ID);
            MercadoPago\SDK::setClientSecret(MERCADO_PAGO_CLIENT_SECRET);
        }

        public function loadMercadoPagoLibrary()
        {
            require_once Helper::acharRaiz() . "vendor/autoload.php";
        }

        public static function factory()
        {
            return new BO_Pedido_simples();
        }

        public function gerarUrlDoMercadoPagoParaPagamento()
        {
            try
            {
                //dados gerais do pedido recebidos por POST
                $dadosGeraisDoPedido = json_decode(file_get_contents("php://input"));

                //objeto principal da lib do MercadoPago
                $mercadoPagoPreference = new MercadoPago\Preference();
                $arrMercadoPagoItens = array();

                //dados que foram postados da tela de compra
                $itensPedidoPostado = $dadosGeraisDoPedido->itensPedido;
                $dadosComprador = $dadosGeraisDoPedido->dadosUsuarioLogado;
                $enderecoEntrega = $dadosGeraisDoPedido->enderecoEntrega;

                //seta itens do pedido
                $valorTotalPedido = 0;
                foreach($itensPedidoPostado as $chave => $itemPedido)
                {
                    $item = new MercadoPago\Item();
                    $item->id = $itemPedido->identificador;
                    $item->title = $itemPedido->descricao;
                    $item->quantity = $itemPedido->quantidade;
                    $item->currency_id = "BRL";
                    $item->unit_price = $itemPedido->preco;

                    $arrMercadoPagoItens[] = $item;
                    $valorTotalPedido += $itemPedido->preco;
                }

                //incluindo item do frete
                $valorFrete = 6.00;
                $item = new MercadoPago\Item();
                $item->id = 99;
                $item->title = "Frete (Servi�o de entrega Rappi)";
                $item->quantity = 1;
                $item->currency_id = "BRL";
                $item->unit_price = $valorFrete;

                $arrMercadoPagoItens[] = $item;
                $valorTotalPedido += $valorFrete;

                //seta os itens no objeto do MercadoPago
                $mercadoPagoPreference->items = $arrMercadoPagoItens;

                //seta dados do comprador
                $payer = new MercadoPago\Payer();
                $payer->name = $dadosComprador->nomeCompleto;
                $payer->surname = "";
                $payer->email = $dadosComprador->email;

                if(!Helper::isNullOrEmpty($dadosComprador->telefone))
                {
                    $payer->phone = array(
                        "area_code" => "",
                        "number" => $dadosComprador->telefone
                    );
                }

                //identifica��o do comprador
                $payer->identification = array(
                    "type" => "CPF",
                    "number" => $dadosComprador->cpf
                );

                //seta objeto com o endere�o
                $arrDadosEntregaMercadoPago = array(
                    "street_name" => $enderecoEntrega->logradouro,
                    "street_number" => $enderecoEntrega->numero,
                    "apartment" => $enderecoEntrega->complemento,
                    "zip_code" => $enderecoEntrega->cep,
                    "city" => $enderecoEntrega->municipio
                );

                $payer->address = $arrDadosEntregaMercadoPago;

                //seta o endere�o de entrega - N�o usar frete, ser� inclu�do na lista de produtos
                //$shipments = new MercadoPago\Shipments();
                //$shipments->receiver_address = $arrDadosEntregaMercadoPago;

                //seta dados do comprador no objeto
                $mercadoPagoPreference->payer = $payer;

                //exclui formas de pagamento
                $mercadoPagoPreference->payment_methods = array(
                    "excluded_payment_types" => array(
                        array("id" => "ticket", "prepaid_card", "bank_transfer", "atm")
                    ),
                    "installments" => 2
                );

                //seta URLs de retorno
                $urlBase = URL_SITE . "/client_area/index.php?tipo=paginas&page=pedido.simples.retorno";
                $mercadoPagoPreference->back_urls = array(
                    "success" => "{$urlBase}&tipoRetorno=success",
                    "failure" => "{$urlBase}&tipoRetorno=failure",
                    "pending" => "{$urlBase}&tipoRetorno=pending"
                );

                $mercadoPagoPreference->auto_return = "approved";
                $mercadoPagoPreference->external_reference = "teste";
                $mercadoPagoPreference->save();

                $urlGeradaMercadoPago = $mercadoPagoPreference->init_point;
                $idPedidoMercadoPago = $mercadoPagoPreference->id;

                $dadosGeraisDoPedido->urlMercadoPago = $urlGeradaMercadoPago;
                $dadosGeraisDoPedido->idPedidoMercadoPago = $idPedidoMercadoPago;

                $strValorPedido = Helper::formatarFloatParaComandoSQL($valorTotalPedido);
                $jsonPedido = json_encode($dadosGeraisDoPedido);

                $db = new Database();
                $msg = $db->queryMensagem(
                    "INSERT INTO `pedido` (
                    `convidado_cervejeiro_id_INT`
                    , `barril_id_INT`
                    , `json`
                    , `qtd_litros_INT`
                    , `estado_pedido_id_INT`
                    , `data`
                    , `valor`) 
                    VALUES ( 
                      null
                    , null
                    , {$db->formatarDados($jsonPedido)} 
                    , null
                    , null
                    , '" . Helper::getDiaEHoraAtualSQL() . "'
                    ,  $strValorPedido )");

                if (Interface_mensagem::checkErro($msg))
                {
                    return $msg;
                }
                else
                {
                    return new Mensagem_generica($urlGeradaMercadoPago);
                }
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

    }

?>
