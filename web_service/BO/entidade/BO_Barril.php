<?php

    /**
     * Created by PhpStorm.
     * User: W10
     * Date: 14/03/2018
     * Time: 19:49
     */
    class BO_Barril
    {
        public function __construct()
        {
        }

        public static function factory()
        {
            return new BO_Barril();
        }

        public function getTotalLitrosOcupados($idBarril, $idsEstadoPedido, $db = null)
        {
            $in = Helper::arrayToString($idsEstadoPedido);

            $q = "SELECT SUM(qtd_litros_INT)
        FROM pedido
        WHERE estado_pedido_id_INT in ( $in ) 
          AND barril_id_INT = $idBarril";

            $db = new Database();
            $db->query($q);

            return $db->getPrimeiraTuplaDoResultSet(0);
        }

        public function getDadosDaFestaDoDegustador()
        {
            $args = json_decode(file_get_contents("php://input"));
            $idBarril = $args->idBarril;
            if (!strlen($idBarril))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO, I18N::getExpression('�rea restrita'));
            }

            $boCervejeiro = new BO_Cervejeiro();
            $idCervejeiroLogado = $boCervejeiro->getIdDoCervejeiroLogado($args->sessaoFacebook);
            if (!strlen($idCervejeiroLogado))
            {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ACESSO_NEGADO
                    , I18N::getExpression('Fa�a o login no facebook para continuar'));
            }
            $q2 = "SELECT 1 
                    FROM barril 
                    WHERE id = $idBarril 
                      AND cervejeiro_id_INT = $idCervejeiroLogado";
            $db = new Database();
            $db->query($q2);

            if ($db->getPrimeiraTuplaDoResultSet(0) != 1)
            {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ACESSO_NEGADO
                    , I18N::getExpression('�rea restrita ao dono do Barril')
                );
            }

            $q = "SELECT p.id idPedido
                , c2.nome nomeConvidado
              , p.estado_pedido_id_INT idEstadoPedido
               , p.qtd_litros_INT qtdLitros
               , p.valor valor
               , p.data data
               , c2.facebook_id facebookId
            FROM pedido p 
              JOIN barril b ON p.barril_id_INT = b.id
              
              JOIN cervejeiro c2 ON p.convidado_cervejeiro_id_INT = c2.id
            WHERE b.cervejeiro_id_INT = {$idCervejeiroLogado}
                AND p.barril_id_INT = $idBarril";

            $db->query($q);
            $rs = Helper::getResultSetToMatrizObj($db->result);

            $valorConfirmado = 0;
            $valorNaoConfirmado = 0;
            $totalLitrosConfirmados = 0;
            $totalLitrosNaoConfirmados = 0;
            if (count($rs))
            {
                for ($i = 0; $i < count($rs); $i++)
                {
                    $dados = $rs[ $i ];
                    $rs[ $i ]->estadoPedido = utf8_encode(BO_Estado_pedido::getLabelParaODono($dados->idEstadoPedido));
                    $rs[ $i ]->data = Helper::formatarDataTimeParaExibicao($rs[ $i ]->data);

                    switch ($dados->idEstadoPedido)
                    {
                        case BO_Estado_pedido::pagamento_confirmado:
                            $totalLitrosConfirmados += $rs[ $i ]->qtdLitros;
                            $valorConfirmado += $rs[ $i ]->valor;
                            break;
                        case BO_Estado_pedido::nao_pagou:
                            $totalLitrosNaoConfirmados += $rs[ $i ]->qtdLitros;;
                            $valorNaoConfirmado += $rs[ $i ]->valor;
                            break;
                    }
                    //depois de utilizar o valor util, transformamos o atributo valor para exibicao
                    $rs[ $i ]->valor = Helper::formatarFloatParaExibicao($rs[ $i ]->valor);
                }
            }

            $ret = new stdClass();
            $ret->pedidos = $rs;
            $ret->valorNaoConfirmado = Helper::formatarFloatParaExibicao($valorNaoConfirmado);
            $ret->valorConfirmado = Helper::formatarFloatParaExibicao($valorConfirmado);
            $ret->totalLitrosConfirmados = $totalLitrosConfirmados;
            $ret->totalLitrosNaoConfirmados = $totalLitrosNaoConfirmados;

            $ret->barril = $this->getDadosBarrilParaODono($db, $idBarril);
            $ret->convidadosSemBeber = $this->getCervejeirosConvidadosQueNaoVaoBeber($db, $idBarril);
            $ret->convidadosNaoConfirmados = $this->getCervejeirosConvidadosQueAindaNaoConfirmaram($db, $idBarril);
            $boConvite = new BO_Convite();
            $ret->totalConvidados = $boConvite->getTotalConvidados($db, $idBarril);
            //TODO alterar para o valor vindo do barril
            $ret->maxConvidados = 100;

            $boPedidoDebito = new BO_Pedido_debito();
            $pedidoDebito = $boPedidoDebito->getPedidosDebito($idBarril, $db);

            $ret->pedidosDebito = $pedidoDebito->matriz;
            $ret->totalLitrosJaBebidos = $pedidoDebito->totalLitrosJaBebidos;
            $ret->saldo = $this->getSaldoDosDegustadores($db, $idBarril);
            $ret->convite = $this->getConvite($idBarril);

            return new Mensagem_generica($ret);
        }

        public function getConvite($idDoBarril)
        {
            $objCrypt = new Crypt();
//            $objCrypt->
            $campo = $objCrypt->cryptGet(
                array("idDoBarril"),
                array($idDoBarril),
                360 * 24 * 60);

            return $campo;
        }

        public function getDadosBarrilParaODono($db, $idBarril)
        {
            $q = "SELECT b.cervejeiro_id_INT idCervejeiro
    , c.tipo_cervejeiro_id_INT idTipoCervejeiro
    , b.json jsonBarril
    , tc.id idTipoCerveja
    , tc.nome tipoCerveja
    , c.nome nomeCervejeiro
    , c.lat_INT
    , c.lng_INT 
    , b.estado_barril_id_INT idEstadoBarril";

            $q .= " FROM barril b 
  left join tipo_cerveja tc ON b.tipo_cerveja_id_INT = tc.id
  join cervejeiro c ON c.id = b.cervejeiro_id_INT ";
            $q .= " WHERE b.id = $idBarril ";
            $db->query($q);
            $obj = Helper::getResultSetToArrayDeObjetos($db->result);

            if (count($obj))
            {
                $dadosBarril = $obj[0];
                $jsonBarril = $dadosBarril->jsonBarril;
                $dadosBarril->volumeTotal = 0;
                if (strlen($jsonBarril))
                {
                    $objJsonBarril = json_decode($jsonBarril);
                    $dadosBarril->volumeTotal = 0;
                    if (isset($objJsonBarril->volumeTotalProducao))
                    {
                        $dadosBarril->volumeTotal = $objJsonBarril->volumeTotalProducao;
                    }
                    else
                    {
                        $dadosBarril->volumeTotal = $objJsonBarril->volumeTotalEmLitros;
                    }
                }
                $dadosBarril->totalLitrosComprometidos = $this->getTotalLitrosOcupados(
                    $idBarril
                    , array(
                        BO_Estado_pedido::pagamento_confirmado
                    , BO_Estado_pedido::disse_que_pagou)
                    , $db);

//            $dadosBarril->totalLitrosConfirmados = $totalLitrosConfirmados;
//            $dadosBarril->totalLitrosNaoConfirmados = $totalLitrosNaoConfirmados;
//            $dadosBarril->totalLitrosBloqueados = $totalLitrosBloqueados;

                $boBarril = new BO_Barril();
                $dadosBarril->totalLitrosComprometidos = $this->getTotalLitrosOcupados(
                    $idBarril
                    , array(BO_Estado_pedido::disse_que_pagou, BO_Estado_pedido::pagamento_confirmado));

                $dados = json_decode($dadosBarril->jsonBarril);
                unset($dadosBarril->jsonBarril);
                $estadoBarril = BO_Estado_barril::getLabel(
                    $dadosBarril->idTipoCervejeiro, $dadosBarril->idEstadoBarril);
                $dadosBarril->estadoBarril = utf8_encode($estadoBarril);
                $dadosBarril->objJson = $dados;

                return $dadosBarril;
            }
            else
            {
                return null;
            }
        }

        public function barrilPertenceAoCervejeiroLogado($db, $idBarril, $sessaoFacebook = null)
        {

            $hf = new HelperFacebook($sessaoFacebook);
            $me = $hf->loadMe();

            $db->query(
                "SELECT 1 
          FROM cervejeiro c
            JOIN barril b ON b.cervejeiro_id_INT = c.id
          WHERE b.id=$idBarril AND c.facebook_id= '{$me->getId()}'");

            return $db->getPrimeiraTuplaDoResultSet(0) == 1;
        }

        public function getCervejeirosConvidadosQueNaoVaoBeber($db, $idBarril)
        {

            $q = "SELECT c.convidado_cervejeiro_id_INT idCervejeiroConvidado
              , c.estado_convite_id_INT idEstadoConvite
              , c.historico historico
              , ce.nome nomeConvidado
              , ce.facebook_id facebookIdConvidado
              , c.data
            FROM convite c 
              join cervejeiro ce 
                ON c.convidado_cervejeiro_id_INT = ce.id
            WHERE c.barril_id_INT=$idBarril AND estado_convite_id_INT = " . BO_Estado_convite::vai_sem_beber;

            $db->query($q);
            $convites = Helper::getResultSetToMatrizObj($db->result);

            for ($i = 0; $i < count($convites); $i++)
            {
                $convites[ $i ]->estadoConvite = BO_Estado_convite::getLabel($convites[ $i ]->idEstadoConvite);
            }

            return $convites;
        }

        public function getCervejeirosConvidadosQueAindaNaoConfirmaram($db, $idBarril)
        {

            $q = "SELECT c.convidado_cervejeiro_id_INT idCervejeiroConvidado
              , c.estado_convite_id_INT idEstadoConvite
              , c.historico historico
              , ce.nome nomeConvidado
              , ce.facebook_id facebookIdConvidado
              , c.data
            FROM convite c 
              join cervejeiro ce 
              ON c.convidado_cervejeiro_id_INT = ce.id
            WHERE c.barril_id_INT=$idBarril AND estado_convite_id_INT = " . BO_Estado_convite::visualizou_mas_nao_confirmou;

            $db->query($q);
            $convites = Helper::getResultSetToMatrizObj($db->result);

            for ($i = 0; $i < count($convites); $i++)
            {
                $convites[ $i ]->estadoConvite = utf8_encode(BO_Estado_convite::getLabel($convites[ $i ]->idEstadoConvite));
            }

            return $convites;
        }

        public function getVolumeMaximoBarril(
            Database $db
            , $idBarril)
        {
            $q = "SELECT  b.json jsonBarril
            FROM barril b 
            WHERE b.id = $idBarril ";
            $db->query($q);
            $json = $db->getPrimeiraTuplaDoResultSet(0);

            if (strlen($json))
            {
                $obj = json_decode($json);
                if ($obj != null)
                {
                    if (isset($obj->volumeTotalProducao))
                    {
                        return $obj->volumeTotalProducao;
                    }
                    else
                    {
                        return $obj->volumeTotalEmLitros;
                    }
                }
            }

            return 0;
        }

        public function getSaldoDosDegustadores($db, $idBarril)
        {

            $q = "SELECT c.id idCervejeiro, c.nome nomeCervejeiro, SUM(p.qtd_litros_INT) pago 
FROM cervejeiro c
	JOIN convite co 
		ON co.convidado_cervejeiro_id_INT = c.id
	JOIN pedido p
		ON p.convidado_cervejeiro_id_INT = co.convidado_cervejeiro_id_INT
	
WHERE p.estado_pedido_id_INT = " . BO_Estado_pedido::pagamento_confirmado . "
    AND p.barril_id_INT = $idBarril
	
GROUP BY c.id, c.nome
ORDER BY c.id";
            $db->query($q);
            $m = Helper::getResultSetToMatrizObj($db->result);

            $q2 = "SELECT c.id idCervejeiro, SUM(pd.qtd_litros_INT) bebido 
FROM cervejeiro c
	JOIN convite co 
		ON co.convidado_cervejeiro_id_INT = c.id
	
	JOIN pedido_debito pd
		ON pd.convite_id_INT = co.id
WHERE co.barril_id_INT = $idBarril
	AND pd.cancelador_cervejeiro_id_INT IS NULL
GROUP BY c.id
ORDER BY c.id";
            $db->query($q2);
            $m2 = Helper::getResultSetToMatrizObj($db->result);
            $totalPago = 0;
            $totalBebido = 0;
            $totalSaldo = 0;
            for ($i = 0; $i < count($m); $i++)
            {
                for ($j = 0; $j < count($m2); $j++)
                {
                    if ($m[ $i ]->idCervejeiro == $m2[ $j ]->idCervejeiro)
                    {
                        $m[ $i ]->saldo = $m[ $i ]->pago - $m2[ $j ]->bebido;
                        $m[ $i ]->bebido = $m2[ $j ]->bebido;
                        $totalPago += $m[ $i ]->pago;
                        $totalBebido += $m[ $i ]->bebido;
                        $totalSaldo += $m[ $i ]->saldo;
                        break;
                    }
                }
            }
            $ret = new stdClass();
            $ret->totalPago = $totalPago;
            $ret->totalBebido = $totalBebido;
            $ret->totalSaldo = $totalSaldo;
            $ret->convidados = $m;

            return $ret;
        }

    }