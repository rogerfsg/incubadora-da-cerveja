<?php

    /**
     * Created by PhpStorm.
     * User: W10
     * Date: 13/03/2018
     * Time: 18:18
     */
    class BO_Estado_pedido
    {
        const    disse_que_pagou = 1;
        const    pagamento_confirmado = 2;
        const    nao_pagou = 3;
        const    confirmou_presenca_e_nao_vai_beber = 4;

        //const	motorista_da_rodada = 5;

        public static function getLabel($id)
        {
            switch ($id)
            {
                case self::disse_que_pagou;
                    return I18N::getExpression("Aguardando confirma��o do pagamento");
                case self::nao_pagou;
                    return I18N::getExpression("Desculpa, n�o encontrei seu comprovante de pagamento");
                case self::pagamento_confirmado;
                    return I18N::getExpression("Pagamento confirmado");
                case self::confirmou_presenca_e_nao_vai_beber;
                    return I18N::getExpression("Confirmei presen�a e n�o vou beber");
            }

            return null;
        }

        public static function getLabelParaODono($id)
        {
            switch ($id)
            {
                case self::disse_que_pagou;
                    return I18N::getExpression("Confirmar o deposito");
                case self::nao_pagou;
                    return I18N::getExpression("N�o encontrei seu pagamento");
                case self::pagamento_confirmado;
                    return I18N::getExpression("Pagamento confirmado");
                case self::confirmou_presenca_e_nao_vai_beber;
                    return I18N::getExpression("Confirmou presen�a e n�o vai beber");
            }

            return null;
        }

    }