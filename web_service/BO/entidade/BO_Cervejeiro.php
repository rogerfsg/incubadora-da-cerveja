<?php

    class BO_Cervejeiro
    {
        public static function factory()
        {
            return new BO_Cervejeiro();
        }

        public function getDadosBasicosUsuario()
        {
            try
            {
                $idFacebook = Helper::GET("idFacebook");
                if (!Helper::isNullOrEmpty($idFacebook))
                {
                    $db = new Database();
                    $db->query("SELECT id, tipo_cervejeiro_id_INT FROM cervejeiro WHERE facebook_id= '{$idFacebook}'");

                    if ($db->rows() > 0)
                    {
                        $idCervejeiro = $db->getPrimeiraTuplaDoResultSet(0);
                        $idTipoCervejeiro = $db->getPrimeiraTuplaDoResultSet(1);

                        $objRetorno = new stdClass();
                        $objRetorno->idCervejeiro = $idCervejeiro;
                        $objRetorno->idTipoCervejeiro = $idTipoCervejeiro;

                        return new Mensagem_generica($objRetorno);
                    }
                    else
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO,
                                            I18N::getExpression("Usu�rio n�o cadastrado."));
                    }
                }
                else
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO,
                                        I18N::getExpression("Par�metros inv�lidos."));
                }
            }
            catch (Exception $ex)
            {
                HelperLog::logException($ex, true);

                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, null, $ex);
            }
        }

        public function getDadosCompletosUsuario()
        {
            try
            {
                $idCervjeiro = Helper::GET("idCervejeiro");
                if (!Helper::isNullOrEmpty($idCervjeiro))
                {
                    $db = new Database();
                    $db->query("SELECT json FROM cervejeiro WHERE id= '{$idCervjeiro}'");

                    if ($db->rows() > 0)
                    {
                        $jsonCervejeiro = $db->getPrimeiraTuplaDoResultSet(0);
                        $dadosFormulario = json_decode($jsonCervejeiro);

                        $objRetorno = new stdClass();
                        $objRetorno->idCervejeiro = $idCervjeiro;
                        $objRetorno->dadosFormulario = $dadosFormulario;

                        if (!Helper::isNullOrEmpty($objRetorno))
                        {
                            return new Mensagem_generica($objRetorno, null, "Dados carregados com sucesso.");
                        }
                        else
                        {
                            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                                                I18N::getExpression("Dados n�o encontrados."));
                        }
                    }
                    else
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                                            I18N::getExpression("Dados n�o encontrados."));
                    }
                }
                else
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO,
                                        I18N::getExpression("Par�metros inv�lidos."));
                }
            }
            catch (Exception $ex)
            {
                HelperLog::logException($ex, true);

                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, null, $ex);
            }
        }

        public function getDadosFesta()
        {
            try
            {
                $args = json_decode(file_get_contents("php://input"));

                //Persistencia no banco
                if (!is_null($args))
                {
                    $db = new Database();

                    $idCervejeiro = $args->idCervejeiro;
                    $idBarril = $args->idBarril;

                    if (!is_null($idCervejeiro) && !is_null($idBarril))
                    {
                        //faz a busca da festa em quest�o na tabela de barril
                        $msgBarril = $db->queryMensagem("SELECT `json` FROM `barril`
                                                                 WHERE `is_ativo_BOOLEAN` = 1
                                                                     AND `cervejeiro_id_INT` = {$idCervejeiro}
                                                                     AND `id` = {$idBarril}");

                        if (Interface_mensagem::checkErro($msgBarril))
                        {
                            return $msgBarril;
                        }
                        else
                        {
                            if ($db->rows() > 0)
                            {
                                $jsonBarril = $db->getPrimeiraTuplaDoResultSet(0);
                                $objBarril = json_decode($jsonBarril);

                                if (!Helper::isNullOrEmpty($objBarril))
                                {
                                    return new Mensagem_generica($objBarril, null, "Dados carregados com sucesso.");
                                }
                                else
                                {
                                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                                                        I18N::getExpression("Dados n�o encontrados."));
                                }
                            }
                            else
                            {
                                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                                                    I18N::getExpression("Dados n�o encontrados."));
                            }
                        }
                    }
                    else
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                                            I18N::getExpression("Par�metros inv�lidos."));
                    }
                }
                else
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                                        I18N::getExpression("Par�metros n�o definidos."));
                }
            }
            catch (Exception $ex)
            {
                HelperLog::logException($ex, true);

                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, null, $ex);
            }
        }

        public function getDadosBarrilEmpresa()
        {
            try
            {
                $args = json_decode(file_get_contents("php://input"));

                //Persistencia no banco
                if (!is_null($args))
                {
                    $db = new Database();
                    $idBarril = $args->idBarril;

                    $hf = new HelperFacebook($args->sessaoFacebook);
                    $me = $hf->loadMe();

                    if ($me == null)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                                            I18N::getExpression("Voc� deve realizar o login no Facebook."));
                    }
                    else
                    {
                        $idCervejeiro = $this->getIdDoCervejeiroLogado($args->sessaoFacebook);

                        if (!is_null($idCervejeiro) && !is_null($idBarril))
                        {
                            //faz a busca da festa em quest�o na tabela de barril
                            $msgBarril = $db->queryMensagem("SELECT `json` FROM `barril`
                                                                     WHERE `is_ativo_BOOLEAN` = 1
                                                                         AND `cervejeiro_id_INT` = {$idCervejeiro}
                                                                         AND `id` = {$idBarril}");

                            if (Interface_mensagem::checkErro($msgBarril))
                            {
                                return $msgBarril;
                            }
                            else
                            {
                                if ($db->rows() > 0)
                                {
                                    $jsonBarril = $db->getPrimeiraTuplaDoResultSet(0);
                                    $objBarril = json_decode($jsonBarril);

                                    if (!Helper::isNullOrEmpty($objBarril))
                                    {
                                        return new Mensagem_generica($objBarril, null, "Dados carregados com sucesso.");
                                    }
                                    else
                                    {
                                        return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                                                            I18N::getExpression("Dados n�o encontrados."));
                                    }
                                }
                                else
                                {
                                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                                                        I18N::getExpression("Dados n�o encontrados."));
                                }
                            }
                        }
                        else
                        {
                            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                                                I18N::getExpression("Par�metros inv�lidos."));
                        }
                    }
                }
                else
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                                        I18N::getExpression("Par�metros n�o definidos."));
                }
            }
            catch (Exception $ex)
            {
                HelperLog::logException($ex, true);

                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, null, $ex);
            }
        }

        public function cadastrarCervejeiroEmpresa()
        {
            try
            {
                $args = json_decode(file_get_contents("php://input"));

                //Persistencia no banco
                if (!is_null($args))
                {
                    $db = new Database();

                    $cpfCnpj = $args->cpf;
                    if (is_null($args->cpf))
                    {
                        $cpfCnpj = $args->cnpj;
                    }

                    $idCervejeiroFacebook = $this->getIdDoCervejeiroLogado($args->sessaoFacebook);
                    $idCervejeiro = $this->getIdCervejeiroComCpfCnpj($db, $cpfCnpj);

                    //Como podemos ter ate 3 chamados do mobile se tiver uma recente, enviamos a mensagem que ele j� foi cadastrado na primeirao
                    //TODO ajustar para verificar a data de cadastro, se esta a menos de um min, para trocar o retorno para
                    // CADASTRO REALIZADO COM SUCESSO
                    if ($idCervejeiro != null)
                    {
                        return new Mensagem(
                            PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                            I18N::getExpression("J� existe um cervejeiro com o CPNF/CNPJ {$cpfCnpj}"));
                    }
                    else
                    {
                        if ($idCervejeiroFacebook != null)
                        {
                            return new Mensagem(
                                PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                                I18N::getExpression("O seu facebook j� est� vinculado a um usu�rio"));
                        }
                    }

                    //se chegar ate aqui � porque o cervejeiro ainda n�o foi cadastrado
                    $idCervejeiro = $this->insertCervejeiroComFacebook($db, $args);

                    //itera os barris e insere no banco de dados
                    if (!is_null($idCervejeiro) && !is_null($args->barris) && is_array($args->barris))
                    {
                        for ($i = 0; $i < count($args->barris); $i++)
                        {
                            $barrilAtual = $args->barris[ $i ];
                            $jsonBarril = Helper::jsonEncode($barrilAtual);

                            //insere o barril corrente
                            $msgBarril = $db->queryMensagem("INSERT INTO `barril` (
                                                                          `cervejeiro_id_INT`
                                                                          , `tipo_cerveja_id_INT`
                                                                          , `is_ativo_BOOLEAN`
                                                                          , `json`
                                                                          , `estado_barril_id_INT`)
                                                                     VALUES ({$idCervejeiro}
                                                                     , {$barrilAtual->tipoCerveja}
                                                                     , 1
                                                                     , '{$jsonBarril}'
                                                                     , 1)");

                            if (Interface_mensagem::checkErro($msgBarril))
                            {
                                return $msgBarril;
                            }
                        }
                    }

                    return new Mensagem_generica($idCervejeiro);
                }
                else
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                                        I18N::getExpression("Dados inv�lidos."));
                }
            }
            catch (Exception $ex)
            {
                HelperLog::logException($ex, true);

                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, null, $ex);
            }
        }

        public function cadastrarBarrilEmpresa()
        {
            try
            {
                $args = json_decode(file_get_contents("php://input"));

                //Persistencia no banco
                if (!is_null($args))
                {
                    $db = new Database();
                    $hf = new HelperFacebook($args->sessaoFacebook);

                    $me = $hf->loadMe();
                    if ($me == null)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                                            I18N::getExpression("Voc� deve realizar o login no facebook"));
                    }
                    else
                    {
                        $dadosBarril = $args->dadosBarril;
                        $idTipoCerveja = $dadosBarril->tipoCerveja;
                        $idCervejeiro = $this->getIdDoCervejeiroLogado($args->sessaoFacebook);

                        if (!is_null($idTipoCerveja) && !is_null($idCervejeiro) && !is_null($dadosBarril))
                        {
                            $jsonBarril = Helper::jsonEncode($dadosBarril);

                            //insere o barril corrente
                            $msgBarril = $db->queryMensagem("INSERT INTO `barril` (
                                                                    `cervejeiro_id_INT`
                                                                  , `tipo_cerveja_id_INT`
                                                                  , `is_ativo_BOOLEAN`
                                                                  , `json`
                                                                  , `estado_barril_id_INT`)
                                                             VALUES ({$idCervejeiro}
                                                             , {$idTipoCerveja}
                                                             , 1
                                                             , '{$jsonBarril}'
                                                             , 1)");

                            if (Interface_mensagem::checkErro($msgBarril))
                            {
                                return $msgBarril;
                            }
                            else
                            {
                                $idBarril = $db->getLastInsertId();

                                return new Mensagem_generica($idBarril, PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, "Barril Cadastrado com sucesso.");
                            }
                        }
                        else
                        {
                            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                                                I18N::getExpression("Par�metros inv�lidos."));
                        }
                    }
                }
                else
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                                        I18N::getExpression("Par�metros inexistentes."));
                }
            }
            catch (Exception $ex)
            {
                HelperLog::logException($ex, true);

                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, null, $ex);
            }
        }

        public function cadastrarFesta()
        {
            try
            {
                $args = json_decode(file_get_contents("php://input"));

                //Persistencia no banco
                if (!is_null($args))
                {
                    $db = new Database();

                    $idCervejeiro = $args->idCervejeiro;
                    $dadosFesta = $args->dadosFesta;

                    if (!is_null($idCervejeiro) && !is_null($dadosFesta))
                    {
                        $jsonFesta = Helper::jsonEncode($dadosFesta);

                        //insere o barril corrente
                        $msgBarril = $db->queryMensagem("INSERT INTO `barril` (
                                                                        `cervejeiro_id_INT`
                                                                      , `tipo_cerveja_id_INT`
                                                                      , `is_ativo_BOOLEAN`
                                                                      , `json`
                                                                      , `estado_barril_id_INT`)
                                                                 VALUES ({$idCervejeiro}
                                                                 , null
                                                                 , 1
                                                                 , '{$jsonFesta}'
                                                                 , 1)");

                        if (Interface_mensagem::checkErro($msgBarril))
                        {
                            return $msgBarril;
                        }
                        else
                        {
                            $idBarril = $db->getLastInsertId();

                            return new Mensagem_generica($idBarril, null, "Festa criada com sucesso.");
                        }
                    }
                    else
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                                            I18N::getExpression("Dados inv�lidos."));
                    }
                }
            }
            catch (Exception $ex)
            {
                HelperLog::logException($ex, true);

                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, null, $ex);
            }
        }

        public function editarCervejeiroEmpresa()
        {
            try
            {
                $args = json_decode(file_get_contents("php://input"));

                //Persistencia no banco
                if (!is_null($args))
                {
                    $db = new Database();

                    //clona objeto inteiro
                    $objectCervejeiro = clone $args;

                    //remove sess�o facebook e id do cervejeiro
                    unset($objectCervejeiro->sessaoFacebook);
                    unset($objectCervejeiro->idCervejeiro);

                    //cria json do cervejeiro (sem a sess�o do facebook)
                    $jsonCervejeiro = Helper::jsonEncode($objectCervejeiro);

                    $hf = new HelperFacebook($args->sessaoFacebook);
                    $me = $hf->loadMe();

                    if ($me == null)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                                            I18N::getExpression("Voc� deve realizar o login no facebook"));
                    }
                    else
                    {
                        $idCervejeiro = $this->getIdDoCervejeiroLogado($args->sessaoFacebook);

                        $query = "UPDATE `cervejeiro` 
                                  SET `json` = '{$jsonCervejeiro}'
                                  WHERE `id` = {$idCervejeiro}";

                        $msgBanco = $db->queryMensagem($query);
                        if (Interface_mensagem::checkErro($msgBanco))
                        {
                            throw new Exception($msgBanco->mMensagem);
                        }

                        return new Mensagem_generica(null, PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, "Dados alterados com sucesso.");
                    }
                }
                else
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                                        I18N::getExpression("Dados inv�lidos."));
                }
            }
            catch (Exception $ex)
            {
                HelperLog::logException($ex, true);

                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, null, $ex);
            }
        }

        public function editarBarrilEmpresa()
        {
            try
            {
                $args = json_decode(file_get_contents("php://input"));

                //Persistencia no banco
                if (!is_null($args))
                {
                    $db = new Database();
                    $hf = new HelperFacebook($args->sessaoFacebook);

                    $me = $hf->loadMe();
                    if ($me == null)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                                            I18N::getExpression("Voc� deve realizar o login no facebook"));
                    }
                    else
                    {
                        $idCervejeiro = $this->getIdDoCervejeiroLogado($args->sessaoFacebook);
                        $idBarril = $args->idBarril;

                        $dadosBarril = $args->dadosBarril;
                        $idTipoCerveja = $dadosBarril->tipoCerveja;
                        if (!is_null($idBarril) && !is_null($idTipoCerveja) && !is_null($idCervejeiro) && !is_null($dadosBarril))
                        {
                            $jsonBarril = Helper::jsonEncode($dadosBarril);

                            //insere o barril corrente
                            $msgBarril = $db->queryMensagem("UPDATE `barril` 
                                                                    SET `json` = '{$jsonBarril}' ,
                                                                        `tipo_cerveja_id_INT` = {$idTipoCerveja}
                                                                   WHERE `id` = {$idBarril}
                                                                    AND `cervejeiro_id_INT` = {$idCervejeiro};");

                            if (Interface_mensagem::checkErro($msgBarril))
                            {
                                return $msgBarril;
                            }
                            else
                            {
                                return new Mensagem_generica($idBarril, null, "Dados do barril alterados com sucesso.");
                            }
                        }
                        else
                        {
                            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                                                I18N::getExpression("Par�metros inv�lidos."));
                        }
                    }
                }
                else
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                                        I18N::getExpression("Par�metros inexistentes."));
                }
            }
            catch (Exception $ex)
            {
                HelperLog::logException($ex, true);

                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, null, $ex);
            }
        }

        public function editarFesta()
        {
            try
            {
                $args = json_decode(file_get_contents("php://input"));

                //Persistencia no banco
                if (!is_null($args))
                {
                    $db = new Database();
                    $hf = new HelperFacebook($args->sessaoFacebook);

                    $me = $hf->loadMe();
                    if ($me == null)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                                            I18N::getExpression("Voc� deve realizar o login no facebook"));
                    }
                    else
                    {
                        $idCervejeiro = $this->getIdDoCervejeiroLogado($args->sessaoFacebook);
                        $idBarril = $args->idBarril;
                        $dadosFesta = $args->dadosFesta;

                        if (!is_null($idBarril) && !is_null($idCervejeiro) && !is_null($dadosFesta))
                        {
                            $jsonFesta = Helper::jsonEncode($dadosFesta);

                            //insere o barril corrente
                            $msgBarril = $db->queryMensagem("UPDATE `barril` 
                                                                    SET `json` = '{$jsonFesta}'
                                                                   WHERE `id` = {$idBarril}
                                                                    AND `cervejeiro_id_INT` = {$idCervejeiro};");

                            if (Interface_mensagem::checkErro($msgBarril))
                            {
                                return $msgBarril;
                            }
                            else
                            {
                                return new Mensagem_generica($idBarril, null, "Dados da festa alterados com sucesso.");
                            }
                        }
                        else
                        {
                            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                                                I18N::getExpression("Dados inv�lidos."));
                        }
                    }
                }
            }
            catch (Exception $ex)
            {
                HelperLog::logException($ex, true);

                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, null, $ex);
            }
        }

        /*
         * -----------------------------
         * M�todos internos
         * -----------------------------
         */

        public function getIdCervejeiroComFacebook(Database $db, $idFacebook)
        {
            $db->query("SELECT id FROM cervejeiro WHERE facebook_id= '{$idFacebook}'");
            $idCervejeiro = $db->getPrimeiraTuplaDoResultSet(0);
            if (strlen($idCervejeiro))
            {
                return $idCervejeiro;
            }
        }

        public function getIdCervejeiroComCpfCnpj(Database $db, $cpfCnpj)
        {
            $db->query("SELECT id FROM cervejeiro WHERE cpf_cnpj = $cpfCnpj");

            return $db->getPrimeiraTuplaDoResultSet(0);
        }

        public function insertCervejeiroComFacebook($db, $args)
        {
            $cpfCnpj = $args->cpf;
            if (is_null($args->cpf))
            {
                $cpfCnpj = $args->cnpj;
            }

            //clona objeto inteiro
            $objectCervejeiro = clone $args;

            //remove atributo barris
            unset($objectCervejeiro->barris);

            //remove sess�o facebook
            unset($objectCervejeiro->sessaoFacebook);

            //cria json do cervejeiro (sem o vetor dos barris)
            $jsonCervejeiro = Helper::jsonEncode($objectCervejeiro);

            $hf = new HelperFacebook($args->sessaoFacebook);
            $me = $hf->loadMe();

            if ($me == null)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                                    I18N::getExpression("Voc� deve realizar o login no facebook"));
            }
            else
            {
                $query = "INSERT INTO `cervejeiro` (`nome`, `facebook_id`, `cpf_cnpj`, `tipo_cervejeiro_id_INT`, `json`)
                         VALUES ('{$args->nome}'
                         , '{$me->getId()}'
                         , '{$cpfCnpj}'
                         , '{$args->idDoTipoDoUsuario}'
                         , '{$jsonCervejeiro}')";

                $msgBanco = $db->queryMensagem($query);
                if (Interface_mensagem::checkErro($msgBanco))
                {
                    throw new Exception($msgBanco->mMensagem);
                }

                $idCervejeiro = $db->getLastInsertId();

                return $idCervejeiro;
            }
        }

        public function getIdDoCervejeiroLogado($sessaoFacebook = null, $db = null)
        {
            $hf = new HelperFacebook($sessaoFacebook);
            $me = $hf->loadMe();
            if ($db == null)
            {
                $db = new Database();
            }

            return $this->getIdCervejeiroDoFacebookId($db, $me->getId());
        }

        public function getIdCervejeiroDoFacebookId($db, $idFacebook)
        {
            $db->query("SELECT id FROM cervejeiro WHERE facebook_id= '{$idFacebook}'");

            return $db->getPrimeiraTuplaDoResultSet(0);
        }

        public function getIdETipoDoCervejeiroLogado(Database $db, $sessaoFacebook = null)
        {
            $hf = new HelperFacebook($sessaoFacebook);
            $me = $hf->loadMe();

            $db->query("SELECT id, tipo_cervejeiro_id_INT idTipoCervejeiro, json jsonCervejeiro
                                 FROM cervejeiro
                                  WHERE facebook_id= '{$me->getId()}'");
            $m = Helper::getResultSetToMatrizObj($db->result);
            if (count($m))
            {
                return $m[0];
            }
            else
            {
                return null;
            }
        }

        public function getDadosLogin(Database $db, $sessaoFacebook = null)
        {
            $hf = new HelperFacebook($sessaoFacebook);

            $me = $hf->loadMe();
            if ($me == null)
            {
                return null;
            }

            $db->query("SELECT tc.nome tipoCervejeiro
                                  FROM cervejeiro c 
                                    JOIN tipo_cervejeiro tc ON tc.id = c.tipo_cervejeiro_id_INT
                                  WHERE c.facebook_id= '{$me->getId()}'");

            $m = Helper::getResultSetToMatrizObj($db->result);
            if (count($m))
            {
                return $m[0];
            }
            else
            {
                return null;
            }
        }

        public function getDadosLoginCervejeiro()
        {

            $args = json_decode(file_get_contents("php://input"));

            //Persistencia no banco
            $db = new Database();

            $idCervejeiroFacebook = $this->getDadosLogin($db, $args->sessaoFacebook);
            if ($idCervejeiroFacebook == null)
            {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                    "Realize o login pelo facebook");
            }

            return new Mensagem_generica($idCervejeiroFacebook);
        }
    }

?>