<?php

    class BO_Tipo_cerveja
    {

        public function getTiposCerveja()
        {
            try
            {
                $db = new Database();
                $msg = $db->queryMensagem("SELECT id, nome from tipo_cerveja");

                if (Interface_mensagem::checkErro($msg))
                {
                    $db->closeResult();

                    return $msg;
                }
                else
                {
                    $matriz = Helper::getResultSetToMatrizObj($db->result);
                    $db->closeResult();

                    return new Mensagem_generica($matriz, PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
                }
            }
            catch (Exception $ex)
            {
                HelperLog::logException($ex, true);

                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, null, $ex);
            }
        }

        public function getListaTiposCerveja(Database $db)
        {
            $db->query("SELECT id, nome from tipo_cerveja");

            $matriz = Helper::getResultSetToMatrizObj($db->result);

            return $matriz;
        }

        public static function factory()
        {
            return new BO_Tipo_cerveja();
        }

    }
