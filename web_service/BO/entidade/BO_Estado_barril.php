<?php

    /**
     * Created by PhpStorm.
     * User: W10
     * Date: 09/03/2018
     * Time: 20:10
     */
    class BO_Estado_barril
    {
        const    crowdfunding = 1;
        const    executando = 2;
        const    iniciada_entrega = 3;
        const    entregue = 4;
        //const	cancelado_pelo_cliente = 5;
        const    cancelado_pelo_cervejeiro = 6;
        const    cancelado_pela_incubadora = 7;
        const    aguardando_primeiro_pedido = 8;
        const    aguardando_aprovacao = 9;

        public static function getLabel($idTipoCervejeiro, $idEstadoBarril)
        {
            switch ($idTipoCervejeiro)
            {
                case BO_Tipo_cervejeiro::brew_pub:
                    switch ($idEstadoBarril)
                    {
                        case BO_Estado_barril::aguardando_primeiro_pedido:
                            return I18N::getExpression("Aguardando primeiro pedido.");
                        case BO_Estado_barril::crowdfunding:
                            return I18N::getExpression("Crowdfunding...");
                        case BO_Estado_barril::executando:
                            return I18N::getExpression("Preparando a festa.");
                        case BO_Estado_barril::iniciada_entrega:
                            return I18N::getExpression("Festa rolando!");
                        case BO_Estado_barril::entregue:
                            return I18N::getExpression("J� passou a festa.");
                        case BO_Estado_barril::cancelado_pelo_cervejeiro:
                            return I18N::getExpression("O Brew Pub cancelou o barril");
                        case BO_Estado_barril::cancelado_pela_incubadora:
                            return I18N::getExpression("O barril foi cancelado pela IncubadoraDaCerveja.com.br.");
                        case BO_Estado_barril::aguardando_aprovacao:
                            return I18N::getExpression("Aguardando aprova��o do barril pela IncubadoraDaCerveja.com.br.");
                    }
                    break;
                case BO_Tipo_cervejeiro::cigano:
                    switch ($idEstadoBarril)
                    {
                        case BO_Estado_barril::aguardando_primeiro_pedido:
                            return I18N::getExpression("Aguardando primeiro pedido.");
                        case BO_Estado_barril::crowdfunding:
                            return I18N::getExpression("Crowdfunding...");
                        case BO_Estado_barril::executando:
                            return I18N::getExpression("Produzindo a cerveja na cervejaria");
                        case BO_Estado_barril::aguardando_aprovacao:
                            return I18N::getExpression("Aguardando aprova��o do barril pela IncubadoraDaCerveja.com.br");
                        case BO_Estado_barril::cancelado_pela_incubadora:
                            return I18N::getExpression("O barril foi cancelado pela IncubadoraDaCerveja.com.br");

                        default:
                            throw new NotImplementedException("cigano");
                    }
                    break;
                case BO_Tipo_cervejeiro::degustadores:
                    switch ($idEstadoBarril)
                    {
                        case BO_Estado_barril::aguardando_primeiro_pedido:
                            return I18N::getExpression("Aguardando primeira confirma��o");
                        case BO_Estado_barril::crowdfunding:
                            return I18N::getExpression("Arrecandando a vaquinha...");
                        case BO_Estado_barril::executando:
                            return I18N::getExpression("Preparando a festa.");
                        case BO_Estado_barril::iniciada_entrega:
                            return I18N::getExpression("Festa rolando!");
                        case BO_Estado_barril::entregue:
                            return I18N::getExpression("J� passou a festa.");
                        case BO_Estado_barril::cancelado_pelo_cervejeiro:
                            return I18N::getExpression("O Degustador organizador cancelou o barril");
                        case BO_Estado_barril::cancelado_pela_incubadora:
                            return I18N::getExpression("O barril foi cancelado pela IncubadoraDaCerveja.com.br.");
                        case BO_Estado_barril::aguardando_aprovacao:
                            return I18N::getExpression("Aguardando aprova��o do barril pela IncubadoraDaCerveja.com.br.");

                        default:
                            throw new NotImplementedException("cigano");
                    }
                    break;
                case BO_Tipo_cervejeiro::cervejaria:
                    switch ($idEstadoBarril)
                    {
                        case BO_Estado_barril::aguardando_primeiro_pedido:
                            return I18N::getExpression("Aguardando primeiro pedido.");
                        case BO_Estado_barril::crowdfunding:
                            return I18N::getExpression("Crowdfunding...");
                        case BO_Estado_barril::executando:
                            return I18N::getExpression("Produzindo a cerveja na cervejaria");
                        case BO_Estado_barril::aguardando_aprovacao:
                            return I18N::getExpression("Aguardando aprova��o do barril pela IncubadoraDaCerveja.com.br");
                        case BO_Estado_barril::cancelado_pela_incubadora:
                            return I18N::getExpression("O barril foi cancelado pela IncubadoraDaCerveja.com.br");

                        default:
                            throw new NotImplementedException("cigano");
                    }
                    break;
                default:
                    break;
            }

            return null;
        }
    }