<?php

    /**
     * Created by PhpStorm.
     * User: W10
     * Date: 14/03/2018
     * Time: 19:49
     */
    class BO_Pedido_debito
    {
        public function __construct()
        {
        }

        public static function factory()
        {
            return new BO_Pedido_debito();
        }

        public function requisitaAprovacaoPedido()
        {
            $db = new Database();

            $args = json_decode(file_get_contents("php://input"));

            $boCervejeiro = new BO_Cervejeiro();
            $idCervejeiro = $boCervejeiro->getIdDoCervejeiroLogado($args->sessaoFacebook);
            if (!strlen($idCervejeiro))
            {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ACESSO_NEGADO
                    , I18N::getExpression("Realize o login no facebook para prosseguir."));
            }
            else
            {
                if (!strlen($args->qtdMlEscolhida))
                {
                    return new Mensagem(
                        PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO
                        , I18N::getExpression("Digite a quantidade de ml que o convidado vai beber"));
                }
            }
            $boConvite = new BO_Convite();
            $dados = $boConvite->getDadosConvidado(
                $db
                , $args->idBarril
                , $args->chaveConvidado);

            if ($dados == null)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO
                    , I18N::getExpression("O seu QR Code n�o � v�lido"));
            }

            $boPedido = new BO_Pedido();

            $totalLitrosDoConvidadoCervejeiro = $boPedido->getTotalLitrosDoConvidadoCervejeiro(
                $args->idBarril
                , $dados->idConvidadoCervejeiro
                , array(BO_Estado_pedido::pagamento_confirmado)
                , $db
            );
            $totalLitrosDebitados = $this->getTotalLitrosDebitados($dados->idConvite, $db);
            $atual = $totalLitrosDoConvidadoCervejeiro - $totalLitrosDebitados;
            $qtdLitroEscolhida = $args->qtdMlEscolhida / 1000;
            $final = $atual - floatval($qtdLitroEscolhida);
            if ($final <= 0)
            {

                return new Mensagem_generica(
                    $atual
                    , PROTOCOLO_SISTEMA::ACESSO_NEGADO
                    , I18N::getExpression("Saldo insuficiente"));
            }
            else
            {
                $strFinal = Helper::formatarFloatParaExibicao($final);
                $q = "INSERT INTO `pedido_debito` (
`data_DATETIME`, `qtd_litros_INT`, `saldo_litros_final_INT`, `convite_id_INT`, `data_cancelamento_DATETIME`, `id_chamada_INT`
) 
 VALUES (
 '" . Helper::getDiaEHoraAtualSQL() . "'
 , '{$args->qtdMlEscolhida}'
 , '$strFinal'
 , '{$dados->idConvite}'
 , '" . Helper::getDiaEHoraAtualSQL() . "'
 , '{$args->identificadorChamada}')
";
                $db->query($q);
                $ret = new stdClass();

                $ret->saldoAtual = $final;
                if ($db->getAffectedRows() > 0)
                {
                    $idPedido = $db->selectLastId();
                    $ret->idPedido = $idPedido;

                    return new Mensagem_generica(
                        $ret
                        , null
                        , I18N::getExpression("Pedido aceito!")
                    );
                }
                else
                {
                    return new Mensagem_generica(
                        $ret,
                        PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                        I18N::getExpression("Tente novamente"));
                }
            }

//        $dadosBarril->volumeTotal = $objJsonBarril->volumeTotalProducao;
//
//        //TODO alterar o precoLitro para o valor calculado
//        $dadosBarril->precoLitro = 3.6;
//        $dadosBarril->qtdLitroNovoPedido = 0;
//
//
//        return new Mensagem_generica($qrCode);

        }

        public function getTotalLitrosDebitados($idConvite, $db = null)
        {

            $q = "SELECT SUM(qtd_litros_INT)
        FROM pedido_debito
        WHERE convite_id_INT = $idConvite 
          AND data_cancelamento_DATETIME IS NULL";
            if ($db == null)
            {
                $db = new Database();
            }
            $db->query($q);

            return $db->getPrimeiraTuplaDoResultSet(0);
        }

        public function cancelarPedido()
        {
            $db = new Database();

            $args = json_decode(file_get_contents("php://input"));

            $boCervejeiro = new BO_Cervejeiro();
            $idCervejeiro = $boCervejeiro->getIdDoCervejeiroLogado($args->sessaoFacebook);
            if (!strlen($idCervejeiro))
            {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ACESSO_NEGADO
                    , I18N::getExpression("Realize o login no facebook para prosseguir."));
            }

            $boConvite = new BO_Convite();
            $dados = $boConvite->getDadosConvidado(
                $db
                , $args->idBarril
                , $args->chaveConvidado);

            if ($dados == null)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO
                    , I18N::getExpression("O seu QR Code n�o � v�lido"));
            }

            $q = "UPDATE pedido_debito
        SET data_cancelamento_DATETIME = '" . Helper::getDiaEHoraAtualSQL() . "',
            cancelador_cervejeiro_id_INT = {$idCervejeiro}
        WHERE id= " . $args->idPedido . "
          AND data_cancelamento_DATETIME IS NULL";

            $db->query($q);
            $boPedido = new BO_Pedido();
            $totalLitrosDoConvidadoCervejeiro = $boPedido->getTotalLitrosDoConvidadoCervejeiro(
                $args->idBarril
                , $dados->idConvidadoCervejeiro
                , array(BO_Estado_pedido::pagamento_confirmado)
                , $db
            );
            $totalLitrosDebitados = $this->getTotalLitrosDebitados($dados->idConvite, $db);
            $atual = $totalLitrosDoConvidadoCervejeiro - $totalLitrosDebitados;
            $ret = new stdClass();
            $ret->saldoAtual = $atual;

            return new Mensagem(
                $ret,
                null,
                I18N::getExpression("O pedido {0} foi cancelado com sucesso", $args->idPedido));
        }

        public function getPedidosDebito(
            $idBarril
            , Database $db = null)
        {

            $q = "SELECT pd.id idPedidoDebito, pd.data_DATETIME data, pd.qtd_litros_INT qtdLitros, data_cancelamento_DATETIME dataCancelamento, ce.nome nomeCervejeiro 
FROM pedido_debito pd
	JOIN convite c ON pd.convite_id_INT = c.id
	JOIN cervejeiro ce ON ce.id = c.convidado_cervejeiro_id_INT
WHERE c.barril_id_INT= $idBarril ";

            $db->query($q);

            $totalLitrosJaBebidos = 0;
            $matriz = Helper::getResultSetToMatrizObj($db->result);
            if (count($matriz) > 0)
            {
                for ($i = 0; $i < count($matriz); $i++)
                {
                    $matriz[ $i ]->data = Helper::formatarDataTimeParaExibicao($matriz[ $i ]->data);
                    if ($matriz[ $i ]->dataCancelamento != null)
                    {
                        $matriz[ $i ]->dataCancelamento = Helper::formatarDataTimeParaExibicao($matriz[ $i ]->dataCancelamento);
                        $matriz[ $i ]->status = I18N::getExpression("cancelado");
                    }
                    else
                    {
                        $matriz[ $i ]->status = I18N::getExpression("ok");
                        $totalLitrosJaBebidos += $matriz[ $i ]->qtdLitros;
                    }
                }
            }
            $ret = new stdClass();
            $ret->matriz = $matriz;
            $ret->totalLitrosJaBebidos = $totalLitrosJaBebidos;

            return $ret;
        }

    }