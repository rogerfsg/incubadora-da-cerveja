<?php

    class BO_Barris
    {
        public static function factory()
        {
            return new BO_Barris();
        }

        public function getDadosTelaPrincipal($objFacebook = null)
        {
            try
            {
                $db = new Database();
                $hf = new HelperFacebook($objFacebook);
                $me = $hf->loadMe();
                $boCervejeiro = new BO_Cervejeiro();
                $dadosCervejeiro = $boCervejeiro->getIdETipoDoCervejeiroLogado($db, $objFacebook);
                $idCervejeiroLogado = $dadosCervejeiro->id;
                $idTipoCervejeiro = $dadosCervejeiro->idTipoCervejeiro;
                $strInFriends = $hf->getStrIdsFriends();
                $estadosBarril = array(
                    BO_Estado_barril::aguardando_primeiro_pedido
                , BO_Estado_barril::crowdfunding
                , BO_Estado_barril::executando
                , BO_Estado_barril::cancelado_pelo_cervejeiro
                , BO_Estado_barril::cancelado_pela_incubadora);

                $meus = $this->getMeusBarris($me, $db, $idTipoCervejeiro);

                $degustadores = $this->getBarrisPrivados(
                    BO_Tipo_cervejeiro::degustadores
                    , $db
                    , $strInFriends
                    , $estadosBarril
                    , $idCervejeiroLogado);

                $ciganos = $this->getBarrisPublicos(
                    $db,
                    BO_Tipo_cervejeiro::cigano
                    , $estadosBarril);

                $cervejarias = $this->getBarrisPublicos(
                    $db,
                    BO_Tipo_cervejeiro::cervejaria
                    , $estadosBarril);

                $obj = new stdClass();
                $obj->idCervejeiroLogado = 0;
                $obj->barrisPrivadosDegustadores = $degustadores;
                $obj->barrisPrivadosCiganos = $ciganos;
                $obj->barrisPublicosCervejarias = $cervejarias;
                $obj->barrisMeus = $meus;
                $obj->amigosFacebook = $hf->getFriends();
                $obj->idCervejeiroLogado = $idCervejeiroLogado;

                return new Mensagem_generica($obj);
            }
            catch (Exception $ex)
            {
                HelperLog::logException($ex, true);

                return new Mensagem(null, null, $ex);
            }
        }

        public function getBarrisPrivados(
            $idTipoCervejeiro = null,
            Database $db = null,
            $strInFriends = null,
            $idsEstadosBarris = null,
            $idCervejeiroLogado = null)
        {

            if ($db == null)
            {
                $db = new Database();
            }

            //Para os barris privados, obrigatoriamente voc� s� v� amigos
            if (!strlen($strInFriends))
            {
                return null;
            }

            $q = "SELECT MAX(b.id)
                    , b.cervejeiro_id_INT idCervejeiro
                    , b.json jsonBarril
                    , tc.id idTipoCerveja
                    , tc.nome tipoCerveja
                    , c.nome cervejeiro
                    , c.json jsonCervejeiro
                    , c.id  idCervejeiro
                FROM cervejeiro c
                    join barril b ON c.id = b.cervejeiro_id_INT
                    join convite co ON co.barril_id_INT = b.id
                    left join tipo_cerveja tc ON b.tipo_cerveja_id_INT = tc.id
                    ";
            $where = "";
            if ($idTipoCervejeiro != null)
            {
                $where .= " c.tipo_cervejeiro_id_INT = $idTipoCervejeiro ";
            }
            if (strlen($strInFriends))
            {
                if (strlen($where))
                {
                    $where .= " AND ";
                }
                $where .= " c.facebook_id IN ($strInFriends) ";
            }
            if (strlen($idCervejeiroLogado))
            {
                if (strlen($where))
                {
                    $where .= " AND ";
                }
                $where .= " co.convidado_cervejeiro_id_INT = $idCervejeiroLogado ";
            }
            if (count($idsEstadosBarris))
            {
                $strIdsEstadosBarris = Helper::arrayToString($idsEstadosBarris);
                if (strlen($where))
                {
                    $where .= " AND ";
                }
                $where .= " b.estado_barril_id_INT IN ($strIdsEstadosBarris) ";
            }
            if (strlen($where))
            {
                $q .= " WHERE  $where ";
            }

            $q .= " GROUP BY b.cervejeiro_id_INT 
                , b.json 
                , tc.id 
                , tc.nome 
                , c.nome 
                , c.json ";
            //recupera a lista de cervejeiros
            $db->query($q);

            $res = Helper::getResultSetToMatriz($db->result);

            // Foto instagram da cerveja
            // BARRIL (Colorido com a porcentagem %80/100%)
            // %80/100% - label pequena embaixo do barril
            // Nome do cervejeiro
            //
            //  IBDU X
            //  ALCOOL Y
            // De 5,00 a 6,00 o litro
            //
            //O vetor abaixo sera composto dos objetos acima
            $a = $this->factoryModels($idTipoCervejeiro, $res, $db);
            for ($i = 0; $i < count($a); $i++)
            {
                if (strlen($a[ $i ]->cervejeiro))
                {
                    $objFirstLast = Helper::splitName($a[ $i ]->cervejeiro);
                    if ($objFirstLast != null && isset($objFirstLast ['first']))
                    {
                        $a[ $i ]->cervejeiro = $objFirstLast['first'];
                    }
                }
            }

            return $a;
        }

        public function getBarrisPublicos(
            Database $db, $idTipoCervejeiro = null, $idsEstadosBarris = null)
        {

            $q = "SELECT 
                        MAX(b.id) idBarril
                    , b.cervejeiro_id_INT idCervejeiro
                    , b.json jsonBarril
                    , tc.id idTipoCerveja
                    , tc.nome tipoCerveja
                    , c.nome cervejeiro
                    , c.json jsonCervejeiro
                    , c.id  idCervejeiro
                FROM cervejeiro c 
                    join barril b ON c.id = b.cervejeiro_id_INT  
                    left join tipo_cerveja tc ON b.tipo_cerveja_id_INT = tc.id
                ";
            $where = "";
            if ($idTipoCervejeiro != null)
            {
                $where .= " c.tipo_cervejeiro_id_INT = $idTipoCervejeiro ";
            }

            if (count($idsEstadosBarris))
            {
                $strIdsEstadosBarris = Helper::arrayToString($idsEstadosBarris);

                if (strlen($where))
                {
                    $where .= " AND ";
                }
                $where .= " b.estado_barril_id_INT IN ($strIdsEstadosBarris) ";
            }
            if (strlen($where))
            {
                $q .= " WHERE $where";
            }

            $q .= " GROUP BY b.cervejeiro_id_INT 
                    , b.json 
                    , tc.id 
                    , tc.nome 
                    , c.nome 
                    , c.json ";
            //recupera a lista de cervejeiros
            $db->query($q);

            $res = Helper::getResultSetToMatriz($db->result);

            // Foto instagram da cerveja
            // BARRIL (Colorido com a porcentagem %80/100%)
            // %80/100% - label pequena embaixo do barril
            // Nome do cervejeiro
            //
            //  IBDU X
            //  ALCOOL Y
            // De 5,00 a 6,00 o litro
            //
            //O vetor abaixo sera composto dos objetos acima
            $a = $this->factoryModels($idTipoCervejeiro, $res, $db);

            return $a;
        }

        public function factoryModels($idTipoCervejeiro, $inarray, $db)
        {
            $a = array();
            $boBarril = new BO_Barril();
            for ($i = 0; $i < count($inarray); $i++)
            {
                $res = $inarray[ $i ];
                $idBarril = $res[0];
                $jsonBarril = $res[2];
                $idTipoCerveja = $res[3];
                $tipoCerveja = $res[4];
                $cervejeiro = $res[5];
                $idCervejeiro = $res[7];
                $objBarril = json_decode($jsonBarril);

                $min = null;
                $max = null;
                $resumo = new stdClass();

                if ($idTipoCervejeiro == BO_Tipo_cervejeiro::degustadores)
                {
                    $resumo->precoMedio = utf8_encode(I18N::getExpression("R$ {0}", Helper::formatarFloatParaExibicao($objBarril->valorPorLitro)));
                    $resumo->totalMl = $objBarril->volumeTotalEmLitros;
                    $resumo->preco = '';
                }
                else
                {
                    for ($j = 0; $j < count($objBarril->embalagensDisponiveis); $j++)
                    {
                        $objPreco = $objBarril->embalagensDisponiveis[ $j ];
                        $precoPorLitro = ($objPreco->preco * $objPreco->capacidade) / 1000;

                        if ($min == null || $min > $precoPorLitro)
                        {
                            $min = $precoPorLitro;
                        }
                        if ($max == null || $max < $precoPorLitro)
                        {
                            $max = $precoPorLitro;
                        }
                    }

                    $med = null;
                    if ($max != null)
                    {
                        $med = ($max + $min) / 2;
                    }

                    $resumo->precoMedio = utf8_encode(I18N::getExpression("R$ {0}", Helper::formatarFloatParaExibicao($med)));
                    $resumo->totalMl = 0;
                    if (isset($objBarril->volumeTotalProducao))
                    {
                        $resumo->totalMl = $objBarril->volumeTotalProducao;
                    }
                    else
                    {
                        $resumo->totalMl = $objBarril->volumeTotalEmLitros;
                    }

                    $resumo->preco = utf8_encode(I18N::getExpression("De R$ {0} � {1} o litro", $min, $max));
                }

                $resumo->idBarril = $idBarril;
                $resumo->IBU = $objBarril->IBU;
                $resumo->percentualAlcool = $objBarril->alcool;
                $resumo->cervejeiro = $cervejeiro;

                $resumo->totalMlVendido = $boBarril->getTotalLitrosOcupados(
                    $idBarril,
                    array(BO_Estado_pedido::disse_que_pagou, BO_Estado_pedido::pagamento_confirmado),
                    $db);

                if (!strlen($resumo->totalMlVendido))
                {
                    $resumo->totalMlVendido = 0;
                }

                $resumo->fotoInstagram = $objBarril->fotoInstagram;
                //nome da cerveja
                //$resumo->cerveja=$objBarril["cerveja"];
                $resumo->tipoCerveja = $tipoCerveja;
                $resumo->idTipoCerveja = $idTipoCerveja;
                $resumo->idCervejeiro = $idCervejeiro;
                $a[ count($a) ] = $resumo;
            }

            return $a;
        }

        public function getDadosBarril($idBarril, $sessaoFacebook = null)
        {
            try
            {
                if (!strlen($idBarril))
                {

                    return new Mensagem_token(
                        PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO,
                        HelperMensagem::GERAL_FALHA_CADASTRO(
                            HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                                "getDadosBarril",
                                array($idBarril)
                            ),
                            array()
                        )
                    );
                }
                $idCervejeiroLogado = null;
                if ($sessaoFacebook != null)
                {

                    $boCervejeiro = new BO_Cervejeiro();
                    $idCervejeiroLogado = $boCervejeiro->getIdDoCervejeiroLogado($sessaoFacebook);
                }

                $db = new Database();
                //recupera os dados do barril em questao
                $q = "SELECT b.cervejeiro_id_INT idCervejeiro
                        , c.tipo_cervejeiro_id_INT idTipoCervejeiro
                        , b.json jsonBarril
                        , tc.id idTipoCerveja
                        , tc.nome tipoCerveja
                        , c.nome nomeCervejeiro
                        , c.lat_INT
                        , c.lng_INT 
                        , b.estado_barril_id_INT idEstadoBarril
                        , c.facebook_id facebookId
                        ";
                if ($idCervejeiroLogado != null)
                {
                    $q .= ", p.json jsonPedido";
                    $q .= ", co.estado_convite_id_INT idEstadoConvite";
                    $q .= ", p.id idPedido";
                    $q .= ", p.qtd_litros_INT qtdLitros";
                    $q .= ", p.data dataPedido";
                    $q .= ", p.estado_pedido_id_INT idEstadoPedido";
                    $q .= ", co.chave chave";
                    $q .= ", co.id idConvite";
                    $q .= ", p.valor valor";
                }

                $q .= " ";
                $q .= " FROM barril b 
                          left join tipo_cerveja tc ON b.tipo_cerveja_id_INT = tc.id
                          join cervejeiro c ON c.id = b.cervejeiro_id_INT ";
                if ($idCervejeiroLogado != null)
                {

                    $q .= " left join pedido p ON b.id = p.barril_id_INT ";
                    $q .= " AND p.convidado_cervejeiro_id_INT = $idCervejeiroLogado ";

                    $q .= " left join convite co ON b.id = co.barril_id_INT ";
                    $q .= " AND co.convidado_cervejeiro_id_INT = $idCervejeiroLogado ";
                }
                $q .= " WHERE b.id = $idBarril ";
                $db->query($q);
                $obj = Helper::getResultSetToArrayDeObjetos($db->result);

                if (count($obj))
                {
                    $dadosBarril = $obj[0];
                    if (!strlen($dadosBarril->chave)
                        && strlen($dadosBarril->idConvite)
                    )
                    {
                        $boConvite = new BO_Convite();
                        $msg = $boConvite->geraChaveQrCodeConvite($db, $idBarril, $idCervejeiroLogado);
                        if (Interface_mensagem::checkErro($msg))
                        {
                            return $msg;
                        }
                        $dadosBarril->chave = $msg->mObj;
                    }

                    $dadosBarril->estadoBarril = BO_Estado_barril::getLabel(
                        $dadosBarril->idTipoCervejeiro
                        , $dadosBarril->idEstadoBarril);
                    $pedidos = array();
                    $totalLitrosNaoConfirmados = 0;
                    $totalLitrosConfirmados = 0;
                    $totalLitrosBloqueados = 0;
                    if ($idCervejeiroLogado != null)
                    {
                        $validade = false;
                        for ($i = 0; $i < count($obj); $i++)
                        {
                            $o = $obj[ $i ];
                            if (strlen($o->idPedido))
                            {
                                $validade = true;
                                $pedido = new stdClass();
                                $pedido->id = $o->idPedido;
                                $pedido->qtdLitros = $o->qtdLitros;
                                $pedido->data = Helper::formatarDataTimeParaExibicao($o->dataPedido);
                                $pedido->estadoPedido = utf8_encode(BO_Estado_pedido::getLabel($o->idEstadoPedido));
                                $pedido->valor = Helper::formatarFloatParaExibicao($o->valor);
                                $pedido->idEstadoPedido = $o->idEstadoPedido;
                                switch ($pedido->idEstadoPedido)
                                {
                                    case BO_Estado_pedido::pagamento_confirmado:
                                        $totalLitrosConfirmados += $pedido->qtdLitros;
                                        break;
                                    case BO_Estado_pedido::nao_pagou:
                                        $totalLitrosBloqueados += $pedido->qtdLitros;
                                        break;
                                    case BO_Estado_pedido::disse_que_pagou:
                                        $totalLitrosNaoConfirmados += $pedido->qtdLitros;
                                        break;
                                }
                                $pedidos[ count($pedidos) ] = $pedido;
                            };
                        }
                        if (!$validade)
                        {
                            $pedidos = null;
                        }
                    }

                    if (isset($dadosBarril->jsonPedido))
                    {
                        unset($dadosBarril->jsonPedido);
                    }
                    $objJsonBarril = json_decode($dadosBarril->jsonBarril);
                    $dadosBarril->volumeTotal = 0;
                    $dadosBarril->precoLitro = 0;
                    if ($dadosBarril->idTipoCervejeiro == BO_Tipo_cervejeiro::degustadores)
                    {
                        $dadosBarril->volumeTotal = $objJsonBarril->volumeTotalEmLitros;
                        $dadosBarril->precoLitro = $objJsonBarril->valorPorLitro;
                    }
                    else
                    {
                        $dadosBarril->volumeTotal = $objJsonBarril->volumeTotalProducao;
                        $dadosBarril->precoLitro = $objJsonBarril->valorPorLitro;
                    }

                    $dadosBarril->qtdLitroNovoPedido = 0;

                    $dadosBarril->totalLitrosConfirmados = $totalLitrosConfirmados;
                    $dadosBarril->totalLitrosNaoConfirmados = $totalLitrosNaoConfirmados;
                    $dadosBarril->totalLitrosBloqueados = $totalLitrosBloqueados;

                    $boBarril = new BO_Barril();
                    $dadosBarril->totalLitrosComprometidos = $boBarril->getTotalLitrosOcupados(
                        $idBarril
                        , array(BO_Estado_pedido::disse_que_pagou, BO_Estado_pedido::pagamento_confirmado));

                    if (isset($idCervejeiroLogado)
                        && !strlen($dadosBarril->idConvite)
                    )
                    {
                        //como essa rotina � executada durante o carregamento da pagina de detalhes do barril
                        //nesse momento ele ainda n�o confirmou que ir� a festa
                        //logo iremos inserir um registro mostrando que ele acessou e visualizou seu convite
                        $dadosBarril->idEstadoConvite = BO_Estado_convite::visualizou_mas_nao_confirmou;

                        $historico = "Acessou " . Helper::getDiaEHoraAtual();
                        $historico = $db->formatarDados($historico);
                        //se nao tiver ainda o convite do usu�rio em quest�o
                        $msg = $db->queryMensagem(
                            "INSERT INTO `convite` (
                                      `convidado_cervejeiro_id_INT`
                                      , `barril_id_INT`
                                      , `estado_convite_id_INT`
                                      , `historico`
                                      , `data`) 
                          VALUES (
                                      $idCervejeiroLogado
                                      , $idBarril
                                      , " . $dadosBarril->idEstadoConvite . " 
                                      , $historico
                                      , '" . Helper::getDiaEHoraAtualSQL() . "')");
                        if (Interface_mensagem::checkErro($msg))
                        {
                            $db->query("SELECT id FROM convite WHERE barril_id_INT = {$idBarril} AND convidado_cervejeiro_id_INT = $idCervejeiroLogado");
                            $idConvite = $db->getPrimeiraTuplaDoResultSet(0);
                            if (!strlen($idConvite))
                            {
                                return $msg;
                            }
                        }
                    }
                    //todos os pedidos do convidado para a festa
                    $dadosBarril->pedidos = $pedidos;

                    if ($dadosBarril->tipoCerveja == BO_Tipo_cervejeiro::degustadores)
                    {
                        //verifica se o barril � do pr�prio usuario
                        if ($idCervejeiroLogado != null)
                        {
                            if ($dadosBarril->idCervejeiro == $idCervejeiroLogado)
                            {
                                $dadosBarril->meu = true;
                            }
                        }
                        else
                        {
                            return new Mensagem(
                                PROTOCOLO_SISTEMA::ACESSO_NEGADO
                                , I18N::getExpression("Fa�a o login no Facebook para acessar o barril")
                            );
                        }
                    }
                    else
                    {
                        // o usuario tem permissao de ver todos os outros tipos de cervejeiros
                        //se nao estiver logado

                        $dadosBarril->meu = false;
                    }

                    return new Mensagem_generica($dadosBarril);
                }

                return null;
            }
            catch (Exception $ex)
            {
                HelperLog::logException($ex, true);

                return new Mensagem(null, null, $ex);
            }
        }

        public function getMeusBarris($me, Database $db, $idTipoCervejeiro)
        {

            if ($me == null)
            {
                return null;
            }

            if ($db == null)
            {
                $db = new Database();
            }

            $q = "SELECT 
                        MAX(b.id)
                    , b.cervejeiro_id_INT idCervejeiro
                    , b.json jsonBarril
                    , tc.id idTipoCerveja
                    , tc.nome tipoCerveja
                    , c.nome cervejeiro
                    , c.json jsonCervejeiro
                    , c.id  idCervejeiro
                FROM cervejeiro c 
                    join barril b ON c.id = b.cervejeiro_id_INT  
                    left join tipo_cerveja tc ON b.tipo_cerveja_id_INT = tc.id
                    ";
            $q .= " WHERE c.facebook_id = '{$me->getId()}' ";

            $q .= " GROUP BY b.cervejeiro_id_INT 
                    , b.json 
                    , tc.id 
                    , tc.nome 
                    , c.nome 
                    , c.json ";
            //recupera a lista de cervejeiros
            $db->query($q);

            $res = Helper::getResultSetToMatriz($db->result);

            // Foto instagram da cerveja
            // BARRIL (Colorido com a porcentagem %80/100%)
            // %80/100% - label pequena embaixo do barril
            // Nome do cervejeiro
            //
            //  IBDU 1
            //  ALCOOL Y
            // De 5,00 a 6,00 o litro
            //
            //O vetor abaixo sera composto dos objetos acima
            $a = $this->factoryModels($idTipoCervejeiro, $res, $db);

            return $a;
        }

    }
