<?php

    /**
     * Created by PhpStorm.
     * User: W10
     * Date: 16/03/2018
     * Time: 20:24
     */
    class BO_Cervejeiro_degustador
    {

        public static function factory()
        {
            return new BO_Cervejeiro_degustador();
        }

        public function salvar()
        {
            $args = json_decode(file_get_contents("php://input"));

            //Persistencia no banco
            if (!is_null($args))
            {
                $db = new Database();
                $boCervejeiro = new BO_Cervejeiro();

                //$idCervejeiro = $this->getIdCervejeiroComCpfCnpj($db, $cpfCnpj);
                //Como podemos ter ate 3 chamados do mobile se tiver uma recente, enviamos a mensagem que ele j� foi cadastrado na primeirao
                //TODO ajustar para verificar a data de cadastro, se esta a menos de um min, para trocar o retorno para
                // CADASTRO REALIZADO COM SUCESSO
                $dadosCervejeiroFacebook = null;
                if (isset($args->sessaoFacebook))
                {
                    $dadosCervejeiroFacebook = $boCervejeiro->getIdETipoDoCervejeiroLogado($db, $args->sessaoFacebook);
                }
                if (isset($dadosCervejeiroFacebook))
                {
                    if ($dadosCervejeiroFacebook->idTipoCervejeiro != BO_Tipo_cervejeiro::degustadores)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                                            I18N::getExpression("Essa tela � apenas para os Degustadores!"));
                    }
                }
                if (!isset($dadosCervejeiroFacebook))
                {
                    //se chegar ate aqui � porque o cervejeiro ainda n�o foi cadastrado
                    $idCervejeiro = $this->insertCervejeiroComFacebookV2($db, $args);
                }
                else
                {
                    //se chegar ate aqui � porque o cervejeiro ainda n�o foi cadastrado
                    $idCervejeiro = $this->updateCervejeiroComFacebookV2($db, $args);
                }

                return new Mensagem_generica(
                    $idCervejeiro,
                    PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                    I18N::getExpression("Os dados foram salvos com sucesso!"));
            }
            else
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                                    I18N::getExpression("Dados inv�lidos."));
            }
        }

        public function insertCervejeiroComFacebookV2($db, $args)
        {
            //clona objeto inteiro
            $objectCervejeiro = clone $args;
            unset($objectCervejeiro->sessaoFacebook);

            //cria json do cervejeiro (sem o vetor dos barris)
            $jsonCervejeiro = Helper::jsonEncode($objectCervejeiro);

            $hf = new HelperFacebook($args->sessaoFacebook);
            $me = $hf->loadMe();

            if ($me == null)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                                    I18N::getExpression("Voc� deve realizar o login no facebook"));
            }
            $nome = utf8_encode($me->getName());
            $query = "INSERT INTO `cervejeiro` (`nome`, `facebook_id`, `tipo_cervejeiro_id_INT`, `json`)
                         VALUES ('{$nome}'
                         , '{$me->getId()}'
                         , " . BO_Tipo_cervejeiro::degustadores . "
                         , '{$jsonCervejeiro}')";

            $db->query($query);

            $idCervejeiro = $db->getLastInsertId();

            return $idCervejeiro;
        }

        public function updateCervejeiroComFacebookV2($db, $args)
        {
            //clona objeto inteiro
            $objectCervejeiro = clone $args;
            unset($objectCervejeiro->sessaoFacebook);

            //cria json do cervejeiro (sem o vetor dos barris)
            $jsonCervejeiro = Helper::jsonEncode($objectCervejeiro);

            $hf = new HelperFacebook($args->sessaoFacebook);
            $me = $hf->loadMe();

            if ($me == null)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                                    I18N::getExpression("Voc� deve realizar o login no facebook"));
            }
            $nome = utf8_encode($me->getName());
            $query = "UPDATE `cervejeiro` 
        SET nome= '$nome',
          json = '{$jsonCervejeiro}'
         WHERE facebook_id = '{$me->getId()}'";

            $db->query($query);

            $idCervejeiro = $db->getLastInsertId();

            return $idCervejeiro;
        }

        public function gerarQrCode()
        {
            $args = json_decode(file_get_contents("php://input"));

            //Persistencia no banco
            if (!is_null($args))
            {
                $db = new Database();
                $boCervejeiro = new BO_Cervejeiro();

                $idCervejeiroFacebook = $boCervejeiro->getIdDoCervejeiroLogado($args->sessaoFacebook);
                //$idCervejeiro = $this->getIdCervejeiroComCpfCnpj($db, $cpfCnpj);
                //Como podemos ter ate 3 chamados do mobile se tiver uma recente, enviamos a mensagem que ele j� foi cadastrado na primeirao
                //TODO ajustar para verificar a data de cadastro, se esta a menos de um min, para trocar o retorno para
                // CADASTRO REALIZADO COM SUCESSO
                if ($idCervejeiroFacebook != null)
                {
                    return new Mensagem_token(
                        PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                        I18N::getExpression("O seu facebook j� est� vinculado a outra conta"));
                }

                //se chegar ate aqui � porque o cervejeiro ainda n�o foi cadastrado
                $idCervejeiro = $this->insertCervejeiroComFacebookV2($db, $args);

                return new Mensagem_generica($idCervejeiro);
            }
            else
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                                    I18N::getExpression("Dados inv�lidos."));
            }
        }

        public function getDadosFormularioCervejeiroDegustador()
        {

            $args = json_decode(file_get_contents("php://input"));

            $db = new Database();
            $boCervejeiro = new BO_Cervejeiro();

            $dadosCervejeiroFacebook = null;
            if (isset($args->sessaoFacebook))
            {
                $dadosCervejeiroFacebook = $boCervejeiro->getIdETipoDoCervejeiroLogado($db, $args->sessaoFacebook);
            }
            if (isset($dadosCervejeiroFacebook))
            {
                if ($dadosCervejeiroFacebook->idTipoCervejeiro != BO_Tipo_cervejeiro::degustadores)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                                        I18N::getExpression("Essa tela � apenas para os Degustadores!"));
                }
            }
            //$idCervejeiro = $this->getIdCervejeiroComCpfCnpj($db, $cpfCnpj);
            //Como podemos ter ate 3 chamados do mobile se tiver uma recente, enviamos a mensagem que ele j� foi cadastrado na primeirao
            //TODO ajustar para verificar a data de cadastro, se esta a menos de um min, para trocar o retorno para
            // CADASTRO REALIZADO COM SUCESSO
            $boTipoCerveja = new BO_Tipo_cerveja();
            $tipos = $boTipoCerveja->getListaTiposCerveja($db);
            if ($dadosCervejeiroFacebook == null)
            {
                return new Mensagem_generica(
                    $tipos,
                    PROTOCOLO_SISTEMA::RESULTADO_VAZIO);
            }
            else
            {
                $ret = new stdClass();
                $objJson = null;
                if (strlen($dadosCervejeiroFacebook->jsonCervejeiro))
                {
                    $objJson = json_decode($dadosCervejeiroFacebook->jsonCervejeiro);
                    unset($dadosCervejeiroFacebook->jsonCervejeiro);
                }
                $ret->objJsonCervejeiro = $objJson;
                $ret->dadosCervejeiro = $dadosCervejeiroFacebook;
                $ret->tiposCerveja = $tipos;

                return new Mensagem_generica($ret);
            }
        }

    }