<?php

    /**
     * Created by PhpStorm.
     * User: W10
     * Date: 09/03/2018
     * Time: 20:10
     */
    class BO_Estado_convite
    {
        const    nao_vai = 1;
        const    vai_sem_beber = 2;
        const    vai_beber = 3;
        const    visualizou_mas_nao_confirmou = 4;

        public static function getLabel($idEstado)
        {
            switch ($idEstado)
            {
                case BO_Estado_convite::nao_vai:
                    return I18N::getExpression("N�o vai");
                case BO_Estado_convite::vai_sem_beber:
                    return I18N::getExpression("Vai sem beber");
                case BO_Estado_convite::vai_beber:
                    return I18N::getExpression("Vai beber");
                case BO_Estado_convite::visualizou_mas_nao_confirmou:
                    return I18N::getExpression("Viu o convite mas n�o confirmou");
            }

            return null;
        }

    }