<?php

    class BO_Newsletter
    {
        public static function factory()
        {
            return new BO_Newsletter();
        }

        public function cadastrar()
        {

            $args = json_decode(file_get_contents("php://input"));

            if ($args == null || !isset($args->email) || strpos($args->email, ';') !== false)
            {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                    "Voc� deve digitar o email");
            }

            //Persistencia no banco
            $db = new Database();
            $db->queryMensagem("SELECT 1 FROM newsletter WHERE email='{$args->email}'");
            $existe = $db->getPrimeiraTuplaDoResultSet(0);
            if ($existe == 1)
            {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                    "Obrigado pelo apoio!");
            }
            $msg = $db->queryMensagem("INSERT INTO newsletter (email) VALUES ('{$args->email}')");

            if (Interface_mensagem::checkErro($msg))
            {
                return $msg;
            }
            else
            {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                    "Obrigado pelo apoio!");
            }

            return new Mensagem_generica($idCervejeiroFacebook);
        }
    }
