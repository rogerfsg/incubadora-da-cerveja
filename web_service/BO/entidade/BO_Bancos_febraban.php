<?php

    class BO_Bancos_febraban
    {

        public function getListaBancos()
        {
            try
            {
                $matriz = "";

                return new Mensagem_generica($matriz, PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
            }
            catch (Exception $ex)
            {
                HelperLog::logException($ex, true);

                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, null, $ex);
            }
        }

        public static function factory()
        {
            return new BO_Bancos_febraban();
        }

    }

?>
