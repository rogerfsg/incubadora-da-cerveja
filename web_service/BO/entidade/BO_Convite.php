<?php

    /**
     * Created by PhpStorm.
     * User: W10
     * Date: 13/03/2018
     * Time: 12:45
     */
    class BO_Convite
    {
        public function __construct()
        {
        }

        public function factory()
        {
            return new BO_Convite();
        }

        public function beberClick()
        {
            $args = json_decode(file_get_contents("php://input"));
            //$args = $args->params;
            //Persistencia no banco
            $db = new Database();

            $boCervejeiro = new BO_Cervejeiro();
            $helperFacebook = new HelperFacebook($args->sessaoFacebook);
            $me = $helperFacebook->loadMe();

            $idCervejeiroConvidado = $boCervejeiro->getIdCervejeiroDoFacebookId($db, $me->getId());

            $idBarril = $args->idBarril;
            if (!strlen($idBarril))
            {
                return Mensagem::factoryParametroInvalido();
            }

            $boBarril = new BO_Barril();
            $totalLitrosOcupados = $boBarril->getTotalLitrosOcupados(
                $idBarril
                , array(
                    BO_Estado_pedido::pagamento_confirmado
                , BO_Estado_pedido::disse_que_pagou
                )
                , $db);

            $maxVol = $boBarril->getVolumeMaximoBarril($db, $idBarril);
            if ($totalLitrosOcupados + $args->qtdLitroNovoPedido > $maxVol)
            {
                $maxParaPedir = $maxVol - $totalLitrosOcupados;

                return new Mensagem_generica(
                    $maxParaPedir
                    , PROTOCOLO_SISTEMA::ACESSO_NEGADO
                    , I18N::getExpression(
                    "O barril tem a capacidade m�xima de {0} litros. O m�ximmo que d� para pedir � {1}"
                    , $maxVol
                    , $maxParaPedir)
                );
            }
            //verifica se ja existe convite para a pessoa
            $q = "SELECT c.id idConvite
, c.estado_convite_id_INT idEstadoConvite
, c.historico
, b.cervejeiro_id_INT idCervejeiro
, ce.facebook_id idFacebook
, b.json jsonBarril
              FROM convite c 
                JOIN barril b ON c.barril_id_INT = b.id
                JOIN cervejeiro ce ON ce.id = b.cervejeiro_id_INT
              WHERE c.convidado_cervejeiro_id_INT = $idCervejeiroConvidado 
                AND c.barril_id_INT = $idBarril";

            $msg = $db->queryMensagem($q);
            if (Interface_mensagem::checkErro($msg))
            {
                return $msg;
            }
            $rs = Helper::getResultFirstObject($db->result);

//        if(!$helperFacebook->hasFriend($rs["idFacebook"])){
//            return new Mensagem(
//                PROTOCOLO_SISTEMA::ACESSO_NEGADO,
//                I18N::getExpression("Desculpe, somente amigos do facebook do organizador podem participar da festa")
//            );
//        }

            if ($rs != null)
            {
                $idEstadoConvite = $rs[1];
                $historico = utf8_decode($rs[2]);
                $historico = $historico == null ? '' : $historico . ' ';

                switch ($idEstadoConvite)
                {
                    case BO_Estado_convite::nao_vai:
                        $historico .= I18N::getExpression("Mudou de id�ia e resolveu ir �s {0}", Helper::getDiaEHoraAtual());
                        break;
                    case BO_Estado_convite::vai_beber:
                        if ($args->vaiBeber)
                        {
                            $historico .= I18N::getExpression("Resolveu beber mais �s {0}", Helper::getDiaEHoraAtual());
                        }
                        else
                        {
                            //situacao nao ocorre, pois quando ele fala que vai beber, ele j� confirmou e pagou
                            //deve resolver por fora com o amigo dele
                        }
                        break;
                    case BO_Estado_convite::vai_sem_beber:
                        //se mudou de ideia e resolveu beber
                        if ($args->vaiBeber)
                        {
                            $historico .= I18N::getExpression("Resolveu beber �s {0}", Helper::getDiaEHoraAtual());
                        }
                        else
                        {
                            //situacao nao ocorre, pois se ele nao estiver bebendo, essa opcao nao ficar� habilitada
                        }
                        break;
                    case BO_Estado_convite::visualizou_mas_nao_confirmou:
                        if ($args->vaiBeber)
                        {
                            $historico .= I18N::getExpression("Disse que vai beber �s {0}", Helper::getDiaEHoraAtual());
                        }
                        break;
                }
            }
            $historico = utf8_encode(Helper::substring($historico, 512));
            $historico = $db->formatarDados($historico);

            $idNovoEstadoConvite = $args->vaiBeber ? BO_Estado_convite::vai_beber : BO_Estado_convite::vai_sem_beber;
            $qUpdate = "UPDATE convite 
  SET estado_convite_id_INT = $idNovoEstadoConvite
    , historico={$historico} 
  WHERE id = " . $rs["idConvite"];
            //atualiza o novo estado do convite
            $db->query($qUpdate);

            $jsonPedidos = null;
            if (count($args->itensPedidos))
            {
                $jsonPedidos = json_encode($args->itensPedidos);
            }
            $args->qtdLitroNovoPedido = $args->qtdLitroNovoPedido == null ? 0 : $args->qtdLitroNovoPedido;

            $objJsonBarril = json_decode($rs["jsonBarril"]);
            $helperFacebook->hasFriend($rs["idFacebook"]);
            //TODO alterar para o valor correto do litro do barril
            $valor = $args->qtdLitroNovoPedido * $objJsonBarril->valorPorLitro;
            if ($valor != $args->valorNovoPedido)
            {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR
                    , "O valor apresentado ao cliente no javascript diverge do valor calculado no servidor $valor != {$args->valorNovoPedido}. Dados: {$args->qtdLitroNovoPedido} * {$objJsonBarril->valorPorLitro}"
                );
            }
            $strValor = Helper::formatarFloatParaComandoSQL($valor);
            $msg = $db->queryMensagem(
                "INSERT INTO `pedido` (
`convidado_cervejeiro_id_INT`
, `barril_id_INT`
, `json`
, `qtd_litros_INT`
, `estado_pedido_id_INT`
, `data`
, `valor`) 
VALUES ( 
$idCervejeiroConvidado
, $args->idBarril
, {$db->formatarDados($jsonPedidos)} 
, {$args->qtdLitroNovoPedido}
, " . BO_Estado_pedido::disse_que_pagou . "
, '" . Helper::getDiaEHoraAtualSQL() . "'
,  $strValor )");

            if (Interface_mensagem::checkErro($msg))
            {
                return $msg;
            }

            $idProtocolo = $db->getPrimeiraTuplaDoResultSet(0);

            return new Mensagem_token(
                PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO
                , I18N::getExpression("Enviamos seu recado")
                , $idProtocolo);
        }

        public function getTotalConvidados($db, $idBarril)
        {
            $q = "SELECT count(c.id)
            FROM convite c 
            WHERE c.barril_id_INT = $idBarril";
            $db->query($q);

            return $db->getPrimeiraTuplaDoResultSet(0);
        }

        public function geraChaveQrCodeConvite(
            Database $db
            , $idBarril
            , $idCervejeiroConvidado)
        {

            $q1 = "SELECT id, chave FROM convite WHERE convidado_cervejeiro_id_INT = $idCervejeiroConvidado 
  AND barril_id_INT = $idBarril";
            $db->query($q1);
            $res = Helper::getPrimeiroRegistroDoResultSetAsObject($db->result);
            if ($res == null)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO, "Voc� n�o foi convidado para essa festa");
            }
            else
            {
                if (isset($res->chave))
                {
                    return new Mensagem_generica($res->chave);
                }
            }
            $r = rand(100000, 999999999);
            $r62 = Helper::encodeBase62($r);
            $q = "UPDATE convite 
SET chave  = '$r62'
 WHERE convidado_cervejeiro_id_INT = $idCervejeiroConvidado 
  AND barril_id_INT = $idBarril";
            $db->query($q);
            if ($db->getAffectedRows() == 0)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Falha durante a geracao da chave");
            }

            return new Mensagem_generica($r62);
        }

        public function getDadosConvidado(
            Database $db
            , $idBarril
            , $chave)
        {

            $q1 = "SELECT id idConvite, convidado_cervejeiro_id_INT idConvidadoCervejeiro
          FROM convite
          WHERE chave = '$chave' 
            AND barril_id_INT = $idBarril";
            $db->query($q1);

            return Helper::getPrimeiroRegistroDoResultSetAsObject($db->result);
        }
    }